<?php
/**
 * Template Name: Index Page
 *
 *
 * @package ascent
 */

get_header(); ?>
<div class="row" style="background:rgba(99, 99, 98, 0.1) none repeat scroll 0 0;height:70px;margin-top:-20px;margin-bottom:20px;">
<div class="container text-center">
	<img src="http://sandbox.beyondbi-training.com/wp-content/themes/ascent/includes/images/abb-logo.png" style="height:70px;padding:10px 10px;" />
	<img src="http://sandbox.beyondbi-training.com/wp-content/themes/ascent/includes/images/sap-logo.png" style="height:70px;padding:10px 10px;" />
</div>
</div>
<div class="row">
<div class="container">
    <div class="col-sm-12 col-md-12">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'content', 'page' ); ?>
            <?php
                // If comments are open or we have at least one comment, load up the comment template
                if ( comments_open() || '0' != get_comments_number() )
                    comments_template();
            ?>
        <?php endwhile; // end of the loop. ?>
    </div>
</div>
</div>
<div class="row custom_features">
<div class="container">
	<div class="col-sm-12 col-md-12">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php woothemes_features( array( 'limit' => 10, 'size' => 100 ) ); ?>	
        <?php endwhile; // end of the loop. ?>
    </div>
</div>
</div>
<div class="row">
<div class="container">
	<div class="col-sm-12 col-md-12">
        <?php while ( have_posts() ) : the_post(); ?>
			<?php
				echo testimonial_rotator( array( 'id' => 29) );
			?>
        <?php endwhile; // end of the loop. ?>
    </div>
</div>
</div>
<?php get_footer(); ?>
