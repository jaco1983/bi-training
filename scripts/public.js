window.onload = initAll;



function initAll() {

	initForms();

}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}


function MM_goToURL() { //v3.0 20090926
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}


// validation scripts
function initForms() {
	for (var i=0; i< document.forms.length; i++) {
		document.forms[i].onsubmit = function() {return validForm();}
	}
	//document.getElementById("sunroof").onclick = doorSet;

}


function validForm() {
	var allGood = true;
	var allTags = document.getElementsByTagName("*");
	for (var i=0; i<allTags.length; i++) {
		if (!validTag(allTags[i])) {
			allGood = false;
		}
	}
	return allGood;

function validTag(thisTag) {
	var outClass = "";
	var allClasses = thisTag.className.split(" ");
		for (var j=0; j<allClasses.length; j++) {
			outClass += validBasedOnClass(allClasses[j]) + " ";
		}
		thisTag.className = outClass;
		if (outClass.indexOf("invalid") > -1) {
			//invalidLabel(thisTag.parentNode);
			thisTag.focus();
			if (thisTag.nodeName == "INPUT") {
				thisTag.select();
			}
			return false;
		}
		return true;

function validBasedOnClass(thisClass) {
		var classBack = "";		

		switch(thisClass) {
			case "":
			case "invalid":
				break;
					
			case "reqd":
				if (allGood && thisTag.value == "") {						classBack = "invalid ";
				}
				classBack += thisClass;
				break;
				
			case "del":
				if (allGood && thisTag.value == " --- Select Delivery --- ") {
					classBack = "invalid ";
				}
				classBack += thisClass;
				break;
			case "pay":
				if (allGood && (thisTag.value == " --- Select Payment --- " || thisTag.value == " --- Kies Betaling --- ")) {
					classBack = "invalid ";
				}
				classBack += thisClass;
				break;
				//case "radio":
				//	if (allGood && !radioPicked(thisTag.name)) {
				//		classBack = "invalid ";
				//	}
				//	classBack += thisClass;
				//	break;

			case "isNum":
				if (allGood && !isNum(thisTag.value)) {
					classBack = "invalid ";
				}
					classBack += thisClass;
				break;

			case "email":
				if (allGood && !validEmail(thisTag.value)) {
					classBack = "invalid ";
				}
				classBack += thisClass;
				break;

				default:
					/*if (allGood && !crossCheck(thisTag,thisClass)) {
						classBack = "invalid ";
					}*/
					classBack += thisClass;
			}
			return classBack;
		}

			

function isNum(passedVal) {
			if (passedVal == "") {
				return false;
			}

			for (var k=0; k<passedVal.length; k++) {
				if (passedVal.charAt(k) < "0") {
					return false;
				}
				if (passedVal.charAt(k) > "9") {
					return false;
				}
			}
			return true;
		}
		

function isZip(inZip) {
			if (inZip == "") {
				return true;
			}
			return (isNum(inZip));
		}

		
function validEmail(email) {
			var invalidChars = " /:,;";
			if (email == "") {
				return false;
			}
			for (var k=0; k<invalidChars.length; k++) {
				var badChar = invalidChars.charAt(k);
				if (email.indexOf(badChar) > -1) {
					return false;
				}
			}
			var atPos = email.indexOf("@",1);
			if (atPos == -1) {
				return false;
			}
			if (email.indexOf("@",atPos+1) != -1) {
				return false;
			}

			var periodPos = email.indexOf(".",atPos);
			if (periodPos == -1) {	
				return false;
			}
			if (periodPos+3 > email.length)	{
				return false;
			}
			return true;
		}
	}
}



function confirmDelete(yesLinkPage, noLinkPage, curLang) {
	var msg = "The record will be permenantly deleted. Are you sure?";
	if (curLang == "1") { msg = "Die rekord sal permanent verwyder word. Is jy seker?"; }
  if (confirm(msg)) {
		document.location.href = yesLinkPage;
	} else {
		document.location.href = noLinkPage;
	}
		return false;
}

/*---------------------------------------------------------------------Jaco Ferreira---------------------------------------------------------------*/
function signOut(){
	var base_url = $("#base_url").val();
	var url = base_url + "ajax/ajax-main.php";
    $.post( url, { action:"Logout"
        }, function(data){
            if(data.result == 1){
                document.location.href = "?id=0&logout=1";
            } else {

            }
        },
        "json");
}


function signIn(){
    if($('#RegistrationModal').is(':visible')){
        $("#RegistrationModal").modal("hide");
    }
    $("#LoginModal").modal("show");
}

function LoginrecaptchaCallback(){
    alert(grecaptcha.getResponseHeader(LoginreCaptcha));
    var btnSubmit = document.getElementById("btnSubmit");

    if ( btnSubmit.classList.contains("hidden") ) {
        btnSubmit.classList.remove("hidden");
        btnSubmitclassList.add("show");
    }
}



function signUp(){
    $("#RegistrationModal").modal("show");
}

function forgotPassword(){
    $("#ForgotPasswordModal").modal("show");
}

function NewVoucher(){
    $.post("ajax/ajax-voucher.php", { action:"GenerateVoucherNum"
        }, function(data){
            if(data.result == 1){
                $("#m_vouchernum").val(data.voucher);
            } else {

            }
        },
        "json");
    $("#NewVoucherModal").modal("show");
}

function captchaRefresh(){
    $.post( "ajax/ajax-main.php", { action:"refreshCaptcha"
        }, function(data){
            if(data.result == 1){
                $("#captcha2").attr("src","php/newCaptcha.php?rnd=" + data.captcha);
            }
        },
        "json");
};

function regcaptchaRefresh(){
    $.post( "ajax/ajax-main.php", { action:"refreshCaptcha"
        }, function(data){
            if(data.result == 1){
                $("#regcaptcha2").attr("src","php/newCaptcha.php?rnd=" + data.captcha);
            }
        },
        "json");
};

function captchaRefresh2(){
    $.post( "ajax/ajax-main.php", { action:"refreshCaptcha"
        }, function(data){
            if(data.result == 1){
                $("#captcha4").attr("src","php/newCaptcha.php?rnd=" + data.captcha);
            }
        },
        "json");
};

function approveTransaction(id){
    var id = id;

    $.post( "ajax/ajax-transaction.php", { action:"ApproveTransaction",id:id
        }, function(data){
            if(data.result == 1){
                document.location.href = "?id=104&login=1&opt=0&key=0&set=0";
            } else {

            }
        },
        "json");
}

function selectUser(val) {
    $("#m_voucheremail").val(val);
    $("#m_vouchersuggestionbox").hide();
}

function selectQuestion(val){
    $("#pracquestion").val(val);
    $("#pracsuggestionbox").remove();
}

function editConfig(){
    $("#siteconfig1").hide();
    $("#siteconfig2").show();
}

function saveConfig(){
    var site_title = $("#site_title").val();
    var site_keywords = $("#site_keywords").val();
    var site_description = $("#site_description").val();
    var site_name = $("#site_name").val();
    var admin_email = $("#admin_email").val();
    var support_email = $("#support_email").val();

    $.post( "ajax/ajax-main.php", { action:"SaveConfig",site_title:site_title,site_keywords:site_keywords,site_description:site_description,site_name:site_name,admin_email:admin_email,support_email:support_email
        }, function(data){
            if(data.result == 1){
                $("#siteconfig1").show();
                $("#siteconfig2").hide();
                var $elm = $(data.html);
                $(".content-pad").prepend($elm);
                setTimeout(function() {
                    $elm.remove();
                }, 3000);
            } else {
                var $elm = $(data.html);
                $(".content-pad").prepend($elm);
                setTimeout(function() {
                    $elm.remove();
                }, 3000);
            }
        },
        "json");
}

function editInvoiceConfig(){
    $("#invoiceconfig1").hide();
    $("#invoiceconfig2").show();
}

function saveInvoiceConfig(){
    var companyname = $("#company_name").val();
    var vatnumber = $("#vat_number").val();
    var postaladdress1 = $("#postal_address1").val();
    var postaladdress2 = $("#postal_address2").val();
    var postaladdress3 = $("#postal_address3").val();
    var postaladdress4 = $("#postal_address4").val();

    $.post( "ajax/ajax-main.php", { action:"SaveInvoiceConfig",companyname:companyname,vatnumber:vatnumber,postaladdress1:postaladdress1,postaladdress2:postaladdress2,postaladdress3:postaladdress3,postaladdress4:postaladdress4
        }, function(data){
            if(data.result == 1){
                $("#invoiceconfig1").show();
                $("#invoiceconfig2").hide();
                var $elm = $(data.html);
                $(".content-pad").prepend($elm);
                setTimeout(function() {
                    $elm.remove();
                }, 3000);
            } else {
                var $elm = $(data.html);
                $(".content-pad").prepend($elm);
                setTimeout(function() {
                    $elm.remove();
                }, 3000);
            }
        },
        "json");
}

function PlayDemo(){
    $("#DemoVideoModal").modal("show");
}

function EditVoucher(id){
    $.post( "ajax/ajax-voucher.php", { action:"GetVoucher",id:id
        }, function(data){
            if(data.result == 1){
                $("#NewVoucherModal").modal("show");

                $("#editvouchersubmitbuttons").show();
                $("#addvouchersubmitbuttons").hide();

                $("#m_voucherid").val(data.voucher_id);
                $("#m_vouchernum").val(data.voucher_number);

                $("#m_voucheremail").val(data.user_email);
                $("#m_voucheremail").prop("disabled",true);
                $("#m_voucherdisc").val(data.discount);
                $("#m_voucherexam").val(data.exam);
                $("#m_voucherexpiry").val(data.expiry_date);

                if(data.status == "1"){
                    $("#m_voucherstatus").prop("checked",true);
                }
            }
        },
        "json");
}

$(document).ready(function() {

    $('#tool_id').multiselect({

        columns: 1,

        placeholder: 'Select options'
    });

//    $(":file").filestyle({ buttonName:"btn-primary"});

     $("#exam_id").on("change",function(){
         var examid =$("#exam_id").val();
         $.post( "ajax/ajax-test.php", { action:"GetSubject",examid:examid
         }, function(data){
         if(data.result == 1){
         $("#txtHint").html(data.html);
         }
         },
         "json");
     })

    /*$(".pracexamid").on("change",function(){
        var examid =$(".pracexamid").val();
        $.post( "ajax/ajax-test.php", { action:"GetPracExamQuestionDropdown",examid:examid
            }, function(data){
                if(data.result == 1){
                    $("#pracquestion").html(data.html);
                }
            },
            "json");
    });*/

        //CONTACT FORM SUBMIT
    $("#contactsubmit").click(function(){

        var bool = 0;

        if($("#cfname").val().length < 3){
            $("#cfname").removeClass("alert-success").addClass("alert-danger");
            $("#cfname").effect("shake", {times: 3}, 50);
            bool++;
        }

        if($("#cemail").val().length < 3){
            $("#cemail").removeClass("alert-success").addClass("alert-danger");
            $("#cemail").effect("shake", {times: 3}, 50);
            bool++;
        }

        if($("#csubject").val().length < 3){
            $("#csubject").removeClass("alert-success").addClass("alert-danger");
            $("#csubject").effect("shake", {times: 3}, 50);
            bool++;
        }

        /*if($("#creference").val().length < 3){
            $("#creference").removeClass("alert-success").addClass("alert-danger");
            $("#creference").effect("shake", {times: 3}, 50);
            bool++;
        }*/

        if($("#cmessage").val().length < 3){
            $("#cmessage").removeClass("alert-success").addClass("alert-danger");
            $("#cmessage").effect("shake", {times: 3}, 50);
            bool++;
        }

        if(bool == 0){

            var cfname = $("#cfname").val();
            var cemail = $("#cemail").val();
            var csubject = $("#csubject").val();
            var creference = $("#creference").val();
            var cmessage = $("#cmessage").val();

            $.post( "ajax/ajax-main.php", { action:"Contact",cfname:cfname,cemail:cemail,csubject:csubject,creference:creference,cmessage:cmessage
                }, function(data){
                    if(data.result == 1){

                        $("#cfname").val('');
                        $("#cemail").val('');
                        $("#csubject").val('');
                        $("#creference").val('');
                        $("#cmessage").val('');

                        var $elm = $(data.html);
                        $(".contactsubmit").prepend($elm);
                        setTimeout(function() {
                            $elm.remove();
                        }, 3000);
                    } else {
                        var $elm = $(data.html);
                        $(".contactsubmit").prepend($elm);
                        setTimeout(function() {
                            $elm.remove();
                        }, 3000);
                    }
                },
                "json");
        }
    })

    $("#m_vouchergen").on("click",function(){
        $.post("ajax/ajax-voucher.php", { action:"GenerateVoucherNum"
            }, function(data){
                if(data.result == 1){
                    $("#m_vouchernum").val(data.voucher);
                } else {

                }
            },
            "json");
    });

    $("#m_voucheremail").keyup(function(){
        $.post("ajax/ajax-voucher.php", { action:"FindUser",term:$(this).val()
            }, function(data){
                if(data.result == 1){
                    $("#m_vouchersuggestionbox").show();
                    $("#m_vouchersuggestionbox").html(data.html);
                } else {

                }
            },
            "json");
    });

    $("#pracquestion").keyup(function(){
        var examid = $("#pracexam_id").val();
        $.post("ajax/ajax-test.php", { action:"FindQuestion",term:$(this).val(),examid:examid
            }, function(data){
                if(data.result == 1){
					$("#pracquestion").after("<div id=\"pracsuggestionbox\">" + data.html + "</div>");
                    /*$("#pracsuggestionbox").show();
                    $("#pracsuggestionbox").html(data.html);*/
                } else {

                }
            },
            "json");
    })

    $("#sub_voucher").keyup(function(){
        var key = $("#sub_exam_id").val();
        var amount = $("#sub_exam_cost").val();
        var email = $("#sub_email").val();
        var sub_voucher = $("#sub_voucher").val();

        $.post("ajax/ajax-voucher.php", { action:"CalculateCost",key:key,email:email,sub_voucher:sub_voucher,amount:amount
            }, function(data){
                if(data.result == 1){
					if(data.amount == 0){
						//$('[name=sub_paymethod][value="cc"]').prop('disabled',true);
                        $(".voucherval").css("display","none");
						$(".paymeth").css("display","none");
					}
                    $("#paypalamount").val(data.amount);
					

                } else {
					$("#paypalamount").val(data.amount);
					$(".paymeth").css("display","block");
					$("#voucher-msg").html(data.html);
                }
            },
            "json");
    });

    $("#addvouchersubmit").on("click",function(){
        var vouchernum = $("#m_vouchernum").val();
        var voucheremail = $("#m_voucheremail").val();
        var voucherdisc = $("#m_voucherdisc").val();
        var voucherexpiry = $("#m_voucherexpiry").val();
        var voucherexam = $("#m_voucherexam").val();
        
            var voucherstatus = 0;
        

        $.post("ajax/ajax-voucher.php", { action:"SaveVoucher",vouchernum:vouchernum,voucheremail:voucheremail,voucherdisc:voucherdisc,voucherexpiry:voucherexpiry,voucherexam:voucherexam,voucherstatus:voucherstatus
            }, function(data){
                if(data.result == 1){
                    $("#m_vouchernum").val('');
                    $("#m_voucheremail").val('');
                    $("#m_voucherdisc").val('');
                    $("#m_voucherexpiry").val('');
                    $("#m_voucherexam").val('');
                    $("#m_voucherstatus").prop("checked",false);

                    $('input[type=text], textarea').removeClass("alert-success").removeClass("alert-danger");
                    $('input[type=email], textarea').removeClass("alert-success").removeClass("alert-danger");

                    $("#NewVoucherModal").modal("hide");
                    var $elm = $(data.html);
                    $(".main-content").prepend($elm);
                    setTimeout(function() {
                        $elm.remove();
                    }, 3000);
                } else {
                    var $elm = $(data.html);
                    $(".main-content").prepend($elm);
                    setTimeout(function() {
                        $elm.remove();
                    }, 3000);
                }
            },
            "json");
    });

    $("#editvouchersubmit").on("click",function(){
        var voucherid = $("#m_voucherid").val();
        var vouchernum = $("#m_vouchernum").val();
        var voucheremail = $("#m_voucheremail").val();
        var voucherdisc = $("#m_voucherdisc").val();
        var voucherexpiry = $("#m_voucherexpiry").val();
        var voucherexam = $("#m_voucherexam").val();
        if($("#m_voucherstatus").is(':checked')){
            var voucherstatus = "on";
        } else {
            var voucherstatus = "off";
        }

        $.post("ajax/ajax-voucher.php", { action:"UpdateVoucher",voucherid:voucherid,vouchernum:vouchernum,voucheremail:voucheremail,voucherdisc:voucherdisc,voucherexpiry:voucherexpiry,voucherexam:voucherexam,voucherstatus:voucherstatus
            }, function(data){
                if(data.result == 1){

                    $("#editvouchersubmitbuttons").hide();
                    $("#addvouchersubmitbuttons").show();

                    $("#m_voucherid").val('');
                    $("#m_vouchernum").val('');
                    $("#m_voucheremail").val('');
                    $("#m_voucherdisc").val('');
                    $("#m_voucherexpiry").val('');
                    $("#m_voucherexam").val('');

                    $('input[type=text], textarea').removeClass("alert-success").removeClass("alert-danger");
                    $('input[type=email], textarea').removeClass("alert-success").removeClass("alert-danger");

                    $("#NewVoucherModal").modal("hide");
                    var $elm = $(data.html);
                    $(".main-content").prepend($elm);
                    setTimeout(function() {
                        $elm.remove();
                    }, 3000);
                } else {
                    var $elm = $(data.html);
                    $(".main-content").prepend($elm);
                    setTimeout(function() {
                        $elm.remove();
                    }, 3000);
                }
            },
            "json");
    });

    $("#addvoucherclose").on("click",function(){
        $("#NewVoucherModal").modal("hide");
    });

    $("#editvoucherclose").on("click",function(){
        $("#NewVoucherModal").modal("hide");
    });

    //REGISTRATION FORM SUBMIT
    $("#r_submit").click(function (e) {
		var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
        var bool = 0;

        if ($("#r_username").val().length < 3) {
            $("#r_username").removeClass("alert-success").addClass("alert-danger");
            $("#r_username").effect("shake", {times: 3}, 50);
            bool++;
        } else {
            $("#r_username").removeClass("alert-danger").addClass("alert-success");
        }

        if ($("#r_name").val().length < 3) {
            $("#r_name").removeClass("alert-success").addClass("alert-danger");
            $("#r_name").effect("shake", {times: 3}, 50);
            bool++;
        } else {
            $("#r_name").removeClass("alert-danger").addClass("alert-success");
        }

        /*if ($("#r_idnumber").val().length < 3) {
            $("#r_idnumber").removeClass("alert-success").addClass("alert-danger");
            $("#r_idnumber").effect("shake", {times: 3}, 50);
            bool++;
        } else {
			if(numberRegex.test($("#r_idnumber").val())){
				$("#r_idnumber").removeClass("alert-danger").addClass("alert-success");
			} else {
				$("#r_idnumber").removeClass("alert-success").addClass("alert-danger");
				$("#r_idnumber").effect("shake", {times: 3}, 50);
				bool++;
			}
        }

        if ($("#r_telnumber").val().length < 3) {
            $("#r_telnumber").removeClass("alert-success").addClass("alert-danger");
            $("#r_telnumber").effect("shake", {times: 3}, 50);
            bool++;
        } else {
			if(numberRegex.test($("#r_telnumber").val())){
				$("#r_telnumber").removeClass("alert-danger").addClass("alert-success");
			} else {
				$("#r_telnumber").removeClass("alert-success").addClass("alert-danger");
				$("#r_telnumber").effect("shake", {times: 3}, 50);
				bool++;	
			}
        }

        if ($("#r_mobilenumber").val().length < 3) {
            $("#r_mobilenumber").removeClass("alert-success").addClass("alert-danger");
            $("#r_mobilenumber").effect("shake", {times: 3}, 50);
            bool++;
        } else {
			if(numberRegex.test($("#r_mobilenumber").val())){
				$("#r_mobilenumber").removeClass("alert-danger").addClass("alert-success");
			} else {
				$("#r_mobilenumber").removeClass("alert-success").addClass("alert-danger");
				$("#r_mobilenumber").effect("shake", {times: 3}, 50);
				bool++;
			}
        }*/

        if (document.getElementById("r_email").value.indexOf("@") < 0) {
            $("#r_email").removeClass("alert-success").addClass("alert-danger");
            $("#r_email").effect("shake", { times:3 }, 50);
            bool++;
        } else {
            reg = /^[a-z0-9_\-]+(\.[_a-z0-9\-]+)*@([_a-z0-9\-]+\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel)/gi;
            if (reg.test(document.getElementById("r_email").value) == false) {
                $("#r_email").removeClass("alert-success").addClass("alert-danger");
                $("#r_email").effect("shake", { times:3 }, 50);
                bool++;
            } else {
                $("#r_email").removeClass("alert-danger").addClass("alert-success");
            }
        }

        var err = 0;
        var response = grecaptcha.getResponse( RegisterreCaptcha );
        //console.log( 'g-recaptcha-response: ' + response );
        // proceed to submit the form or whatever you happen to be doing.

        if(response === "") {
            err++;
        }

        if(err <= 0){
            console.log( 'g-recaptcha-response: ' + response );
            //$(this).submit();

        if(bool == 0){

		if($("#r_trial").val() == "Trial"){
            var r_trial = 1;
		} else {
            var r_trial = 0;
		}
            var r_username = $("#r_username").val();
            var r_name = $("#r_name").val();
            /*var r_idnumber = $("#r_idnumber").val();
            var r_telnumber = $("#r_telnumber").val();
            var r_mobilenumber = $("#r_mobilenumber").val();*/
            var r_email = $("#r_email").val();


                        $.post("ajax/ajax-registration.php", { action:"CheckEmail",r_email:r_email
                            }, function(data){
                                if(data.result == 1){
                                    $.post("ajax/ajax-registration.php", { action:"NewRegistration",r_username:r_username,r_name:r_name,r_email:r_email,r_trial:r_trial
                                        }, function(data){
                                            if(data.result == 1){

                                                $("#r_trial").val('');
                                                $("#r_username").val('');
                                                $("#r_name").val('');
                                                /*$("#r_idnumber").val('');
                                                $("#r_telnumber").val('');
                                                $("#r_mobilenumber").val('');*/
                                                $("#r_email").val('');

                                                $('input[type=text], textarea').removeClass("alert-success").removeClass("alert-danger");
                                                $('input[type=email], textarea').removeClass("alert-success").removeClass("alert-danger");

                                                $("#RegistrationModal").modal("hide");
                                                var $elm = $(data.html);
                                                $(".main-content").prepend($elm);
                                                setTimeout(function() {
                                                    $elm.remove();
                                                }, 3000);
                                            } else {
                                                var $elm = $(data.html);
                                                $(".reg-main-content").prepend($elm);
                                                setTimeout(function() {
                                                    $elm.remove();
                                                }, 3000);
                                            }
                                        },
                                        "json");
                                } else {
                                    var $elm = $(data.html);
                                    $(".reg-main-content").prepend($elm);
                                    setTimeout(function() {
                                        $elm.remove();
                                    }, 5000);
                                    $("#r_email").removeClass("alert-success").addClass("alert-danger");
                                }
                            },
                            "json");

        }} else {
            e.preventDefault();
        }
    });

    $("#existingusersubscribeform").on('submit', function (e) {

        e.preventDefault();

        $key = $("#examkey").val();
        $aemail2 = $("#aemail2").val();
        $apwd2 = $("#apwd2").val();
        $url = $("#url").val();

        $.post( "ajax/ajax-main.php", { action:"Login",aemail2:$aemail2,apwd2:$apwd2,url:$url
            }, function(data){
                if(data.result == 1){
                    document.location.href = "?id=106&key=" + $key + "&login=1&step=4";
                } else {
                    var $elm = $(data.html);
                    $("#existingusersubscribeform").prepend($elm);
                    setTimeout(function() {
                        $elm.remove();
                    }, 3000);
                }
            },
            "json");

    });
    $("#confirmpaymentmethod").on('submit',function(e){
        var paymethod = $(".confirmpaymentmethod").val();

        if(paymethod == "eft"){
            e.preventDefault();

            $.post( "ajax/ajax-transaction.php", { action:"ConfirmTransaction"
                }, function(data){
                    if(data.result == 1){
                        document.location.href = "?id=103";
                    } else {
                        var $elm = $(data.html);
                        $("#confirmpaymentmethod").prepend($elm);
                        setTimeout(function() {
                            $elm.remove();
                        }, 3000);
                    }
                },
                "json");
        }
    });
    $("#newusersubscribeform").on('submit', function (e) {

        e.preventDefault();

        $nemail2 = $("#nemail2").val();
        $ncemail2 = $("#ncemail2").val();

		$.post("ajax/ajax-registration.php", { action:"CheckEmail",r_email:$nemail2
                            }, function(data){
                                if(data.result == 1){
									$("#nemail2").removeClass("alert-danger").addClass("alert-success");
									$.post( "ajax/ajax-registration.php", { action:"EmailCompare",nemail2:$nemail2,ncemail2:$ncemail2
										}, function(data){
											if(data.result == 1){
												$("#step1").hide();
												$("#step2").hide();
												$("#step3").show();
												$("#nemail9").val($nemail2);
												$("#step4").hide();
											} else {
												var $elm = $(data.html);
												$("#newusersubscribeform").prepend($elm);
												setTimeout(function() {
													$elm.remove();
												}, 3000);
											}
										},
										"json");
								} else {
									var $elm = $(data.html);
									$("#newusersubscribeform").prepend($elm);
                                    setTimeout(function() {
                                        $elm.remove();
                                    }, 5000);
                                    $("#nemail2").removeClass("alert-success").addClass("alert-danger");
								}
							
	},"json");
});

    $("#newusersubscribeform2").on('submit', function (e) {

        e.preventDefault();

        $key = $("#examkey").val();
        $url = '';
        $username = $("#nusername").val();
        $email = $("#nemail9").val();
        $name = $("#nfullname").val();
        $idnumber = $("#nidnumber").val();
        $gender = $("#ngender").val();
        $phone = $("#ntelnum").val();
        $mobile = $("#ncellnum").val();


        $.post( "ajax/ajax-registration.php", { action:"NewUserRegister",username:$username,email:$email,fullname:$name,idnumber:$idnumber,gender:$gender,phone:$phone,mobile:$mobile
            }, function(data){
                if(data.result == 1){
                    $pwd = data.pass;
                    $("#step1").hide();
                    $("#step2").hide();
                    $("#step3").hide();

                    $.post( "ajax/ajax-main.php", { action:"Login",aemail2:$email,apwd2:$pwd,url:$url
                        }, function(data){
                            if(data.result == 1){
                                document.location.href = "?id=106&key=" + $key + "&login=1&step=4";
                            } else {
                                var $elm = $(data.html);
                                $("#existingusersubscribeform").prepend($elm);
                                setTimeout(function() {
                                    $elm.remove();
                                }, 3000);
                            }
                        },
                        "json");

                } else {
                    var $elm = $(data.html);
                    $("#newusersubscribeform2").prepend($elm);
                    setTimeout(function() {
                        $elm.remove();
                    }, 3000);
                }
            },
            "json");

    });

    tinymce.init({
        selector:'#topic-areas',
        menubar:false,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true,})
    tinymce.init({
        selector:'#long-description',
        menubar:false,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true,});
    tinymce.init({
        selector:'#long-description2',
        menubar:false,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true,});
});

function newsub(step){
    if(step == "step2"){
        $("#step1").hide();
        $("#step2").show();
        $("#step3").hide();
        $("#step4").hide();
    }
}

function showRSS() {

    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            document.getElementById("rssOutput").innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","includes/sap_news_feed.php",true);
    xmlhttp.send();
}

function StopVideo(){
    $('video').each(function(){
            $('video')[0].pause();
    })
}

function Trial(){
	$("#RegistrationModal").modal("show");
	$("#r_trial").val("Trial");
}