<?php

echo '<div id="LoginModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Sign In</h4>
      </div>
      <div class="modal-body">';

    echo '<form id="loginform" name="login" method="post" action="?id=0&opt='.$opt.'&set='.$set.'&login=3" role="form" autocomplete="off">
            <div class="form-group">
                <label for="aemail" class="label-control col-sm-3">Email: <font color="#F00">*</font></label>
                <div class="col-sm-9">
                    <input name="aemail" type="email" class="form-control" id="aemail" placeholder="Email" value="'.(isset($_POST["aemail2"]) ? $_POST["aemail2"] : '' ).'">
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label for="apwd" class="label-control col-sm-3">Password: <font color="#F00">*</font></label>
                <div class="col-sm-9">
                    <input name="apwd" type="password" class="form-control" id="apwd" placeholder="Password" value="'.(isset($_POST["apwd2"]) ? $_POST["apwd2"] : '' ).'">
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label for="captcha" class="label-control col-sm-3">Captcha: <font color="#F00">*</font></label>
                <div class="col-sm-9" id="LoginreCaptcha">
                    
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <div class="col-sm-12 text-center">
                    <button type="submit" name="submit" id="btnSubmit" class="btn btn-default">Sign in</button>&nbsp;
                    <a href="javascript:void(0)" name="forgotpass" class="btn btn-default" onclick="forgotPassword()">Forgot Password</a>
                </div>
                <div class="clearfix"></div>
            </div>
          </form>';



    echo '</div>
      <div class="modal-footer">

      </div>
    </div>

  </div>
</div>';

?>
<script type='text/javascript'>
    $(document).ready(function() {
        //option A
        $("#loginform").submit(function(e){

            var err = 0;
            var response = grecaptcha.getResponse( LoginreCaptcha );
            //console.log( 'g-recaptcha-response: ' + response );
            // proceed to submit the form or whatever you happen to be doing.

            if(response === "") {
                err++;
            }

            if(err <= 0){
                console.log( 'g-recaptcha-response: ' + response );
                //$(this).submit();
            } else {
                e.preventDefault();
            }
        });
    });
</script>
