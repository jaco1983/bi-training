<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/10/02
 * Time: 2:19 PM
 */


echo '<div id="DemoVideoModal" class="modal fade" role="dialog" style="width:70%;margin:0px auto;">
  <div class="modal-dialog" style="width: 100%;background: transparent !important;">

    <!-- Modal content-->
    <div class="modal-content" style="width: 100%;background-color: transparent !important;box-shadow: none !important;border:0px;">
      <div class="modal-header" style="border-bottom: 0px !important;">
        <button type="button" class="close" data-dismiss="modal" style="font-size:32px;color:#FFFFFF;opacity:1 !important;" onclick="StopVideo()">&times;</button>
      </div>
      <div class="modal-body">
        <video controls style="width:100%">
                          <source src="videos/beyondbi_training_demo.mp4" type="video/mp4" />
        </video>
      </div>
      </div>
      <div class="modal-footer" style="border: 0px !important;">

      </div>
    </div>

  </div>
</div>

<script>



</script>';

?>