<?php

echo '<div id="NewVoucherModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Voucher</h4>
      </div>
      <div class="modal-body">';

      echo '<div class="form-group">
                <label for="m_vouchernum" class="col-sm-12">Voucher Number:</label>
                <div class="col-sm-8">
                    <input type="hidden" class="form-control" id="m_voucherid" name="m_voucherid" class="form-control" />
                    <input type="text" class="form-control" id="m_vouchernum" name="m_vouchernum" class="form-control" />
                </div>
                <div class="col-sm-3">
                    <input type="button" class="btn btn-success" id="m_vouchergen" value="Generate New" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label for="m_voucheremail" class="col-sm-12">User Email:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="m_voucheremail" name="m_voucheremail" class="form-control" />
                    <div id="m_vouchersuggestionbox"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label for="m_voucherdisc" class="col-sm-12">Exam:</label>
                <div class="col-sm-12">
                    <select class="form-control" id="m_voucherexam" name="m_voucherexam">
                        <option value="">Please Select</option>
                        <option value="0">Any</option>';
                        $sql = "select exam_id,code from aca_exam";

                        $result_set = mysqli_query($connection, $sql);

                        while ($row = mysqli_fetch_array($result_set)) {
                            echo '<option value="'.$row["exam_id"].'">'.$row["code"].'</option>';
                        }
                    echo '</select>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label for="m_voucherdisc" class="col-sm-12">% Discount:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="m_voucherdisc" name="m_voucherdisc" class="form-control" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label for="m_voucherdisc" class="col-sm-12">Expiry Date:</label>
                <div class="col-sm-12">
					<div class="input-group date col-sm-7" id="voucherdate">
                    <input type="text" class="form-control" id="m_voucherexpiry" name="m_voucherexpiry" class="form-control" />
					<span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
					</div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group" id="addvouchersubmitbuttons">
                <div class="col-sm-12 text-center">
                    <input type="button" name="addvouchersubmit" id="addvouchersubmit" class="btn btn-success" value="Save" />&nbsp;
                    <input type="button" name="addvoucherclose" id="addvoucherclose" class="btn btn-default" value="Close" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group" id="editvouchersubmitbuttons" style="display: none;">
                <div class="col-sm-12 text-center">
                    <input type="button" name="editvouchersubmit" id="editvouchersubmit" class="btn btn-success" value="Save" />&nbsp;
                    <input type="button" name="editvoucherclose" id="editvoucherclose" class="btn btn-default" value="Close" />
                </div>
                <div class="clearfix"></div>
            </div>
			<script>
			$(function () {
				var fullDate = new Date();

				var currentDate = new Date(fullDate.getFullYear(), fullDate.getMonth(),fullDate.getDay() -1);

				$("#voucherdate").datetimepicker({format: "YYYY-MM-DD",defaultDate: currentDate});

			});
			</script>';

echo '</div>
      <div class="modal-footer">

      </div>
    </div>

  </div>
</div>';

?>