<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 9/17/2017
 * Time: 11:46 PM
 */


echo '<div id="ForgotPasswordModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Forgot Password</h4>
      </div>
      <div class="modal-body">';

    echo '<form id="forgotpassform" name="login" method="post" action="?id=0&opt='.$opt.'&set='.$set.'&forgot=1" role="form" autocomplete="off">
            <div class="form-group">
                <label for="aemail" class="label-control col-sm-3">Email: <font color="#F00">*</font></label>
                <div class="col-sm-9">
                    <input name="femail" type="email" class="form-control" id="femail" placeholder="Email" value="'.(isset($_POST["femail"]) ? $_POST["femail"] : '' ).'">
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label for="captcha" class="label-control col-sm-3">Captcha: <font color="#F00">*</font></label>
                <div class="col-sm-9" id="ForgotPassreCaptcha">
                    
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <div class="col-sm-12 text-center">
                    <button type="submit" name="submit" id="forgotbtnSubmit" class="btn btn-default">Submit</button>
                </div>
                <div class="clearfix"></div>
            </div>
          </form>';



    echo '</div>
      <div class="modal-footer">

      </div>
    </div>

  </div>
</div>';

?>
<script type='text/javascript'>
    $(document).ready(function() {
        //option A
        $("#forgotpassform").submit(function(e){

            var err = 0;
            var response = grecaptcha.getResponse( ForgotPassreCaptcha );
            //console.log( 'g-recaptcha-response: ' + response );
            // proceed to submit the form or whatever you happen to be doing.

            if(response === "") {
                err++;
            }
			
			if (document.getElementById("femail").value.indexOf("@") < 0) {
				$("#femail").removeClass("alert-success").addClass("alert-danger");
				$("#femail").effect("shake", { times:3 }, 50);
				err++;
			} else {
				reg = /^[a-z0-9_\-]+(\.[_a-z0-9\-]+)*@([_a-z0-9\-]+\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel)/gi;
				if (reg.test(document.getElementById("femail").value) == false) {
					$("#femail").removeClass("alert-success").addClass("alert-danger");
					$("#femail").effect("shake", { times:3 }, 50);
					err++;
				} else {
					$("#femail").removeClass("alert-danger").addClass("alert-success");
				}
			}

            if(err <= 0){
                console.log( 'g-recaptcha-response: ' + response );
                //$(this).submit();
            } else {
                e.preventDefault();
            }
        });
    });
</script>
