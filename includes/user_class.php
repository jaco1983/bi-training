<?php	
// user_class id = 22
// add a class to a user
// a user can add an exam to his profile (needs a voucher or some check). The ideal is to register or buy an exam.
// Admin can edit or remove an exam from a user's profile.

// a user can join a class but cannot leave or change a class

//echo "TEST --------- Opt: ".$opt." ----- User ID: ".$userx." -- Set: ".$set."  --- ID: " . $id;

if ($id == 22) {


	if ($set == 2) { // insert	
		$class_password = "";
		$cp = "";		
																
		if (isset($_POST['submit'])) {
			$user_id = trim(mysql_prep($_POST['user_id']));
			$class_id = trim(mysql_prep($_POST['class_id']));
			$expiry_date = trim(mysql_prep($_POST['expiry_date']));
			$create_date = $today;	
			//$class_ed = (strtotime($today)) + (60*60*24);
			$creator_id = $userx;
			$status = '1';	
			
			// set the expiry date to the class expiry date
			//$class_ed = "";
			/*
			$result_setI = get_aca_class_rec($class_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$class_id = $row["$class_id"];			
				$class_ed = $row["$expiry_date"];			
				//$class_ed = strtotime($row["$today"]);		
			}
			//if ($expiry_date > $class_exd) { $expiry_date = $class_exd; }
			*/
		}
		
		if ($test_aca == 1) {
			$msg = "set 2 Post opt=".$opt."</br>";
			$msg .= "create_date: ".$create_date."</br>";
			$msg .= "creator_id: ".$userx."</br>";
			echo $msg;
		}

		// test class password if not administrator
		if ($aca_accesslevel < 9) {
			
			$class_password = trim(mysql_prep($_POST['class_password']));
			
			$result_set = get_aca_class_rec($class_id);
			while ($row = mysqli_fetch_array($result_set)) {	
				$cp = $row["password"];
			}
		} else {
			$class_password = "passed";
			$cp = "passed";					
		}
		if ($class_password == $cp && $class_password <> "") {
			$query = "INSERT INTO aca_user_class( ";
			$query .= " user_id, class_id, expiry_date, create_date, "; 
			$query .= " status, creator_id ) ";
			$query .= " VALUES (   ";
			$query .= " {$user_id}, {$class_id}, '{$expiry_date}', '{$create_date}', ";
            $query .= " '{$status}', {$userx} ) " ;
            $result = mysqli_query( $connection, $query);
            if (mysqli_affected_rows($connection) == 1) {
                $query1 = "Update aca_user set student_type = '1' where user_id = '".$user_id."'" ;
                $result1 = mysqli_query( $connection, $query1);
                    if (mysqli_affected_rows($connection) == 1) {
                        // Success
                        echo "<div class=\"alert alert-success\">";
                        echo "Record Created";
                        echo "</div>";
                    } else {
                        echo "<div class=\"alert alert-danger\">";
                        echo "Record creation failed1";
                        echo "</div>";
                    }
				$set = 0; // set  action back to listing
				} else {
				echo "<div class=\"alert alert-danger\">";
					echo "Record creation failed";
				echo "</div>";
			}
		$set = 0; // set action back to form
		if ($aca_accesslevel == 9) { $set = 1; }
		} else {
			echo "<div class=\"alert alert-danger\">";
				echo "The class password you provided could not be authenticated.";
			echo "</div>";
			$set = 1;
		}
	//} // end opt = 1
		
	} // end of set = 2
	
		
	if ($set == 9 && $key > 0) { // set = 9 to delete record						
		$table_name = "aca_user_class";
		$field_name = "user_class_id";
		$query = " DELETE FROM {$table_name} WHERE {$field_name} = {$key} ";				
		$result_set = mysqli_query($connection, $query);
			//$message = (mysqli_affected_rows($connection));	
				echo "<div class=\"alert alert-success\">";
					echo "Record Deleted";
				echo "</div>";
		 $set = 0;
		 $key = 0; 
	} // end of 9
	

	if ($set == 4 && $key <> 0) { // update record													
		if (isset($_POST['submit'])) {
			$class_id = trim(mysql_prep($_POST['class_id']));
			$expiry_date = trim(mysql_prep($_POST['expiry_date']));
			$create_date = $today;	
			$creator_id = $userx;
		}
		
			
			if ($test_aca == 1) {
				$msg = "set 2 Post</br>";
				$msg .= "create_date: ".$create_date."</br>";
				$msg .= "status: ".$status."</br>";
				$msg .= "creator_id: ".$userx."</br>";
				//echo $msg;
			}
			
			// Update
			$query = "UPDATE aca_user_class SET ";
			$query .= " user_id = '{$user_id}', ";
			$query .= " class_id = '{$class_id}' ";
			$query .= " expiry_date = '{$expiry_date}', ";
			$query .= " status = '{$status}', ";
			$query .= " creator_id = '{$userx}', ";
			$query .= " WHERE user_class_id = {$key} ";
			$query .= " LIMIT 1 ";
		
			$result = mysqli_query( $connection, $query);
			if (mysqli_affected_rows($connection) == 1) {	
				// Success	
				echo "<div class=\"alert alert-success\">";
					echo "The record was updated successfully.";
				echo "</div>";
				$set = 0; // set action back to listing
				} else {
				echo "<div class=\"alert alert-danger\">";
					echo "The record was not updated. Please try again. Make sure that all fields are completed.";
				echo "</div>";
				$set = 3; // set action back to form
			}
				
	} // end of set = 4 update

	if ($set == 9 && $key > 0) { // set = 9 to delete record											
		$table_name = "user_class";
		$field_name = "user_class_id";
		$query = " DELETE FROM {$table_name} WHERE {$field_name} = {$key} ";				
		$result = mysqli_query( $connection, $query);
			$anniversary = (mysqli_affected_rows($connection));
				echo "<div class=\"alert alert-success\">";
					echo "The record was successfully deleted.";
				echo "</div>";
		 $set = 0;
		 $key = 0; 
	} // end of 9


	if ($set == 3) { // edit																
		echo "<div class=\"panel panel-primary\"> "; 
		  echo "<div class=\"panel-heading\">User Class Maintenance</div> "; 
		  echo "<div class=\"panel-body\">";
				
		$result_set = get_aca_user_class_rec($key);
		while ($row = mysqli_fetch_array($result_set)) {	
			$key = $row["user_class_id"];
			$user_id = $row["user_id"];
			$class_id = $row["class_id"];	
			$expiry_date = $row["expiry_date"];	
			$create_date = $row["create_date"];	
			$status = $row["status"];
					
			$class = "";
			$result_setI = get_aca_class_rec($class_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$class_name = $row["name"];		
			}
			$user_name = "";
			$result_setI = get_aca_user_rec($user_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$user_name = $row["name"];		
			}
			$d_status = $status;
				$result_set74 = get_dim("status", $status);
				while ($row = mysqli_fetch_array($result_set74)) {
					$dim_key = $row["dim_key"]; 
					$d_status = $row["description"];
				}
		}

		
		echo "<form name=\"aca_user_class\" method=\"post\" action=\"?id=".$id."&set=4&opt=" . $opt . "&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat5=".$cat5." \"enctype=\"multipart/form-data\">";



		echo "<div class=\"form-group\">";
		  echo "<label for=\"user_id\">User:</label>";	
		  echo "<input type=\"text\" disabled=\"disabled\" name=\"user_id\" class=\"form-control reqd\" id=\"user_id\" value=\"".$user_name." (".$user_id.") \">";
		echo "</div>";
		
		echo "<div class=\"form-group\">";
		  echo "<label for=\"class_id\">Class:</label>";		 
			echo "<select name=\"class_id\" class=\"form-control reqd\" id=\"class_id\">";					
						  echo "<option value=\"".$class_id."\">".$class_name."</option>";
								$result_set19 = get_aca_userX_class_list(0, 99, 1, $today, $user_id); // $npage, $no_of_list, $status, $expiry, $thisuser
								while ($row = mysqli_fetch_array($result_set19)) {
									$dim_key = $row["class_id"]; 
									$display_name = $row["name"];
									$class_expiry_date = $row["expiry_date"];
									echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								}
			echo "</select>";
		echo "</div>";
		
		echo "<div class=\"form-group\">";
		  echo "<label for=\"expiry_date\">Expiry Date:</label>";
		  echo "<input type=\"date\" name=\"expiry_date\" class=\"form-control reqd\" id=\"expiry_date\" placeholder=\"Expiry Date\" value=\"".$expiry_date."\">";
		echo "</div>";
		
		echo "<div class=\"form-group\">";
		  echo "<label for=\"status\">Status</label>";
			echo "<select name=\"status\" class=\"form-control reqd\" onchange=\"showTool(this.value)\" id=\"status\">";					
						  echo "<option value=\"".$status."\">".$d_status."</option>";
								  $result_set9 = get_dim_list("status");
								  while ($row = mysqli_fetch_array($result_set9)) {
									  $dim_key = $row["dim_key"]; 
									  $display_name = $row["description"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }
								  echo "</select>";
		echo "</div>";
										
			// buttons
			$update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Submit</button>";			
			$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";		
			$remove = "<button name=\"RemoveBut\" type=\"button\" value=\"Remove\" class=\"btn btn-danger\" confirmDelete(MM_goToURL('parent','?id=".$id."&opt=".$opt."&set=9&key=".$key."');return document.MM_returnValue, MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\)\"/>Remove Studend from Class</button>";
			
			
		  echo "<p align=\"center\">" . $update . " " . $cancel . " " . $remove . "</p>";
		  
		echo "</form>";			
		
		
		
		
		  echo "</div> "; 
	   echo " </div> "; 	
		
		
		echo "<p>&nbsp;</p>";	
		
	} // end of edit


	

	if ($set == 1) { // create assignment
		$startdate = strtotime("Saturday");
		$d = (strtotime("+18 weeks",$startdate));
		$expiry_date = date("Y-m-d", $d);
		
		// test is any classes not joined
		$cid  = "";
		$result_set111 = get_aca_userX_class_list(0, 99, 1, $today, $userx); // $npage, $no_of_list, $status, $expiry, $thisuser
		while ($row = mysqli_fetch_array($result_set111)) {
			$cid  = "";
			$cid = $row["class_id"];
		}

        if (logged_in() && $aca_accesslevel >= 1) {
            echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Join a Class</h2><div class=\"colored-line-left\"></div></div>
          <div class=\"clearfix\"></div>
          <div class=\"row content-pad\">
          <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";
        }


		if ($cid <> "") {
			  
			
			echo "<form name=\"aca_user_class\" method=\"post\" action=\"?id=".$id."&set=2&opt=" . $opt . "&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat5=".$cat5."&cat7=".$cat7." \"enctype=\"multipart/form-data\">";

            echo "<table class=\"table table-striped\">";
            echo "<thead>";
            echo "<tr><th colspan=\"2\" style=\"background:#58595b;color:#FFFFFF;\">Select Class</th></tr>";
            echo "</thead>";
            echo "</tbody>";

			if ($opt == 1) { // student
				echo "<tr>";
				  echo "<td>User:</td>";
					echo "<td><select name=\"user_id\" class=\"form-control reqd\" id=\"user_id\">";
								  echo "<option value=\"".$userx."\">".$username."</option>";
										//$result_set21 = get_aca_user($user_id);
										//while ($row = mysqli_fetch_array($result_set21)) {
										//	$dim_key = $row["user_id"]; 
										//	$display_name = $row["name"];
										//	echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
										//}
					echo "</select></td>";
				echo "</tr>";
			}
	
			// build opt = 51 *********************
			
			if ($opt == 51 || $opt == 91) { // instructor
			// select the class and show all uers not assigned to that class or select the user and display all the classes not assigned to?
				echo "<tr>";
				  echo "<td>User:</td>";
					echo "<td><select name=\"user_id\" class=\"form-control reqd\" onchange=\"showClass(this.value)\" id=\"class_id\">";
								  echo "<option value=\"\"></option>";
										$result_set21 = get_aca_user_list(0, 99999, 99);
										while ($row = mysqli_fetch_array($result_set21)) {
											$dim_key = $row["user_id"]; 
											$display_username = $row["username"];
											$display_name = $row["name"];
											$d = $display_username . " - " . $display_name;
											// classes assigned to
											$result_set29 = get_aca_user_class_list_assigned(1, $dim_key);
											while ($row = mysqli_fetch_array($result_set29)) {
												$dim_class = $row["name"]; 
												$d .= " (". $dim_class . ") ";
											}
											echo "<option value=\"" . $dim_key . "\">" . $d . "</option>";
										}
					echo "</select></td>";
				echo "</tr>";
			}
			
			if ($opt == 1) { // student
			echo "<tr>";
			  echo "<td>Class:</td>";
				echo "<td><select name=\"class_id\" class=\"form-control reqd\" id=\"class_id\">";
							  echo "<option value=\"\"></option>";
									$result_set19 = get_aca_userX_class_list(0, 99, 1, $today, $userx); // $npage, $no_of_list, $status, $expiry, $thisuser
									while ($row = mysqli_fetch_array($result_set19)) {
										$dim_key = $row["class_id"]; 
										$display_name = $row["name"];
										$class_expiry_date = $row["expiry_date"];
										echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
									}
				echo "</select></td>";
			echo "</tr>";
			}
			
			if ($opt > 1) { // not student
			echo "<tr id=\"txtHint\">";
			echo "<td>Class:</td><td></td>";
			echo "</tr>";
			}
			
			if ($opt < 91) { // instructor			
				echo "<tr>";
				  echo "<td>Class Password:</td>";
				  echo "<td><input type=\"text\" name=\"class_password\" class=\"form-control reqd\" id=\"class_password\" placeholder=\"Class Password\" value=\"\"></td>";
				echo "</tr>";
			}
			
			// Dynamic lookup required ********************
			echo "<tr>";
			  echo "<td>Class Subscription Expiry Date:</td>";
			  echo "<td><input type=\"date\" name=\"expiry_date\" class=\"form-control reqd\" id=\"expiry_date\" placeholder=\"Expiry Date\" value=\"".$expiry_date."\"></td>";
			echo "</tr>";
			/*
			echo "<div class=\"form-group\">";
			  echo "<label for=\"status\">Status</label>";
				echo "<select name=\"status\" class=\"form-control reqd\" onchange=\"showTool(this.value)\" id=\"status\">";					
							  //echo "<option value=\"".$status."\">".$d_status."</option>";
									  $result_set9 = get_dim_list("status");
									  while ($row = mysqli_fetch_array($result_set9)) {
										  $dim_key = $row["dim_key"]; 
										  $display_name = $row["description"];
										  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
									  }
									  echo "</select>";
			echo "</div>";		
			*/								
				// buttons
				$update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Join Class</button>";			
				$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";
				if ($aca_accesslevel == 9) { 
					$viewClasses = "<button name=\"ViewClassesBut\" type=\"button\" value=\"View\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=".$opt."&set=10&key=0');return document.MM_returnValue\"/>View Classes</button>";
				} else {
					$viewClasses = "";	
				}
			  echo "<tr><td colspan=\"2\" class=\"text-center\">" . $update . " " . $cancel . " " . $viewClasses . "</td></tr>";
				echo "</tbody>";
				echo "</table>";

			echo "</form>";			
			

			


		} else { // end of if classes not joined			
			echo "<div class=\"alert alert-info\">";
				echo "No more classes to join.";
			echo "</div>";
			$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=0&opt=0&key=0');return document.MM_returnValue\"/>Back</button>";
				
			echo "<p align=\"center\">" . $cancel . "</p>";
		}
		if ($opt == 1) { // student then display class list
		
		
		
		
			$set = 0;
		}

        echo "</div>";
        echo "</div>";

	} // end of set = 1
	


	if ($set == 0) {	
		// Student - display classes already joined
		if ($aca_accesslevel == 1) {
			// get records
			$cat = $userx;
			$p_records = 0;
			$result_setAA = get_aca_user_class_count(99, $cat, $cat7); // status 1 = valid, access 0 for all
			while ($row = mysqli_fetch_array($result_setAA)) {
				$p_records = $row["record_count"]; 
			}
			$page_break = $page_break_d;
			$anchor = "x";
			$p_link = "?id=".$id."&opt=".$opt."&cat2=0&cat3=0&cat6=".$cat6."&key=".$key."&cat=".$cat."&cat4=".$cat4."&cat7=".$cat7."&set=".$set;
			$p = page_nav_last($page, $page_break, $p_records, $p_link, $anchor);
			$n = "<a href=\"?id=".$id."&set=1&key=".$key."\">New</a>";
			$c = " ";
			$n = "";			

				
			echo "<table class=\"table table-striped\">";
			echo "<thead>";
            echo '<tr><th colspan="3" style="background:#58595b;color:#FFFFFF;line-height:2em;">User Class<div style="float:right;">' . $n.' '.$p . '</div></th></tr>';
			echo "<tr>";
				echo "<th>Class</th>";
				//echo "<th>User</th>";
				echo "<th>Expiry Date</th>";
				echo "<th>Instructor</th>";
			echo "</tr>";		
			echo "</thead>";
			echo "<tbody>";
			// get data
			$npage = $page;
			$result_set = get_aca_user_class_list($npage, $page_break, 99, $cat, $cat7);
			while ($row = mysqli_fetch_array($result_set)) {		
				$key = $row["user_class_id"];
				$user_id = $row["user_id"];
				$class_id = $row["class_id"];		
				$expiry_date = $row["expiry_date"];
				$note = $row["note"];
				$status = $row["status"];
						
				$user_name = "";
				$result_setU = get_aca_user_rec($user_id);
				while ($row = mysqli_fetch_array($result_setU)) {		
					$user_name = $row["name"];		
				}
				$exam = "";
				$result_setI = get_aca_class_rec($class_id);
				while ($row = mysqli_fetch_array($result_setI)) {		
					$class_name = $row["name"];		
					$instructor_id = $row["instructor_id"];			
				}
				$instructor = "";
				$result_setIS = get_aca_instructor_rec($instructor_id);
				while ($row = mysqli_fetch_array($result_setIS)) {		
					$instructor = $row["name"];		
				}
				$d_status = $status;
					$result_set74 = get_dim("status", $status);
					while ($row = mysqli_fetch_array($result_set74)) {
						$dim_key = $row["dim_key"]; 
						$d_status = $row["description"];
					}
				$un = $user_name;
				$ed = "";
				if ($expiry_date < $today) { $ed = " <span class=\"badge alert-danger\">Expired</span>"; }
			echo "<tr>";
				echo "<td>".$class_name."</td>";
				//echo "<td>".$un."</td>";
				echo "<td>".$expiry_date.$ed."</td>";
				echo "<td>".$instructor."</td>";
			echo "</tr>";
			}	
			echo "<tr>";
				echo "<td align=\"left\" colspan=\"2\">".$c."</td>"; // close, new, page nav
				echo "<td align=\"right\" colspan=\"2\">".$n."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;".$p."</td>";
			echo "</tr>";
	
	
			echo "</tbody>";
			echo "</table>";
		
		
											
		} else { 
			// get record count
			$p_records = 0;
			$result_setA = get_aca_user_class_count(99, 0, $cat7); // status 1 = valid, access 0 for all
			while ($row = mysqli_fetch_array($result_setA)) {
				$p_records = $row["record_count"]; 
			}
			$page_break = $page_break_d;
			$anchor = "x";
			$p_link = "?id=".$id."&opt=".$opt."&cat2=0&cat3=0&cat6=".$cat6."&key=".$key."&cat=".$cat."&cat4=".$cat4."&cat7=".$cat7."&set=".$set;
			$p = page_nav_last($page, $page_break, $p_records, $p_link, $anchor);
			$n = "<a href=\"?id=".$id."&set=1&key=".$key."\">New</a>";
			$c = " ";
			if ($aca_accesslevel >= 5) {
				if ($aca_accesslevel == 5) { $opt = 51; }
				if ($aca_accesslevel == 9) { $opt = 91; }
				$n =  "<button name=\"NewBut\" type=\"button\" value=\"New\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=1&opt=".$opt."&key=0');return document.MM_returnValue\"/>New</button>&nbsp;&nbsp;";
			//echo "<p align=\"right\">" . $n." ".$p . "</p></br>";
			} else {	
				$n = "";
			}

            echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Join a Class</h2><div class=\"colored-line-left\"></div></div>";
        	echo '<div class="clearfix"></div>';
			echo '<div class="row content-pad">';
			echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";

			echo "<table class=\"table table-striped\">";
			echo "<thead>";
            echo '<tr><th colspan="5" style="background:#58595b;color:#FFFFFF;line-height:2em;">User Class<div style="float:right;">' . $n.' '.$p . '</div></th></tr>';
			echo "<tr>";
				echo "<th>Class</th>";
				echo "<th>User</th>";
				echo "<th>Expiry Date</th>";
				echo "<th>Status</th>";
				echo "<th></th>";
			echo "</tr>";		
			echo "</thead>";
			echo "<tbody>";
			// get data
			$npage = $page;
			$result_set = get_aca_user_class_list($npage, $page_break, 99, 0, $cat7);
			while ($row = mysqli_fetch_array($result_set)) {		
				$key = $row["user_class_id"];
				$user_id = $row["user_id"];
				$class_id = $row["class_id"];		
				$expiry_date = $row["expiry_date"];
				$status = $row["status"];
						
				$user_name = "";
				$result_setU = get_aca_user_rec($user_id);
				while ($row = mysqli_fetch_array($result_setU)) {		
					$user_name = $row["name"];		
				}
				$exam = "";
				$result_setI = get_aca_class_rec($class_id);
				while ($row = mysqli_fetch_array($result_setI)) {		
					$class_name = $row["name"];		
				}
				$d_status = $status;
					$result_set74 = get_dim("status", $status);
					while ($row = mysqli_fetch_array($result_set74)) {
						$dim_key = $row["dim_key"]; 
						$d_status = $row["description"];
					}
				
				if ($aca_accesslevel == 9) {
					$un = "<a href=\"?id=".$id."&set=3&opt=0&login=1&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&key=".$key."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$user_name."</a>";
				} else {
					$un = $user_name;
				}						
				$rs = "<button name=\"RemoveBut\" type=\"button\" value=\"Remove\" class=\"btn btn-danger\" onclick=\"MM_goToURL('parent','?id=".$id."&set=9&opt=".$opt."&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&key=".$key."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."');return document.MM_returnValue\"/>Remove</button>";
				
			echo "<tr>";
				echo "<td>".$class_name."</td>";
				echo "<td>".$un."</td>";
				echo "<td>".$expiry_date."</td>";
				echo "<td>".$d_status."</td>";
				echo "<td align=\"right\">".$rs."</td>";
			echo "</tr>";
			}	
			echo "<tr>";
				echo "<td align=\"right\" colspan=\"5\">".$c." ".$n."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;".$p."</td>";
			echo "</tr>";
	
	
			echo "</tbody>";
			echo "</table>";

			echo "</div>";
			echo "</div>";

		  // filter class list
		  if ($aca_accesslevel >= 5) {
				
			echo "<table class=\"table table-striped\">";
			echo "<thead>";
              echo '<tr><th colspan="5" style="background:#58595b;color:#FFFFFF;">Class Filter<div style="float:right;">' . $n.' '.$p . '</div></th></tr>';

			echo "</thead>";
			echo "<tbody>";
			echo "<tr>";
				echo "<td><a href=\"?id=".$id."&set=".$set."&opt=".$opt."&cat7=0\">[ ALL ]</a>&nbsp;&nbsp;";
			// get data
			$result_setC = get_aca_class_list(0, 999, 99, 0, 0);
			while ($row = mysqli_fetch_array($result_setC)) {		
				$ckey = $row["class_id"];
				$cname = $row["name"];
				echo " <a href=\"?id=".$id."&set=".$set."&opt=".$opt."&cat7=".$ckey."\">[".$cname."]</a>&nbsp;&nbsp;";
			}	
				echo "</td>";
			echo "</tr>";	
			
			$class_selected = "Class Filter: ALL";
			if ($cat7 <> 0) {
			  $result_setD = get_aca_class_rec($cat7);
			  while ($row = mysqli_fetch_array($result_setD)) {	
				  $class_selected = "Class Filter: " . $row["name"];
			  }				
			}
			echo "<tr>";
				echo "<td align=\"left\" colspan=\"2\">".$class_selected."</td>";
			echo "</tr>";
			
			echo "</tbody>";
			echo "</table>";
		} // end of if not accesslevel >= 5	for filtered list	   
		   
		   
		} // end of if not accesslevel = 1
		
	} // end of set = 0
	
	
	
	if ($set == 10) { // view classes
		// get record count
		$p_records = 0;
		$result_setA = get_aca_class_count(99, 0); // status 1 = valid, access 0 for all
		while ($row = mysqli_fetch_array($result_setA)) {
			$p_records = $row["record_count"]; 
		}
		$page_break = $page_break_d;
		$anchor = "x";
		$p_link = "?id=".$id."&opt=".$opt."&cat2=0&cat3=0&cat6=".$cat6."&key=".$key."&cat=".$cat."&cat4=".$cat4."&set=".$set;
		$p = page_nav_last($page, $page_break, $p_records, $p_link, $anchor);
		//$n = "<a href=\"?id=".$id."&set=1&key=".$key."\">New</a>";
		$c = " ";
		if ($aca_accesslevel == 9) {
			$n =  "<button name=\"NewBut\" type=\"button\" value=\"New\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=1&opt=0&key=".$key."');return document.MM_returnValue\"/>New Class</button>&nbsp;&nbsp;";
			$nx =  "<button name=\"NewBut\" type=\"button\" value=\"New\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=1&opt=0&key=".$key."');return document.MM_returnValue\"/>Join Student to Class</button>&nbsp;&nbsp;";
		echo "<p align=\"right\">" . $n." " . $nx." ".$p . "</p></br>";
		} else {	
			$n = "";
		}
		
		
		echo "<div class=\"panel panel-primary\"> "; 
		  echo "<div class=\"panel-heading\">Class</div> "; 
		  echo "<div class=\"panel-body\">";
		  	
		echo "<table class=\"table table-striped\">";
		echo "<thead>";
		echo "<tr>";
			echo "<th>Class</th>";
			echo "<th>Password</th>";
			echo "<th>Expiry Date</th>";
			echo "<th>Status</th>";
		echo "</tr>";		
		echo "</thead>";
		echo "<tbody>";
		// get data
		$npage = $page;
		$result_set = get_aca_class_list($npage, $page_break, 99, 0);
		while ($row = mysqli_fetch_array($result_set)) {		
			$key = $row["class_id"];
			$name = $row["name"];
			$password = $row["password"];		
			$expiry_date = $row["expiry_date"];
			$status = $row["status"];
			$d_status = $status;
				$result_set74 = get_dim("status", $status);
				while ($row = mysqli_fetch_array($result_set74)) {
					$dim_key = $row["dim_key"]; 
					$d_status = $row["description"];
				}
			
			$un = "<a href=\"?id=".$id."&set=3&opt=0&login=1&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&key=".$key."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$name."</a>";
			
		echo "<tr>";
			echo "<td>".$un."</td>";
			echo "<td>".$password."</td>";
			echo "<td>".$expiry_date."</td>";
			echo "<td>".$d_status."</td>";
		echo "</tr>";
		}	
		echo "<tr>";
			echo "<td align=\"left\" colspan=\"2\">".$c."</td>"; // close, new, page nav
			echo "<td align=\"right\" colspan=\"2\">".$n." ".$nx."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;".$p."</td>";
		echo "</tr>";


		echo "</tbody>";
		echo "</table>";
		  echo "</div> "; 
	   echo " </div> "; 	
	
	} // end of set = 10


	
} // end of user id = 22
?>
 