<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>Purchase History</h2><div class="colored-line-left"></div></div>
<div class="clearfix"></div>
<div class="row content-pad">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        
                    <table class="table">
						<tr>
							<th>Date</th>
							<th>Order ID</th>
							<th>Detail</th>
							<th>Payment Status</th>
							<th></th>
							<th></th>
						</tr>
                        <?php

                        $query = "select * from aca_transaction where userid = '".$_SESSION["aca_userx"]."'";

                        $result = mysqli_query( $connection, $query);

                        while ($row = mysqli_fetch_array($result)) {
							
							$transaction = unserialize($row["s_transaction"]);
							$paymentstatus = $row["paymentcomplete"];
							$status = $row["status"];

							
                            echo '<tr>
                                <td>'.date("Y-m-d",strtotime($row["transactiondate"])).'</td>
                                <td>'.$row["id"].'</td>
                                <td>'.$transaction[0]["code"].' - '.$transaction[0]["description"].'</td>
								<td align="center">';
								
								switch ($row["paymentcomplete"]) {
									case "0":
										echo '<font color="#777"><i>Pending</i></font>';
										break;
									case "1":
										echo '<font color="#0F0"><i>Successful</i></font>';
										break;
									case "2":
										echo '<font color="#F00"><i>Declined</i></font>';
										break;
									case "3":
										echo '<font color="#F00"><i>Cancelled</i></font>';
										break;
									case "4":
										echo '<font color="#F00"><i>Cancelled</i></font>';
										break;
									default:
										echo "N/A";
								}
								
								
								echo '</td>
                                <td><a href="index.php?id=114&transaction='.$row["id"].'" class="transactiondetail btn btn-default">Detail</a></td>';
								
								$query2 = "select a.*,b.* from aca_exam a inner join aca_user_exam b on a.exam_id=b.exam_id where b.user_id = '".$_SESSION["aca_userx"]."' and a.exam_id = '".$transaction[0]["examid"]."' limit 1";

									$result2 = mysqli_query( $connection, $query2);

									while ($row2 = mysqli_fetch_array($result2)) {
										
										if(($row2["pdf"] == '0')){
											echo '<td><button class="transactiondownloadpdf btn btn-default" disabled=disabled alt="Download Unavailable" title="Download Unavailable">Download</button></td>';
										}
										if($row2["pdf"] == '1' && $paymentstatus == '1'){
											$curdate = new DateTime(date("Y-m-d"));
											$mydate = new DateTime($row2["expiry_date"]);
											if($curdate > $mydate){
												echo '<td><button class="transactiondownloadpdf btn btn-danger" disabled=disabled alt="Download Expired" title="Download Expired">Download</button></td>';
											} else {
												echo '<td><a class="btn btn-success" onclick="downloadpdf('.$row["id"].')">Download</a></td>';
											}
										}
										
									}
                            echo '</tr>';
                        }
                        ?>
</table>
        
    </div>
</div>

<script>
    $(document).ready(function() {
		
    })
	
	function downloadpdf(id){
			
			var transactionid = id;
			
			
				$.post( "ajax/ajax-transaction.php", { action:"DownloadPDF",transactionid:transactionid
				}, function(data){
					if(data.result == 1){
						window.open(data.link, '_blank');
					} else {
						
					}
				},
				"json");
		}
</script>