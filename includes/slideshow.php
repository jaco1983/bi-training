<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/08/23
 * Time: 9:13 AM
 */
//$url = get_base_url();
echo '

    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>  <!-- SLIDE  -->
          <li data-transition="fade" data-slotamount="25" data-masterspeed="1000"  data-thumb="'.get_base_url().'images/sap-certification.jpg"  data-saveperformance="off">
            <!-- MAIN IMAGE -->
            
			<img src="'.get_base_url().'images/sap-certification.jpg"  alt="seo,search engine optimization" data-bgposition="center center" data-kenburns="on" data-duration="12000" data-ease="Power0.easeInOut" data-bgfit="115" data-bgfitend="100" data-bgpositionend="center center">
			<div class="tp-caption very_large_text skewfromrightshort tp-resizeme head-tag"
            data-x="center" data-hoffset="0"
            data-y="center" data-voffset="-120"
            data-speed="0"
            data-start="0"
            data-easing="Power3.easeInOut"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.1"
            data-endelementdelay="0.1"
            data-endspeed="300"
            style="z-index: 5; max-width: 100%; max-height: 100%; white-space: nowrap;"><img src="'.get_base_url().'images/BlackBoard-BI-Logo-Inverted.png" />
          </div>
		  <!-- LAYER NR. 2 -->
            <div class="tp-caption  lfl tp-resizeme small-text"
            data-x="left" data-hoffset="100"
            data-y="center" data-voffset="100"
            data-speed="1000"
            data-start="1500"
            data-easing="Power1.easeInOut"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.1"
            data-endelementdelay="0.1"
            data-endspeed="300"
            style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;color:#FFFFFF;">
            <p><ul style="font-size:16px;list-style:outside disc;">
			<li>Randomized exam questions</li>
			<li>Prepared by experienced SAP Professionals</li>
			<li>Real-time marking</li>
			<li>Test or Exam mode</li>
			<li>Hundreds of exam questions</li>
			<li>Download PDF or practice online</li>
			</ul></p>
			<a href="javascript:void(0)" onclick="PlayDemo()" class="btn btn-success">View Demo&nbsp;<span class="glyphicon glyphicon-facetime-video"></span></a>
            </div>
			
			<div class="tp-caption  lfl tp-resizeme small-text"
            data-x="right" data-hoffset="-60"
            data-y="center" data-voffset="85"
            data-speed="1000"
            data-start="1500"
            data-easing="Power1.easeInOut"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.1"
            data-endelementdelay="0.1"
            data-endspeed="300"
            style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;color:#FFFFFF;text-align:left !important;">
            <h1>Online SAP Certification<br />Practice Questions</h1>
			<p style="text-align:left;">The Blackboard BI Training Subscription service is used by thousands<br />of students to prepare for the SAP certification Exam.</p>
			<a href="?id=1" class="btn btn-success" style="padding:6px 50px !important;">Subscribe</a>
			<a href="javascript:void(0)" onclick="Trial()" class="btn btn-success" style="padding:6px 50px !important;">14 Day Trial</a>
			<p style="text-align:left;">Six weeks online access and PDF download from $35</p>
            </div>
            </li>
		</ul>
	</div>
</div>

';