<?php	
// evaluation id = 26

if ($id == 26) {
    echo '';

    if ($aca_accesslevel == 1 && $set == 0) { $set = 3; } // user can only edit and insert
	if ($aca_accesslevel > 1 && $set <> 0) { $set = 0; } // instructor can only see list


	if ($set == 2) { // insert													
		if (isset($_POST['submit'])) {
			$class_id = trim(mysql_prep($_POST['class_id']));
			//$user_id = trim(mysql_prep($_POST['user_id']));
			$evaluation_date = trim(mysql_prep($_POST['evaluation_date']));
			$current_position = trim(mysql_prep($_POST['current_position']));
			$courses_attended = trim(mysql_prep($_POST['courses_attended']));
			$function_id_01 = trim(mysql_prep($_POST['function_id_01']));
			$function_id_02 = trim(mysql_prep($_POST['function_id_02']));
			$function_id_03 = trim(mysql_prep($_POST['function_id_03']));
			$function_id_04 = trim(mysql_prep($_POST['function_id_04']));
			$function_id_05 = trim(mysql_prep($_POST['function_id_05']));
			$experience_yr_01 = trim(mysql_prep($_POST['experience_yr_01']));
			$experience_yr_02 = trim(mysql_prep($_POST['experience_yr_02']));
			$experience_yr_03 = trim(mysql_prep($_POST['experience_yr_03']));
			$experience_yr_04 = trim(mysql_prep($_POST['experience_yr_04']));
			$experience_yr_05 = trim(mysql_prep($_POST['experience_yr_05']));
			$experience_01 = trim(mysql_prep($_POST['experience_01']));
			$experience_02 = trim(mysql_prep($_POST['experience_02']));
			$experience_03 = trim(mysql_prep($_POST['experience_03']));
			$experience_04 = trim(mysql_prep($_POST['experience_04']));
			$experience_05 = trim(mysql_prep($_POST['experience_05']));
			$role_id = trim(mysql_prep($_POST['role_id']));
			$courses_id_01 = trim(mysql_prep($_POST['courses_id_01']));
			$courses_id_02 = trim(mysql_prep($_POST['courses_id_02']));
			$courses_id_03 = trim(mysql_prep($_POST['courses_id_03']));
			$courses_id_04 = trim(mysql_prep($_POST['courses_id_04']));
			$courses_id_05 = trim(mysql_prep($_POST['courses_id_05']));
			$courses_id_06 = trim(mysql_prep($_POST['courses_id_06']));
			$courses_id_07 = trim(mysql_prep($_POST['courses_id_07']));
			$courses_id_08 = trim(mysql_prep($_POST['courses_id_08']));
			$courses_id_09 = trim(mysql_prep($_POST['courses_id_09']));
			$courses_id_10 = trim(mysql_prep($_POST['courses_id_10']));
			$qualification_01 = trim(mysql_prep($_POST['qualification_01']));
			$qualification_02 = trim(mysql_prep($_POST['qualification_02']));
			$qualification_03 = trim(mysql_prep($_POST['qualification_03']));
			$qualification_04 = trim(mysql_prep($_POST['qualification_04']));
			$qualification_05 = trim(mysql_prep($_POST['qualification_05']));
			$create_date = $today;	
			$creator_id = $userx;
			$status = '1';	
		}
		
		if ($test_aca == 1) {
			$msg = "set 2 Post opt=".$opt."</br>";
			$msg .= "class_id: ".$class_id."</br>";
			$msg .= "user_id: ".$userx."</br>";
			$msg .= "current_position: ".$current_position."</br>";
			$msg .= "courses_attended: ".$courses_attended."</br>";
			$msg .= "evaluation_date: ".$evaluation_date."</br>";
			$msg .= "function_id_01: ".$function_id_01."</br>";
			$msg .= "function_id_02: ".$function_id_02."</br>";
			$msg .= "function_id_03: ".$function_id_03."</br>";
			$msg .= "function_id_04: ".$function_id_04."</br>";
			$msg .= "function_id_05: ".$function_id_05."</br>";
			$msg .= "experience_yr_01: ".$experience_yr_01."</br>";
			$msg .= "experience_yr_02: ".$experience_yr_02."</br>";
			$msg .= "experience_yr_03: ".$experience_yr_03."</br>";
			$msg .= "experience_yr_04: ".$experience_yr_04."</br>";
			$msg .= "experience_yr_05: ".$experience_yr_05."</br>";
			$msg .= "experience_01: ".$experience_01."</br>";
			$msg .= "experience_02: ".$experience_02."</br>";
			$msg .= "experience_03: ".$experience_03."</br>";
			$msg .= "experience_04: ".$experience_04."</br>";
			$msg .= "experience_05: ".$experience_05."</br>";
			$msg .= "role_id: ".$role_id."</br>";
			$msg .= "courses_id_01: ".$courses_id_01."</br>";
			$msg .= "courses_id_02: ".$courses_id_02."</br>";
			$msg .= "courses_id_03: ".$courses_id_03."</br>";
			$msg .= "courses_id_04: ".$courses_id_04."</br>";
			$msg .= "courses_id_05: ".$courses_id_05."</br>";
			$msg .= "courses_id_06: ".$courses_id_06."</br>";
			$msg .= "courses_id_07: ".$courses_id_07."</br>";
			$msg .= "courses_id_08: ".$courses_id_08."</br>";
			$msg .= "courses_id_09: ".$courses_id_09."</br>";
			$msg .= "courses_id_10: ".$courses_id_10."</br>";
			$msg .= "qualification_01: ".$qualification_01."</br>";
			$msg .= "qualification_02: ".$qualification_02."</br>";
			$msg .= "qualification_03: ".$qualification_03."</br>";
			$msg .= "qualification_04: ".$qualification_04."</br>";
			$msg .= "qualification_05: ".$qualification_05."</br>";
			$msg .= "create_date: ".$create_date."</br>";
			$msg .= "creator_id: ".$userx."</br>";
			echo $msg;
		}

		$query = "INSERT INTO aca_evaluation( ";
		$query .= " class_id, user_id, evaluation_date,  ";
		$query .= " current_position, courses_attended, ";
		$query .= " function_id_01, function_id_02, function_id_03, function_id_04, function_id_05, ";
		$query .= " experience_yr_01, experience_yr_02, experience_yr_03, experience_yr_04, experience_yr_05, ";
		$query .= " experience_01, experience_02, experience_03, experience_04, experience_05, ";
		$query .= " role_id, ";
		$query .= " courses_id_01, courses_id_02, courses_id_03, courses_id_04, courses_id_05, ";
		$query .= " courses_id_06, courses_id_07, courses_id_08, courses_id_09, courses_id_10, ";
		$query .= " qualification_01, qualification_02, qualification_03, qualification_04, qualification_05, ";
		$query .= " create_date, status, creator_id ) ";
		$query .= " VALUES (   ";
		$query .= " {$class_id}, {$userx}, '{$evaluation_date}', ";  
		$query .= " '{$current_position}', '{$courses_attended}', ";   
		$query .= " '{$function_id_01}', '{$function_id_02}', '{$function_id_03}', '{$function_id_04}', '{$function_id_05}',  "; 
		$query .= " '{$experience_yr_01}', '{$experience_yr_02}', '{$experience_yr_03}', '{$experience_yr_04}', '{$experience_yr_05}',  ";  
		$query .= " '{$experience_01}', '{$experience_02}', '{$experience_03}', '{$experience_04}', '{$experience_05}',  ";  
		$query .= " '{$role_id}',  "; 
		$query .= " '{$courses_id_01}', '{$courses_id_02}', '{$courses_id_03}', '{$courses_id_04}', '{$courses_id_05}',  ";   
		$query .= " '{$courses_id_06}', '{$courses_id_07}', '{$courses_id_08}', '{$courses_id_09}', '{$courses_id_10}',  ";   
		$query .= " '{$qualification_01}', '{$qualification_02}', '{$qualification_03}', '{$qualification_04}', '{$qualification_05}',  "; 
		$query .= " '{$create_date}', '{$status}', {$userx} ) " ;		
		$result = mysqli_query( $connection, $query);
		if (mysqli_affected_rows($connection) == 1) {	
			// Success	
			echo "<div class=\"alert alert-success\">";
				echo "Record Created";
			echo "</div>";
			$set = 8; // set action back to listing
			$cat3 = $userx;
			} else {
			echo "<div class=\"alert alert-danger\">";
				echo "Record creation failed";
			echo "</div>";
			$set = 3; // set action back to form
		}
		
	} // end of set = 2
	


	if ($set == 4 && $key <> 0) { // update record													
		if (isset($_POST['submit'])) {
			$class_id = trim(mysql_prep($_POST['class_id']));
			//$user_id = trim(mysql_prep($_POST['user_id']));
			$evaluation_date = trim(mysql_prep($_POST['evaluation_date']));
			$current_position = trim(mysql_prep($_POST['current_position']));
			$courses_attended = trim(mysql_prep($_POST['courses_attended']));
			$function_id_01 = trim(mysql_prep($_POST['function_id_01']));
			$function_id_02 = trim(mysql_prep($_POST['function_id_02']));
			$function_id_03 = trim(mysql_prep($_POST['function_id_03']));
			$function_id_04 = trim(mysql_prep($_POST['function_id_04']));
			$function_id_05 = trim(mysql_prep($_POST['function_id_05']));
			$experience_yr_01 = trim(mysql_prep($_POST['experience_yr_01']));
			$experience_yr_02 = trim(mysql_prep($_POST['experience_yr_02']));
			$experience_yr_03 = trim(mysql_prep($_POST['experience_yr_03']));
			$experience_yr_04 = trim(mysql_prep($_POST['experience_yr_04']));
			$experience_yr_05 = trim(mysql_prep($_POST['experience_yr_05']));
			$experience_01 = trim(mysql_prep($_POST['experience_01']));
			$experience_02 = trim(mysql_prep($_POST['experience_02']));
			$experience_03 = trim(mysql_prep($_POST['experience_03']));
			$experience_04 = trim(mysql_prep($_POST['experience_04']));
			$experience_05 = trim(mysql_prep($_POST['experience_05']));
			$role_id = trim(mysql_prep($_POST['role_id']));
			$courses_id_01 = trim(mysql_prep($_POST['courses_id_01']));
			$courses_id_02 = trim(mysql_prep($_POST['courses_id_02']));
			$courses_id_03 = trim(mysql_prep($_POST['courses_id_03']));
			$courses_id_04 = trim(mysql_prep($_POST['courses_id_04']));
			$courses_id_05 = trim(mysql_prep($_POST['courses_id_05']));
			$courses_id_06 = trim(mysql_prep($_POST['courses_id_06']));
			$courses_id_07 = trim(mysql_prep($_POST['courses_id_07']));
			$courses_id_08 = trim(mysql_prep($_POST['courses_id_08']));
			$courses_id_09 = trim(mysql_prep($_POST['courses_id_09']));
			$courses_id_10 = trim(mysql_prep($_POST['courses_id_10']));
			$qualification_01 = trim(mysql_prep($_POST['qualification_01']));
			$qualification_02 = trim(mysql_prep($_POST['qualification_02']));
			$qualification_03 = trim(mysql_prep($_POST['qualification_03']));
			$qualification_04 = trim(mysql_prep($_POST['qualification_04']));
			$qualification_05 = trim(mysql_prep($_POST['qualification_05']));
			//$status = trim(mysql_prep($_POST['status']));
			$status = 1;
			$create_date = $today;	
			$creator_id = $userx;
		}
		
			
			if ($test_aca == 1) {
				$msg = "set 4 Post</br>";
			$msg .= "class_id: ".$class_id."</br>";
			$msg .= "user_id: ".$userx."</br>";
			$msg .= "current_position: ".$current_position."</br>";
			$msg .= "courses_attended: ".$courses_attended."</br>";
			$msg .= "evaluation_date: ".$evaluation_date."</br>";
			$msg .= "function_id_01: ".$function_id_01."</br>";
			$msg .= "function_id_02: ".$function_id_02."</br>";
			$msg .= "function_id_03: ".$function_id_03."</br>";
			$msg .= "function_id_04: ".$function_id_04."</br>";
			$msg .= "function_id_05: ".$function_id_05."</br>";
			$msg .= "experience_yr_01: ".$experience_yr_01."</br>";
			$msg .= "experience_yr_02: ".$experience_yr_02."</br>";
			$msg .= "experience_yr_03: ".$experience_yr_03."</br>";
			$msg .= "experience_yr_04: ".$experience_yr_04."</br>";
			$msg .= "experience_yr_05: ".$experience_yr_05."</br>";
			$msg .= "experience_01: ".$experience_01."</br>";
			$msg .= "experience_02: ".$experience_02."</br>";
			$msg .= "experience_03: ".$experience_03."</br>";
			$msg .= "experience_04: ".$experience_04."</br>";
			$msg .= "experience_05: ".$experience_05."</br>";
			$msg .= "role_id: ".$role_id."</br>";
			$msg .= "courses_id_01: ".$courses_id_01."</br>";
			$msg .= "courses_id_02: ".$courses_id_02."</br>";
			$msg .= "courses_id_03: ".$courses_id_03."</br>";
			$msg .= "courses_id_04: ".$courses_id_04."</br>";
			$msg .= "courses_id_05: ".$courses_id_05."</br>";
			$msg .= "courses_id_06: ".$courses_id_06."</br>";
			$msg .= "courses_id_07: ".$courses_id_07."</br>";
			$msg .= "courses_id_08: ".$courses_id_08."</br>";
			$msg .= "courses_id_09: ".$courses_id_09."</br>";
			$msg .= "courses_id_10: ".$courses_id_10."</br>";
			$msg .= "qualification_01: ".$qualification_01."</br>";
			$msg .= "qualification_02: ".$qualification_02."</br>";
			$msg .= "qualification_03: ".$qualification_03."</br>";
			$msg .= "qualification_04: ".$qualification_04."</br>";
			$msg .= "qualification_05: ".$qualification_05."</br>";
			$msg .= "create_date: ".$create_date."</br>";
			$msg .= "creator_id: ".$userx."</br>";
				$msg .= "status: ".$status."</br>";
				echo $msg;
			}
			
			// Update
			$query = "UPDATE aca_evaluation SET ";
			$query .= " class_id = {$class_id}, ";
			//$query .= " user_id = {$user_id}, ";
			$query .= " evaluation_date = '{$evaluation_date}', ";
			$query .= " current_position = '{$current_position}', ";
			$query .= " courses_attended = '{$courses_attended}', ";
			$query .= " function_id_01 = '{$function_id_01}', ";
			$query .= " function_id_02 = '{$function_id_02}', ";
			$query .= " function_id_03 = '{$function_id_03}', ";
			$query .= " function_id_04 = '{$function_id_04}', ";
			$query .= " function_id_05 = '{$function_id_05}', ";
			$query .= " experience_yr_01 = '{$experience_yr_01}', ";
			$query .= " experience_yr_02 = '{$experience_yr_02}', ";
			$query .= " experience_yr_03 = '{$experience_yr_03}', ";
			$query .= " experience_yr_04 = '{$experience_yr_04}', ";
			$query .= " experience_yr_05 = '{$experience_yr_05}', ";
			$query .= " experience_01 = '{$experience_01}', ";
			$query .= " experience_02 = '{$experience_02}', ";
			$query .= " experience_03 = '{$experience_03}', ";
			$query .= " experience_04 = '{$experience_04}', ";
			$query .= " experience_05 = '{$experience_05}', ";
			$query .= " role_id = '{$role_id}', ";
			$query .= " courses_id_01 = '{$courses_id_01}', ";
			$query .= " courses_id_02 = '{$courses_id_02}', ";
			$query .= " courses_id_03 = '{$courses_id_03}', ";
			$query .= " courses_id_04 = '{$courses_id_04}', ";
			$query .= " courses_id_05 = '{$courses_id_05}', ";
			$query .= " courses_id_06 = '{$courses_id_06}', ";
			$query .= " courses_id_07 = '{$courses_id_07}', ";
			$query .= " courses_id_08 = '{$courses_id_08}', ";
			$query .= " courses_id_09 = '{$courses_id_09}', ";
			$query .= " courses_id_10 = '{$courses_id_10}', ";
			$query .= " qualification_01 = '{$qualification_01}', ";
			$query .= " qualification_02 = '{$qualification_02}', ";
			$query .= " qualification_03 = '{$qualification_03}', ";
			$query .= " qualification_04 = '{$qualification_04}', ";
			$query .= " qualification_05 = '{$qualification_05}', ";
			$query .= " status = '{$status}', ";
			$query .= " creator_id = '{$userx}' ";
			$query .= " WHERE eval_id = {$key} ";
			$query .= " LIMIT 1 ";
		
			$result = mysqli_query( $connection, $query);
			if (mysqli_affected_rows($connection) == 1) {	
				// Success	
				echo "<div class=\"alert alert-success\">";
					echo "The record was updated successfully.";
				echo "</div>";
				$set = 8; // set action back to listing
				$cat3 = $userx;
				} else {
				echo "<div class=\"alert alert-danger\">";
					echo "The record was not updated. Please try again. Make sure that all fields are completed.";
				echo "</div>";
				$set = 3; // set action back to form
			}
				
	} // end of set = 4 update


	if ($set == 3) { // edit																
		echo "<div class=\"panel panel-primary\"> ";
		  echo "<div class=\"panel-heading\">Evaluation</div> ";
		  echo "<div class=\"panel-body\">";
		  
		  // test for user_eval record
		  // if exist, edit
		  // if not, new 
		  
		  
				
		$result_set = get_aca_evaluation_rec($userx);	
		while ($row = mysqli_fetch_array($result_set)) {	
			$key = $row["eval_id"];
			$class_id = $row["class_id"];
			$user_id = $row["user_id"];	
			$evaluation_date = $row["evaluation_date"];	
			$current_position = $row["current_position"];
			$courses_attended = $row["courses_attended"];
			$function_id_01 = $row["function_id_01"];
			$function_id_02 = $row["function_id_02"];				
			$function_id_03 = $row["function_id_03"];				
			$function_id_04 = $row["function_id_04"];				
			$function_id_05 = $row["function_id_05"];			
			$experience_yr_01 = $row["experience_yr_01"];				
			$experience_yr_02 = $row["experience_yr_02"];				
			$experience_yr_03 = $row["experience_yr_03"];				
			$experience_yr_04 = $row["experience_yr_04"];				
			$experience_yr_05 = $row["experience_yr_05"];				
			$experience_01 = $row["experience_01"];				
			$experience_02 = $row["experience_02"];				
			$experience_03 = $row["experience_03"];				
			$experience_04 = $row["experience_04"];				
			$experience_05 = $row["experience_05"];	
			$role_id = $row["role_id"];	
			$courses_id_01 = $row["courses_id_01"];
			$courses_id_02 = $row["courses_id_02"];				
			$courses_id_03 = $row["courses_id_03"];				
			$courses_id_04 = $row["courses_id_04"];				
			$courses_id_05 = $row["courses_id_05"];				
			$courses_id_06 = $row["courses_id_06"];				
			$courses_id_07 = $row["courses_id_07"];				
			$courses_id_08 = $row["courses_id_08"];				
			$courses_id_09 = $row["courses_id_09"];				
			$courses_id_10 = $row["courses_id_10"];	
			$qualification_01 = $row["qualification_01"];
			$qualification_02 = $row["qualification_02"];				
			$qualification_03 = $row["qualification_03"];				
			$qualification_04 = $row["qualification_04"];				
			$qualification_05 = $row["qualification_05"];															
			$status = $row["status"];
			$class = "";
			$result_setI = get_aca_class_rec($class_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$class = $row["name"];		
			}
			$student = "";
			$result_setS = get_aca_user_rec($user_id);
			while ($row = mysqli_fetch_array($result_setS)) {		
				$student = $row["name"];		
			}	
			$d_status = $status;
			$result_set74 = get_dim("status", $status);
			while ($row = mysqli_fetch_array($result_set74)) {
				$dim_key = $row["dim_key"]; 
				$d_status = $row["description"];
			}
			$role = "";
			$result_set01x = get_dim_rec_z("aca_role", "role_id",  $role_id);
			while ($row = mysqli_fetch_array($result_set01x)) {
				$role = $row["name"] . " " . $row["description"];
			}
			$function_01 = "";
			$result_set001 = get_dim_rec_z("aca_function", "function_id", $function_id_01);
			while ($row = mysqli_fetch_array($result_set001)) {
				$function_01 = $row["description"];	
			}	
			$function_02 = "";
			$result_set002 = get_dim_rec_z("aca_function", "function_id", $function_id_02);
			while ($row = mysqli_fetch_array($result_set002)) {
				$function_02 = $row["description"];	
			}		
			$function_03 = "";
			$result_set003 = get_dim_rec_z("aca_function", "function_id", $function_id_03);
			while ($row = mysqli_fetch_array($result_set003)) {
				$function_03 = $row["description"];	
			}		
			$function_04 = "";
			$result_set004 = get_dim_rec_z("aca_function", "function_id", $function_id_04);
			while ($row = mysqli_fetch_array($result_set004)) {
				$function_04 = $row["description"];	
			}		
			$function_05 = "";
			$result_set005 = get_dim_rec_z("aca_function", "function_id", $function_id_05);
			while ($row = mysqli_fetch_array($result_set005)) {
				$function_05 = $row["description"];	
			}	
			$course_01 = "";
			$result_set001 = get_dim_rec_z("aca_course", "course_id", $courses_id_01);
			while ($row = mysqli_fetch_array($result_set001)) {
				$course_01 = $row["name"];	
			}	
			$course_02 = "";
			$result_set002 = get_dim_rec_z("aca_course", "course_id", $courses_id_02);
			while ($row = mysqli_fetch_array($result_set002)) {
				$course_02 = $row["name"];	
			}	
			$course_03 = "";
			$result_set003 = get_dim_rec_z("aca_course", "course_id", $courses_id_03);
			while ($row = mysqli_fetch_array($result_set003)) {
				$course_03 = $row["name"];	
			}		
			$course_04 = "";
			$result_set004 = get_dim_rec_z("aca_course", "course_id", $courses_id_04);
			while ($row = mysqli_fetch_array($result_set004)) {
				$course_04 = $row["name"];	
			}		
			$course_05 = "";
			$result_set005 = get_dim_rec_z("aca_course", "course_id", $courses_id_05);
			while ($row = mysqli_fetch_array($result_set005)) {
				$course_05 = $row["name"];	
			}					
			$course_06 = "";
			$result_set006 = get_dim_rec_z("aca_course", "course_id", $courses_id_06);
			while ($row = mysqli_fetch_array($result_set006)) {
				$course_06 = $row["name"];	
			}	
			$course_07 = "";
			$result_set007 = get_dim_rec_z("aca_course", "course_id", $courses_id_07);
			while ($row = mysqli_fetch_array($result_set007)) {
				$course_07 = $row["name"];	
			}		
			$course_08 = "";
			$result_set008 = get_dim_rec_z("aca_course", "course_id", $courses_id_08);
			while ($row = mysqli_fetch_array($result_set008)) {
				$course_08 = $row["name"];	
			}		
			$course_09 = "";
			$result_set009 = get_dim_rec_z("aca_course", "course_id", $courses_id_09);
			while ($row = mysqli_fetch_array($result_set009)) {
				$course_09 = $row["name"];	
			}		
			$course_10 = "";
			$result_set010 = get_dim_rec_z("aca_course", "course_id", $courses_id_10);
			while ($row = mysqli_fetch_array($result_set010)) {
				$course_10 = $row["name"];	
			}	
		}
		if (mysqli_num_rows($result_set) == 1) {
			$set = 4; // record exists and must be edited and updated 
		} else {
			$set = 2; // display edit form and insert record	
			$key = 0;
			$class_id = 0; // $cat
			$user_id = 0; // $cat	
			$evaluation_date = $today;	
			$current_position = "";
			$courses_attended = "";
			$function_id_01 = 0;
			$function_id_02 = 0;				
			$function_id_03 = 0;				
			$function_id_04 = 0;			
			$function_id_05 = 0;				
			$experience_yr_01 = 0;				
			$experience_yr_02 = 0;				
			$experience_yr_03 = 0;			
			$experience_yr_04 = 0;				
			$experience_yr_05 = 0;				
			$experience_01 = 0;				
			$experience_02 = 0;				
			$experience_03 = 0;			
			$experience_04 = 0;				
			$experience_05 = 0;	
			$role_id = 0;	
			$courses_id_01 = 0;
			$courses_id_02 = 0;				
			$courses_id_03 = 0;				
			$courses_id_04 = 0;				
			$courses_id_05 = 0;			
			$courses_id_06 = 0;				
			$courses_id_07 = 0;				
			$courses_id_08 = 0;			
			$courses_id_09 = 0;				
			$courses_id_10 = 0;
			$qualification_01 = "";
			$qualification_02 = "";			
			$qualification_03 = "";				
			$qualification_04 = "";				
			$qualification_05 = "";														
			$status = 1;
			$function_01 = "";
			$function_02 = "";				
			$function_03 = "";				
			$function_04 = "";			
			$function_05 = "";				
			$experience_yr_01 = "";				
			$experience_yr_02 = "";				
			$experience_yr_03 = "";			
			$experience_yr_04 = "";				
			$experience_yr_05 = "";				
			$experience_01 = "";				
			$experience_02 = "";				
			$experience_03 = "";			
			$experience_04 = "";				
			$experience_05 = "";	
			$role = "";	
			$course_01 = "";
			$course_02 = "";				
			$course_03 = "";				
			$course_04 = "";				
			$course_05 = "";			
			$course_06 = "";				
			$course_07 = "";				
			$course_08 = "";			
			$course_09 = "";				
			$course_10 = "";	
			$class = "";
			$student = $username;	
		}
		
		echo "<form name=\"aca_evaluation\" method=\"post\" action=\"?id=".$id."&set=".$set."&opt=" . $opt . "&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat7=".$cat7."&cat8=".$cat8."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat5=".$cat5." \"enctype=\"multipart/form-data\">";

		echo "<div class=\"form-group\">";
		  echo "<label for=\"class_id\">Name:</label>";
		  echo "<input disabled=\"disabled\" type=\"text\" name=\"student\" class=\"form-control\" id=\"student_id\" value=\"".$student."\">";
		echo "</div>";
		
		echo "<div class=\"form-group\">";
		  echo "<label for=\"class_id\">Ref / Class:</label>";
			echo "<select name=\"class_id\" class=\"form-control reqd\" id=\"class_id\">";					
						  echo "<option value=\"".$class_id."\">".$class."</option>";
								  $result_setR0x = get_aca_user_class_list_assigned(1, $userx);
								  while ($row = mysqli_fetch_array($result_setR0x)) {
									  $dim_key = $row["class_id"]; 
									  $display_name = $row["name"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }
								  echo "</select>";
		echo "</div>";
		
		echo "<div class=\"form-group\">";
		  echo "<label for=\"courses_attended\">Current Position: </label>";
		  echo "<input type=\"text\" name=\"current_position\" class=\"form-control reqd\" id=\"current_position\" placeholder=\"\" value=\"".$current_position."\">";
		echo "</div>";
		
		echo "<div class=\"form-group\">";
		  echo "<label for=\"evaluation_date\">Evaluation Date:</label>";
		  echo "<input type=\"date\" name=\"evaluation_date\" class=\"form-control reqd\" id=\"evaluation_date\" placeholder=\"Evaluation Date\" value=\"".$evaluation_date."\">";
		echo "</div>";

		echo "<div class=\"form-group\">";
		  echo "<label for=\"role_id\">Evaluated Role / Function: </label>";
			echo "<select name=\"role_id\" class=\"form-control reqd\" id=\"role_id\">";					
						  echo "<option value=\"".$role_id."\">".$role."</option>";
								  $result_setR01 = get_dim_z("aca_role", "description", 1, 1);
								  while ($row = mysqli_fetch_array($result_setR01)) {
									  $dim_key = $row["role_id"]; 
									  $display_name = $row["description"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"0\">Other</option>";
								  echo "</select>";
		echo "</div>";

		// function
		echo "<div class=\"form-group\">";
		 
		 	echo "<table width=\"580\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";	
		echo "<tr>";
			echo "<td colspan=\"5\"><strong>Experience</strong></td>";
		echo "</tr>";	
		echo "<tr>";
			echo "<td></td>";
			echo "<td>Function</td>";
			echo "<td>Years</td>";
			echo "<td>Months</td>";
		echo "</tr>";	 
		echo "<tr>";
			echo "<td>Primary</td>";
			echo "<td><select name=\"function_id_01\" class=\"form-control\" id=\"function_id_01\">";					
						  echo "<option value=\"".$function_id_01."\">".$function_01."</option>";
								  $result_setF01 = get_dim_z("aca_function", "description", 1, 1);
								  while ($row = mysqli_fetch_array($result_setF01)) {
									  $dim_key = $row["function_id"]; 
									  $display_name = $row["description"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
			echo "<td><select name=\"experience_yr_01\" class=\"form-control\" id=\"experience_yr_01\">";						
						  echo "<option value=\"".$experience_yr_01."\">".$experience_yr_01."</option>";
						  $x = 0;
						  while ($x <= 30) {				
						  	echo "<option value=\"".$x."\">".$x."</option>";
						  	$x = $x + 1;
						  } 
			echo "</select></td>";
			echo "<td><select name=\"experience_01\" class=\"form-control\" id=\"experience_01\">";						
						  echo "<option value=\"".$experience_01."\">".$experience_01."</option>";
						  $x = 0;
						  while ($x <= 12) {				
						  	echo "<option value=\"".$x."\">".$x."</option>";
						  	$x = $x + 1;
						  } 
			echo "</select></td>";
		echo "</tr>";				
		echo "<tr>";
			echo "<td>Secondary</td>";
			echo "<td><select name=\"function_id_02\" class=\"form-control\" id=\"function_id_02\">";					
						  echo "<option value=\"".$function_id_02."\">".$function_02."</option>";
								  $result_setF02 = get_dim_z("aca_function", "description", 1, 1);
								  while ($row = mysqli_fetch_array($result_setF02)) {
									  $dim_key = $row["function_id"]; 
									  $display_name = $row["description"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
			echo "<td><select name=\"experience_yr_02\" class=\"form-control\" id=\"experience_yr_02\">";						
						  echo "<option value=\"".$experience_yr_02."\">".$experience_yr_02."</option>";
						  $x = 0;
						  while ($x <= 30) {				
						  	echo "<option value=\"".$x."\">".$x."</option>";
						  	$x = $x + 1;
						  } 
			echo "</select></td>";
			echo "<td><select name=\"experience_02\" class=\"form-control\" id=\"experience_02\">";						
						  echo "<option value=\"".$experience_02."\">".$experience_02."</option>";
						  $x = 0;
						  while ($x <= 12) {				
						  	echo "<option value=\"".$x."\">".$x."</option>";
						  	$x = $x + 1;
						  } 
			echo "</select></td>";
		echo "</tr>";				
		echo "<tr>";
			echo "<td>Third</td>";
			echo "<td><select name=\"function_id_03\" class=\"form-control\" id=\"function_id_03\">";					
						  echo "<option value=\"".$function_id_03."\">".$function_03."</option>";
								  $result_setF03 = get_dim_z("aca_function", "description", 1, 1);
								  while ($row = mysqli_fetch_array($result_setF03)) {
									  $dim_key = $row["function_id"]; 
									  $display_name = $row["description"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
			echo "<td><select name=\"experience_yr_03\" class=\"form-control\" id=\"experience_yr_03\">";						
						  echo "<option value=\"".$experience_yr_03."\">".$experience_yr_03."</option>";
						  $x = 0;
						  while ($x <= 30) {				
						  	echo "<option value=\"".$x."\">".$x."</option>";
						  	$x = $x + 1;
						  } 
			echo "</select></td>";
			echo "<td><select name=\"experience_03\" class=\"form-control\" id=\"experience_03\">";						
						  echo "<option value=\"".$experience_03."\">".$experience_03."</option>";
						  $x = 0;
						  while ($x <= 12) {				
						  	echo "<option value=\"".$x."\">".$x."</option>";
						  	$x = $x + 1;
						  } 
			echo "</select></td>";
		echo "</tr>";				
		echo "<tr>";
			echo "<td>Fourth</td>";
			echo "<td><select name=\"function_id_04\" class=\"form-control\" id=\"function_id_04\">";					
						  echo "<option value=\"".$function_id_04."\">".$function_04."</option>";
								  $result_setF04 = get_dim_z("aca_function", "description", 1, 1);
								  while ($row = mysqli_fetch_array($result_setF04)) {
									  $dim_key = $row["function_id"]; 
									  $display_name = $row["description"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
			echo "<td><select name=\"experience_yr_04\" class=\"form-control\" id=\"experience_yr_04\">";						
						  echo "<option value=\"".$experience_yr_04."\">".$experience_yr_04."</option>";
						  $x = 0;
						  while ($x <= 30) {				
						  	echo "<option value=\"".$x."\">".$x."</option>";
						  	$x = $x + 1;
						  } 
			echo "</select></td>";
			echo "<td><select name=\"experience_04\" class=\"form-control\" id=\"experience_04\">";						
						  echo "<option value=\"".$experience_04."\">".$experience_04."</option>";
						  $x = 0;
						  while ($x <= 12) {				
						  	echo "<option value=\"".$x."\">".$x."</option>";
						  	$x = $x + 1;
						  } 
			echo "</select></td>";
		echo "</tr>";				
		echo "<tr>";
			echo "<td>Fifth</td>";
			echo "<td><select name=\"function_id_05\" class=\"form-control\" id=\"function_id_05\">";					
						  echo "<option value=\"".$function_id_05."\">".$function_05."</option>";
								  $result_setF05 = get_dim_z("aca_function", "description", 1, 1);
								  while ($row = mysqli_fetch_array($result_setF05)) {
									  $dim_key = $row["function_id"]; 
									  $display_name = $row["description"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
			echo "<td><select name=\"experience_yr_05\" class=\"form-control\" id=\"experience_yr_05\">";						
						  echo "<option value=\"".$experience_yr_05."\">".$experience_yr_05."</option>";
						  $x = 0;
						  while ($x <= 30) {				
						  	echo "<option value=\"".$x."\">".$x."</option>";
						  	$x = $x + 1;
						  } 
			echo "</select></td>";
			echo "<td><select name=\"experience_05\" class=\"form-control\" id=\"experience_05\">";						
						  echo "<option value=\"".$experience_05."\">".$experience_05."</option>";
						  $x = 0;
						  while ($x <= 12) {				
						  	echo "<option value=\"".$x."\">".$x."</option>";
						  	$x = $x + 1;
						  } 
			echo "</select></td>";
		echo "</tr>";	
 							
		echo "<tr>";
			echo "<td colspan=\"5\">&nbsp;</td>";
		echo "</tr>";
			  	
	echo "</table>";
		 
	echo "<table width=\"580\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";	

 							
		echo "<tr>";
			echo "<td colspan=\"5\"><strong>Courses Attended</strong></td>";
		echo "</tr>";	
 							
		echo "<tr>";
			echo "<td><select name=\"courses_id_01\" class=\"form-control\" id=\"courses_id_01\">";					
						  echo "<option value=\"".$courses_id_01."\">".$course_01."</option>";
								  $result_setC01 = get_aca_course_list(0, 999, 1);
								  while ($row = mysqli_fetch_array($result_setC01)) {
									  $dim_key = $row["course_id"]; 
									  $display_name = $row["name"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
			echo "<td><select name=\"courses_id_02\" class=\"form-control\" id=\"courses_id_02\">";					
						  echo "<option value=\"".$courses_id_02."\">".$course_02."</option>";
								  $result_setC02 = get_aca_course_list(0, 999, 1);
								  while ($row = mysqli_fetch_array($result_setC02)) {
									  $dim_key = $row["course_id"]; 
									  $display_name = $row["name"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
			echo "<td><select name=\"courses_id_03\" class=\"form-control\" id=\"courses_id_03\">";					
						  echo "<option value=\"".$courses_id_03."\">".$course_03."</option>";
								  $result_setC03 = get_aca_course_list(0, 999, 1);
								  while ($row = mysqli_fetch_array($result_setC03)) {
									  $dim_key = $row["course_id"]; 
									  $display_name = $row["name"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
			echo "<td><select name=\"courses_id_04\" class=\"form-control\" id=\"courses_id_04\">";					
						  echo "<option value=\"".$courses_id_04."\">".$course_04."</option>";
								  $result_setC04 = get_aca_course_list(0, 999, 1);
								  while ($row = mysqli_fetch_array($result_setC04)) {
									  $dim_key = $row["course_id"]; 
									  $display_name = $row["name"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
			echo "<td><select name=\"courses_id_05\" class=\"form-control\" id=\"courses_id_05\">";					
						  echo "<option value=\"".$courses_id_05."\">".$course_05."</option>";
								  $result_setC05 = get_aca_course_list(0, 999, 1);
								  while ($row = mysqli_fetch_array($result_setC05)) {
									  $dim_key = $row["course_id"]; 
									  $display_name = $row["name"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
		echo "</tr>";		
 							
		echo "<tr>";
			echo "<td><select name=\"courses_id_06\" class=\"form-control\" id=\"courses_id_06\">";					
						  echo "<option value=\"".$courses_id_06."\">".$course_06."</option>";
								  $result_setC06 = get_aca_course_list(0, 999, 1);
								  while ($row = mysqli_fetch_array($result_setC06)) {
									  $dim_key = $row["course_id"]; 
									  $display_name = $row["name"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
			echo "<td><select name=\"courses_id_07\" class=\"form-control\" oid=\"courses_id_07\">";					
						  echo "<option value=\"".$courses_id_07."\">".$course_07."</option>";
								  $result_setC07 = get_aca_course_list(0, 999, 1);
								  while ($row = mysqli_fetch_array($result_setC07)) {
									  $dim_key = $row["course_id"]; 
									  $display_name = $row["name"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
			echo "<td><select name=\"courses_id_08\" class=\"form-control\" id=\"courses_id_08\">";					
						  echo "<option value=\"".$courses_id_08."\">".$course_08."</option>";
								  $result_setC08 = get_aca_course_list(0, 999, 1);
								  while ($row = mysqli_fetch_array($result_setC08)) {
									  $dim_key = $row["course_id"]; 
									  $display_name = $row["name"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
			echo "<td><select name=\"courses_id_09\" class=\"form-control\" id=\"courses_id_09\">";					
						  echo "<option value=\"".$courses_id_09."\">".$course_09."</option>";
								  $result_setC09 = get_aca_course_list(0, 999, 1);
								  while ($row = mysqli_fetch_array($result_setC09)) {
									  $dim_key = $row["course_id"]; 
									  $display_name = $row["name"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
			echo "<td><select name=\"courses_id_10\" class=\"form-control\" id=\"courses_id_10\">";					
						  echo "<option value=\"".$courses_id_10."\">".$course_10."</option>";
								  $result_setC10 = get_aca_course_list(0, 999, 1);
								  while ($row = mysqli_fetch_array($result_setC10)) {
									  $dim_key = $row["course_id"]; 
									  $display_name = $row["name"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }				
						  		  echo "<option value=\"\"></option>";
								  echo "</select></td>";
		echo "</tr>";	
		echo "<tr>";
			echo "<td colspan=\"5\">&nbsp;</td>";
		echo "</tr>";	
		
					  	
	echo "</table>";
			
		echo "<div class=\"form-group\">";
		  echo "<label for=\"courses_attended\">Other Relevant Courses Attended:</label>";
		  echo "<input type=\"text\" name=\"courses_attended\" class=\"form-control\" id=\"courses_attended\" placeholder=\"Courses Attended\" value=\"".$courses_attended."\">";
		echo "</div>";
		
	// Qualificatins		 
	echo "<table width=\"580\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";	
	
		echo "<tr>";
			echo "<td><strong>Qualifications</strong></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td><input type=\"text\" name=\"qualification_01\" class=\"form-control\" id=\"qualification_01\" placeholder=\"Qualification\" value=\"".$qualification_01."\"></td>";
		echo "</tr>";	
		echo "<tr>";
			echo "<td><input type=\"text\" name=\"qualification_02\" class=\"form-control\" id=\"qualification_02\" placeholder=\"Qualification\" value=\"".$qualification_02."\"></td>";
		echo "</tr>";	
		echo "<tr>";
			echo "<td><input type=\"text\" name=\"qualification_03\" class=\"form-control\" id=\"qualification_03\" placeholder=\"Qualification\" value=\"".$qualification_03."\"></td>";
		echo "</tr>";	
		echo "<tr>";
			echo "<td><input type=\"text\" name=\"qualification_04\" class=\"form-control\" id=\"qualification_04\" placeholder=\"Qualification\" value=\"".$qualification_04."\"></td>";
		echo "</tr>";	
		echo "<tr>";
			echo "<td><input type=\"text\" name=\"qualification_05\" class=\"form-control\" id=\"qualification_05\" placeholder=\"Qualification\" value=\"".$qualification_05."\"></td>";
		echo "</tr>";	
		echo "<tr>";
			echo "<td>&nbsp;</td>";
		echo "</tr>";	
					  	
	echo "</table>";	
	/*
		echo "<div class=\"form-group\">";
		  echo "<label for=\"status\">Status</label>";
			echo "<select name=\"status\" class=\"form-control reqd\" onchange=\"showTool(this.value)\" id=\"status\">";					
						  echo "<option value=\"".$status."\">".$d_status."</option>";
								  $result_set9 = get_dim_list("status");
								  while ($row = mysqli_fetch_array($result_set9)) {
									  $dim_key = $row["dim_key"]; 
									  $display_name = $row["description"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }
								  echo "</select>";
		echo "</div>";
		*/			
							
			// buttons
			$update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Submit</button>";			
			$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";
			
		  echo "<p align=\"center\">" . $update . " " . $cancel . "</p>";
		  
		echo "</form>";			
		
		
		

		  echo "</div> ";
	   echo " </div> ";
		
		
		echo "<p>&nbsp;</p>";	
		
	} // end of edit




	if ($set == 0) {													
	// get posted values from the class / user filter	
		$srch = 0;	
		$srch_class_id = 0;
		$srch_user_id = 0;										
		if (isset($_POST['submit'])) {
			$srch_class_id = trim(mysql_prep($_POST['class_id']));
			$srch_user_id = trim(mysql_prep($_POST['user_id']));
			$srch = 1;
		}
	
		// get instructor number from the signed in user
		if ($aca_accesslevel == 9){ $cat7 = 0; } else {
			$result_set19 = get_aca_instructor_user_rec($userx); 
			while ($row = mysqli_fetch_array($result_set19)) {
				$cat7 = $row["instructor_id"]; 
				$instructor = $row["name"];
			}
			
		}								

		// get record count
		$p_records = 0;
		$result_setA = get_aca_evaluation_count_search(99, $cat7, $srch_class_id, $srch_user_id);
		while ($row = mysqli_fetch_array($result_setA)) {
			$p_records = $row["record_count"]; 
		}
		
		//echo "TEST P Records = ".$p_records."</br>";			
		
		$page_break = $page_break_d;
		$anchor = "x";
		$p_link = "?id=".$id."&opt=".$opt."&cat2=0&cat3=0&cat6=".$cat6."&key=".$key."&cat=".$cat."&cat4=".$cat4."&set=".$set;
		$p = page_nav_last($page, $page_break, $p_records, $p_link, $anchor);
		//$n = "<a href=\"?id=".$id."&set=1&key=".$key."\">New</a>";
		$c = " ";
		//$n =  "<button name=\"NewBut\" type=\"button\" value=\"New\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=1&opt=0&key=".$key."');return document.MM_returnValue\"/>New</button>&nbsp;&nbsp;";
		//echo "<p align=\"right\">" . $n." ".$p . "</p></br>";
		
		
		
		echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Evaluation</h2><div class=\"colored-line-left\"></div>
          <div class=\"clearfix\"></div>
          <div class=\"row content-pad\">
          <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";
		  	
		

		// aplay a filter by class
		echo "<form name=\"aca_evaluation\" method=\"post\" action=\"?id=".$id."&set=0&opt=11&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat7=".$cat7."&cat8=".$cat8."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat5=".$cat5." \"enctype=\"multipart/form-data\">";
		
		echo "<div class=\"form-group\">";
		echo "<label for=\"class_id\">Select a Class</label>";
			echo "<select name=\"class_id\" class=\"form-control\" onchange=\"showUserClassAll(this.value)\" id=\"class_id\">";							
						  echo "<option value=\"\"></option>";	
								$result_set18 = get_aca_class_list_type(0,999,1,0, $cat7, 2);  // sw20160225 filter on class_type_id 
								while ($row = mysqli_fetch_array($result_set18)) {
									$dim_key = $row["class_id"]; 
									$display_name = $row["name"];
									echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								}
			echo "</select>";
		echo "</div>";
		
		echo "<div class=\"form-group\">";
		  echo "<div id=\"txtHint\"></div>";							
		echo "</div>";
					
			// buttons
			$update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Submit</button>";			
			$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";
			
		  //echo "<p align=\"center\">" . $update . " " . $cancel . "</p>";
		  echo $update;
		  
		echo "</form>";			
		echo "</div></div>";
		 
		/*
		echo "TEST P Records = ".$p_records."</br>";
		echo "TEST cat7 = ".$cat7."</br>";
		echo "TEST user = ".$userx."</br>";
		*/
			
		if ($opt == 11) { // display detail only after selecting a class
		
		$opt = 0; // reset opt = 11
			
		echo "<table class=\"table table-striped\">";
		echo "<tbody>";
		// get data
		$npage = $page;
		$result_setB = get_aca_evaluation_list_search($npage, $page_break, 99, $cat7, $srch_class_id, $srch_user_id);
		while ($row = mysqli_fetch_array($result_setB)) {		
			$key = $row["eval_id"];
			$class_id = $row["class_id"];
			$user_id = $row["user_id"];
			$cat7 = $class_id;	
			$cat3 = $user_id;
			$evaluation_date = $row["evaluation_date"];	
			$current_position = $row["current_position"];
			$courses_attended = $row["courses_attended"];
			$function_id_01 = $row["function_id_01"];
			$function_id_02 = $row["function_id_02"];				
			$function_id_03 = $row["function_id_03"];				
			$function_id_04 = $row["function_id_04"];				
			$function_id_05 = $row["function_id_05"];			
			$experience_yr_01 = $row["experience_yr_01"];				
			$experience_yr_02 = $row["experience_yr_02"];				
			$experience_yr_03 = $row["experience_yr_03"];				
			$experience_yr_04 = $row["experience_yr_04"];				
			$experience_yr_05 = $row["experience_yr_05"];					
			$experience_01 = $row["experience_01"];				
			$experience_02 = $row["experience_02"];				
			$experience_03 = $row["experience_03"];				
			$experience_04 = $row["experience_04"];				
			$experience_05 = $row["experience_05"];	
			$role_id = $row["role_id"];	
			$courses_id_01 = $row["courses_id_01"];
			$courses_id_02 = $row["courses_id_02"];				
			$courses_id_03 = $row["courses_id_03"];				
			$courses_id_04 = $row["courses_id_04"];				
			$courses_id_05 = $row["courses_id_05"];				
			$courses_id_06 = $row["courses_id_06"];				
			$courses_id_07 = $row["courses_id_07"];				
			$courses_id_08 = $row["courses_id_08"];				
			$courses_id_09 = $row["courses_id_09"];				
			$courses_id_10 = $row["courses_id_10"];	
			$qualification_01 = $row["qualification_01"];
			$qualification_02 = $row["qualification_02"];				
			$qualification_03 = $row["qualification_03"];				
			$qualification_04 = $row["qualification_04"];				
			$qualification_05 = $row["qualification_05"];															
			$status = $row["status"];
					
			$class = "";
			$result_setI = get_aca_class_rec($class_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$class = $row["name"];		
			}	
			$student = "";
			$result_setS = get_aca_user_rec($user_id);
			while ($row = mysqli_fetch_array($result_setS)) {		
				$student = $row["name"];		
				$sname = $row["username"];			
			}	
			$d_status = $status;
			$result_set74 = get_dim("status", $status);
			while ($row = mysqli_fetch_array($result_set74)) {
				$dim_key = $row["dim_key"]; 
				$d_status = $row["description"];
			}
			$role = "";
			$result_set01x = get_dim_rec_z("aca_role", "role_id",  $role_id);
			while ($row = mysqli_fetch_array($result_set01x)) {
				$role = $row["name"] . " " . $row["description"];
			}
			$function_01 = "";
			$result_set001 = get_dim_rec_z("aca_function", "function_id", $function_id_01);
			while ($row = mysqli_fetch_array($result_set001)) {
				$function_01 = $row["description"];	
			}	
			$function_02 = "";
			$result_set002 = get_dim_rec_z("aca_function", "function_id", $function_id_02);
			while ($row = mysqli_fetch_array($result_set002)) {
				$function_02 = $row["description"];	
			}		
			$function_03 = "";
			$result_set003 = get_dim_rec_z("aca_function", "function_id", $function_id_03);
			while ($row = mysqli_fetch_array($result_set003)) {
				$function_03 = $row["description"];	
			}		
			$function_04 = "";
			$result_set004 = get_dim_rec_z("aca_function", "function_id", $function_id_04);
			while ($row = mysqli_fetch_array($result_set004)) {
				$function_04 = $row["description"];	
			}		
			$function_05 = "";
			$result_set005 = get_dim_rec_z("aca_function", "function_id", $function_id_05);
			while ($row = mysqli_fetch_array($result_set005)) {
				$function_05 = $row["description"];	
			}	
			$course_01 = "";
			$result_set001 = get_dim_rec_z("aca_course", "course_id", $courses_id_01);
			while ($row = mysqli_fetch_array($result_set001)) {
				$course_01 = $row["name"];	
			}	
			$course_02 = "";
			$result_set002 = get_dim_rec_z("aca_course", "course_id", $courses_id_02);
			while ($row = mysqli_fetch_array($result_set002)) {
				$course_02 = $row["name"];	
			}	
			$course_03 = "";
			$result_set003 = get_dim_rec_z("aca_course", "course_id", $courses_id_03);
			while ($row = mysqli_fetch_array($result_set003)) {
				$course_03 = $row["name"];	
			}		
			$course_04 = "";
			$result_set004 = get_dim_rec_z("aca_course", "course_id", $courses_id_04);
			while ($row = mysqli_fetch_array($result_set004)) {
				$course_04 = $row["name"];	
			}		
			$course_05 = "";
			$result_set005 = get_dim_rec_z("aca_course", "course_id", $courses_id_05);
			while ($row = mysqli_fetch_array($result_set005)) {
				$course_05 = $row["name"];	
			}					
			$course_06 = "";
			$result_set006 = get_dim_rec_z("aca_course", "course_id", $courses_id_06);
			while ($row = mysqli_fetch_array($result_set006)) {
				$course_06 = $row["name"];	
			}	
			$course_07 = "";
			$result_set007 = get_dim_rec_z("aca_course", "course_id", $courses_id_07);
			while ($row = mysqli_fetch_array($result_set007)) {
				$course_07 = $row["name"];	
			}		
			$course_08 = "";
			$result_set008 = get_dim_rec_z("aca_course", "course_id", $courses_id_08);
			while ($row = mysqli_fetch_array($result_set008)) {
				$course_08 = $row["name"];	
			}		
			$course_09 = "";
			$result_set009 = get_dim_rec_z("aca_course", "course_id", $courses_id_09);
			while ($row = mysqli_fetch_array($result_set009)) {
				$course_09 = $row["name"];	
			}		
			$course_10 = "";
			$result_set010 = get_dim_rec_z("aca_course", "course_id", $courses_id_10);
			while ($row = mysqli_fetch_array($result_set010)) {
				$course_10 = $row["name"];	
			}	
		
		
		
			$un = "<a href=\"?id=".$id."&set=3&opt=0&login=1&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&key=".$key."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$student."</a>";
			$rep =  "<button name=\"ReportBut\" type=\"button\" value=\"Report\" class=\"btn btn-success\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=8&opt=0&cat7=".$cat7."&cat8=".$cat8."&key=".$key."');return document.MM_returnValue\"/>Report</button>&nbsp;&nbsp;";
			
						
		echo "<tr>";
			echo "<td>Name: </td>";
			echo "<td colspan=\"3\"><strong>".$student."</strong></td>";
		echo "</tr>";							
		echo "<tr>";
			echo "<td>Group: </td>";
			echo "<td colspan=\"3\">".$class." - " .$sname. "</td>";
		echo "</tr>";			
		echo "<tr>";
			echo "<td>Eval Date: </td>";
			echo "<td colspan=\"3\">".$evaluation_date."</td>";
		echo "</tr>";				
		echo "<tr>";
			echo "<td>Current Position</td>";
			echo "<td colspan=\"3\">".$current_position."</td>";
		echo "</tr>";				
		echo "<tr>";
			echo "<td colspan=\"2\">Experience</td>";
			echo "<td>Year(s)</td>";
			echo "<td>Month(s)</td>";
		echo "</tr>";			
		echo "<tr>";
			echo "<td>Primary</td>";
			echo "<td>".$function_01."</td>";
			echo "<td>".$experience_yr_01."</td>";
			echo "<td>".$experience_01."</td>";
		echo "</tr>";
		if ($function_02 <> "") {				
			echo "<tr>";
				echo "<td>Secondary</td>";
				echo "<td>".$function_02."</td>";
				echo "<td>".$experience_yr_02."</td>";
				echo "<td>".$experience_02."</td>";
			echo "</tr>";	
		}
		if ($function_03 <> "") {	
			echo "<tr>";
				echo "<td>Third</td>";
				echo "<td>".$function_03."</td>";
				echo "<td>".$experience_yr_03."</td>";
				echo "<td>".$experience_03."</td>";
			echo "</tr>";	
		}
		if ($function_04 <> "") {				
			echo "<tr>";
				echo "<td>Fourth</td>";
				echo "<td>".$function_04."</td>";
				echo "<td>".$experience_yr_04."</td>";
				echo "<td>".$experience_04."</td>";
			echo "</tr>";	
		}
		if ($function_05 <> "") {				
			echo "<tr>";
				echo "<td>Fifth</td>";
				echo "<td>".$function_05."</td>";
				echo "<td>".$experience_yr_05."</td>";
				echo "<td>".$experience_05."</td>";
			echo "</tr>";		
		}			
		echo "<tr>";
			echo "<td>Evaluated Role</td>";
			echo "<td colspan=\"3\">".$role."</td>";
		echo "</tr>";
		if ($course_01 <> "" || $course_02 <> "" || $course_03 <> "" || $course_04 <> "" || $course_05 <> "") { 	
			if ($course_02 <> "") { $course_02 = ", " . $course_02; }
			if ($course_03 <> "") { $course_03 = ", " . $course_03; }
			if ($course_04 <> "") { $course_04 = ", " . $course_04; }
			if ($course_05 <> "") { $course_05 = ", " . $course_05; }						
			echo "<tr>";
			echo "<td>Courses Attended</td>";
			echo "<td colspan=\"3\">".$course_01.$course_02.$course_03.$course_04.$course_05."</td>";
			echo "</tr>";
		}
		if ($course_06 <> "" || $course_07 <> "" || $course_08 <> "" || $course_09 <> "" || $course_10 <> "") { 
			if ($course_07 <> "") { $course_07 = ", " . $course_07; }
			if ($course_08 <> "") { $course_08 = ", " . $course_08; }
			if ($course_09 <> "") { $course_09 = ", " . $course_09; }	
			if ($course_10 <> "") { $course_10 = ", " . $course_10; }								
			echo "<tr>";
			echo "<td>&nbsp;</td>";
			echo "<td colspan=\"3\">".$course_06.$course_07.$course_08.$course_09.$course_10."</td>";
			echo "</tr>";
		}
		if ($courses_attended <> "") { 							
			echo "<tr>";
				echo "<td>Other Courses</td>";
				echo "<td colspan=\"3\">".$courses_attended."</td>";
			echo "</tr>";	
		}		
		//echo "<tr>";
		//	echo "<td colspan=\"3\">Qualifications</td>";
		//echo "</tr>";
		if ($qualification_01 <> "" || $qualification_02 <> "" || $qualification_03 <> "" || $qualification_04 <> "" || $qualification_05 <> "") { 	
			echo "<tr>";
				echo "<td>Qualifications</td>";
				echo "<td colspan=\"3\">".$qualification_01."</td>";
			echo "</tr>";
		}
		if ($qualification_02 <> "") { 	
			echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo "<td colspan=\"3\">".$qualification_02."</td>";
			echo "</tr>";
		}
		if ($qualification_03 <> "") { 	
			echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo "<td colspan=\"3\">".$qualification_03."</td>";
			echo "</tr>";
		}
		if ($qualification_04 <> "") { 	
			echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo "<td colspan=\"3\">".$qualification_04."</td>";
			echo "</tr>";
		}
		if ($qualification_05 <> "") { 	
			echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo "<td colspan=\"3\">".$qualification_05."</td>";
			echo "</tr>";
		}
		
		/*
		echo "<tr>";
			echo "<td>".$un."</td>";
			echo "<td colspan=\"2\">".$rep."</td>";
		echo "</tr>";
		*/
		
		echo "<tr>";
			echo "<td colspan=\"4\">";
				
		// display test results		
		echo "<h3>Test Summary</h3>";
		// get test results		
		
		// variables
		$subject = "xxxx";
		$exam = "xxxx";
		$no_of_tests = 0;
		$no_of_questions = 0;
		$score = 0;
			
		echo "<table class=\"table table-striped\">";
	
		$result_setQ = get_aca_results_eval(0, $cat3, $cat7); // $cat = exam_id, $cat3 = $user_id, $cat7 = $class_id
		while ($row = mysqli_fetch_array($result_setQ)) {
			$class_id = $row["class_id"];	
			$user_id = $row["user_id"];		
			$exam_id = $row["exam_id"];	
			$no_of_tests = $row["testcnt"];	
			$no_of_questions = $row["qcnt"];		
			$score  = $row["score"];
			if ($no_of_questions == 0) { $scorep = 0; } else { $scorep = ($score / $no_of_questions) * 100; }
			$result_set1 = get_aca_exam_rec($exam_id);
			while ($row = mysqli_fetch_array($result_set1)) {
				$exam = $row["code"];
			} 			 
			echo "<tr>";
				echo "<th>Exam</th>";
				echo "<td>".$exam."</td>";
				echo "<th># Tests</th>";
				echo "<td>".$no_of_tests."</td>";
				echo "<th># Questions</th>";
				echo "<td>".$no_of_questions."</td>";
				echo "<th>Score</th>";
				echo "<td>".number_format($scorep,0,",",",")."%</td>";
			echo "</tr>";		
		}
		echo "<tr>";
			echo "<th colspan=\"8\">&nbsp;</th>";
		echo "</tr>";	
		echo "</table>";
		
		
		// variables
		$subject = "xxxx";
		$exam = "xxxx";
		$no_of_tests = 0;
		$no_of_questions = 0;
		$score = 0;
		$tq = 0;
		$ts = 0;
		
		echo "<table class=\"table table-striped\">";
		echo "<thead>";
		echo "<tr>";
			echo "<th>Exam</th>";
			echo "<th>Subject</th>";
			echo "<th>No of Questions</th>";
			echo "<th>Score</th>";
		echo "</tr>";		
		echo "</thead>";
		echo "<tbody>";
	
		$result_setQ1 = get_aca_results_eval_tool(0, 0, $cat3, $cat7); // $cat = exam_id, $cat6 = $tool_id, $cat3 = $user_id, $cat7 = $class_id
		while ($row = mysqli_fetch_array($result_setQ1)) {
			$class_id = $row["class_id"];	
			$user_id = $row["user_id"];		
			$tool_id = $row["tool_id"];	
			$no_of_questions = $row["qcnt"];		
			$score  = $row["score"];
			$tq = $tq + $no_of_questions;
			$ts = $ts + $score;
			if ($no_of_questions == 0) { $scorep = 0; } else { $scorep = ($score / $no_of_questions) * 100; }
			$result_set11 = get_aca_exam_rec($exam_id);
			while ($row = mysqli_fetch_array($result_set11)) {
				$exam = $row["code"];
			} 			 			
			$result_set6 = get_aca_tool_rec($tool_id);
			while ($row = mysqli_fetch_array($result_set6)) {
				$subject = $row["description"];
			} 
			/**/
			echo "<tr>";
				echo "<td>".$exam."</td>";
				echo "<td>".$subject."</td>";
				echo "<td>".$no_of_questions."</td>";
				echo "<td>".number_format($scorep,0,",",",")."%</td>";
			echo "</tr>";		
		}
			// totals
			if ($tq == 0) { $tp = 0; } else { $tp = ($ts / $tq) * 100; }
			echo "<tr>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td><strong>".$tq."</strong></td>";
				echo "<td><strong>".number_format($tp,0,",",",")."%</strong></td>";
			echo "</tr>";		
		

		echo "</tbody>";
		echo "</table>";
		
		
		/*
		*/
		/*
		echo "<tr>";
			echo "<td colspan=\"2\">Display test results summary here</td>";
			echo "<td>".$rep."</td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td align=\"left\" colspan=\"3\">".$c."</td>"; // close, new, page nav
			echo "<td align=\"right\" colspan=\"3\">".$p."</td>";
		echo "</tr>";
		*/
		

		echo "<p>&nbsp;</p>";
		
		
				echo "</td>";
			echo "</tr>";
		
		}	
		echo "<p>".$c." ".$p."</p>";


		echo "</tbody>";
		echo "</table>";
		  echo "</div> "; 
	   echo " </div> "; 	
		
		} // end of opt = 11
		
	} // end of set = 0







	if ($set == 8) {	
		echo "<div class=\"panel panel-primary\"> "; 
		  echo "<div class=\"panel-heading\">Evaluation Results</div> "; 
		  echo "<div class=\"panel-body\">";
		  
		  
		echo "<table class=\"table table-striped\">";
		echo "<tbody>";
		// get data
		$result_setB = get_aca_evaluation_rec($cat3);
		while ($row = mysqli_fetch_array($result_setB)) {		
			$key = $row["eval_id"];
			$class_id = $row["class_id"];
			$user_id = $row["user_id"];
			$cat7 = $class_id;	
			$cat3 = $user_id;
			$evaluation_date = $row["evaluation_date"];	
			$current_position = $row["current_position"];
			$courses_attended = $row["courses_attended"];
			$function_id_01 = $row["function_id_01"];
			$function_id_02 = $row["function_id_02"];				
			$function_id_03 = $row["function_id_03"];				
			$function_id_04 = $row["function_id_04"];				
			$function_id_05 = $row["function_id_05"];			
			$experience_yr_01 = $row["experience_yr_01"];				
			$experience_yr_02 = $row["experience_yr_02"];				
			$experience_yr_03 = $row["experience_yr_03"];				
			$experience_yr_04 = $row["experience_yr_04"];				
			$experience_yr_05 = $row["experience_yr_05"];					
			$experience_01 = $row["experience_01"];				
			$experience_02 = $row["experience_02"];				
			$experience_03 = $row["experience_03"];				
			$experience_04 = $row["experience_04"];				
			$experience_05 = $row["experience_05"];	
			$role_id = $row["role_id"];	
			$courses_id_01 = $row["courses_id_01"];
			$courses_id_02 = $row["courses_id_02"];				
			$courses_id_03 = $row["courses_id_03"];				
			$courses_id_04 = $row["courses_id_04"];				
			$courses_id_05 = $row["courses_id_05"];				
			$courses_id_06 = $row["courses_id_06"];				
			$courses_id_07 = $row["courses_id_07"];				
			$courses_id_08 = $row["courses_id_08"];				
			$courses_id_09 = $row["courses_id_09"];				
			$courses_id_10 = $row["courses_id_10"];	
			$qualification_01 = $row["qualification_01"];
			$qualification_02 = $row["qualification_02"];				
			$qualification_03 = $row["qualification_03"];				
			$qualification_04 = $row["qualification_04"];				
			$qualification_05 = $row["qualification_05"];															
			$status = $row["status"];
					
			$class = "";
			$result_setI = get_aca_class_rec($class_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$class = $row["name"];		
			}	
			$student = "";
			$result_setS = get_aca_user_rec($user_id);
			while ($row = mysqli_fetch_array($result_setS)) {		
				$student = $row["name"];		
				$sname = $row["username"];			
			}	
			$d_status = $status;
			$result_set74 = get_dim("status", $status);
			while ($row = mysqli_fetch_array($result_set74)) {
				$dim_key = $row["dim_key"]; 
				$d_status = $row["description"];
			}
			$role = "";
			$result_set01x = get_dim_rec_z("aca_role", "role_id",  $role_id);
			while ($row = mysqli_fetch_array($result_set01x)) {
				$role = $row["name"] . " " . $row["description"];
			}
			$function_01 = "";
			$result_set001 = get_dim_rec_z("aca_function", "function_id", $function_id_01);
			while ($row = mysqli_fetch_array($result_set001)) {
				$function_01 = $row["description"];	
			}	
			$function_02 = "";
			$result_set002 = get_dim_rec_z("aca_function", "function_id", $function_id_02);
			while ($row = mysqli_fetch_array($result_set002)) {
				$function_02 = $row["description"];	
			}		
			$function_03 = "";
			$result_set003 = get_dim_rec_z("aca_function", "function_id", $function_id_03);
			while ($row = mysqli_fetch_array($result_set003)) {
				$function_03 = $row["description"];	
			}		
			$function_04 = "";
			$result_set004 = get_dim_rec_z("aca_function", "function_id", $function_id_04);
			while ($row = mysqli_fetch_array($result_set004)) {
				$function_04 = $row["description"];	
			}		
			$function_05 = "";
			$result_set005 = get_dim_rec_z("aca_function", "function_id", $function_id_05);
			while ($row = mysqli_fetch_array($result_set005)) {
				$function_05 = $row["description"];	
			}	
			$course_01 = "";
			$result_set001 = get_dim_rec_z("aca_course", "course_id", $courses_id_01);
			while ($row = mysqli_fetch_array($result_set001)) {
				$course_01 = $row["name"];	
			}	
			$course_02 = "";
			$result_set002 = get_dim_rec_z("aca_course", "course_id", $courses_id_02);
			while ($row = mysqli_fetch_array($result_set002)) {
				$course_02 = $row["name"];	
			}	
			$course_03 = "";
			$result_set003 = get_dim_rec_z("aca_course", "course_id", $courses_id_03);
			while ($row = mysqli_fetch_array($result_set003)) {
				$course_03 = $row["name"];	
			}		
			$course_04 = "";
			$result_set004 = get_dim_rec_z("aca_course", "course_id", $courses_id_04);
			while ($row = mysqli_fetch_array($result_set004)) {
				$course_04 = $row["name"];	
			}		
			$course_05 = "";
			$result_set005 = get_dim_rec_z("aca_course", "course_id", $courses_id_05);
			while ($row = mysqli_fetch_array($result_set005)) {
				$course_05 = $row["name"];	
			}					
			$course_06 = "";
			$result_set006 = get_dim_rec_z("aca_course", "course_id", $courses_id_06);
			while ($row = mysqli_fetch_array($result_set006)) {
				$course_06 = $row["name"];	
			}	
			$course_07 = "";
			$result_set007 = get_dim_rec_z("aca_course", "course_id", $courses_id_07);
			while ($row = mysqli_fetch_array($result_set007)) {
				$course_07 = $row["name"];	
			}		
			$course_08 = "";
			$result_set008 = get_dim_rec_z("aca_course", "course_id", $courses_id_08);
			while ($row = mysqli_fetch_array($result_set008)) {
				$course_08 = $row["name"];	
			}		
			$course_09 = "";
			$result_set009 = get_dim_rec_z("aca_course", "course_id", $courses_id_09);
			while ($row = mysqli_fetch_array($result_set009)) {
				$course_09 = $row["name"];	
			}		
			$course_10 = "";
			$result_set010 = get_dim_rec_z("aca_course", "course_id", $courses_id_10);
			while ($row = mysqli_fetch_array($result_set010)) {
				$course_10 = $row["name"];	
			}	
		
		
		
			$un = "<a href=\"?id=".$id."&set=3&opt=0&login=1&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&key=".$key."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$student."</a>";
			$rep =  "<button name=\"ReportBut\" type=\"button\" value=\"Report\" class=\"btn btn-success\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=8&opt=0&cat7=".$cat7."&cat8=".$cat8."&key=".$key."');return document.MM_returnValue\"/>Report</button>&nbsp;&nbsp;";
			
						
		echo "<tr>";
			echo "<td>Name: </td>";
			echo "<td colspan=\"3\"><strong>".$student."</strong></td>";
		echo "</tr>";							
		echo "<tr>";
			echo "<td>Group: </td>";
			echo "<td colspan=\"3\">".$class." - " .$sname. "</td>";
		echo "</tr>";			
		echo "<tr>";
			echo "<td>Eval Date: </td>";
			echo "<td colspan=\"3\">".$evaluation_date."</td>";
		echo "</tr>";				
		echo "<tr>";
			echo "<td>Current Position</td>";
			echo "<td colspan=\"3\">".$current_position."</td>";
		echo "</tr>";				
		echo "<tr>";
			echo "<td colspan=\"2\">Experience</td>";
			echo "<td>Year(s)</td>";
			echo "<td>Month(s)</td>";
		echo "</tr>";			
		echo "<tr>";
			echo "<td>Primary</td>";
			echo "<td>".$function_01."</td>";
			echo "<td>".$experience_yr_01."</td>";
			echo "<td>".$experience_01."</td>";
		echo "</tr>";	
		if ($function_02 <> "") {					
			echo "<tr>";
				echo "<td>Secondary</td>";
				echo "<td>".$function_02."</td>";
				echo "<td>".$experience_yr_02."</td>";
				echo "<td>".$experience_02."</td>";
			echo "</tr>";
		}
		if ($function_03 <> "") {						
			echo "<tr>";
				echo "<td>Third</td>";
				echo "<td>".$function_03."</td>";
				echo "<td>".$experience_yr_03."</td>";
				echo "<td>".$experience_03."</td>";
			echo "</tr>";
		}
		if ($function_04 <> "") {					
			echo "<tr>";
				echo "<td>Fourth</td>";
				echo "<td>".$function_04."</td>";
				echo "<td>".$experience_yr_04."</td>";
				echo "<td>".$experience_04."</td>";
			echo "</tr>";
		}
		if ($function_05 <> "") {				
			echo "<tr>";
				echo "<td>Fifth</td>";
				echo "<td>".$function_05."</td>";
				echo "<td>".$experience_yr_05."</td>";
				echo "<td>".$experience_05."</td>";
			echo "</tr>";
		}
		echo "<tr>";
			echo "<td>Evaluated Role</td>";
			echo "<td colspan=\"3\">".$role."</td>";
		echo "</tr>";
		if ($course_01 <> "" || $course_02 <> "" || $course_03 <> "" || $course_04 <> "" || $course_05 <> "") { 	
			if ($course_02 <> "") { $course_02 = ", " . $course_02; }
			if ($course_03 <> "") { $course_03 = ", " . $course_03; }
			if ($course_04 <> "") { $course_04 = ", " . $course_04; }
			if ($course_05 <> "") { $course_05 = ", " . $course_05; }						
			echo "<tr>";
			echo "<td>Courses Attended</td>";
			echo "<td colspan=\"3\">".$course_01.$course_02.$course_03.$course_04.$course_05."</td>";
			echo "</tr>";
		}
		if ($course_06 <> "" || $course_07 <> "" || $course_08 <> "" || $course_09 <> "" || $course_10 <> "") { 
			if ($course_07 <> "") { $course_07 = ", " . $course_07; }
			if ($course_08 <> "") { $course_08 = ", " . $course_08; }
			if ($course_09 <> "") { $course_09 = ", " . $course_09; }	
			if ($course_10 <> "") { $course_10 = ", " . $course_10; }								
			echo "<tr>";
			echo "<td>&nbsp;</td>";
			echo "<td colspan=\"3\">".$course_06.$course_07.$course_08.$course_09.$course_10."</td>";
			echo "</tr>";
		}
		if ($courses_attended <> "") { 							
			echo "<tr>";
				echo "<td>Other Courses</td>";
				echo "<td colspan=\"3\">".$courses_attended."</td>";
			echo "</tr>";	
		}	
		if ($qualification_01 <> "") { 	
			echo "<tr>";
				echo "<td>Qualifications</td>";
				echo "<td colspan=\"2\">".$qualification_01."</td>";
			echo "</tr>";
		}
		if ($qualification_02 <> "") { 	
			echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo "<td colspan=\"3\">".$qualification_02."</td>";
			echo "</tr>";
		}
		if ($qualification_03 <> "") { 	
			echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo "<td colspan=\"3\">".$qualification_03."</td>";
			echo "</tr>";
		}
		if ($qualification_04 <> "") { 	
			echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo "<td colspan=\"3\">".$qualification_04."</td>";
			echo "</tr>";
		}
		if ($qualification_05 <> "") { 	
			echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo "<td colspan=\"3\">".$qualification_05."</td>";
			echo "</tr>";
		}
		
		/*
		echo "<tr>";
			echo "<td>".$un."</td>";
			echo "<td colspan=\"2\">".$rep."</td>";
		echo "</tr>";
		*/
		
		echo "<tr>";
			echo "<td colspan=\"4\">";
				
		// display test results		
		echo "<h3>Test Summary</h3>";
		// get test results		
		
		// variables
		$subject = "xxxx";
		$exam = "xxxx";
		$no_of_tests = 0;
		$no_of_questions = 0;
		$score = 0;
			
		echo "<table class=\"table table-striped\">";
	
		$result_setQ = get_aca_results_eval(0, $cat3, $cat7); // $cat = exam_id, $cat3 = $user_id, $cat7 = $class_id
		while ($row = mysqli_fetch_array($result_setQ)) {
			$class_id = $row["class_id"];	
			$user_id = $row["user_id"];		
			$exam_id = $row["exam_id"];	
			$no_of_tests = $row["testcnt"];	
			$no_of_questions = $row["qcnt"];		
			$score  = $row["score"];
			if ($no_of_questions == 0) { $scorep = 0; } else { $scorep = ($score / $no_of_questions) * 100; }
			$result_set1 = get_aca_exam_rec($exam_id);
			while ($row = mysqli_fetch_array($result_set1)) {
				$exam = $row["code"];
			} 			 
			echo "<tr>";
				echo "<th>Exam</th>";
				echo "<td>".$exam."</td>";
				echo "<th># Tests</th>";
				echo "<td>".$no_of_tests."</td>";
				echo "<th># Questions</th>";
				echo "<td>".$no_of_questions."</td>";
				echo "<th>Score</th>";
				echo "<td>".number_format($scorep,0,",",",")."%</td>";
			echo "</tr>";		
		}
		echo "<tr>";
			echo "<th colspan=\"8\">&nbsp;</th>";
		echo "</tr>";	
		echo "</table>";
		
		
		// variables
		$subject = "xxxx";
		$exam = "xxxx";
		$no_of_tests = 0;
		$no_of_questions = 0;
		$score = 0;
		$tq = 0;
		$ts = 0;
		
		echo "<table class=\"table table-striped\">";
		echo "<thead>";
		echo "<tr>";
			echo "<th>Exam</th>";
			echo "<th>Subject</th>";
			echo "<th>No of Questions</th>";
			echo "<th>Score</th>";
		echo "</tr>";		
		echo "</thead>";
		echo "<tbody>";
	
		$result_setQ1 = get_aca_results_eval_tool(0, 0, $cat3, $cat7); // $cat = exam_id, $cat6 = $tool_id, $cat3 = $user_id, $cat7 = $class_id
		while ($row = mysqli_fetch_array($result_setQ1)) {
			$class_id = $row["class_id"];	
			$user_id = $row["user_id"];		
			$tool_id = $row["tool_id"];	
			$no_of_questions = $row["qcnt"];		
			$score  = $row["score"];
			$tq = $tq + $no_of_questions;
			$ts = $ts + $score;
			if ($no_of_questions == 0) { $scorep = 0; } else { $scorep = ($score / $no_of_questions) * 100; }
			$result_set11 = get_aca_exam_rec($exam_id);
			while ($row = mysqli_fetch_array($result_set11)) {
				$exam = $row["code"];
			} 			 			
			$result_set6 = get_aca_tool_rec($tool_id);
			while ($row = mysqli_fetch_array($result_set6)) {
				$subject = $row["description"];
			} 
			/**/
			echo "<tr>";
				echo "<td>".$exam."</td>";
				echo "<td>".$subject."</td>";
				echo "<td>".$no_of_questions."</td>";
				echo "<td>".number_format($scorep,0,",",",")."%</td>";
			echo "</tr>";		
		}
			// totals
			if ($tq == 0) { $tp = 0; } else { $tp = ($ts / $tq) * 100; }
			echo "<tr>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td><strong>".$tq."</strong></td>";
				echo "<td><strong>".number_format($tp,0,",",",")."%</strong></td>";
			echo "</tr>";		
		

		echo "</tbody>";
		echo "</table>";

		echo "<p>&nbsp;</p>";
		
		
				echo "</td>";
			echo "</tr>";
		
		}	

		echo "</tbody>";
		echo "</table>";
		  echo "</div> "; 
	   echo " </div> "; 	
		
		
	} // end of set = 8
	
echo '</div>';
echo '</div>';
echo '</div>';
} // end of evaluation id = 26
?>
 