<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/09/28
 * Time: 4:01 AM
 */
ini_set("display_errors", 1);
$data = array();

if(isset($_GET['files']))
{
    $error = false;
    $files = array();

    $uploaddir = '../videos/';

    foreach($_FILES as $file)
    {
        $temp = explode(".", $file["name"]);
        $newfilename = md5($file["name"]);
        if(move_uploaded_file($file['tmp_name'], $uploaddir . $newfilename . '.' . $temp[1]))
        {
            $files[] = $uploaddir .$newfilename . '.' . $temp[1];
        }
        else
        {
            $error = true;
        }
    }
    $data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $files);
}
else
{
    $data = array('success' => 'Form was submitted', 'formData' => $_POST);
}

echo json_encode($data);