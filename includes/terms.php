<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/08/28
 * Time: 6:40 PM
 */
echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Terms and Conditions</h2></div>
	  ";
echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><ol>
        <li>Detailed description of goods and/or services<br />
            Blackboard BI (Pty) Ltd is a business in the Business Intelligence industry that provides Professional Services and Training .</li>
        <li>Delivery policy<br />
            Subject to availability and receipt of payment, requests will be processed within 2 hours and delivery confirmed by way of subscription and exam number.</li>
        <li>Return and Refunds policy<br />
            The provision of goods and services by Blackboard BI (Pty) Ltd is subject to availability. In cases of unavailability, Blackboard BI (Pty) Ltd will refund the client in full within 30 days. Cancellation of orders by the client will attract a 100% administration fee.</li>
        <li>Customer Privacy policy<br />
            Blackboard BI (Pty) Ltd shall take all reasonable steps to protect the personal information of users. For the purpose of this clause, "personal information" shall be defined as detailed in the Promotion of Access to Information Act 2 of 2000 (PAIA). The PAIA may be downloaded from: http://www.polity.org.za/attachment.php?aa_id=3569 .</li>
        <li>Payment options accepted<br />
            Payment may be made via PayPal Visa and MasterCard.</li>
        <li>Card acquiring and security<br />
            Card transactions will be acquired for Blackboard BI (Pty) Ltd via Paypal. PayPal uses the strictest form of encryption, namely Secure Socket Layer 3 (SSL3) and no Card details are stored on the website. Users may go to www.paypal.com to view their security certificate and security policy.</li>
        <li>Customer details separate from card details<br />
            Customer details will be stored by Blackboard BI (Pty) Ltd separately from card details which are entered by the client on PayPal’s secure site. For more detail on PayPal refer to www.paypal.co.za.</li>
        <li>Merchant Outlet country and transaction currency<br />
            The merchant outlet country at the time of presenting payment options to the cardholder is South Africa. Transaction currency is United States Dollar (USD).</li>
        <li>Responsibility<br />
            Blackboard BI (Pty) Ltd takes responsibility for all aspects relating to the transaction including sale of goods and services sold on this website, customer service and support, dispute resolution and delivery of goods.</li>
        <li>Country of domicile<br />
            This website is governed by the laws of South Africa and Blackboard BI (Pty) Ltd chooses as its domicilium citandi et executandi for all purposes under this agreement, whether in respect of court process, notice, or other documents or communication of whatsoever nature.</li>
        <li>Variation<br />
            Blackboard BI (Pty) Ltd may, in its sole discretion, change this agreement or any part thereof at any time without notice.</li>
        <li>Company information<br />
            This website is run by a private company based in South Africa trading as Blackboard BI (Pty) Ltd and with registration number 2017/322797/07.</li>
			Blackboard BI (Pty) Ltd contact details<br />
            Email: info@blackboardbi.com</li>
      </ol></div>';