<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/08/23
 * Time: 9:18 AM
 */
echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>Exams</h2><div class="colored-line-left"></div></div>
          <div class="clearfix"></div> ';
echo '<div class="row content-pad">
        <div class="form-group">';

        $examcnt = 10;
        $i = 1;
        $result_set = get_aca_exam_list(0,$examcnt,1);
        while ($row = mysqli_fetch_array($result_set)) {
            echo '
                <div class="col-sm-12" '.($i < $examcnt ? 'style="border-bottom: 1px solid #777777;"' : '').'>
                    <div class="col-sm-2" style="min-height:150px;">
                        <p><img src="'.$row["exam_image"].'" height="70" /></p>
                    </div>
                    <div class="col-sm-10" style="min-height:150px;">
                        <table class="table">
                            <tr>
                                <td><strong>Code:</strong></td>
                                <td>'.$row["code"].'</td>
                            </tr>
                            <tr>
                                <td><strong>Name:</strong></td>
                                <td>'.$row["description"].'</td>
                            </tr>
                            <tr>
                                <td valign="top"><strong>Description:</strong></td>
                                <td>'.$row["long_description"].'</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-12 text-right">
                        <p><a href="?id=2&key='.$row["exam_id"].'" class="btn btn-default">Read more</a>&nbsp;
                        <a href="'.(logged_in() ? '?id=21&cat=0&login=1&set=11&opt=0&key='.$row["exam_id"] : '?id=106&key=' . $row["exam_id"] . '&step=1' ).'" class="btn btn-success">Subscribe</a></p>
                    </div>
                    </div>';
            $i++;
        }
        echo '
		</div>
    </div>';