<?php

/*
 * Include the helper PayWeb 3 class
 */
require_once('paygate.payweb3.php');

/*
 * insert the returned data as well as the merchant specific data PAYGATE_ID and REFERENCE in array
 */
$data = array(
    'PAYGATE_ID'         => $_SESSION['pgid'],
    'PAY_REQUEST_ID'     => $_POST['PAY_REQUEST_ID'],
    'TRANSACTION_STATUS' => $_POST['TRANSACTION_STATUS'],
    'REFERENCE'          => $_SESSION['reference'],
    'CHECKSUM' => $_POST["CHECKSUM"]
);

/*
 * initiate the PayWeb 3 helper class
 */
$PayWeb3 = new PayGate_PayWeb3();
/*
 * Set the encryption key of your PayGate PayWeb3 configuration
 */
$PayWeb3->setEncryptionKey($_SESSION['key']);
/*
 * Check that the checksum returned matches the checksum we generate
 */
$isValid = $PayWeb3->validateChecksum($data);
?>

    <div class="panel panel-primary">

        <div class="panel-heading">Subscription Summary</div>

        <div class="panel-body">
            <?php

            if($data["TRANSACTION_STATUS"] == 0){
                echo '<p><strong>Transaction Unsuccessful</strong></p>

                    <p>We apologise, your transaction was unsuccessful.</p>
					<p>&nbsp;</p>
                    <p><strong>Your reference is: '.$_SESSION["paygateDetails"]["reference"].'</strong></p>
						<p>&nbsp;</p>
						<p>For any queries please contact us via email <a href="mailto:info@beyondbi-training.com">info@beyondbi-training.com</a>.</p>
						<p>&nbsp;</p>
						<a href="?id=21&login=1&opt=0&key=0&set=0" class="btn btn-default">Back to Exam List</a>';
                $reference = $_SESSION["paygateDetails"]["reference"];
                $paymentcomplete = 0;
                $paygatestatus = "Unsuccessful";

            }

            if($data["TRANSACTION_STATUS"] == 1){
				echo '<p><strong>Transaction Successful</strong></p>
						<p>&nbsp;</p>
						<p>Thank you for subscribing to the <strong>'.$_SESSION["transactionDetails"][0]["description"].'</strong> exam.</p>
						<p>&nbsp;</p>
						<p><strong>Your reference is: '.$_SESSION["paygateDetails"]["reference"].'</strong></p>
						<p>&nbsp;</p>
						<p>For any queries please contact us via email <a href="mailto:'.$support_email.'">'.$support_email.'</a>.</p>
						<p>&nbsp;</p>
						<a href="?id=21&login=1&opt=0&key=0&set=0" class="btn btn-default">Back to Exam List</a>';
                $reference = $_SESSION["paygateDetails"]["reference"];
                $paymentcomplete = 1;
                $paygatestatus = "Successful";



				$query = "update aca_user_exam set status='1' where user_id = '".$userx."' and exam_id = '".$_SESSION["payeeDetails"]["examid"]."'" ;
				$result = mysqli_query( $connection, $query);
				
							$query2 = "select a.*,b.* from aca_exam a inner join aca_user_exam b on a.exam_id=b.exam_id where a.exam_id = '".$_SESSION["payeeDetails"]["examid"]."' limit 1";

							$result2 = mysqli_query( $connection, $query2);

							while ($row = mysqli_fetch_array($result2)) {

							include 'lib/swift_required.php';

							$msgcontent = '';
							$output = '';

							$message = Swift_Message::newInstance();
							$message->setSubject("BeyondBI Training Exam Subscription");
							$message->setFrom(array($admin_email => $admin_email));
							$message->setTo(array($_SESSION["payeeDetails"]["email"] => $_SESSION["payeeDetails"]["firstname"]." ".$_SESSION["payeeDetails"]["surname"]));
							$message->setBcc(array("jaco@itechnologiessolutions.co.za" => "Jaco Ferreira"));
							$message->setBody('This email requires HTML to view.');

							$msgcontent.= "<p>Dear ".$_SESSION["payeeDetails"]["firstname"]." ".$_SESSION["payeeDetails"]["surname"].", </p>";

							$msgcontent.= "<p>&nbsp;</p>";

							$msgcontent.= "Thank you for subscribing to the <strong>".$row["code"]."</strong> exam. Your payment reference is <strong>".$_SESSION["paygateDetails"]["reference"]."</strong>.";
							$msgcontent.= "Your payment was successful. You'll have access to the exam until <strong>".$row["expiry_date"]."</strong>.";
							$msgcontent.= "<p>&nbsp;</p>";
							$msgcontent.= "<p>Regards</p>";
							$msgcontent.= "<p>The Webmaster</p>";

                            $pdf = generateInvoice($_SESSION["paygateDetails"]["reference"],"cc");

                            $message->addPart($msgcontent, 'text/html');
                            $attachment = Swift_Attachment::fromPath('C:/Websites/Beyond-Training/pdf/'.date("ymd").'_'.$_SESSION["paygateDetails"]["reference"].'.pdf','application/pdf');
                            $message->attach($attachment);

							$transport = Swift_MailTransport::newInstance();
							$mailer = Swift_Mailer::newInstance($transport);
							$result = $mailer->send($message);
							}
            }

            if($data["TRANSACTION_STATUS"] == 2){
                echo '<p><strong>Transaction Declined</strong></p>

                    <p>We apologise, your transaction was declined.</p>

                    <p>&nbsp;</p>
                    <p><strong>Your reference is: '.$_SESSION["paygateDetails"]["reference"].'</strong></p>
						<p>&nbsp;</p>
						<p>For any queries please contact us via email <a href="mailto:info@beyondbi-training.com">info@beyondbi-training.com</a>.</p>
						<p>&nbsp;</p>
						<a href="?id=21&login=1&opt=0&key=0&set=0" class="btn btn-default">Back to Exam List</a>';
                $reference = $_SESSION["paygateDetails"]["reference"];
                $paymentcomplete = 0;
                $paygatestatus = "Declined";
            }

            if($data["TRANSACTION_STATUS"] == 3){
                echo '<p><strong>Transaction Cancelled</strong></p>

                    <p>We apologise, your transaction was cancelled.</p>
					<p>&nbsp;</p>
                    <p><strong>Your reference is: '.$_SESSION["paygateDetails"]["reference"].'</strong></p>
						<p>&nbsp;</p>
						<p>For any queries please contact us via email <a href="mailto:info@beyondbi-training.com">info@beyondbi-training.com</a>.</p>
						<p>&nbsp;</p>
						<a href="?id=21&login=1&opt=0&key=0&set=0" class="btn btn-default">Back to Exam List</a>';
                $reference = $_SESSION["paygateDetails"]["reference"];
                $paymentcomplete = -1;
                $paygatestatus = "Cancelled";
				$query = "delete from aca_user_exam where user_exam_id = '".$_SESSION["payeeDetails"]["userexamid"]."'";

				$result = mysqli_query( $connection, $query);
            }
			
			if($data["TRANSACTION_STATUS"] == 4){
                echo '<p><strong>Transaction Cancelled</strong></p>

                    <p>We apologise, your transaction was cancelled.</p>
					<p>&nbsp;</p>
                    <p><strong>Your reference is: '.$_SESSION["paygateDetails"]["reference"].'</strong></p>
						<p>&nbsp;</p>
						<p>For any queries please contact us via email <a href="mailto:info@beyondbi-training.com">info@beyondbi-training.com</a>.</p>
						<p>&nbsp;</p>
						<a href="?id=21&login=1&opt=0&key=0&set=0" class="btn btn-default">Back to Exam List</a>';
                $reference = $_SESSION["paygateDetails"]["reference"];
                $paymentcomplete = -1;
                $paygatestatus = "User Cancelled";
				$query = "delete from aca_user_exam where user_exam_id = '".$_SESSION["payeeDetails"]["userexamid"]."'";

				$result = mysqli_query( $connection, $query);
            }

            $query = "update aca_transaction set paymentcomplete = '".$paymentcomplete."',paygatestatus = '".$paygatestatus."' where paymentreference = '".$reference."'";

            $result = mysqli_query( $connection, $query);

            ?>
            <p>&nbsp;</p>
        </div>
    </div>
	<!-- Google Code for Sale Conversion Conversion Page -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 870165756;
	var google_conversion_language = "en";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "f7qICKqesmsQ_Nn2ngM";
	var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/870165756/?label=f7qICKqesmsQ_Nn2ngM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
<!--<form action="query.php" method="post" name="query_paygate_form">
				<label for="checksumResult">Checksum result</label>
				<p id="checksumResult" class="form-value"><?php echo (!$isValid ? 'The checksums do not match' : 'Checksums match OK'); ?></p>
<hr>
<label for="PAY_REQUEST_ID">Pay Request ID</label>
<p id="PAY_REQUEST_ID" class="form-value"><?php echo $data['PAY_REQUEST_ID']; ?></p>
<input type="hidden" name="PAY_REQUEST_ID" value="<?php echo $data['PAY_REQUEST_ID']; ?>" />
<br>
<label for="TRANSACTION_STATUS">Transaction Status</label>
<p id="TRANSACTION_STATUS" class="form-value"><?php echo $data['TRANSACTION_STATUS']; ?> (<?php echo $PayWeb3->getTransactionStatusDescription($data['TRANSACTION_STATUS']) ?>)</p>
<input type="hidden" name="TRANSACTION_STATUS" value="<?php echo $data['TRANSACTION_STATUS']; ?>" />
<br>
<label for="CHECKSUM" class="col-sm-3 text-right">Checksum</label>
<p id="CHECKSUM" class="form-value"><?php echo $data['CHECKSUM']; ?></p>
<br>

<input type="hidden" name="PAYGATE_ID" value="<?php echo $data['PAYGATE_ID']; ?>" />
<input type="hidden" name="REFERENCE" value="<?php echo $data['REFERENCE']; ?>" />
<input type="hidden" name="encryption_key" value="<?php echo $_SESSION['key']; ?>" />

</form>-->
</div>