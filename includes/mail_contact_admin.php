<?php 
$email_style = 	"<style type=\"text/css\">";
$email_style .=	"<!--";
$email_style .=	"body, width { width: 700px; }";
$email_style .=	"body,td,th { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #333333; }";
$email_style .=	"-->";
$email_style .=	"</style>";

$email_body = 	"<p>Attention, </p>";
$email_body .= 	"<br>&nbsp;</br>";
$email_body .= 	"<br>" . "A registration was submitted from ".$site_name;
$email_body .= 	"<br>&nbsp;</br>";
$email_body .= 	"<br>Name: ".$r_name."</br>";
$email_body .= 	"<br>Create Date: ".$create_date."</br>";
$email_body .= 	"<br>E-Mail: ".$r_email."</br>";
$email_body .= 	"<br>&nbsp;</br>";
$email_body .= 	"<br>&nbsp;</br>";
$email_body .= 	"<br>Regards</br>";
$email_body .= 	"<br>The Webmaster</br>";
?>