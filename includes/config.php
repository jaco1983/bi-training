<?php 
// config.php
// Maintain site parameters and global variables.

// Constants. Set parameters here

require_once("connection.php");

require_once("functions.php");

require_once("form_functions.php");

$paypal_email = 'info@blackboardbi.com';
//$paypal_email = 'support@blackboardbi.com';
$paypal_return = 'https://training.blackboardbi.com/index.php?id=115';
$paypal_cancel = 'https://training.blackboardbi.com/index.php?id=117';
$paypal_notify = 'https://training.blackboardbi.com/index.php?id=116';


$sql = "select * from aca_site_config limit 1";

$result = mysqli_query($connection, $sql);
while ($row = mysqli_fetch_array($result)) {
    $site_title = $row["site_title"];
    $site_keywords = $row["site_keywords"];
    $site_description = $row["site_description"];
    $site_name = $row["site_name"];
    $admin_email = $row["admin_email"];
    $support_email = $row["support_email"];
}

$sql2 = "select * from aca_invoice_config limit 1";

$result2 = mysqli_query($connection, $sql2);
while ($row = mysqli_fetch_array($result2)) {
    $company_name = $row["company_name"];
    $vat_number = $row["vat_number"];
    $address1 = $row["postal_address1"];
    $address2 = $row["postal_address2"];
    $address3 = $row["postal_address3"];
    $address4 = $row["postal_address4"];
}
	
	$today = date("Y-m-d");	
	
	$test = 0; // 1 to display test messages
	$test_aca = 0; // 1 to display test messages
	// for testing only
	if ( $test == 1) { $defaultemail = "stefan@menloweb.co.za"; } else { $defaultemail = ""; }
	if ( $test == 1) { $defaultpass = "123456"; } else { $defaultpass = ""; }
	if ( $test == 1) { $defaultpin = "12345"; } else { $defaultpin = ""; }
	
	$page_break_a = 5; // 
	$page_break_b = 10; // 
	$page_break_c = 15; // 
	$page_break_d = 20; // 
	$page_break_e = 25; // n
	$page_break_f = 100; // 
	$page_break_g = 50; // 
	
	
	$xDuplicateCheck = 0; // 1 = Avoids duplicate questions, 0/2 - Allow questions to be displayed multiple times.
	$xDisplayQuestionNumber = 0; // 1 - Display Question Number, 0/2 - Hide Question number during Test
	
	// PIN Code
	$x_pin = "369258";
	//if ($today > "2015-09-01") { $x_pin = "987654"; } // next pin
	$x_nommer = "42597";	
		
/*

Coding.

id
-------------------------------------------------
0		index
1		company
2		employee
3		customer

10		quotation
11		wbs
12		timesheet
13		site report
14		assignment

20		cv
21		skills
22		qualifications
23		leave form

30		calendar
31		cost center
32		country
33		cv_company
34		status
35		emp level
36		emp position
37		leave type
38		marital status



50		messages

60		term
61 		cv_print
		
	
Login
-------------------------------------------------
login		0	Login Form
			1	Validated
			3	Validate
			4	Forgot Password
			8	Incorrect

logout		1	Log out
			
set			0	list
			1	new
			2	insert
			3	edit one record
			4	update
			5	active
			7	delete
			8	view one record
			9	permanent delete	

status		0	new/created
			1	active
			2	suspended
			7	to be deleted

access		0	none
			1	customer (customer information only)
			2	
			3	contractor (cv, skill, qualifications, employee, timesheet, site report, contract/asignment, billing)
			5	employee (cv, skill, qualifications, employee, timesheet, site report, contract/asignment, billing)
			7	project admin (5+ wbs, quotation, customer)
			9	admin	(all)	


cat			0	employee
			2	$syrwk	WEEK FROM
			3			WEEK TO
			4	version
			5	status
			6

*/


?>

