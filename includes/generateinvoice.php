<?php

		
function generateInvoice2($reference,$type){


	ini_set("log_errors", 1);
	ini_set("error_log", "../php-error.log");
    require_once "lib/fpdf/fpdf.php";


  
    class PDF extends FPDF
    {
        // Page header
        function Header()
        {
            
        }

        // Page footer
        function Footer()
        {
            
        }
    }

	// Instanciation of inherited class
    global $connection;

    $sql2 = "select * from aca_invoice_config limit 1";

    $result2 = mysqli_query($connection, $sql2);
    while ($row = mysqli_fetch_array($result2)) {
        $company_name = $row["company_name"];
        $vat_number = $row["vat_number"];
        $address1 = $row["postal_address1"];
        $address2 = $row["postal_address2"];
        $address3 = $row["postal_address3"];
        $address4 = $row["postal_address4"];
    }
    $sql = "SELECT * FROM aca_transaction where paymentreference = '".$_SESSION["invpdf"]."'";

    $result_set = mysqli_query($connection, $sql);

    while ($row = mysqli_fetch_array($result_set)) {

        $s_details = unserialize($row["s_details"]);
        $s_transaction = unserialize($row["s_transaction"]);

        $arr = explode("\n", $s_details["postal"]);

        $quote_nr = $row["paymentreference"];
        $pdf = new PDF();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->Ln(10);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(95, 5, 'FROM:', 0, 0, 'L');
        $pdf->Cell(95, 5, 'TO:', 0, 1, 'L');
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->SetTextColor(102, 102, 102);
        $pdf->Cell(95, 5, $company_name, 0, 0, 'L');
		if($s_details["invoiceto"] == "company"){
        $pdf->Cell(95, 5, $row["ccompanyname"], 0, 1, 'L');
		} else {
        $pdf->Cell(95, 5, $row["cname"].' '.$row["csurname"], 0, 1, 'L');
		}

        $pdf->SetFont('Arial', 'B', 9);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(48, 5, 'VAT NO:', 0, 0, 'L');
        $pdf->SetFont('Arial', 'I', 9);
		$pdf->SetTextColor(102, 102, 102);
        $pdf->Cell(47, 5, ($vat_number != '' ? $vat_number : ''), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
		$pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(48, 5, 'CUSTOMER VAT NO:', 0, 0, 'L');
		$pdf->SetTextColor(102, 102, 102);
		$pdf->SetFont('Arial', '', 9);
        $pdf->Cell(47, 5, $s_details["vatnumber"], 0, 1, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(48, 5, 'POSTAL ADDRESS:', 0, 0, 'L');
		$pdf->SetFont('Arial', '', 9);
        $pdf->SetTextColor(102, 102, 102);
		$pdf->Cell(47, 5, $address1, 0, 0, 'L');
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(48, 5, 'POSTAL ADDRESS:', 0, 0, 'L');
		$pdf->SetFont('Arial', '', 9);
		$pdf->SetTextColor(102, 102, 102);
        $pdf->Cell(47, 5, (isset($arr[0]) ? $arr[0] : ''), 0, 1, 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->SetTextColor(102, 102, 102);
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, $address2, 0, 0, 'L');
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, (isset($arr[1]) ? $arr[1] : ''), 0, 1, 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, $address3, 0, 0, 'L');
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, (isset($arr[2]) ? $arr[2] : ''), 0, 1, 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, $address4, 0, 0, 'L');
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, (isset($arr[3]) ? $arr[3] : ''), 0, 1, 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, '', 0, 0, 'L');
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, (isset($arr[4]) ? $arr[4] : ''), 0, 1, 'L');


        $pdf->Ln(10);
        $pdf->SetFont('Arial', 'IB', 8);
        $pdf->SetTextColor(159, 159, 159);
        $pdf->SetFillColor(250, 250, 250);
        $pdf->SetDrawColor(222, 222, 222);
        $pdf->Cell(75, 5, 'Description', 'TB', 0, 'L', 1);
        $pdf->Cell(20, 5, 'Qty', 'TB', 0, 'L', 1);
        $pdf->Cell(25, 5, 'Excl. Price', 'TB', 0, 'L', 1);
        $pdf->Cell(25, 5, 'VAT %', 'TB', 0, 'L', 1);
        $pdf->Cell(25, 5, 'Excl. Total', 'TB', 0, 'L', 1);
        $pdf->Cell(25, 5, 'Incl. Total', 'TB', 1, 'L', 1);
		$pdf->SetFont('Arial', '', 8);
		$pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetDrawColor(255, 255, 255);
		$pdf->__currentY=$pdf->GetY();
		$pdf->MultiCell(75, 5, $s_transaction[0]["description"], 1, 'L',false);
		$pdf->SetXY($pdf->GetX()+75, $pdf->__currentY);
		$pdf->MultiCell(20, 5, '1', 1, 'L',false);
		$pdf->SetXY($pdf->GetX()+95, $pdf->__currentY);
		$pdf->MultiCell(25, 5, $_SESSION["currencySymbol"].' '.$s_transaction[0]["amount"], 1, 'L',false);
		$pdf->SetXY($pdf->GetX()+120, $pdf->__currentY);
		$pdf->MultiCell(25, 5, '0', 0, 'L',false);
		$pdf->SetXY($pdf->GetX()+145, $pdf->__currentY);
		$pdf->MultiCell(25, 5, $_SESSION["currencySymbol"].' '.$s_transaction[0]["amount"], 0, 'L',false);
		$pdf->SetXY($pdf->GetX()+170, $pdf->__currentY);
		$pdf->MultiCell(25, 5, $_SESSION["currencySymbol"].' '.$s_transaction[0]["amount"], 0, 'L',false);



    $out = $pdf->Output('/home/blackwrd/public_html/training/pdf/' . date("ymd") . '_'.$row["paymentreference"].'.pdf', 'F');

    }
//    $out = $pdf->Output('/home/blackwrd/public_html/training/pdf/' . date("ymd") . '_'.$row["paymentreference"].'.pdf', 'F');


}
generateInvoice2('151_Ferreira','paypal');

echo 'boo';
?>