<?php	
// assessment id = 27

if ($id == 27) {


	if ($set == 2) { // insert																
		if (isset($_POST['submit'])) {
			$description = trim(mysql_prep($_POST['description']));
			$effective_date = trim(mysql_prep($_POST['effective_date']));
			$expiry_date = trim(mysql_prep($_POST['expiry_date']));
			$question_note = trim(mysql_prep($_POST['question_note']));
			$question_01_text = trim(mysql_prep($_POST['question_01_text']));
			$question_02_text = trim(mysql_prep($_POST['question_02_text']));
			$question_03_text = trim(mysql_prep($_POST['question_03_text']));
			$question_04_text = trim(mysql_prep($_POST['question_04_text']));
			$question_05_text = trim(mysql_prep($_POST['question_05_text']));
			$question_06_text = trim(mysql_prep($_POST['question_06_text']));
			$question_07_text = trim(mysql_prep($_POST['question_07_text']));
			$question_08_text = trim(mysql_prep($_POST['question_08_text']));
			$question_09_text = trim(mysql_prep($_POST['question_09_text']));
			$question_10_text = trim(mysql_prep($_POST['question_10_text']));
			$create_date = $today;	
			$creator_id = $userx;
			$status = '1';	
		}
		
		if ($test_aca == 1) {
			$msg = "set 2 Post opt=".$opt."</br>";
			$msg .= "description: ".$description."</br>";
			$msg .= "effective_date: ".$effective_date."</br>";
			$msg .= "expiry_date: ".$expiry_date."</br>";
			$msg .= "question_0: ".$question_0."</br>";
			$msg .= "question_note: ".$question_note."</br>";
			$msg .= "create_date: ".$create_date."</br>";
			$msg .= "creator_id: ".$userx."</br>";
			echo $msg;
		}

		$query = "INSERT INTO aca_assessment_questions( ";
		$query .= " description, effective_date, expiry_date, question_note, "; 
		$query .= " question_01_text, question_02_text, question_03_text, question_04_text, question_05_text, "; 
		$query .= " question_06_text, question_07_text, question_08_text, question_09_text, question_10_text, "; 
		$query .= " create_date, status, creator_id ) ";
		$query .= " VALUES (   ";
		$query .= " '{$description}', '{$effective_date}', '{$expiry_date}', '{$question_note}',  ";  
		$query .= " '{$question_01_text}', '{$question_02_text}', '{$question_03_text}',  '{$question_04_text}', '{$question_05_text}', ";  
		$query .= " '{$question_06_text}', '{$question_07_text}', '{$question_08_text}',  '{$question_09_text}', '{$question_10_text}', "; 
		$query .= " '{$create_date}', '{$status}', {$userx} ) " ;
	
		$result = mysqli_query( $connection, $query);
		if (mysqli_affected_rows($connection) == 1) {	
			// Success	
			echo "<div class=\"alert alert-success\">";
				echo "Record Created";
			echo "</div>";
			$set = 0; // set action back to listing
			} else {
			echo "<div class=\"alert alert-danger\">";
				echo "Record creation failed";
			echo "</div>";
		}
		$set = 0; // set action back to form
	//} // end opt = 1
		
	} // end of set = 2
	


	if ($set == 4 && $key <> 0) { // update record													
		if (isset($_POST['submit'])) {
			$description = trim(mysql_prep($_POST['description']));
			$effective_date = trim(mysql_prep($_POST['effective_date']));
			$expiry_date = trim(mysql_prep($_POST['expiry_date']));
			$question_note = trim(mysql_prep($_POST['question_note']));
			$question_01_text = trim(mysql_prep($_POST['question_01_text']));
			$question_02_text = trim(mysql_prep($_POST['question_02_text']));
			$question_03_text = trim(mysql_prep($_POST['question_03_text']));
			$question_04_text = trim(mysql_prep($_POST['question_04_text']));
			$question_05_text = trim(mysql_prep($_POST['question_05_text']));
			$question_06_text = trim(mysql_prep($_POST['question_06_text']));
			$question_07_text = trim(mysql_prep($_POST['question_07_text']));
			$question_08_text = trim(mysql_prep($_POST['question_08_text']));
			$question_09_text = trim(mysql_prep($_POST['question_09_text']));
			$question_10_text = trim(mysql_prep($_POST['question_10_text']));
			$status = trim(mysql_prep($_POST['status']));
			$create_date = $today;	
			$creator_id = $userx;
		}
		
			
			if ($test_aca == 1) {
				$msg = "set 2 Post</br>";
				$msg .= "create_date: ".$create_date."</br>";
				$msg .= "status: ".$status."</br>";
				$msg .= "creator_id: ".$userx."</br>";
				//echo $msg;
			}
			
			// Update
			$query = "UPDATE aca_assessment_questions SET ";
			$query .= " description = '{$description}', ";
			$query .= " effective_date = '{$effective_date}', ";
			$query .= " expiry_date = '{$expiry_date}', ";
			$query .= " question_note = '{$question_note}', ";
			$query .= " question_01_text = '{$question_01_text}', ";
			$query .= " question_02_text = '{$question_02_text}', ";
			$query .= " question_03_text = '{$question_03_text}', ";
			$query .= " question_04_text = '{$question_04_text}', ";
			$query .= " question_05_text = '{$question_05_text}', ";
			$query .= " question_06_text = '{$question_06_text}', ";
			$query .= " question_07_text = '{$question_07_text}', ";
			$query .= " question_08_text = '{$question_08_text}', ";
			$query .= " question_09_text = '{$question_09_text}', ";
			$query .= " question_10_text = '{$question_10_text}', ";
			$query .= " status = '{$status}', ";
			$query .= " creator_id = '{$userx}' ";
			$query .= " WHERE assessment_questions_id = {$key} ";
			$query .= " LIMIT 1 ";
		
			$result = mysqli_query( $connection, $query);
			if (mysqli_affected_rows($connection) == 1) {	
				// Success	
				echo "<div class=\"alert alert-success\">";
					echo "The record was updated successfully.";
				echo "</div>";
				$set = 0; // set action back to listing
				} else {
				echo "<div class=\"alert alert-danger\">";
					echo "The record was not updated. Please try again. Make sure that all fields are completed.";
				echo "</div>";
				$set = 3; // set action back to form
			}
				
	} // end of set = 4 update


	if ($set == 3) { // edit																
			echo "<div class=\"panel panel-primary\"> "; 
		  echo "<div class=\"panel-heading\">Assessment Question Maintenance</div> "; 
		  echo "<div class=\"panel-body\">";
				
		$result_set = get_aca_assessment_questions_rec($key);
		while ($row = mysqli_fetch_array($result_set)) {			
			$key = $row["assessment_questions_id"];
			$description = $row["description"];
			$effective_date = $row["effective_date"];	
			$expiry_date = $row["expiry_date"];	
			$question_note = $row["question_note"];	
			$question_01_text = $row["question_01_text"];
			$question_02_text = $row["question_02_text"];
			$question_03_text = $row["question_03_text"];
			$question_04_text = $row["question_04_text"];
			$question_05_text = $row["question_05_text"];
			$question_06_text = $row["question_06_text"];
			$question_07_text = $row["question_07_text"];
			$question_08_text = $row["question_08_text"];
			$question_09_text = $row["question_09_text"];
			$question_10_text = $row["question_10_text"];			
			$status = $row["status"];
					
			$d_status = $status;
				$result_set74 = get_dim("status", $status);
				while ($row = mysqli_fetch_array($result_set74)) {
					$dim_key = $row["dim_key"]; 
					$d_status = $row["description"];
				}
		}
		
		echo "<form name=\"aca_assessment_questions\" method=\"post\" action=\"?id=".$id."&set=4&opt=" . $opt . "&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat5=".$cat5." \"enctype=\"multipart/form-data\">";



		echo "<div class=\"form-group\">";
		  echo "<label for=\"description\">Description:</label>";
		  echo "<input type=\"text\" name=\"description\" class=\"form-control reqd\" id=\"description\" placeholder=\"Enter description\" value=\"".$description."\">";
		echo "</div>";
		
		echo "<div class=\"form-group\">";
		  echo "<label for=\"effective_date\">Effective Date:</label>";
		  echo "<input type=\"date\" name=\"effective_date\" class=\"form-control reqd\" id=\"effective_date\" placeholder=\"Effective Date\" value=\"".$today."\">";
		echo "</div>";
		
		echo "<div class=\"form-group\">";
		  echo "<label for=\"expirye_date\">Expiry Date:</label>";
		  echo "<input type=\"date\" name=\"expiry_date\" class=\"form-control reqd\" id=\"expiry_date\" placeholder=\"Expirye Date\" value=\"".$today."\">";
		echo "</div>";

		echo "<div class=\"form-group\">";
		  echo "<label for=\"question_note\">Question Note:</label>";
		  echo "<input type=\"text\" name=\"question_note\" class=\"form-control reqd\" id=\"question_note\" placeholder=\"Question Note\" value=\"".$question_note."\">";
		echo "</div>";

		echo "<div class=\"form-group\">";
		  echo "<label for=\"question_01_text\">Question 1:</label>";
		  echo "<input type=\"text\" name=\"question_01_text\" class=\"form-control reqd\" id=\"question_01_text\" placeholder=\"Question Text\" value=\"".$question_01_text."\">";
		echo "</div>";

		echo "<div class=\"form-group\">";
		  echo "<label for=\"question_02_text\">Question 2:</label>";
		  echo "<input type=\"text\" name=\"question_02_text\" class=\"form-control reqd\" id=\"question_02_text\" placeholder=\"Question Text\" value=\"".$question_02_text."\">";
		echo "</div>";

		echo "<div class=\"form-group\">";
		  echo "<label for=\"question_03_text\">Question 3:</label>";
		  echo "<input type=\"text\" name=\"question_03_text\" class=\"form-control\" id=\"question_03_text\" placeholder=\"Question Text\" value=\"".$question_03_text."\">";
		echo "</div>";

		echo "<div class=\"form-group\">";
		  echo "<label for=\"question_04_text\">Question 4:</label>";
		  echo "<input type=\"text\" name=\"question_04_text\" class=\"form-control\" id=\"question_04_text\" placeholder=\"Question Text\" value=\"".$question_04_text."\">";
		echo "</div>";

		echo "<div class=\"form-group\">";
		  echo "<label for=\"question_05_text\">Question 5:</label>";
		  echo "<input type=\"text\" name=\"question_05_text\" class=\"form-control\" id=\"question_05_text\" placeholder=\"Question Text\" value=\"".$question_05_text."\">";
		echo "</div>";

		echo "<div class=\"form-group\">";
		  echo "<label for=\"question_06_text\">Question 6:</label>";
		  echo "<input type=\"text\" name=\"question_06_text\" class=\"form-control\" id=\"question_06_text\" placeholder=\"Question Text\" value=\"".$question_06_text."\">";
		echo "</div>";

		echo "<div class=\"form-group\">";
		  echo "<label for=\"question_07_text\">Question 7:</label>";
		  echo "<input type=\"text\" name=\"question_07_text\" class=\"form-control\" id=\"question_07_text\" placeholder=\"Question Text\" value=\"".$question_07_text."\">";
		echo "</div>";

		echo "<div class=\"form-group\">";
		  echo "<label for=\"question_08_text\">Question 8:</label>";
		  echo "<input type=\"text\" name=\"question_08_text\" class=\"form-control\" id=\"question_08_text\" placeholder=\"Question Text\" value=\"".$question_08_text."\">";
		echo "</div>";

		echo "<div class=\"form-group\">";
		  echo "<label for=\"question_09_text\">Question 9:</label>";
		  echo "<input type=\"text\" name=\"question_09_text\" class=\"form-control\" id=\"question_09_text\" placeholder=\"Question Text\" value=\"".$question_09_text."\">";
		echo "</div>";

		echo "<div class=\"form-group\">";
		  echo "<label for=\"question_10_text\">Question 10:</label>";
		  echo "<input type=\"text\" name=\"question_10_text\" class=\"form-control\" id=\"question_10_text\" placeholder=\"Question Text\" value=\"".$question_10_text."\">";
		  
		echo "<div class=\"form-group\">";
		  echo "<label for=\"status\">Status</label>";
			echo "<select name=\"status\" class=\"form-control reqd\" onchange=\"showTool(this.value)\" id=\"status\">";					
						  echo "<option value=\"".$status."\">".$d_status."</option>";
								  $result_set9 = get_dim_list("status");
								  while ($row = mysqli_fetch_array($result_set9)) {
									  $dim_key = $row["dim_key"]; 
									  $display_name = $row["description"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }
								  echo "</select>";
		echo "</div>";
										
			// buttons
			$update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Submit</button>";			
			$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";
			
		  echo "<p align=\"center\">" . $update . " " . $cancel . "</p>";
		  
		echo "</form>";			
		
		
		
		
		  echo "</div> "; 
	   echo " </div> "; 	
		
		
		echo "<p>&nbsp;</p>";	
		
	} // end of edit




	if ($set == 0) {										

		// get record count
		$p_records = 0;
		$result_setA = get_aca_assessment_questions_count(99, 0); // status 1 = valid, access 0 for all
		while ($row = mysqli_fetch_array($result_setA)) {
			$p_records = $row["record_count"]; 
		}
		$page_break = $page_break_d;
		$anchor = "x";
		$p_link = "?id=".$id."&opt=".$opt."&cat2=0&cat3=0&cat6=".$cat6."&key=".$key."&cat=".$cat."&cat4=".$cat4."&set=".$set;
		$p = page_nav_last($page, $page_break, $p_records, $p_link, $anchor);
		$n = "<a href=\"?id=".$id."&set=1&key=".$key."\">New</a>";
		$c = " ";
		$n =  "<button name=\"NewBut\" type=\"button\" value=\"New\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=1&opt=0&key=".$key."');return document.MM_returnValue\"/>New</button>&nbsp;&nbsp;";

        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Assessment Questions</h2><div class=\"colored-line-left\"></div></div>";
        echo '<div class="clearfix"></div>';
        echo '<div class="row content-pad">';
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";

		echo "<table class=\"table table-striped\">";
		echo "<thead>";
        echo '<tr><th colspan="6" style="background:#58595b;color:#FFFFFF;line-height:2em;">List of Assesments<div style="float:right;">' . $n.' '.$p . '</div></th></tr>';
		echo "<tr>";
			echo "<th>ID</th>";
			echo "<th>Description</th>";
			echo "<th>Effective Date</th>";
			echo "<th>Expiry Date</th>";
			echo "<th>Status</th>";
			echo "<th></th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		// get data
		$npage = $page;
		$result_set = get_aca_assessment_questions_list($npage, $page_break, 99);
		while ($row = mysqli_fetch_array($result_set)) {		
			$key = $row["assessment_questions_id"];
			$description = $row["description"];
			$effective_date = $row["effective_date"];	
			$expiry_date = $row["expiry_date"];	
			$question_note = $row["question_note"];	
			$question_01_text = $row["question_01_text"];
			$question_02_text = $row["question_02_text"];
			$question_03_text = $row["question_03_text"];
			$question_04_text = $row["question_04_text"];
			$question_05_text = $row["question_05_text"];
			$question_06_text = $row["question_06_text"];
			$question_07_text = $row["question_07_text"];
			$question_08_text = $row["question_08_text"];
			$question_09_text = $row["question_09_text"];
			$question_10_text = $row["question_10_text"];			
			$status = $row["status"];
			
			$d_status = $status;
				$result_set74 = get_dim("status", $status);
				while ($row = mysqli_fetch_array($result_set74)) {
					$dim_key = $row["dim_key"]; 
					$d_status = $row["description"];
				}
			
			$un = "<a href=\"?id=".$id."&set=3&opt=0&login=1&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&key=".$key."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$description."</a>";
			
		echo "<tr>";
			echo "<td>".$key."</td>";
			echo "<td>".$un."</td>";
			echo "<td>".$effective_date."</td>";
			echo "<td>".$expiry_date."</td>";
			echo "<td>".$d_status."</td>";
			echo "<td><a href=\"?id=".$id."&set=3&opt=0&login=1&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&key=".$key."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\" class=\"btn\">Edit</a></td>";
		echo "</tr>";
		}	
		echo "<tr>";
			echo "<td align=\"left\" colspan=\"4\">".$c."</td>"; // close, new, page nav
			echo "<td align=\"right\" colspan=\"4\">".$n."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;".$p."</td>";
		echo "</tr>";


		echo "</tbody>";
		echo "</table>";
		  echo "</div> "; 
	   echo " </div> "; 	
	
		
	}


	if ($set == 1) { // create new		

        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Assessment Questions</h2><div class=\"colored-line-left\"></div></div>";
        echo '<div class="clearfix"></div>';
        echo '<div class="row content-pad">';
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";
				
		
		echo "<form name=\"aca_assessment_questions\" method=\"post\" action=\"?id=".$id."&set=2&opt=" . $opt . "&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat5=".$cat5." \"enctype=\"multipart/form-data\">";

        echo "<table class=\"table table-striped\">";
        echo "<thead>";
        echo '<tr><th colspan="5" style="background:#58595b;color:#FFFFFF;">Add Assesment</th></tr>';
        echo "</thead>";
        echo "<tbody>";

		echo "<tr>";
		  echo "<td>Description:</td>";
		  echo "<td><input type=\"text\" name=\"description\" class=\"form-control reqd\" id=\"description\" placeholder=\"Enter description\" value=\"\"></td>";
		echo "</tr>";
		
		echo "<tr>";
		  echo "<td>Effective Date:</td>";
		  echo "<td><input type=\"date\" name=\"effective_date\" class=\"form-control reqd\" id=\"effective_date\" placeholder=\"Effective Date\" value=\"".$today."\"></td>";
		echo "</tr>";
		
		echo "<tr>";
		  echo "<td>Expiry Date:</td>";
		  echo "<td><input type=\"date\" name=\"expiry_date\" class=\"form-control reqd\" id=\"expiry_date\" placeholder=\"Expirye Date\" value=\"".$today."\"></td>";
		echo "</tr>";

		echo "<tr>";
		  echo "<td>Question Note:</td>";
		  echo "<td><input type=\"text\" name=\"question_note\" class=\"form-control reqd\" id=\"question_note\" placeholder=\"Question Note\" value=\"\"></td>";
		echo "</tr>";

		echo "<tr>";
		  echo "<td>Question 1:</td>";
		  echo "<td><input type=\"text\" name=\"question_01_text\" class=\"form-control reqd\" id=\"question_01_text\" placeholder=\"Question Text\" value=\"\"></td>";
		echo "</tr>";

		echo "<tr>";
		  echo "<td>Question 2:</td>";
		  echo "<td><input type=\"text\" name=\"question_02_text\" class=\"form-control reqd\" id=\"question_02_text\" placeholder=\"Question Text\" value=\"\"></td>";
		echo "</tr>";

		echo "<tr>";
		  echo "<td>Question 3:</td>";
		  echo "<td><input type=\"text\" name=\"question_03_text\" class=\"form-control\" id=\"question_03_text\" placeholder=\"Question Text\" value=\"\"></td>";
		echo "</tr>";

		echo "<tr>";
		  echo "<td>Question 4:</td>";
		  echo "<td><input type=\"text\" name=\"question_04_text\" class=\"form-control\" id=\"question_04_text\" placeholder=\"Question Text\" value=\"\"></td>";
		echo "</tr>";

		echo "<tr>";
		  echo "<td>Question 5:</td>";
		  echo "<td><input type=\"text\" name=\"question_05_text\" class=\"form-control\" id=\"question_05_text\" placeholder=\"Question Text\" value=\"\"></td>";
		echo "</tr>";

		echo "<tr>";
		  echo "<td>Question 6:</td>";
		  echo "<td><input type=\"text\" name=\"question_06_text\" class=\"form-control\" id=\"question_06_text\" placeholder=\"Question Text\" value=\"\"></td>";
		echo "</tr>";

		echo "<tr>";
		  echo "<td>Question 7:</td>";
		  echo "<td><input type=\"text\" name=\"question_07_text\" class=\"form-control\" id=\"question_07_text\" placeholder=\"Question Text\" value=\"\"></td>";
		echo "</tr>";

		echo "<tr>";
		  echo "<td>Question 8:</td>";
		  echo "<td><input type=\"text\" name=\"question_08_text\" class=\"form-control\" id=\"question_08_text\" placeholder=\"Question Text\" value=\"\"></td>";
		echo "</tr>";

		echo "<tr>";
		  echo "<td>Question 9:</td>";
		  echo "<td><input type=\"text\" name=\"question_09_text\" class=\"form-control\" id=\"question_09_text\" placeholder=\"Question Text\" value=\"\"></td>";
		echo "</tr>";

		echo "<tr>";
		  echo "<td>Question 10:</td>";
		  echo "<td><input type=\"text\" name=\"question_10_text\" class=\"form-control\" id=\"question_10_text\" placeholder=\"Question Text\" value=\"\"></td>";
		echo "</tr>";
										
			// buttons
			$update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Submit</button>";			
			$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";
			
		  echo "<tr><td colspan=\"2\" class=\"text-center\">" . $update . " " . $cancel . "</td></tr>";
		  echo "</tbody>";
		  echo "</table>";
		echo "</form>";
		
		
		
		
		  echo "</div> "; 
	   echo " </div> "; 	
		
		
		echo "<p>&nbsp;</p>";	
		
	} // end of set = 1

	
	
} // end of assessment_questions id = 27
?>
 