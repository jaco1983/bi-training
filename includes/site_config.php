<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>Site Configuration</h2><div class="colored-line-left"></div></div>
<div class="clearfix"></div>
<div class="row content-pad">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="siteconfig1">
                    <table class="table table-striped">
                        <tr>
                            <th colspan="2" style="background:#58595b;color:#FFFFFF;">Site Configuration</th>
                        </tr>
                        <?php

                        $query = "select * from aca_site_config";

                        $result = mysqli_query( $connection, $query);

                        while ($row = mysqli_fetch_array($result)) {
                            echo '<tr>
                                <td><strong>Site Title</strong></td>
                                <td>'.$row["site_title"].'</td>
                            </tr>
                            <tr>
                                <td><strong>Site Keywords</strong></td>
                                <td>'.$row["site_keywords"].'</td>
                            </tr>
                            <tr>
                                <td><strong>Site Description</strong></td>
                                <td>'.$row["site_description"].'</td>
                            </tr>
                            <tr>
                                <td><strong>Site Name</strong></td>
                                <td>'.$row["site_name"].'</td>
                            </tr>
                            <tr>
                                <td><strong>Administration Email</strong></td>
                                <td>'.$row["admin_email"].'</td>
                            </tr>
                            <tr>
                                <td><strong>Support Email</strong></td>
                                <td>'.$row["support_email"].'</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="2"><input type="button" class="btn btn-default col-sm-2 col-sm-offset-5" onclick="editConfig()" value="Edit" /></td>
                            </tr>';
                        }
                        ?>
                    </table>
        </div>
        <div id="siteconfig2">
                        <table class="table table-striped">
                            <tr>
                                <th colspan="2" style="background:#58595b;color:#FFFFFF;">Edit Site Configuration</th>
                            </tr>
                            <?php

                            $query = "select * from aca_site_config";

                            $result = mysqli_query( $connection, $query);

                            while ($row = mysqli_fetch_array($result)) {
                                echo '<tr>
                                    <td><strong>Site Title</strong></td>
                                    <td><input type="text" name="site_title" class="form-control" id="site_title" value="'.$row["site_title"].'" /></td>
                                </tr>
                                <tr>
                                    <td><strong>Site Keywords</strong></td>
                                    <td><input type="text" name="site_keywords" class="form-control" id="site_keywords" value="'.$row["site_keywords"].'" /></td>
                                </tr>
                                <tr>
                                    <td><strong>Site Description</strong></td>
                                    <td><textarea cols="30" rows="5" class="form-control" id="site_description">'.$row["site_description"].'</textarea></td>
                                </tr>
                                <tr>
                                    <td><strong>Site Name</strong></td>
                                    <td><input type="text" name="site_name" class="form-control" id="site_name"  value="'.$row["site_name"].'" /></td>
                                </tr>
                                <tr>
                                    <td><strong>Administration Email</strong></td>
                                    <td><input type="text" name="admin_email" class="form-control" id="admin_email" value="'.$row["admin_email"].'" /></td>
                                </tr>
                                <tr>
                                    <td><strong>Support Email</strong></td>
                                    <td><input type="text" name="support_email" class="form-control" id="support_email" value="'.$row["support_email"].'" /></td>
                                </tr>
                                <tr>
                                <td></td>
                                <td></td>
                            </tr>
                                <tr>
                                <td colspan="2"><input type="button" class="btn btn-default col-sm-2 col-sm-offset-5" onclick="saveConfig()" value="Save" /></td>
                            </tr>';
                            }
                            ?>
                        </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#siteconfig1").show();
        $("#siteconfig2").hide();
    })
</script>