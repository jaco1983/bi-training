<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/08/26
 * Time: 11:09 PM
 */

if($id == 2){
	
    require_once("connection.php");

    require_once("functions.php");

    require_once("form_functions.php");

    require_once("config.php");

    $sql = "select * from aca_exam where exam_id = '".$key."' limit 1";

    $result = mysqli_query($connection, $sql);

	

        echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>Exam Information</h2><div class="colored-line-left"></div></div>
          <div class="clearfix"></div> ';
        echo '<div class="row content-pad">

                    <div class="col-sm-12">';
                        while ($row = mysqli_fetch_array($result)) {
							
							if(logged_in()){
								if($row["trial"] == '1'){
									$link = '?id=118&login=1&opt=1&set=0&key='.$row["exam_id"];
								} else {
									$link = '?id=21&cat=0&login=1&set=11&opt=0&key='.$row["exam_id"];
								}
							} else {
								$link = '?id=106&key=' . $row["exam_id"] . '&step=1';
							}
							
                            echo '
                                  <div class="form-group">
                                    <div class="col-sm-3">
                                        <img src="'.$row["exam_image"].'" style="width:100%;" />
										<p>&nbsp;</p>
										<a href="'.$link.'" class="btn btn-success col-sm-12">Subscribe</a>
                                    </div>
                                    <div class="col-sm-9">
                                      <div class="form-group" style="border-bottom:1px solid #f3f3f3;padding-bottom:15px !important;">
                                            <div class="col-sm-3"><strong>Code:</strong></div>
                                            <div class="col-sm-9">'.$row["code"].'</div>
                                            <div class="clearfix"></div>
                                      </div>
                                      <div class="form-group" style="border-bottom:1px solid #f3f3f3;padding-bottom:15px !important;">
                                            <div class="col-sm-3"><strong>Name:</strong></div>
                                            <div class="col-sm-9">'.$row["description"].'</div>
                                            <div class="clearfix"></div>
                                      </div>
                                      <div class="form-group" style="border-bottom:1px solid #f3f3f3;padding-bottom:15px !important;">
                                            <div class="col-sm-3"><strong>Description:</strong></div>
                                            <div class="col-sm-9">'.$row["long_description"].'</div>
                                            <div class="clearfix"></div>
                                      </div>
                                      <div class="form-group" style="border-bottom:1px solid #f3f3f3;padding-bottom:15px !important;">
                                            <div class="col-sm-3"><strong>Exam Length:</strong></div>
                                            <div class="col-sm-9">'.$row["exam_length"].'</div>
                                            <div class="clearfix"></div>
                                      </div>
                                      <div class="form-group" style="border-bottom:1px solid #f3f3f3;padding-bottom:15px !important;">
                                            <div class="col-sm-3"><strong>Pass Score:</strong></div>
                                            <div class="col-sm-9">'.$row["pass_score"].'</div>
                                            <div class="clearfix"></div>
                                      </div>
                                      <div class="form-group" style="border-bottom:1px solid #f3f3f3;padding-bottom:15px !important;">
                                            <div class="col-sm-3"><strong>Duration:</strong></div>
                                            <div class="col-sm-9">'.$row["duration"].'</div>
                                            <div class="clearfix"></div>
                                      </div>
                                      <div class="form-group" style="border-bottom:1px solid #f3f3f3;padding-bottom:15px !important;">
                                            <div class="col-sm-3"><strong>Topic Areas:</strong></div>
                                            <div class="col-sm-9">'.$row["topic_areas"].'</div>
                                            <div class="clearfix"></div>
                                      </div>
                                      <div class="form-group" style="border-bottom:1px solid #f3f3f3;padding-bottom:15px !important;">
                                            <div class="col-sm-3"><strong>Version:</strong></div>
                                            <div class="col-sm-9">'.$row["versions"].'</div>
                                            <div class="clearfix"></div>
                                      </div>
                                      <div class="form-group">
                                            <div class="col-sm-3"><strong>Cost:</strong></div>
                                            <div class="col-sm-9">'.$_SESSION["currencySymbol"].' '.formatMoney(($_SESSION["currencyconverter"] * $row["cost"]),2).'</p></div>
                                            <div class="clearfix"></div>
                                      </div>
                                    </div>

                                      </div>';
                        }
                    echo '</div>
              </div>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>';

//echo '<div class="col-sm-9">'.formatMoney(currencyConverter("USD","ZAR",$row["cost"]),2).'</p></div>';

/*echo '<style>
ul{
	padding:0px 13px !important;
}
</style>';*/
}
