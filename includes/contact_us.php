<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/08/23
 * Time: 9:17 AM
 */
if($id == 3){

    echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>Contact Us</h2><div class="colored-line-left"></div></div>
          <div class="clearfix"></div>
          <div class="row content-pad">

          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 contactsubmit">
            <div class="form-group">
                <label for="fname" class="control-label col-sm-2">Name:<font color="#F00">*</font></label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="fname" id="cfname" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label for="email" class="control-label col-sm-2">Email:<font color="#F00">*</font></label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="email" id="cemail" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label for="subject" class="control-label col-sm-2">Subject:<font color="#F00">*</font></label>
                <div class="col-sm-12">
                    <select class="form-control placeholder" name="subject" id="csubject">
                        <option value="">Please Select</option>
                        <option value="Exam Request">Exam Request</option>
                        <option value="Support">Support</option>
                        <option value="Testimonial">Testimonial</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label for="reference" class="control-label col-sm-2">Reference:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="reference" id="creference" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-sm-2">Message:<font color="#F00">*</font></label>
                <div class="col-sm-12">
                    <textarea class="form-control" name="message" id="cmessage" cols="30" rows="8"></textarea>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="button" name="contactsubmit" id="contactsubmit" value="Send" class="btn col-sm-12" />

                </div>
                <div class="clearfix"></div>
            </div>
            </div>
			<div class="col-xs-12 col-sm-7 col-lg-7">
				<table class="table">
				<tbody>
				<tr>
				<td>
				<h5><a href="https://www.blackboardbi.com/blackboard-business-intelligence/"><strong>Blackboard BI</strong></a></h5>
				</td>
				<td>
				<h5><a href="https://www.blackboardbs.com/"><strong>Blackboard Business Solutions</strong></a></h5>
				</td>
				</tr>
				<tr>
				<td valign="center"><a href="mailto:stefan@blackboardbi.com">stefan@blackboardbi.com</a></td>
				<td valign="center"><a href="mailto:nico@blackboardbs.com">nico@blackboardbs.com</a></td>
				</tr>
				<tr>
				<td valign="center">+27 86 542 6801</td>
				<td valign="center">+27 86 542 6801</td>
				</tr>
				<tr>
				<td valign="center">+27 71 402 9622</td>
				<td valign="center">+27 83 284 8212</td>
				</tr>
				</tbody>
				</table>
				<table class="table">
				<tbody>
				<tr>
				<td>
				<h5><strong>Other Queries</strong></h5>
				</td>
				</tr>
				<tr>
				<td valign="center"><a href="mailto:info@blackboardbi.com">info@blackboardbi.com</a></td>
				</tr>
				<tr>
				<td valign="center"><a href="mailto:support@blackboardbi.com">support@blackboardbi.com</a></td>
				</tr>
				</tbody>
				</table>
			</div>
          </div>
		  ';
}