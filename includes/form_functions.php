<?php 
// form_functions.php
// global form functions

function check_required_fields($required_array) {
	$field_errors = array();
	foreach($required_array as $fieldname) {
		if (!isset($_POST[$fieldname]) || (empty($_POST[$fieldname]))) { 		
			$field_errors[] = $fieldname;
		}
	}		
	return $field_errors;
}

function check_max_field_lengths($field_length_array) {
	$field_errors = array();
	foreach($field_length_array as $fieldname => $maxlength ) {
		if (strlen(trim(mysql_prep($_POST[$fieldname]))) > $maxlength) {
			$field_errors[] = $fieldname;
		}
	}		
	return $field_errors;
}

function display_errors($error_array) {
	echo "<p class=\"errors\">";
	echo "Please review the following fields:<br />";
	foreach($error_array as $error){
		echo " - " . $error . "<br />";
	}
	echo "<p/>";
}
	

/*\\\\\ date          /////*/ 
function fmonth1($date, $lang) {
	$value = "";
	$m = substr($date,5,2);
		if($m == 1) { $value = "January"; }
		if($m == 2) { $value = "February"; }
		if($m == 3) { $value = "March"; }
		if($m == 4) { $value = "April"; }
		if($m == 5) { $value = "May"; }
		if($m == 6) { $value = "June"; }
		if($m == 7) { $value = "July"; }
		if($m == 8) { $value = "August"; }
		if($m == 9) { $value = "September"; }
		if($m == 10) { $value = "October"; }
		if($m == 11) { $value = "November"; }
		if($m == 12) { $value = "December"; }
	return $value;
}	


// robot colours
function rc($x, $t) {
	$value = "";
	if ($t == 1) {
		if($x >= 30) { $value = "g"; }
		if($x < 10) { $value = "r"; }
		if($x < 30 && $x >= 10) { $value = "y"; }
	}
	if ($t == 2) {
		if($x >= 10) { $value = "g"; }
		if($x < 5) { $value = "r"; }
		if($x < 10 && $x >= 5) { $value = "y"; }
	}	
	return $value;
}	

?>