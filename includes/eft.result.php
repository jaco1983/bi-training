<?php

$query = "select a.*,b.* from aca_exam a inner join aca_user_exam b on a.exam_id=b.exam_id where a.exam_id = '".$_SESSION["payeeDetails"]["examid"]."'";

$result = mysqli_query( $connection, $query);

while ($row = mysqli_fetch_array($result)) {
    $examdesc = $row["code"];
    $expire = $row["expiry_date"];
}




$pageWasRefreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0';

if($pageWasRefreshed ) {
    //do something because page was refreshed;
} else {

    /*$query2 = "select a.*,b.* from aca_exam a inner join aca_user_exam b on a.exam_id=b.exam_id where a.exam_id = '" . $_SESSION["payeeDetails"]["examid"] . "' limit 1";

    $result2 = mysqli_query($connection, $query2);

    while ($row = mysqli_fetch_array($result2)) {

        include 'lib/swift_required.php';

        $msgcontent = '';
        $output = '';

        $message = Swift_Message::newInstance();
        $message->setSubject("Beyond Analysis Exam Subscription");
        $message->setFrom(array($admin_email => $admin_email));
        $message->setTo(array($_SESSION["payeeDetails"]["email"] => $_SESSION["payeeDetails"]["firstname"] . " " . $_SESSION["payeeDetails"]["surname"]));
        $message->setBcc(array("jaco@itechnologiessolutions.co.za" => "Jaco Ferreira"));
        $message->setBody('This email requires HTML to view.');

        $msgcontent .= "<p>Dear " . $_SESSION["payeeDetails"]["firstname"] . " " . $_SESSION["payeeDetails"]["surname"] . ", </p>";

        $msgcontent .= "<p>&nbsp;</p>";

        $msgcontent .= "Thank you for subscribing to the <strong>" . $examdesc . "</strong> exam.<br />";
        $msgcontent .= "<br />";
        $msgcontent .= "You will be notified and gain access to the exam once your EFT payment has been received.<br />";
        $msgcontent .= "<br />";
        $msgcontent .= "Please use <strong>" . $_SESSION["paygateDetails"]["reference"] . "</strong> as reference and email your proof of payment to <strong>" . $admin_email . "</strong>.<br />";
        $msgcontent .= '<br />';
        $msgcontent .= '<strong>Banking Details:</strong><br />';
        $msgcontent .= 'Bank : First National Bank<br />';
        $msgcontent .= 'Branch : FNB Private Clients Pretoria (South Africa)<br />';
        $msgcontent .= 'Branch Code : 259-745<br />';
        $msgcontent .= 'Account No : 62624724375<br />';
        $msgcontent .= 'Account Type : Cheque Account<br />';
        $msgcontent .= 'Swift code : FIRNZAJJ<br />';
        $msgcontent .= '<br />';
        $msgcontent .= 'For any queries please send an email to <a href="mailto:' . $support_email . '">' . $support_email . '</a>.<br />';
        $msgcontent .= '<br />';
        $msgcontent .= "<strong>Regards</strong><br />";
        $msgcontent .= "The Webmaster";

        $pdf = generateInvoice($_SESSION["paygateDetails"]["reference"], "cc");

        $message->addPart($msgcontent, 'text/html');
        $attachment = Swift_Attachment::fromPath('C:/Websites/Beyond-Training/pdf/' . date("ymd") . '_' . $_SESSION["paygateDetails"]["reference"] . '.pdf', 'application/pdf');
        $message->attach($attachment);

        $transport = Swift_MailTransport::newInstance();
        $mailer = Swift_Mailer::newInstance($transport);
        $result = $mailer->send($message);


    }*/
}
?>


<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"><img src="images/logo_beyondtraining_200x62.png" class="img-responsive" /></div>
<div class="col-xs-12 col-sm-8 col-md-4 col-lg-4"><h2>Confirm Subscription</h2></div>
<div class="clearfix"></div>
<div class="row content-pad">
    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
<div class="panel panel-primary">

<div class="panel-heading">Subscription Summary</div>

<div class="panel-body">
    <p><strong>Transaction Complete</strong><br />
    <br />
    Thank you for subscribing to the <strong><?php echo $examdesc; ?></strong> exam.<br />
    <br />
    <strong>You will be notified and gain access to the exam once your EFT payment has been received.<br />
    <br />
    Please use <strong><?php echo $_SESSION["paygateDetails"]["reference"]; ?></strong> as reference and email your proof of payment to <strong><?php echo $admin_email; ?></strong>.</strong><br />
    <br />
    <strong>Banking Details:</strong><br />
    Bank : First National Bank<br />
    Branch : FNB Private Clients Pretoria (South Africa)<br />
    Branch Code : 259-745<br />
    Account No : 62624724375<br/>
    Account Type : Cheque Account<br />
    Swift code : FIRNZAJJ<br />
    <br />
    For any queries please send an email to <a href="mailto:<?php echo $support_email; ?>"><?php echo $support_email; ?></a>.<br />
    <br />
        <a href="?id=21&login=1&opt=0&key=0&set=0" class="btn btn-default">Back to Exam List</a>&nbsp;<a href="pdf/<?php echo date("ymd").'_'.$_SESSION["paygateDetails"]["reference"]; ?>.pdf" class="btn btn-default" target="_blank">View Invoice</a>
</div>
</div>
</div>
</div>
<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 870165756;
	var google_conversion_language = "en";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "f7qICKqesmsQ_Nn2ngM";
	var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/870165756/?label=f7qICKqesmsQ_Nn2ngM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>