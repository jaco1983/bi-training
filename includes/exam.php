<?php
// user id = 30

if ($id == 30) {


    if ($set == 2) { // insert
        if (isset($_POST['submit'])) {
            $target = '';
            $code = mysqli_real_escape_string($connection,$_POST['code']);
            $description = mysqli_real_escape_string($connection,$_POST['description']);
            $longdesc = mysqli_real_escape_string($connection,$_POST["long-description"]);
            $examlength = mysqli_real_escape_string($connection,$_POST["exam-length"]);
            $passscore = mysqli_real_escape_string($connection,$_POST["pass-score"]);
            $duration = mysqli_real_escape_string($connection,$_POST["duration"]);
            $topicareas = mysqli_real_escape_string($connection,$_POST["topic-areas"]);
            $cost = mysqli_real_escape_string($connection,$_POST["cost"]);
            $versions = mysqli_real_escape_string($connection,$_POST['versions']);
            $create_date = $today;
            $creator_id = $userx;
            if($_POST["online"] == "on"){
                $online = '1';
            } else {
                $online = '0';
            }
            if($_POST["pdf"] == "on"){
                $pdf = '1';
                $pdf_questions = $_POST["nrquestions"];
            } else {
                $pdf = '0';
                $pdf_questions = '0';
            }
            $status = '1';
            if($_POST["repq"] == "on"){
                $repq = 1;
            } else {
                $repq = 0;
            }
			if($_POST["trial"] == "on"){
                $trial = 1;
            } else {
                $trial = 0;
            }
            if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
                //This is the directory where images will be saved
                $target = "images/";
                $target = $target . basename($_FILES['filename']['name']);

                //This gets all the other information from the form
                $Filename = basename($_FILES['filename']['name']);

                //Writes the Filename to the server
                if (move_uploaded_file($_FILES['filename']['tmp_name'], $target)) {
                    $image = $target;
                } else {
                    //Gives and error if its not
                    echo '<div class="alert alert-danger">Sorry, there was a problem uploading the image.</div>';
                }
            }

            if(isset($_FILES['pdffilename']['name']) && $_FILES['pdffilename']['name'] != '') {
                //This is the directory where images will be saved
                $target1 = "pdf/exams/";
                $target1 = $target1 . basename($_FILES['pdffilename']['name']);

                //This gets all the other information from the form
                $Filename = basename($_FILES['pdffilename']['name']);

                //Writes the Filename to the server
                if (move_uploaded_file($_FILES['pdffilename']['tmp_name'], $target1)) {
                    $pdffile = $target1;
                } else {
                    //Gives and error if its not
                    echo '<div class="alert alert-danger">Sorry, there was a problem uploading the pdf file.</div>';
                }
            }
        }

        if ($test_aca == 1) {
            $msg = "set 2 Post opt=".$opt."</br>";
            $msg .= "create_date: ".$create_date."</br>";
            $msg .= "creator_id: ".$userx."</br>";
            echo $msg;
        }

        $query = "INSERT INTO aca_exam(exam_image, code, description, long_description, exam_length, pass_score, duration, topic_areas, versions, create_date, cost, online, pdf, pdf_nr_questions, status, creator_id, report_question, trial ) VALUES ('{$target}','{$code}','{$description}', '{$longdesc}','{$examlength}','{$passscore}','{$duration}','{$topicareas}','{$versions}', '{$create_date}', '{$cost}', '{$online}','{$pdf}','{$pdf_questions}', '{$status}', {$userx}, '".$repq."', '".$trial."' ) " ;

        $result = mysqli_query( $connection, $query);
        $last_id = mysqli_insert_id($connection);
        if (mysqli_affected_rows($connection) == 1) {
            // Success
            if(isset($pdffile) && $pdffile != ''){
                $pdfquery = "INSERT INTO aca_exam_pdf(exam_id,pdf_file,date_uploaded) values ('".$last_id."','".$pdffile."','".date("Y-m-d")."')";
                $result = mysqli_query( $connection, $pdfquery);
            }
            echo "<div class=\"alert alert-success\">";
            echo "Record Created";
            echo "</div>";
            $set = 0; // set action back to listing
        } else {
            echo "<div class=\"alert alert-danger\">";
            echo "Record creation failed";
            echo "</div>";
        }
        $set = 0; // set action back to form
        //} // end opt = 1

    } // end of set = 2



    if ($set == 4 && $key <> 0) { // update record
        if (isset($_POST['submit'])) {

            if((isset($_FILES['pdffilename']['name']) && $_FILES['pdffilename']['name'] != '') && ($_POST["currentpdf"] != '')){
                $pdfquery2 = "delete from aca_exam_pdf where id = '".$_POST["pdfid"]."'";
                $pdfresult2 = mysqli_query($connection, $pdfquery2);
                unlink($_POST["currentpdf"]);
            }


            $code = mysqli_real_escape_string($connection,$_POST['code']);
            $description = mysqli_real_escape_string($connection,$_POST['description']);
            $longdescription = mysqli_real_escape_string($connection,$_POST['long-description']);
            $examlength = mysqli_real_escape_string($connection,$_POST['exam-length']);
            $passscore = mysqli_real_escape_string($connection,$_POST['pass-score']);
            $duration = mysqli_real_escape_string($connection,$_POST['duration']);
            $topicareas = mysqli_real_escape_string($connection,$_POST['topic-areas']);
            $versions = mysqli_real_escape_string($connection,$_POST['versions']);
            $cost = mysqli_real_escape_string($connection,$_POST['cost']);
            $status = $_POST['status'];
            $create_date = $today;
            $creator_id = $userx;
            $image = $_POST["currentimg"];

            if($_POST["online"] == "on"){
                $online = '1';
            } else {
                $online = '0';
            }
            if($_POST["pdf"] == "on"){
                $pdf = '1';
                $pdf_questions = $_POST["nrquestions"];
            } else {
                $pdf = '0';
                $pdf_questions = '0';
            }
            if($_POST["repq"] == "on"){
                $repq = 1;
            } else {
                $repq = 0;
            }
            if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
                //This is the directory where images will be saved
                $target = "images/";
                $target = $target . basename($_FILES['filename']['name']);

                //This gets all the other information from the form
                $Filename = basename($_FILES['filename']['name']);

                //Writes the Filename to the server
                if (move_uploaded_file($_FILES['filename']['tmp_name'], $target)) {
                    //Tells you if its all ok
                    echo '<div class="alert alert-info">The image file '. basename($_FILES['filename']['name']) .' has been uploaded, and your information has been added to the directory</div>';
                    // Connects to your Database
                    $image = $target;
                } else {
                    //Gives and error if its not
                    echo '<div class="alert alert-alert">Sorry, there was a problem uploading your file.</div>';
                }
            }

            if(isset($_FILES['pdffilename']['name']) && $_FILES['pdffilename']['name'] != '') {
                //This is the directory where images will be saved
                $target1 = "pdf/exams/";
                $target1 = $target1 . basename($_FILES['pdffilename']['name']);

                //This gets all the other information from the form
                $Filename = basename($_FILES['pdffilename']['name']);

                //Writes the Filename to the server
                if (move_uploaded_file($_FILES['pdffilename']['tmp_name'], $target1)) {
                    echo '<div class="alert alert-info">The pdf file '. basename($_FILES['pdffilename']['name']) .' has been uploaded, and your information has been added to the directory</div>';
                    $pdffile = $target1;
                } else {
                    //Gives and error if its not
                    echo '<div class="alert alert-danger">Sorry, there was a problem uploading the pdf file.</div>';
                }
            }
        }


        if ($test_aca == 1) {
            $msg = "set 2 Post</br>";
            $msg .= "create_date: ".$create_date."</br>";
            $msg .= "status: ".$status."</br>";
            $msg .= "creator_id: ".$userx."</br>";
            //echo $msg;
        }

        // Update


        $query = "UPDATE aca_exam SET";
        if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
            $query .= " exam_image = '".$image."', ";
        }
        $query .= " `code` = '".$code."', ";
        $query .= " description = '".$description."', ";
        $query .= " long_description = '".$longdescription."', ";
        $query .= " exam_length = '".$examlength."', ";
        $query .= " pass_score = '".$passscore."', ";
        $query .= " duration = '".$duration."', ";
        $query .= " topic_areas = '".$topicareas."', ";
        $query .= " versions = '".$versions."', ";
        $query .= " online = '".$online."', ";
        $query .= " pdf = '".$pdf."', ";
        $query .= " pdf_nr_questions = '".$pdf_questions."', ";
        $query .= " `status` = '".$status."', ";
        $query .= " cost = '".$cost."', ";
        $query .= " creator_id = '".$userx."', ";
        $query .= " report_question = '".$repq."' ";
        $query .= " WHERE exam_id = '".$key."' ";
        $query .= " LIMIT 1 ";

        $result = mysqli_query( $connection, $query) or trigger_error("Query Failed! SQL: $sql - Error: ".mysqli_error(), E_USER_ERROR);

        if (mysqli_affected_rows($connection) == 1) {
            if(isset($pdffile) && $pdffile != ""){
                $pdfquery = "INSERT INTO aca_exam_pdf(exam_id,pdf_file,date_uploaded) values ('".$_POST["examid"]."','".$pdffile."','".date("Y-m-d")."')";
                $pdfresult = mysqli_query( $connection, $pdfquery);
            }
            // Success
            echo "<div class=\"alert alert-success\">";
            echo "The record was updated successfully.";
            echo "</div>";
            $set = 0; // set action back to listing
        } else {

            echo "<div class=\"alert alert-danger\">";
            echo "Error description: " . mysqli_error($connection);
            echo "The record was not updated. Please try again. Make sure that all fields are completed.";
            echo "</div>";
            $set = 3; // set action back to form
        }

    } // end of set = 4 update


    if ($set == 3) { // edit
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Edit Exam</h2><div class=\"colored-line-left\"></div></div>";
        echo '<div class="clearfix"></div>';
        echo '<div class="row content-pad">';
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";

        $result_set = get_aca_exam_rec($key);
        while ($row = mysqli_fetch_array($result_set)) {
            $exam_id = $row["exam_id"];
            $exam_image = $row["exam_image"];
            $code = $row["code"];
            $description = $row["description"];
            $longdesc = $row["long_description"];
            $examlength = $row["exam_length"];
            $passscore = $row["pass_score"];
            $duration = $row["duration"];
            $topicareas = $row["topic_areas"];
            $cost = $row["cost"];
            $versions = $row["versions"];
            $online = $row["online"];
            $pdf = $row["pdf"];
            $pdf_nr_question = $row["pdf_nr_questions"];
            $status = $row["status"];
            $repq = $row["report_question"];
            $d_status = $status;
            $result_set74 = get_dim("status", $status);
            while ($row = mysqli_fetch_array($result_set74)) {
                $dim_key = $row["dim_key"];
                $d_status = $row["description"];
            }
        }

        echo '<form name="aca_exam" method="post" action="?id='.$id.'&set=4&opt='.$opt.'&key='.$key.'&cat='.$cat.'&cat6='.$cat6.'&cat2='.$cat2.'&cat3='.$cat3.'&cat4='.$cat4.'&cat5='.$cat5.'" enctype="multipart/form-data">
		      <input type="hidden" name="examid" value="'.$exam_id.'" />
		      <table class="table table-striped">
		      <thead>
		        <tr>
		            <th colspan="2" style="background:#58595b;color:#FFFFFF;">Edit Exam</th>
                </tr>
              </thead>
              <tbody>
			  <tr>
			    <td>Type:</td>			  
				<td><input type="checkbox" id="online" name="online" class="custom-checkbox" '.($online == '1' ? 'checked' : '' ).'><label for="online"><strong>Online Test</strong></label>
					<input type="checkbox" id="pdf" name="pdf" class="custom-checkbox" '.($pdf == '1' ? 'checked' : '' ).'><label for="pdf"><strong>PDF Download</strong></label></td>
			  </tr>
			  <tr>
				<td class="pdfchecked" '.($pdf == '1' ? '' : 'style="display:none' ).'">Number of questions:</td>
				<td><input type="text" name="nrquestions" class="form-control" id="nrquestions" value="'.$pdf_nr_question.'"></td>
			  </tr>
			  <tr>
                <td>Image:</td>
                <td>';
                    if(isset($_POST['filename']) && !empty($_POST['filename'])){
                        echo '<input type="text" name="filename" value="'.print($_POST['filename']).'" />';
                                } else {
                        echo '<input id="filename" name="filename" type="file" class="file" data-show-preview="false">';
                    }

            echo '</td>
                    </tr>';

        if($exam_image != ''){
            echo '<tr>
                    <td>Current Image:</td>
					<td><input type="hidden" name="currentimg" value="'.$exam_image.'" /><img src="'.$exam_image.'" height="50" /></td>
                  </tr>';
        }

        echo '<tr>
                    <td>Upload PDF:</td>
                    <td>';
                    if(isset($_POST['pdffilename']) && !empty($_POST['pdffilename'])){
                        echo '<input type="text" name="pdffilename" value="'.print($_POST['pdffilename']).'" />';
                                } else {
                        echo '<input id="pdffilename" name="pdffilename" type="file" class="file" data-show-preview="false">';
                    }

        echo '</td>
                </tr>';

        $pdfsql = "select id,pdf_file from aca_exam_pdf where exam_id = '".$exam_id."'";

        $pdfresult = mysqli_query($connection, $pdfsql);

        while ($pdfrow = mysqli_fetch_array($pdfresult)) {
            echo '<tr>
                    <td>Current PDF:</td>
					<td><input type="hidden" name="currentpdf" value="'.$pdfrow["pdf_file"].'" />
						<input type="hidden" name="pdfid" value="'.$pdfrow["id"].'" />
						<a href="'.$pdfrow["pdf_file"].'" target="_blank">						
						<img src="images/pdficon.png" height="50" /></a></td>
					</tr>';
        }

        echo '<tr>
                    <td>Code:</td>
                    <td><input type="text" name="code" class="form-control reqd" id="code" placeholder="Enter code" value="'.$code.'"></td>
              </tr>
              <tr>
                    <td>Description:</td>
                    <td><input type="text" name="description" class="form-control reqd" id="description" placeholder="Enter description" value="'.$description.'"></td>
              </tr>
              <tr>
                    <td>Long Description</td>
                    <td><textarea class="form-control reqd" rows="5" name="long-description" id="long-description">'.$longdesc.'</textarea></td>
              </tr>
              <tr>
                    <td>Exam Length</td>
                    <td><input type="text" name="exam-length" id="exam-length" class="form-control reqd" placeholder="Exam length" value="'.$examlength.'" /></td>
              </tr>
              <tr>
                    <td>Pass Score</td>
                    <td><input type="text" name="pass-score" id="pass-score" class="form-control reqd" placeholder="Pass Score" value="'.$passscore.'" /></td>
              </tr>
              <tr>
                    <td>Duration:</td>
                    <td><input type="text" name="duration" class="form-control reqd" id="duration" placeholder="Duration" value="'.$duration.'"></td>
              </tr>
              <tr>
                    <td>Topic Areas</td>
                    <td><textarea class="form-control reqd" rows="5" name="topic-areas" id="topic-areas">'.$topicareas.'</textarea></td>
              </tr>
              <tr>
                    <td>Versions:</td>
                    <td><input type="text" name="versions" class="form-control reqd" id="versions" placeholder="Enter versions" value="'.$versions.'"></td>
              </tr>
              <tr>
                    <td>Cost:</td>
                    <td><input type="text" name="cost" class="form-control reqd" id="cost" placeholder="cost" value="'.$cost.'"></td>
              </tr>
              <tr>
                    <td>Report Question:</td>
                    <td><input type="checkbox" name="repq" class="" '.($repq == "1" ? 'checked' : '').' /></td>
              </tr>
              <tr>
                    <td>Status</td>
                    <td><select name="status" class="form-control reqd" onchange="showTool(this.value)" id="status">
                            <option value="'.$status.'">'.$d_status.'</option>';
        $result_set9 = get_dim_list("status");
        while ($row = mysqli_fetch_array($result_set9)) {
            $dim_key = $row["dim_key"];
            $display_name = $row["description"];
            echo '<option value="' . $dim_key . '">'.$display_name.'</option>';
        }
        echo '</select>
               </td>';

        // buttons
        $update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Submit</button>";
        $cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";

        echo "<tr><td colspan=\"2\" class=\"text-center\">" . $update . " " . $cancel . "</td></tr>";
        echo "</tbody>";
        echo "</table>";

        echo "</form>";

        echo "</div> ";
        echo "</div> ";


        echo "<p>&nbsp;</p>";

    } // end of edit



    if ($set == 0) { // list

        // get record count
        $p_records = 0;
        $result_setA = get_aca_exam_count(99); // status 1 = valid, access 0 for all
        while ($row = mysqli_fetch_array($result_setA)) {
            $p_records = $row["record_count"];
        }
        $anchor = "x";
        $p_link = "?id=".$id."&opt=".$opt."&cat2=0&cat3=0&cat6=".$cat6."&key=".$key."&cat=".$cat."&cat4=".$cat4."&set=".$set;
        $p = page_nav_last($page, $page_break_d, $p_records, $p_link, $anchor);
        $c = " ";

        //echo "Select a user to maintain or create a new user</br>";
        $n =  "<button name=\"NewBut\" type=\"button\" value=\"New\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=1&opt=0&key=".$key."');return document.MM_returnValue\"/>New</button>&nbsp;&nbsp;";
        //echo "<p align=\"right\">" . $n." ".$p . "</p></br>";
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Exam Maintenance</h2><div class=\"colored-line-left\"></div></div>";
        echo '<div class="clearfix"></div>';
        echo '<div class="row content-pad">';
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";


        echo "<table class=\"table table-striped\">";
        echo "<thead>";
        echo "<tr>";
        echo "<th colspan=\"7\" style=\"background:#58595b;color:#FFFFFF;line-height:2em\">List of Exams<div style=\"float:right\">".$n."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;".$p."</div> </th>";
        echo "</tr>";
        echo "<tr>";
        echo "<th>ID</th>";
        echo "<th>Code</th>";
        echo "<th>Description</th>";
        echo "<th>Versions</th>";
        echo "<th>Status</th>";
        echo "<th></th>";
        echo "<th></th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        // get data
        $npage = $page;
        $result_set = get_aca_exam_list_admin($npage, $page_break_d, 99);
        while ($row = mysqli_fetch_array($result_set)) {
            $exam_id = $row["exam_id"];
            $code = $row["code"];
            $description = $row["description"];
            $versions = $row["versions"];
            $status = $row["status"];
            $d_status = $status;
            $result_set74 = get_dim("status", $status);
            while ($row = mysqli_fetch_array($result_set74)) {
                $dim_key = $row["dim_key"];
                $d_status = $row["description"];
            }

            $un = "<a href=\"?id=".$id."&set=3&opt=0&login=1&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&key=".$exam_id."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$code."</a>";
            $e =  "<button name=\"EditBut\" type=\"button\" value=\"Edit\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=3&opt=0&key=".$exam_id."');return document.MM_returnValue\"/>Edit</button>&nbsp;&nbsp;";
            $d =  "<button name=\"DeleteBut\" type=\"button\" value=\"Delete\" class=\"btn btn-default\" onclick=\"DeleteExam(".$exam_id.")\"/>Delete</button>&nbsp;&nbsp;";

            echo "<tr>";
            echo "<td>".$exam_id."</td>";
            echo "<td>".$un."</td>";
            echo "<td>".$description."</td>";
            echo "<td>".$versions."</td>";
            echo "<td>".$d_status."</td>";
            echo "<td>".$e."</td>";
            echo "<td>".$d."</td>";
            echo "</tr>";
        }
        echo "<tr>";
        echo "<td align=\"left\" colspan=\"3\">".$c."</td>"; // close, new, page nav
        echo "<td align=\"right\" colspan=\"4\">".$n."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;".$p."</td>";
        echo "</tr>";

        echo "</tbody>";
        echo "</table>";
        echo "</div> ";
        echo " </div> ";


    } // end of list set = 0




    if ($set == 1) { // create new
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Exam Maintenance</h2><div class=\"colored-line-left\"></div></div>";
        echo '<div class="clearfix"></div>';
        echo '<div class="row content-pad">';
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";



        echo "<form name=\"aca_exam\" method=\"post\" action=\"?id=".$id."&set=2&opt=" . $opt . "&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat5=".$cat5." \" enctype=\"multipart/form-data\">";

        echo "<table class=\"table table-striped\">";
        echo "<thead>";
        echo '<tr>
				<th colspan="2" style="background:#58595b;color:#FFFFFF;">Add Exam</th>
			  </tr>';
        echo "</thead>";
        echo "<tbody>";
        echo '<tr>
                <td>Type:</td>
					<td><input type="checkbox" name="online" id="online" class="custom-checkbox"><label for="online"><strong>Online Test</strong></label>
					<input type="checkbox" name="pdf" id="pdf" class="custom-checkbox"><label for="pdf"><strong>PDF Download</strong></label></td>
              </tr>
			  <tr class="pdfchecked" style="display:none">
					<td>Number of questions:</td>
					<td><input type="text" name="nrquestions" class="form-control" id="nrquestions" placeholder="0" value=""></td>
			  </tr>';

        echo '<tr>
                    <td>Image:</td>
                    <td><input id="filename" name="filename" type="file" class="file" data-show-preview="false"></td>
              </tr>';

        echo '<tr>
                    <td>PDF File:</td>
                    <td><input id="pdffilename" name="pdffilename" type="file" class="file" data-show-preview="false"></td>
              </tr>';

        echo "<tr>";
        echo "<td>Code:</td>";
        echo "<td><input type=\"text\" name=\"code\" class=\"form-control reqd\" id=\"code\" placeholder=\"Enter code\" value=\"\"></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>Description:</td>";
        echo "<td><input type=\"text\" name=\"description\" class=\"form-control reqd\" id=\"description\" placeholder=\"Enter description\" value=\"\"></td>";
        echo "</tr>";
        echo '<tr>
                <td>Long Description</td>
                <td><textarea class="form-control reqd" rows="5" name="long-description" id="long-description2"></textarea></td>
            </tr>';
        echo '<tr>
                <td>Exam Length</td>
                <td><input type="text" name="exam-length" id="exam-length" class="form-control reqd" placeholder="Exam length" value="" /></td>
            </tr>';
        echo '<tr>
                <td>Pass Score</td>
                <td><input type="text" name="pass-score" id="pass-score" class="form-control reqd" placeholder="Pass Score" value="" /></td>
            </tr>';
        echo "<tr>";
        echo "<td>Duration:</td>";
        echo "<td><input type=\"text\" name=\"duration\" class=\"form-control reqd\" id=\"duration\" placeholder=\"Duration\" value=\"\"></td>";
        echo "</tr>";
        echo '<tr>
                <td>Topic Areas</td>
                <td><textarea class="form-control reqd" rows="5" name="topic-areas" id="topic-areas"></textarea></td>
            </tr>';
        echo "<tr>";
        echo "<td>Versions:</td>";
        echo "<td><input type=\"text\" name=\"versions\" class=\"form-control reqd\" id=\"versions\" placeholder=\"Enter versions\" value=\"\"></td>";
        echo "</tr>";
        echo '<tr>
                <td>Cost</td>
                <td><input type="text" name="cost" id="cost" class="form-control reqd" placeholder="Cost" value="" /></td>
            </tr>
            <tr>
                <td>Report Question</td>
                <td><input type="checkbox" name="repq" id="repq" class="" /></td>
            </tr>
			<tr>
                <td>Trial</td>
                <td><input type="checkbox" name="trial" id="trial" class="" /></td>
            </tr>';


        // buttons
        $update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Submit</button>";
        $cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";

        echo "<tr><td colspan=\"2\" class=\"text-center\">" . $update . " " . $cancel . "</td></tr>";
        echo "</tbody>";
        echo "</table>";

        echo "</form>";




        echo "</div> ";
        echo " </div> ";
    } // end of set = 1

} // end of user id = 30
?>

<script>

    $("#pdf").on("change",function(){
        if($("#pdf").is(":checked")){
            $(".pdfchecked").show();
        } else {
            $(".pdfchecked").hide();
        }
    });
	
	function DeleteExam(id){
		$.post( "ajax/ajax_admin_exam.php", { action:"DeleteExam",examid:id}, function(data){
                            if(data.result == 1 || data.result == "1"){
								window.location.href = "index.php?id=30&login=1&opt=0&key=0&set=0";
                                console.log("done");
                            } else {

                            }
                        },
                        "json");
	}

</script>