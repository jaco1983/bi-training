<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/08/28
 * Time: 6:40 PM
 */
echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>About Us</h2><div class=\"colored-line-left\"></div></div>
			<div class=\"clearfix\"></div>
	  ";
echo '<div class="row content-pad">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <p>Blackboard BI was found by SAP consultants with many years of global Business Intelligence project and training experience. A passion for training and development and the empowerment of people through knowledge transfer and skills made it easy to create a platform where not only questions are shared but knowledge and insight from practical experience.</p>
          </div>
      </div>
      <div class="row text-center">
      <p><strong>Visit our partner sites.</strong></p>
      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <a href="https://www.blackboardbi.com" target="_blank"><img src="images/blackboardbi_logo.png" class="img-responsive col-xs-12 col-sm-12" style="padding:10px 0px;" /></a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <a href="https://www.blackboardbs.com" target="_blank"><img src="images/blackboardbs_logo.png" class="img-responsive col-xs-12 col-sm-12" style="padding:10px 0px;" /></a>
        </div>
        </div>
      </div>';