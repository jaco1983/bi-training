<?php	
// class_exam id = 23
// add an exam to a class
// Admin can add, edit or remove an exam from a class's profile.

if ($id == 23) {

        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Class Exam Maintenance</h2><div class=\"colored-line-left\"></div></div>";
        echo '<div class="clearfix"></div>';
        echo '<div class="row content-pad">';
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";


	if ($set == 2) { // insert																	
		if (isset($_POST['submit'])) {
			$exam_id = trim(mysql_prep($_POST['exam_id']));
			$user_id = $_POST["user_id"];
			$expiry_date = trim(mysql_prep($_POST['expiry_date']));
			$create_date = $today;	
			//$expiry_date = $today;
			//$expiry_date = (strtotime($today)) + (60*60*24);
			$creator_id = $userx;
			$status = '1';	
		}
		
		if ($test_aca == 1) {
			$msg = "set 2 Post opt=".$opt."</br>";
			$msg .= "create_date: ".$create_date."</br>";
			$msg .= "creator_id: ".$userx."</br>";
			echo $msg;
		}

		$query = "INSERT INTO aca_class_exam( ";
		$query .= " class_id, exam_id, expiry_date, create_date, "; 
		$query .= " status, creator_id ) ";
		$query .= " VALUES (   ";
		$query .= " {$cat7}, {$exam_id}, '{$expiry_date}', '{$create_date}', ";  
		$query .= " '{$status}', {$userx} ) " ;
		
		$result = mysqli_query( $connection, $query);
		
		$query2 = "insert into aca_user_exam (user_id,exam_id,expiry_date,create_date,status,creator_id) values ({$user_id},{$exam_id},'{$expiry_date}','{$create_date}',{$status},{$userx})";
	
		$result2 = mysqli_query( $connection, $query2);
		if (mysqli_affected_rows($connection) == 1) {	
			// Success	
			echo "<div class=\"alert alert-success\">";
				echo "Record Created";
			echo "</div>";
			$set = 0; // set action back to listing
			} else {
			echo "<div class=\"alert alert-danger\">";
				echo "Record creation failed";
			echo "</div>";
		}
		$set = 0; // set action back to form
		
	} // end of set = 2
	
	
	if ($set == 4 && $key <> 0) { // update record													
		if (isset($_POST['submit'])) {
			$exam_id = trim(mysql_prep($_POST['exam_id']));
			$expiry_date = trim(mysql_prep($_POST['expiry_date']));
			$status = trim(mysql_prep($_POST['status']));
			$create_date = $today;	
			$creator_id = $userx;
		}
		
			
			if ($test_aca == 1) {
				$msg = "set 2 Post</br>";
				$msg .= "create_date: ".$create_date."</br>";
				$msg .= "status: ".$status."</br>";
				$msg .= "creator_id: ".$userx."</br>";
				//echo $msg;
			}
			
			// Update
			$query = "UPDATE aca_class_exam SET ";
			//$query .= " class_id = '{$class_id}', ";
			$query .= " exam_id = '{$exam_id}', ";
			$query .= " expiry_date = '{$expiry_date}', ";
			$query .= " status = '{$status}', ";
			$query .= " creator_id = '{$userx}' ";
			$query .= " WHERE class_exam_id = {$key} ";
			$query .= " LIMIT 1 ";
		
			$result = mysqli_query( $connection, $query);
			if (mysqli_affected_rows($connection) == 1) {	
				// Success	
				echo "<div class=\"alert alert-success\">";
					echo "The record was updated successfully.";
				echo "</div>";
				$set = 0; // set action back to listing
				} else {
				echo "<div class=\"alert alert-danger\">";
					echo "The record was not updated. Please try again. Make sure that all fields are completed.";
				echo "</div>";
				$set = 3; // set action back to form
			}
				
	} // end of set = 4 update


	if ($set == 3) { // edit
				
		$result_set = get_aca_class_exam_rec($key);
		while ($row = mysqli_fetch_array($result_set)) {	
			$key = $row["class_exam_id"];
			$class_id = $row["class_id"];
			$exam_id = $row["exam_id"];	
			$expiry_date = $row["expiry_date"];	
			$create_date = $row["create_date"];	
			$status = $row["status"];
					
			$exam_code = "";
			$result_setI = get_aca_exam_rec($exam_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$exam_code = $row["code"];		
			}
			$class_name = "";
			$result_setI = get_aca_class_rec($class_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$class_name = $row["name"];			
				$instructor_id = $row["instructor_id"];		
			}
			$instructor = "";
			$result_setI = get_aca_instructor_rec($instructor_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$instructor = $row["name"];	
			}
			$d_status = $status;
				$result_set74 = get_dim("status", $status);
				while ($row = mysqli_fetch_array($result_set74)) {
					$dim_key = $row["dim_key"]; 
					$d_status = $row["description"];
				}
		}
		
		echo "<form name=\"aca_class_exam\" method=\"post\" action=\"?id=".$id."&set=4&opt=" . $opt . "&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat7=".$cat7."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat5=".$cat5." \"enctype=\"multipart/form-data\">";

        echo "<table class=\"table table-striped\">";
        echo "<thead>";
        echo '<tr><th colspan="5" style="background:#58595b;color:#FFFFFF;">Edit Class Exam</th></tr>';
        echo "</thead>";
        echo "<tbody>";

		echo "<tr>";
		  echo "<td>Class:</td>";
			echo "<td>".$class_name." - " .$instructor. "</td>";
		echo "</tr>";
		
		echo "<tr>";
		  echo "<td>Exam:</td>";
			echo "<td><select name=\"exam_id\" class=\"form-control reqd\" id=\"exam_id\">";
						 		echo "<option value=\"".$exam_id."\">".$exam_code."</option>";
								$result_set19 = get_aca_class_exam_list_available(0, 999, 1, $class_id, $expiry_date); // $npage, $no_of_list, $status, $cat7, $expiry_date
								while ($row = mysqli_fetch_array($result_set19)) {
									$dim_key = $row["exam_id"]; 
									$display_name = $row["code"];
									echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								}
			echo "</select></td>";
		echo "</tr>";
		
		echo "<tr>";
		  echo "<td>Subscription Expiry Date:</td>";
		  echo "<td><input type=\"date\" name=\"expiry_date\" class=\"form-control reqd\" id=\"expiry_date\" placeholder=\"Expiry Date\" value=\"".$expiry_date."\"></td>";
		echo "</tr>";
		
		echo "<tr>";
		  echo "<td>Status</td>";
			echo "<td><select name=\"status\" class=\"form-control reqd\" onchange=\"showTool(this.value)\" id=\"status\">";
						  echo "<option value=\"".$status."\">".$d_status."</option>";
								  $result_set9 = get_dim_list("status");
								  while ($row = mysqli_fetch_array($result_set9)) {
									  $dim_key = $row["dim_key"]; 
									  $display_name = $row["description"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }
								  echo "</select></td>";
		echo "</tr>";
										
			// buttons
			$update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Submit</button>";			
			$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";
			
		  echo "<tr><td colspan=\"2\" class=\"text-center\">" . $update . " " . $cancel . "</td></tr>";
		  echo "</tbody>";
		  echo "</table>";
		echo "</form>";
		
		
		
		
		  echo "</div> "; 
	   echo " </div> "; 	
		
		
		echo "<p>&nbsp;</p>";	
		
	} // end of edit



	if ($set == 1) { // create new														
		// get class info
		$cat7 = $key;
		$result_set = get_aca_class_rec($key);
		while ($row = mysqli_fetch_array($result_set)) {		
			$class_id = $row["class_id"];
			$name = $row["name"];	
			$password = $row["password"];
			
			$instructor_id = $row["instructor_id"];	
			$expiry_date = $row["expiry_date"];				
			$note = $row["note"];	
			$status = $row["status"];
			$instructor = "";
			$result_setI = get_aca_instructor_rec($instructor_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$instructor = $row["name"];	
				$user_id = $row["user_id"];
			}
		}

			
		  echo "<form name=\"aca_class_exam\" method=\"post\" action=\"?id=".$id."&set=2&opt=" . $opt . "&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat7=".$cat7."&cat5=".$cat5." \"enctype=\"multipart/form-data\">";

        echo "<table class=\"table table-striped\">";
        echo "<thead>";
        echo '<tr><th colspan="5" style="background:#58595b;color:#FFFFFF;">New Class Exam</th></tr>';
        echo "</thead>";
        echo "<tbody>";
		echo "<tr>";
		  echo "<td>Class:</td>";
			echo "<td>".$name." - " .$instructor. "<input type=\"hidden\" name=\"user_id\" value=\"".$user_id."\" /><input type=\"hidden\" name=\"instructor_name\" value=\"".$instructor."\" /></td>";
		echo "</tr>";
		
		echo "<tr>";
		  echo "<td>Exam:</td>";
			echo "<td><select name=\"exam_id\" class=\"form-control reqd\" id=\"exam_id\">";
						  echo "<option value=\"\"></option>";
								$result_set19 = get_aca_class_exam_list_available(0, 999, 1, $class_id, $expiry_date); // $npage, $no_of_list, $status, $cat7, $expiry_date
								while ($row = mysqli_fetch_array($result_set19)) {
									$dim_key = $row["exam_id"]; 
									$display_name = $row["code"];
									echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								}
			echo "</select></td>";
		echo "</tr>";
		
		echo "<tr>";
		  echo "<td>Subscription Expiry Date:</td>";
		  echo "<td><input type=\"date\" name=\"expiry_date\" class=\"form-control reqd\" id=\"expiry_date\" placeholder=\"Expiry Date\" value=\"".$expiry_date."\"></td>";
		echo "</tr>";
			// buttons
			$update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Subscribe</button>";			
			$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=34&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";
			
		  echo "<tr><td colspan=\"2\" class=\"text-center\">" . $update . " " . $cancel . "</td></tr>";
		  echo "</tbody>";
		  echo "</table>";

		echo "</form>";			
		
		
		
		
		  echo "</div> "; 
	   echo " </div> "; 	
		
		
		echo "<p>&nbsp;</p>";	
		
		$set = 0; // list available exams
		
	} // end of set = 1

	if ($set == 0) {	
		// get record count
		$p_records = 0;
		$result_setA = get_aca_class_exam_count(99, $cat7, 0); // status 1 = valid, access 0 for all
		while ($row = mysqli_fetch_array($result_setA)) {
			$p_records = $row["record_count"]; 
		}
		if ($p_records > 0) {
			
			$anchor = "x";
			$p_link = "?id=".$id."&opt=".$opt."&cat2=0&cat3=0&cat6=".$cat6."&key=".$key."&cat=".$cat."&cat4=".$cat4."&set=".$set;
			$p = page_nav_last($page, $page_break_d, $p_records, $p_link, $anchor);
			$c = " ";
	
			//echo "Select a user to maintain or create a new user</br>";
			//$n =  "<button name=\"NewBut\" type=\"button\" value=\"New\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=1&opt=0&key=".$key."');return document.MM_returnValue\"/>New</button>&nbsp;&nbsp;";
			$n = "";



            echo "<table class=\"table table-striped\">";
            echo "<thead>";
            echo '<tr><th colspan="5" style="background:#58595b;color:#FFFFFF;">Exams Subscribed To<div style="float:right;">' . $n.' '.$p . '</div></th></tr>';
            echo "<tr>";
				//echo "<th>ID</th>";
				echo "<th>Exam Code</th>";
				echo "<th>Description</th>";
				echo "<th>Versions</th>";
				echo "<th>Subscription End</th>";
				echo "<th></th>";
				//echo "<th></th>";
			echo "</tr>";		
			echo "</thead>";
			echo "<tbody>";
			// get data		
			$npage = $page;
			$un = "";
			$e =  "";
			$result_set = get_aca_class_exam_list($npage, $page_break_d, 99, $cat7, 0);
			while ($row = mysqli_fetch_array($result_set)) {		
				$class_exam_id = $row["class_exam_id"];		
				$class_id = $row["class_id"];
				$cat7 = $class_id;	
				$exam_id = $row["exam_id"];
				$expiry_date = $row["expiry_date"];		
				//$description = $row["description"];	
				$status = $row["status"];
				$d_status = $status;
					$result_set74 = get_dim("status", $status);
					while ($row = mysqli_fetch_array($result_set74)) {
						$dim_key = $row["dim_key"]; 
						$d_status = $row["description"];
					}
					$result_set76 = get_aca_exam_rec($exam_id);
					while ($row = mysqli_fetch_array($result_set76)) {
						$code = $row["code"]; 
						$description = $row["description"];	
						$versions = $row["versions"];	
						$un = "<a href=\"?id=".$id."&set=3&opt=0&login=1&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&key=".$exam_id."&cat3=".$cat3."&cat7=".$cat7."&cat4=".$cat4."&cat5=".$cat5."\">".$code."</a>";
						$e =  "<button name=\"EditBut\" type=\"button\" value=\"Edit\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat7=".$cat7."&cat=".$cat."&login=1&set=3&opt=0&key=".$class_exam_id."');return document.MM_returnValue\"/>Edit</button>&nbsp;&nbsp;";
					}
				
				
			echo "<tr>";
				//echo "<td>".$exam_id."</td>";
				echo "<td>".$code."</td>";
				echo "<td>".$description."</td>";
				echo "<td>".$versions."</td>";
				echo "<td>".$expiry_date."</td>";
				echo "<td>".$e."</td>";
			echo "</tr>";
			}
			echo "<tr>";
				echo "<td align=\"left\" colspan=\"2\">".$c."</td>"; // close, new, page nav
				echo "<td align=\"right\" colspan=\"3\">".$n."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;".$p."</td>";
			echo "</tr>";
			
			echo "</tbody>";
			echo "</table>";
			  echo "</div> "; 
		   echo " </div> "; 	
		   
		} // end of if p_records > 0

		
	}



	
	
} // end of class exam id = 23
?>
 