
<?php	
// results id = 40


// exam - cat (key)
// tool / subject - cat6
// version - cat2
// user - cat3
// create_date - test date - cat4
// instructor - cat5


// set variables
$page_break = $page_break_f;
		
//
if ($id == 40 || $id == 8) {
echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Dashboard</h2><div class=\"colored-line-left\"></div></div>
          <div class=\"clearfix\"></div>
          <div class=\"row content-pad\">";


	if ($set == 0) {
			
			
		// lookup descriptions for selected filters					
		if ($cat <> 0) { // exam
			$c1 = $cat;
			$result_set1 = get_aca_exam_rec($cat);
			while ($row = mysqli_fetch_array($result_set1)) {
				$c1 = $row["code"];
			} 			 
		} else { $c1 = ""; }
		
		if ($cat6 <> 0) { // subject / tool
			$c6 = $cat6;
			$result_set6 = get_aca_tool_rec($cat6);
			while ($row = mysqli_fetch_array($result_set6)) {
				$c6 = $row["description"];
			} 
		} else { $c6 = ""; }
		
		if ($cat2 <> "0" || $cat2 <> 0) { // subject / tool
			$c2 = $cat2;
		} else { $c2 = ""; }
				
		if ($cat3 <> 0) { // user name
			$c3 = $cat3;
			$result_set3 = get_aca_user_rec($cat3);
			while ($row = mysqli_fetch_array($result_set3)) {
				$c3 = $row["username"];
			} 
		} else { $c3 = ""; }
		
		if ($cat4 <> 0) { // create date 
			$c4 = $cat4; 
		} else { $c4 = ""; }
		
		
		if ($aca_accesslevel < 9) { 
			$result_set3 = get_aca_user_rec($userx);
			while ($row = mysqli_fetch_array($result_set3)) {
				$c5 = $row["username"];
				$cat5 = $row["instructor_id"]; 
			} 
		
		} // if instructor logged in
		if ($cat5 <> 0) { // create date 
			$c5 = $cat5; 
		} else { $c5 = ""; }
				
		if ($cat7 <> 0) { // user name
			$c7 = $cat7;
			$result_set7 = get_aca_class_rec($cat7);
			while ($row = mysqli_fetch_array($result_set7)) {
				$c7 = $row["name"];
			} 
		} else { $c7 = ""; }
		
		
		// headers and filters  -------------------------------
		
		$filterExam = "<a href=\"?id=".$id."&set=0&opt=0&key=0&login=1&cat6=".$cat6."&cat5=".$cat5."&cat=0&cat7=".$cat7."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."\">[Exam]</a> ";
		$filterSubject = "<a href=\"?id=".$id."&set=0&opt=0&key=".$key."&cat=".$cat."&cat5=".$cat5."&cat6=0&login=1&cat2="."&cat7=".$cat7.$cat2."&cat3=".$cat3."&cat4=".$cat4."\">[Subject]</a> ";
		$filterClass = "<a href=\"?id=".$id."&set=0&opt=0&key=".$key."&cat=".$cat."&cat5=".$cat5."&cat6=".$cat6."&login=1&cat2="."&cat7=0&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."\">[Class]</a> ";
		$filterVersion = "<a href=\"?id=".$id."&set=0&opt=0&key=".$key."&cat=".$cat."&cat5=".$cat5."&cat6=".$cat6."&cat7=".$cat7."&login=1&cat2=0&cat3=".$cat3."&cat4=".$cat4."\">[Version]</a> ";
		$filterUser = "<a href=\"?id=".$id."&set=0&opt=0&key=".$key."&cat=".$cat."&cat5=".$cat5."&cat6=".$cat6."&cat7=".$cat7."&login=1&cat2=".$cat2."&cat3=0&cat4=".$cat4."\">[User]</a> ";
		$filterTestDate = "<a href=\"?id=".$id."&set=0&opt=0&key=".$key."&cat=".$cat."&cat5=".$cat5."&cat6=".$cat6."&cat7=".$cat7."&login=1&cat2=".$cat2."&cat3=".$cat3."&cat4=0\">[Test Date]</a> ";
		$filterAll = "<a href=\"?id=".$id."&set=0&opt=0&key=".$key."&cat=0&cat5=0&cat6=0&login=1&cat2=0&cat7=0&cat3=0&cat4=0\" class=\"btn btn-default\" style=\"float:right;\">Reset Filters";
		$filterInstructor = "<a href=\"?id=".$id."&set=0&opt=0&key=".$key."&cat=".$cat."&cat5=0&cat6=".$cat6."&cat7=".$cat7."&login=1&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."\">[Instructor]</a> ";
		
		// instructor list
		$inst = "";
		$inst2 = '<select class="form-control" id="instructor">';
		if ($aca_accesslevel == 9) { // if Administrator logged in

			if ($ttest == 1) {
			echo "<div class=\"alert alert-success\" role=\"alert\">";
  			echo "TEST In Results - Only for Administrator</br>";
  			echo "TEST CAT3".$cat3." | Access: ".$aca_accesslevel."</br>";
		    echo "</div>";	
			}
            $inst2 .= '<option value="0">Instructor</option>';
		$result_setII = get_aca_instructor_list(0, 10, 1);
		  while ($row = mysqli_fetch_array($result_setII)) {		
			  $instructor_id = $row["instructor_id"];
			  $name = $row["name"];
			  //$email = $row["email"];
			  //$status = $row["status"];
			  //$d_status = $status;
			  $inst .= "&nbsp;<a href=\"?id=".$id."&set=0&opt=".$opt."&key=".$key."&cat=".$cat."&cat5=".$instructor_id."&cat6=".$cat6."&cat7=".$cat7."&login=1&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."\">[ ".$name." ]</a>&nbsp;";
			  $inst2 .= '<option value="'.$instructor_id.'" '.($cat5 == $instructor_id ? 'selected' : '').'>'.$name.'</option>';
		  }
		} // end of if admin logged in

        if ($aca_accesslevel == 5) { // if Administrator logged in

            $result_set3 = get_aca_user_rec($userx);
            while ($row = mysqli_fetch_array($result_set3)) {
                $c5 = $row["name"];
                $cat5 = $row["instructor_id"];
            }
            $inst2 .= '<option selected disabled>'.$c5.'</option>';

        }
		$inst2 .= '</select>';
		
		// class list
		$class = "";
		$class2 = '<select class="form-control" id="class"><option value="0">Class</option>';
		if ($aca_accesslevel >= 5) { // if Administrator logged in
		  $result_setCl = get_aca_class_list(0, 10, 1, 0, 0); // ($npage, $no_of_list, $status, $expiry, $cat7)
		  while ($row = mysqli_fetch_array($result_setCl)) {		
			  $class_id = $row["class_id"];
			  $name = $row["name"];
			  //$email = $row["email"];		
			  //$status = $row["status"];
			  //$d_status = $status;
			  $class .= "&nbsp;<a href=\"?id=".$id."&set=0&opt=".$opt."&key=".$key."&cat=".$cat."&cat5=".$cat5."&cat6=".$cat6."&cat7=".$class_id."&login=1&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."\">[ ".$name." ]</a>&nbsp;";
              $class2 .= '<option value="'.$class_id.'" '.($cat7 == $class_id ? 'selected' : '').'>'.$name.'</option>';
		  }
		} // end of if admin logged in
        $class2 .= '</select>';
		if ($cat5 <> 0) { 
			$result_setII = get_aca_instructor_rec($cat5);
			while ($row = mysqli_fetch_array($result_setII)) {	
				$inX = $row["name"];
			}
		} else { $inX = ""; }
		
		if ($cat7 <> 0) { 
			$result_setC = get_aca_class_rec($cat7);
			while ($row = mysqli_fetch_array($result_setC)) {	
				$clX = $row["name"];
			}
		} else { $clX = ""; }
	
	
        
	
echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
		echo '<table class="table table-striped">';
		echo '<thead>';
		echo '<tr>
				<th colspan="8" style="background:#58595b;color:#FFFFFF;line-height:2em;">Filters&nbsp;'.$filterAll.'</th>
			  </tr>';
		echo "<tr>";
			echo "<th>".$inst2."</th>";
			echo "<th>".$class2."</th>";
			echo "<th>".$filterExam."</th>";
			echo "<th>".$filterSubject."</th>";
			echo "<th>".$filterClass."</th>";
			echo "<th>".$filterVersion."</th>";
			echo "<th>".$filterUser."</th>";
			echo "<th>".$filterTestDate."</th>";
			//echo "<th></th>";
		echo "</tr>";		
		echo "</thead>";
		echo "<tbody>";
		echo "<tr>";
            echo "<td>&nbsp;</td>";
            echo "<td>&nbsp;</td>";
			echo "<td>".$c1."</td>";
			echo "<td>".$c6."</td>";
			echo "<td>".$c7."</td>";
			echo "<td>".$c2."</td>";
			echo "<td>".$c3."</td>";
			echo "<td>".$c4."</td>";
		echo "</tr>";
		echo "</tbody>";
		echo "</table>";
        echo '</div>';

		// --- end of Filters -----------
		
		
		// print menu options - top 10 incorrect answers		
		$butTopTenIncorrect = "<button name=\"TopBut\" type=\"button\" value=\"Top 10 incorrect answers\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&set=0&opt=5&key=".$key."&cat=".$cat."&login=1&cat6=".$cat6."&cat7=".$cat7."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."');return document.MM_returnValue\"/>Top 10 incorrect answers</button>";
		$butQuestionsErrors = "<button name=\"TopBut\" type=\"button\" value=\"Questions with Errors\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&set=0&opt=6&key=".$key."&cat=".$cat."&login=1&cat6=".$cat6."&cat7=".$cat7."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."');return document.MM_returnValue\"/>Questions with Errors</button>";
		$butQuestionsTrends = "<button name=\"TopBut\" type=\"button\" value=\"Questions with Errors\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&set=0&opt=7&key=".$key."&cat=".$cat."&login=1&cat6=".$cat6."&cat7=".$cat7."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."');return document.MM_returnValue\"/>Question Trends</button>";
		$butSummary = "<button name=\"TopBut\" type=\"button\" value=\"Summary\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&set=0&opt=0&key=".$key."&cat=".$cat."&login=1&cat6=".$cat6."&cat7=".$cat7."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."');return document.MM_returnValue\"/>Summary</button>";
		
		echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 10px;">';
		echo "<p>".$butSummary." ".$butTopTenIncorrect." ".$butQuestionsTrends." ".$butQuestionsErrors."</p>";
	  	// --- end of Buttons -----------
	 
	 
		// report output  -------------------------------		
		
		
				
	
	if ($opt == 5) {	// display exceptions opt = 5		
	// --- Top 10 incorrect answers ------------------------			
				
		/*echo "<div class=\"panel panel-primary\"> ";
		  echo "<div class=\"panel-heading\">Top 10 Questions Incorrect Answered</div> "; 
		  echo "<div class=\"panel-body\">";*/
		  	
		echo "<table class=\"table table-striped\">";
		echo "<thead>";
		echo '<tr><th colspan="6" style="background:#58595b;color:#FFFFFF;">Top 10 Questions Answered Incorrectly</th></tr>';
		echo "<tr>";
			echo "<th></th>";
			echo "<th>Question</th>";
			echo "<th>Subject</th>";
			echo "<th>Questions</th>";
			echo "<th>Wrong</th>";
			echo "<th>% Wrong</th>";
		echo "</tr>";		
		echo "</thead>";
		echo "<tbody>";
		// get data
		$tt_qtot = 0;
		$tt_score = 0;
		$tt_ps = 0;
		$qcl = "l";
		$xxx = 0; 
		// get data	
		$result_set = get_aca_option_results_tool_instructor_top10_incorrect($key, $cat6, $cat2, $cat3, $cat4, $cat5);
		if(mysqli_num_rows($result_set) > 0) {
            while ($row = mysqli_fetch_array($result_set)) {
                $exam_id = $row["exam_id"];
                //$instructor = $row["instructor"];
                $question_id = $row["question_id"];
                $question_text = $row["question_text"];
                $code = $row["code"];
                $tool = $row["tool"];
                $tool_id = $row["tool_id"];
                $t_qtot = $row["questions"];
                $t_score = $row["score"];
                $t_ps = ($t_score / $t_qtot) * 100;
                $tt_qtot = $tt_qtot + $t_qtot;
                $tt_score = $tt_score + $t_score;
                if ($xxx == 1) {
                    $qcl = "qc";
                    $xxx = 0;
                } else {
                    $xxx = 1;
                    $qcl = "l";
                }


                echo "<tr>";
                echo "<td>" . $question_id . "</td>";
                echo "<td>" . nl2br($question_text) . "</td>";
                echo "<td><a href=\"?id=" . $id . "&set=0&opt=" . $opt . "&key=" . $key . "&login=1&key=" . $key . "&cat6=" . $tool_id . "&cat=" . $cat . "&cat2=" . $cat2 . "&cat3=" . $cat3 . "&cat4=" . $cat4 . "&cat5=" . $cat5 . "\">" . $tool . "</a></td>";
                echo "<td>" . $t_qtot . "</td>";
                echo "<td>" . $t_score . "</td>";
                echo "<td>" . number_format($t_ps, 0, ",", ",") . "%</td>";
                echo "</tr>";
            }
            $tt_ps = ($tt_score / $tt_qtot) * 100;
        }
		echo "<tr>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td>".$tt_qtot."</td>";
			echo "<td>".$tt_score."</td>";
			echo "<td>".number_format($tt_ps,0,",",",")."%</td>";
		echo "</tr>";
		echo "</tbody>";
		echo "</table>";
		
		/*echo "</div> ";
	  	echo "</div> "; 	
		echo "</div> ";*/
	
	// ------ end Top 10 incorrect answers ------------------------------	
	} // end of opt = 5
		
		
		
	
	if ($opt == 6) {	// questions with errors
	// --- Questions with errors ------------------------
		
		/*echo "<div class=\"panel panel-primary\"> ";
		  echo "<div class=\"panel-heading\">Question Errors reported</div> "; 
		  echo "<div class=\"panel-body\">";*/
		  	
		echo "<table class=\"table table-striped\">";
		echo "<thead>";
        echo '<tr><th colspan="3" style="background:#58595b;color:#FFFFFF;">Question Errors Reported</th></tr>';
		echo "<tr>";
			echo "<th></th>";
			echo "<th>Question</th>";
			echo "<th>No of Errors</th>";
		echo "</tr>";		
		echo "</thead>";
		echo "<tbody>";// get data	
			$result_setZ = get_aca_question_errors($key);
			while ($row = mysqli_fetch_array($result_setZ)) {
				$qu_id = $row["question_id"];	
				$errors = $row["errors"];
				$result_setQ = get_aca_question_rec($qu_id, 0);
				while ($row = mysqli_fetch_array($result_setQ)) {
					$qu_question_text = $row["question_text"];	
				}
				
		echo "<tr>";
			echo "<td>".$qu_id."</td>";
			echo "<td>".nl2br($qu_question_text)."</td>";
			echo "<td>".number_format($errors,0,",",",")."</td>";
		echo "</tr>";
		}	
		echo "</tbody>";
		echo "</table>";
		
		
		
		// --- Error Questions  ------------------------
		echo "<table class=\"table table-hover\">";
		  echo "<thead>";
			echo "<tr>";
			  echo "<th colspan=\"4\" style=\"background:#58595b;color:#FFFFFF;\">Error Questions (answer not 0 or 1)</th>";
			echo "</tr>";
		  echo "</thead>";
		  echo "<tbody>";
		  		
		$query = "SELECT * FROM aca_question WHERE option_a_answer > 1 or option_b_answer > 1 or option_c_answer > 1 or option_d_answer > 1 or option_e_answer > 1 or option_f_answer > 1 ";
		$result_setZ = mysqli_query($connection,$query);
		confirm_query($result_setZ);
		if (mysqli_num_rows($result_setZ) >= 1) {
			while ($row = mysqli_fetch_array($result_setZ)) {
				$question_id = $row['question_id'];		
				$exam_id = $row["exam_id"];	
				$question_no = $row["question_no"];	
				$question_text = $row["question_text"];	
				$question_note = $row["question_note"];
				$option_a_text = $row["option_a_text"];
				$option_b_text = $row["option_b_text"];	
				$option_c_text = $row["option_c_text"];	
				$option_d_text = $row["option_d_text"];
				$option_e_text = $row["option_e_text"];
				$option_f_text = $row["option_f_text"];
				$option_a_answer = $row["option_a_answer"];	
				$option_b_answer = $row["option_b_answer"];	
				$option_c_answer = $row["option_c_answer"];	
				$option_d_answer = $row["option_d_answer"];		
				$option_e_answer = $row["option_e_answer"];		
				$option_f_answer = $row["option_f_answer"];		
				if ($option_a_answer == 1) { $oa = "A"; } else { $oa = ""; }
				if ($option_b_answer == 1) { $ob = "B"; } else { $ob = ""; }
				if ($option_c_answer == 1) { $oc = "C"; } else { $oc = ""; }
				if ($option_d_answer == 1) { $od = "D"; } else { $od = ""; }
				if ($option_e_answer == 1) { $oe = "E"; } else { $oe = ""; }
				if ($option_f_answer == 1) { $of = "F"; } else { $of = ""; }
				if ($option_a_answer > 1) { $oa = "<span class=\"label label-warning\">".$option_a_answer."</span>"; }
				if ($option_b_answer > 1) { $ob = "<span class=\"label label-warning\">".$option_b_answer."</span>"; }
				if ($option_c_answer > 1) { $oc = "<span class=\"label label-warning\">".$option_c_answer."</span>"; }
				if ($option_d_answer > 1) { $od = "<span class=\"label label-warning\">".$option_d_answer."</span>"; }
				if ($option_e_answer > 1) { $oe = "<span class=\"label label-warning\">".$option_e_answer."</span>"; }
				if ($option_f_answer > 1) { $oe = "<span class=\"label label-warning\">".$option_f_answer."</span>"; }
				$tool_id = $row["tool_id"];			
				$source = $row["source"];			
				$create_date = $row["create_date"];		
				$status = $row["status"];
					$result_set4 = get_dim("status", $status);
					while ($row = mysqli_fetch_array($result_set4)) {
						$dim_key = $row["dim_key"]; 
						$d_status = $row["description"];
					}
					$tool = "";
					$result_set5 = get_aca_tool_rec($tool_id);
					while ($row = mysqli_fetch_array($result_set5)) {
						$tool = $row["description"];
					}
					$exam_code = "";
					$result_setI = get_aca_exam_rec($exam_id);
					while ($row = mysqli_fetch_array($result_setI)) {		
						$exam_code = $row["code"];		
					}
				
			$e = "<button name=\"EditBut\" type=\"button\" value=\"Edit\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=32&q=".$q."&qq=".$q."&login=1&set=3&opt=0&key=".$question_id."&cat=".$cat."');return document.MM_returnValue\"/>Edit</button>";
		  
		  // print list detail
			  
			  	
			echo "<tr>";
				echo "<td align=\"left\">".$question_id."</td>";
				echo "<td align=\"left\">&nbsp;</td>";
				echo "<td align=\"left\"><a href=\"?id=".$id."&set=3&cat2=0&cat3=0&key=".$question_id."&cat=".$cat."\">".$question_text."</a></td>";
				echo "<td align=\"left\">".$e."</td>"; 
			echo "</tr>";	
			echo "<tr>";
				echo "<td align=\"left\"></td>";
				echo "<td align=\"left\"></td>";
				echo "<td align=\"left\">".$question_note."</td>";
				echo "<td align=\"left\">".$tool."</td>";
			echo "</tr>";	
			echo "<tr>";
				echo "<td align=\"left\">&nbsp;</td>";
				echo "<td align=\"left\">A</td>";
				echo "<td align=\"left\">".$option_a_text."</td>";
				echo "<td align=\"left\">".$oa."</td>";
			echo "</tr>";
			echo "<tr>";
				echo "<td align=\"left\">&nbsp;</td>";
				echo "<td align=\"left\">B</td>";
				echo "<td align=\"left\">".$option_b_text."</td>";
				echo "<td align=\"left\">".$ob."</td>";
			echo "</tr>";
			echo "<tr>";
				echo "<td align=\"left\">&nbsp;</td>";
				echo "<td align=\"left\">C</td>";
				echo "<td align=\"left\">".$option_c_text."</td>";
				echo "<td align=\"left\">".$oc."</td>";
			echo "</tr>";
			echo "<tr>";
				echo "<td align=\"left\">&nbsp;</td>";
				echo "<td align=\"left\">D</td>";
				echo "<td align=\"left\">".$option_d_text."</td>";
				echo "<td align=\"left\">".$od."</td>";
			echo "</tr>";
			echo "<tr>";
				echo "<td align=\"left\">&nbsp;</td>";
				echo "<td align=\"left\">E</td>";
				echo "<td align=\"left\">".$option_e_text."</td>";
				echo "<td align=\"left\">".$oe."</td>";
			echo "</tr>";
			echo "<tr>";
				echo "<td align=\"left\">&nbsp;</td>";
				echo "<td align=\"left\">F</td>";
				echo "<td align=\"left\">".$option_f_text."</td>";
				echo "<td align=\"left\">".$of."</td>";
			echo "</tr>";
			echo "<tr>";
				echo "<td align=\"left\"></td>";
				echo "<td align=\"left\"></td>";
				echo "<td align=\"left\">".$source."</td>";
				echo "<td align=\"left\">Status: ".$d_status."</td>";
			echo "</tr>";	
		echo "<tr>";
			echo "<td align=\"right\" colspan=\"4\" class=\"topline\">&nbsp;</td>";
		echo "</tr>";
			  
		}	
		
		

		} else {
			echo "<tr><td>no errors</td></tr>";
		} // end of if
        echo "</tbody>";
        echo "</table>";
	   			
	}	// end of opt = 6
	
	
		
		
	if ($opt == 7) { // trend		
	// --- Question Trends ------------------------		
		
		/*echo "<div class=\"panel panel-primary\"> ";
		  echo "<div class=\"panel-heading\">Question Trend</div> "; 
		  echo "<div cla
		ss=\"panel-body\">";*/

        echo "<table class=\"table table-hover\">";
        echo "<thead>";
        echo "<tr>";
        echo "<th style=\"background:#58595b;color:#FFFFFF;\">Question Trend</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody><tr><td>";
		  	
		// get data
		$tq = 0;
		$ts = 0;
		$tp = 0;
		$barh = 0;
		$style_row = "progress-bar"; // progress-bar
		$pb = " progress-bar-success "; //  . $style_row;
		
		
			//if ($ttest == 1) {
				if ($aca_accesslevel == 9) { $cat3 = 0; } // SW added on 2017/01/15
			//}	
		
		// get data	
		$result_setA = get_aca_v_results($cat, $cat6, $cat2, $cat3, $cat4, $cat5, $cat7);
		while ($row = mysqli_fetch_array($result_setA)) {		
			$date = $row["cdate"];
			$da = substr($date,5,2)."/".substr($date,7,5);
			$tq = $row["questions"];		
			$ts = $row["score"];
			$tp = ($ts / $tq)*100;
			$tp = number_format($tp,0,",",",");
			$tx = $tp."% (".$tq.")";
			if ($tp < 50) { $pb = "progress-bar-danger "; }
			if ($tp >= 50 && $tp < 60) { $pb = "progress-bar-warning "; }
			if ($tp >= 60) { $pb = "progress-bar-success "; }
			
			echo "<div class=\"progress\">";
  			echo "<div class=\"progress-bar ".$pb."\" role=\"progressbar\" aria-valuenow=\"40\"
  aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:".$tp."%\"> " .$tx. " ".$date;
  			echo "</div>";
			echo "</div>";
			 
		}

		echo '</td></tr></tbody></table>';
		 
		  //echo "</div>";
		//  echo "</div> ";
	  // echo " </div> "; 	
	  
	  
			if (isset($ttest) && $ttest == 1) {
			echo "<div class=\"alert alert-success\" role=\"alert\">";
  			echo "TEST In TREND</br>";
  			echo "TEST CAT0".$cat."</br>";
  			echo "TEST CAT2".$cat2."</br>";
  			echo "TEST CAT3".$cat3."</br>";
  			echo "TEST CAT4".$cat4."</br>";
  			echo "TEST CAT5".$cat5."</br>";
  			echo "TEST CAT6".$cat6."</br>";
  			echo "TEST CAT7".$cat7."</br>";
		    echo "</div>";	
			}	
	  
	} // end of opt = 7

	
	
	if ($opt == 0) { // summary by subject
	// --- Summary by Subject ------------------------
	
		
		echo "<table class=\"table table-striped\">";
		echo "<thead>";
		echo '<tr>
				<th colspan="5" style="background:#58595b;color:#FFFFFF;">Summary by Subject</th>
			  </tr>';
		echo "<tr>";
			echo "<th>Exam</th>";
			echo "<th>Subject</th>";
			echo "<th>Questions</th>";
			echo "<th>Correct</th>";
			echo "<th>%</th>";
		echo "</tr>";		
		echo "</thead>";
		echo "<tbody>";

		// set variables
		$tt_qtot = 0;
		$tt_score = 0;
		$tt_ps = 0;
		
		// get data	
		$result_setAA = get_aca_option_results_tool_instructor($cat, $cat6, $cat2, $cat3, $cat4, $cat5, $cat7);
		while ($row = mysqli_fetch_array($result_setAA)) {		
			$exam_id = $row["exam_id"];
			//$instructor = $row["instructor"];		
			$exam_code = $row["code"];		
			$tool = $row["tool"];		
			$tool_id = $row["tool_id"];		
			$t_qtot = $row["questions"];		
			$t_score = $row["score"];
			$t_ps = ($t_score / $t_qtot)*100;
			$tt_qtot = $tt_qtot + $t_qtot;
			$tt_score = $tt_score + $t_score;
			
			$resultSExam = "<a href=\"?id=".$id."&set=0&opt=".$opt."&login=1&cat=".$exam_id."&cat6=".$cat6."&cat7=".$cat7."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$exam_code."</a>";
			$resultSSubject = "<a href=\"?id=".$id."&set=0&opt=".$opt."&key=".$key."&login=1&key=".$key."&cat6=".$tool_id."&cat=".$cat."&cat7=".$cat7."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$tool."</a>";	
				 
		echo "<tr>";
			echo "<td>".$resultSExam."</td>";
			echo "<td>".$resultSSubject."</td>";
			echo "<td>".$t_qtot."</td>";
			echo "<td>".$t_score."</td>";
			echo "<td>".number_format($t_ps,0,",",",")."%</td>";
		echo "</tr>";
		}	
		if ($tt_qtot == 0) { $tt_ps = 0; } else { $tt_ps = ($tt_score / $tt_qtot)*100; }
		echo "<tr>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td>".$tt_qtot."</td>";
			echo "<td>".$tt_score."</td>";
			echo "<td>".number_format($tt_ps,0,",",",")."%</td>";
		echo "</tr>";
		echo "</tbody>";
		echo "</table>";
	
	// ------ end Summary by Subject ------------------------------
	} // end of opt = 0
	
	
		
	// Test
	if ($test_aca == 1) {
		echo "Test before question form</br>";
		echo "Set: ".$set. "</br>";
		echo "Q: ".$q. "</br>";
		//echo "Qno: ".$aca_qno. "</br>";
		//echo "Q Tot: ".$q_tot. "</br>";
		//echo "Q Cnt: ".$q_count. "</br>";
		echo "Login: ".$login. "</br>";
		echo "Cat: ".$cat. "</br>";
		echo "Cat2: ".$cat2. "</br>";
		echo "Cat3: ".$cat3. "</br>";
		echo "Cat4: ".$cat4. "</br>";
		echo "Cat5: ".$cat5. "</br>";
		echo "Cat6: ".$cat6. "</br>";
		echo "Cat7: ".$cat7. "</br>";
	}



	if ($opt == 0 || $opt == 7) { // summary by test		
	// ------- Summary by Test -----------------------
	
		/*echo "<div class=\"panel panel-primary\"> ";
		  echo "<div class=\"panel-heading\">Summary by Test</div> "; 
		  echo "<div class=\"panel-body\">";*/
		  	
		echo "<table class=\"table table-striped\">";
		echo "<thead>";
		echo '<tr><th colspan="8" style="background:#58595b;color:#FFFFFF;">Summary by Test</th></tr>';
		echo "<tr>";
			echo "<th>User</th>";
			echo "<th>Name</th>";
			echo "<th>Exam</th>";
			echo "<th>Version</th>";
			echo "<th>Date</th>";
			echo "<th>Questions</th>";
			echo "<th>Correct</th>";
			echo "<th>%</th>";
		echo "</tr>";		
		echo "</thead>";
		echo "<tbody>";
				// get data
		$tt_qtot = 0;
		$tt_score = 0;
		$tt_ps = 0;	
		// get record count
		$p_records = 0;
		$result_setA = get_aca_option_results_instructor_count($cat, $cat6, $cat2, $cat3, $cat4, $cat5, $cat7); // status 1 = valid, access 0 for all
		while ($row = mysqli_fetch_array($result_setA)) {
			$p_records = $row["record_count"]; 
		}	
		$anchor = "x";
		$p_link = "?id=".$id."&opt=".$opt."&cat2=".$cat2."&cat3=".$cat3."&cat6=".$cat6."&cat7=".$cat7."&key=".$key."&cat=".$cat."&cat4=".$cat4."&set=".$set;
		$p = page_nav_last($page, $page_break, $p_records, $p_link, $anchor);
		$n = "<a href=\"?id=".$id."&set=1&key=".$key."\">New</a>";
		$c = " ";
		echo "<p align=\"right\">".$p . "</p></br>";
		$npage = $page;
		$result_set = get_aca_option_results_instructor($cat, $cat6, $cat2, $cat3, $cat4, $cat5, $cat7, $npage, $page_break);
		while ($row = mysqli_fetch_array($result_set)) {		
			$user = $row["username"];			
			$name = $row["name"];		
			$creator_id = $row["creator_id"];		
			$exam_id = $row["exam_id"];
			$ver = trim($row["userthis"]);	
			$cdate = $row["create_date"];	
			$qtot = $row["questions"];		
			$score = $row["score"];
			$ps = ($score / $qtot)*100;
			$tt_qtot = $tt_qtot + $qtot;
			$tt_score = $tt_score + $score;
			$result_setXX = get_aca_exam_rec($exam_id);
			while ($row = mysqli_fetch_array($result_setXX)) {
				$exam_code = $row["code"];
			} 
			
			$resultUser = "<a href=\"?id=".$id."&set=0&opt=".$opt."&login=1&key=".$key."&cat=".$cat."&cat7=".$cat7."&cat6=".$cat6."&cat2=".$cat2."&cat3=".$creator_id."&cat4=".$cat4."&cat5=".$cat5."\">".$user."</a>";
			$resultName = "<a href=\"?id=".$id."&set=0&opt=".$opt."&login=1&key=".$key."&cat=".$cat."&cat7=".$cat7."&cat6=".$cat6."&cat2=".$cat2."&cat3=".$creator_id."&cat4=".$cat4."&cat5=".$cat5."\">" .$name. "</a>";
			$resultExam = "<a href=\"?id=".$id."&set=0&opt=".$opt."&login=1&cat=".$exam_id."&cat6=".$cat6."&cat7=".$cat7."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$exam_code."</a>";
			$resultVersion = "<a href=\"?id=".$id."&set=0&opt=".$opt."&login=1&key=".$key."&cat=".$cat."&cat6=".$cat6."&cat7=".$cat7."&cat2=".$ver."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$ver."</a>";
			$resultDate = "<a href=\"?id=".$id."&set=0&opt=".$opt."&login=1&key=".$key."&cat=".$cat."&cat6=".$cat6."&cat7=".$cat7."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cdate."&cat5=".$cat5."\">".$cdate."</a>";	 
		echo "<tr>";
			echo "<td>".$resultUser."</td>";
			echo "<td>".$resultName."</td>";
			echo "<td>".$resultExam."</td>";
			echo "<td>".$resultVersion."</td>";
			echo "<td>".$resultDate."</td>";
			echo "<td>".$qtot."</td>";
			echo "<td>".$score."</td>";
			echo "<td>".number_format($ps,0,",",",")."%</td>";
		echo "</tr>";
		}	
		if ($tt_qtot == 0) { $tt_ps = 0; } else { $tt_ps = ($tt_score / $tt_qtot)*100; }
		echo "<tr>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td>".$tt_qtot."</td>";
			echo "<td>".$tt_score."</td>";
			echo "<td>".number_format($tt_ps,0,",",",")."%</td>";
		echo "</tr>";
		echo "</tbody>";
		echo "</table>";
	   echo "<p align=\"right\">".$p . "</p></br>";
	   
		echo "</div> "; 
	    echo "</div> "; 
	echo " </div> "; 	
	
	// ------ end Summary by Test ------------------------------
	} // end of opt = 0



// test summary


if ($cat3 <> 0) {

		/*echo "<div class=\"panel panel-primary\"> ";
		  echo "<div class=\"panel-heading\">User Test Summary</div> "; 
		  echo "<div class=\"panel-body\">";*/
		  	

		// display test results		
		//echo "<h3>User Test Summary</h3>";
		// get test results		
		
		// variables
		$subject = "xxxx";
		$exam = "xxxx";
		$no_of_tests = 0;
		$no_of_questions = 0;
		$score = 0;
			
		echo '<table class="table table-striped">
<tr>
<th colspan="8" style="background:#58595b;color:#FFFFFF;">User Test Summary</th>
</tr>';
	
		$result_setQ = get_aca_results_eval(0, $cat3, $cat7); // $cat = exam_id, $cat3 = $user_id, $cat7 = $class_id
		while ($row = mysqli_fetch_array($result_setQ)) {
			$class_id = $row["class_id"];	
			$user_id = $row["user_id"];		
			$exam_id = $row["exam_id"];	
			$no_of_tests = $row["testcnt"];	
			$no_of_questions = $row["qcnt"];		
			$score  = $row["score"];
			if ($no_of_questions == 0) { $scorep = 0; } else { $scorep = ($score / $no_of_questions) * 100; }
			$result_set1 = get_aca_exam_rec($exam_id);
			while ($row = mysqli_fetch_array($result_set1)) {
				$exam = $row["code"];
			} 			 
			echo "<tr>";
				echo "<th>Exam</th>";
				echo "<td>".$exam."</td>";
				echo "<th># Tests</th>";
				echo "<td>".$no_of_tests."</td>";
				echo "<th># Questions</th>";
				echo "<td>".$no_of_questions."</td>";
				echo "<th>Score</th>";
				echo "<td>".number_format($scorep,0,",",",")."%</td>";
			echo "</tr>";		
		}
		echo "<tr>";
			echo "<th colspan=\"8\">&nbsp;</th>";
		echo "</tr>";	
		echo "</table>";


		//echo "</div> ";
	    /*echo "</div> ";
	echo " </div> ";*/




} // end if cat3 <> 0 -- user selection

// end of test summary



	
	
	} // end of set = 0
	


	echo " </div> "; 	// end of container	
	echo " </div> "; 	// end of container

?>

	<script>

        $("#instructor").on("change",function(){
            var class_id = $("#class").val();
            var instructor_id = $("#instructor").val();
            window.location.href = "?id=<?php echo $_GET["id"]; ?>&set=0&opt=<?php echo $_GET["opt"]; ?>&key=<?php echo $_GET["key"]; ?>&cat=<?php echo $_GET["cat"]; ?>&cat5=" + instructor_id + "&cat6=<?php echo $_GET["cat6"]; ?>&cat7=" + class_id + "&login=1&cat2=<?php echo $_GET["cat2"]; ?>&cat3=<?php echo $_GET["cat3"]; ?>&cat4=<?php echo $_GET["cat4"]; ?>";
        })

        $("#class").on("change",function(){
            var class_id = $("#class").val();
            var instructor_id = $("#instructor").val();
            window.location.href = "?id=<?php echo $_GET["id"]; ?>&set=0&opt=<?php echo $_GET["opt"]; ?>&key=<?php echo $_GET["key"]; ?>&cat=<?php echo $_GET["cat"]; ?>&cat5=" + instructor_id + "&cat6=<?php echo $_GET["cat6"]; ?>&cat7=" + class_id + "&login=1&cat2=<?php echo $_GET["cat2"]; ?>&cat3=<?php echo $_GET["cat3"]; ?>&cat4=<?php echo $_GET["cat4"]; ?>";
        })

</script>
<?php

} // end of id = 40

?>


 