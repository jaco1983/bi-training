<?php

$exam_id = 1;
$nrquestions = 10;

require_once("lib/fpdf/fpdf.php");


$_SESSION["genpdf"] = $exam_id;

class PDF extends FPDF
{
// Page header
function Header()
{
if ( $this->PageNo() == 1 ) {

global $connection;
global $exam_id;

$sql = "select * from aca_exam where exam_id = '".$_SESSION["genpdf"]."'";

$result_set = mysqli_query($connection, $sql);

while ($row = mysqli_fetch_array($result_set)) {
$this->SetFont('Arial', 'B', 12);
$this->SetTextColor(0, 0, 0);
$this->Cell(20,5,'',0,'C',false);
$this->MultiCell(150,5,$row["code"].' - '.$row["description"],0,'C',false);
$this->Cell(20,5,'',0,'C',false);
$this->Ln(2);
$this->SetFont('Arial', 'B', 9);
$this->Cell(20,5,'',0,'C',false);
$this->MultiCell(150,5,'Version: '.$row["versions"],0,'C',false);
$this->Cell(20,5,'',0,'C',false);
}
}
}

// Page footer
function Footer()
{
// Go to 1.5 cm from bottom
$this->SetY(-15);
// Select Arial italic 8
$this->SetFont('Arial','I',8);
// Print centered page number
$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
$this->Cell(0,10,'https://training.blackboardbi.com',0,0,'R');

}
}
global $connection;
$sql = "SELECT * FROM aca_question where exam_id = '".$exam_id."' order by rand() limit ".$nrquestions;

$result_set = mysqli_query($connection, $sql);


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(10);


$i = 1;
$questions = array();
while ($row = mysqli_fetch_array($result_set)) {
$questions[] = $row["question_id"];
$pdf->SetFont('Arial', 'B', 8);
$pdf->SetTextColor(0, 0, 0);
$pdf->Cell(10,5,$i.'.)',0,'L',false);
$pdf->MultiCell(180, 5, $row["question_text"], 0, 'L', false);
$pdf->Ln(3);
$pdf->SetFont('Arial', '', 6);
$pdf->SetTextColor(0, 0, 0);
if(isset($row["option_a_text"]) && $row["option_a_text"] != ''){
$pdf->Cell(10,3,'',0,'L', false);
$pdf->Cell(3,3,'',1,'L', false);
$pdf->Cell(5,3,'A. ',0,'R', false);
$pdf->MultiCell(167, 3, $row["option_a_text"], 0, 'L', false);
$pdf->Ln(3);
}
if(isset($row["option_b_text"]) && $row["option_b_text"] != ''){
$pdf->Cell(10,3,'',0,'L', false);
$pdf->Cell(3,3,'',1,'L', false);
$pdf->Cell(5,3,'B. ',0,'R', false);
$pdf->MultiCell(167, 3, $row["option_b_text"], 0, 'L', false);
$pdf->Ln(3);
}
if(isset($row["option_c_text"]) && $row["option_c_text"] != ''){
$pdf->Cell(10,3,'',0,'L', false);
$pdf->Cell(3,3,'',1,'L', false);
$pdf->Cell(5,3,'C. ',0,'R', false);
$pdf->MultiCell(180, 3, $row["option_c_text"], 0, 'L', false);
$pdf->Ln(3);
}
if(isset($row["option_d_text"]) && $row["option_d_text"] != ''){
$pdf->Cell(10,3,'',0,'L', false);
$pdf->Cell(3,3,'',1,'L', false);
$pdf->Cell(5,3,'D. ',0,'R', false);
$pdf->MultiCell(180, 3, $row["option_d_text"], 0, 'L', false);
$pdf->Ln(3);
}
if(isset($row["option_e_text"]) && $row["option_e_text"] != ''){
$pdf->Cell(10,3,'',0,'L', false);
$pdf->Cell(3,3,'',1,'L', false);
$pdf->Cell(5,3,'E. ',0,'R', false);
$pdf->MultiCell(180, 3, $row["option_e_text"], 0, 'L', false);
$pdf->Ln(3);
}
$pdf->Ln(3);
$i++;
/*if(is_float($i/7) == false){
$pdf->AddPage();
}*/
}

$pdf->AddPage();

$i = 1;

$col = 0;

foreach($questions as $key=>$value) {
$sql2 = "SELECT * FROM aca_question where question_id = '".$value."'";

$result_set2 = mysqli_query($connection, $sql2);


while ($row = mysqli_fetch_array($result_set2)) {
$answers = array();
if($col == '4'){
$pdf->Ln(3);
$pdf->Ln(3);
$col = 0;
}
$pdf->SetFont('Arial', 'B', 8);
$pdf->SetTextColor(0, 0, 0);
$pdf->Cell(10, 5, $i . '.)', 0, 'L', false);
$pdf->SetFont('Arial', '', 8);
$pdf->SetTextColor(0, 0, 0);
if (isset($row["option_a_answer"]) && $row["option_a_answer"] != '') {
if ($row["option_a_answer"] == '1') {
array_push($answers,'a');
}

//$pdf->Ln(3);
}
if (isset($row["option_b_answer"]) && $row["option_b_answer"] != '') {
if ($row["option_b_answer"] == '1') {
array_push($answers,'b');
}

//$pdf->Ln(3);
}
if (isset($row["option_c_answer"]) && $row["option_c_answer"] != '') {
if ($row["option_c_answer"] == '1') {
array_push($answers,'c');
}

//$pdf->Ln(3);
}
if (isset($row["option_d_answer"]) && $row["option_d_answer"] != '') {
if ($row["option_d_answer"] == '1') {
array_push($answers,'d');
}

//$pdf->Ln(3);
}
if (isset($row["option_e_answer"]) && $row["option_e_answer"] != '') {
if ($row["option_e_answer"] == '1') {
array_push($answers,'e');
}

//$pdf->Ln(3);
}
$answers_str = implode(',',$answers);
$pdf->Cell(40, 5, $answers_str, 0, 'L', false);
//$pdf->Ln(3);
$i++;
/*if(is_float($i/7) == false){
$pdf->AddPage();
}*/
$col++;
}
}
$out = $pdf->Output('pdf/exams/'.date("ymd").'.pdf', 'F');


?>