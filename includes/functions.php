<?php
// functions.php
// Beyond Analysis

/*\\\\\\\\\\\\\\\\\\\\\\\\\\\ common functions    //////////////////////////////*/


function mysql_prep($value) {
    $return = '';
    for($i = 0; $i < strlen($value); ++$i) {
        $char = $value[$i];
        $ord = ord($char);
        if($char !== "'" && $char !== "\"" && $char !== '\\' && $ord >= 32 && $ord <= 126)
            $return .= $char;
        else
            $return .= '\\x' . dechex($ord);
    }
    return $return;
}


function redirect_to( $location = NULL) {									
	if ($location != NULL) {
		header('Location: ' . $location);
		exit;
	}		
}

function confirm_query($result_set) {
    global $connection;
	if (!$result_set) {
		die("Database query failed: " . mysqli_error($connection));
	}		
}


//to insure we get the userip 												
function get_ip($type) { 
		 if (getenv("HTTP_CLIENT_IP") 
			 && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown")) 
			 $ip = getenv("HTTP_CLIENT_IP"); 
		 else if (getenv("REMOTE_ADDR") 
			 && strcasecmp(getenv("REMOTE_ADDR"), "unknown")) 
			 $ip = getenv("REMOTE_ADDR"); 
		 else if (getenv("HTTP_X_FORWARDED_FOR") 
			 && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown")) 
			 $ip = getenv("HTTP_X_FORWARDED_FOR"); 
		 else if (isset($_SERVER['REMOTE_ADDR']) 
			 && $_SERVER['REMOTE_ADDR'] 
			 && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown")) 
			 $ip = $_SERVER['REMOTE_ADDR']; 
		 else { 
			 $ip = "unknown"; 
		 return $ip; 
			 } 
		 if ($type==1) {return md5($ip);} 
		 if ($type==0) {return $ip;} 
		 } 

// get yes or no
function get_yn($yn) {
	if ($yn == 1) { $result = "Yes"; } else { $result = "No"; }
	return $result;
}
/*\\\\\\\\\\\\\\\\\\\\\\\\\\\ general functions //////////////////////////////*/

// get the latest record
function get_rec_latest($table, $field) {
	
	global $connection;
	$query = " SELECT max({$field}) as last_no FROM {$table} "; 
	$query .= " LIMIT 1 ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}

// dimention value
function get_dim($table, $key) {
	global $connection;
$query = "SELECT * ";
	$query .= " FROM {$table} ";
	$query .= " WHERE dim_key = '{$key}' ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

// dimention values
function get_dim_list($table) {
	global $connection;
	$query = "SELECT dim_key, description, status ";
	$query .= " FROM {$table} ";
	$query .= " WHERE status = 1 ";
	$query .= " ORDER BY description ASC ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_dim_list_sort($table, $sortorder, $status) { 
	// sortorder: 1 = description asc; 2 = description desc; 3 = dim_key asc; 4 = dim_key desc 
	global $connection;
	$query = "SELECT * ";  
	$query .= " FROM {$table} ";
	if($status <> 99) { $query .= " WHERE status = '{$status}' "; }
	if ($sortorder == 1) { $query .= " ORDER BY description ASC "; }
	if ($sortorder == 2) { $query .= " ORDER BY description DESC "; }
	if ($sortorder == 3) { $query .= " ORDER BY dim_key ASC "; }
	if ($sortorder == 4) { $query .= " ORDER BY dim_key DESC "; }
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

// dimention count
function get_dim_count($table, $status) {
	global $connection;
	$query = "SELECT count(dim_key) as record_count  ";
	$query .= " FROM {$table} ";
	if($status <> 99) { $query .= " WHERE status = '{$status}' "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

// dimention records for all languages
function get_dim_all($table, $status) {
	global $connection;
	$query = "SELECT *  ";
	$query .= " FROM {$table} ";
	if($status <> 99) { $query .= " WHERE status = '{$status}' "; }
	$query .= " ORDER BY dim_key ASC ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	


// for other type of dimention tables
function get_dim_rec_z($table, $field, $key) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM {$table} ";
	$query .= " WHERE {$field} = '{$key}' ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_dim_z($table, $order_field, $direction, $status) {
	if ($direction == 1) { $dir = "ASC"; } else { $dir = "DESC"; }
	global $connection;
	$query = "SELECT *  ";
	$query .= " FROM {$table} ";
	if($status <> 99) { $query .= " WHERE status = '{$status}' "; }
	if($order_field <> 0) { $query .= " ORDER BY '{$order_field}' '{$dir}' "; }
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	


// page navigation							
function page_nav_last($p_page, $p_break, $p_records, $p_link, $id) { 
	
	$pages = $p_records / $p_break;	// max number of pages to display
	if($pages != round($pages,0)) {
		$pages = floor($pages) + 1;
	}
	$current_page = floor($p_page/$p_break); // current page number
	$prev_page = $current_page - 1;
	$next_page = $current_page + 1;
	$pages_link = ($pages - 1) * $p_break;
	$prev_link = $prev_page * $p_break;
	$next_link = $next_page * $p_break;
	$curr_text = "" . ($current_page + 1) . " ";
	
	$first_text = "<button name=\"FirstBut\" type=\"button\" value=\"First\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','" . $p_link . "&page=1#".$id."');return document.MM_returnValue\"/>&lArr; First Page</button>";
	$pages_text = " <a href=\"" . $p_link . "&page=" . $pages_link . "\">" . $pages . "</a> ";
	$prev_text = "<button name=\"PrevBut\" type=\"button\" value=\"Prev\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','" . $p_link . "&page=" . $prev_link . "#".$id."');return document.MM_returnValue\"/>&lt; Previous Page</button>";
	$next_text = " <button name=\"NextBut\" type=\"button\" value=\"Next\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','" . $p_link . "&page=" . $next_link . "#".$id."');return document.MM_returnValue\"/>Next Page &gt;</button>";
	$last_text = "<button name=\"LastBut\" type=\"button\" value=\"Next\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','" . $p_link . "&page=" . $pages_link . "#".$id."');return document.MM_returnValue\"/>Last Page &rArr;</button>";

	// validate
	if($curr_text == 1) { $first_text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; }
	if($prev_page < 0) { $prev_text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; }
	if($next_page >= $pages) { $next_text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";	} 	
	if($pages < 2 || $pages_link = $p_page  ) { $last_text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";	} 	
	if($pages == 0) { $pagesx = ""; } else { $pagesx = " of " . $pages; }
	$page_nav_text = "Page " . $curr_text . $pagesx . "&nbsp;&nbsp;&nbsp;" . $first_text . " " .  $prev_text . " " . $next_text . " " . $last_text;
	
	return $page_nav_text;
}

// page navigation with key					
function page_nav_last_key($p_page, $p_break, $p_records, $p_link, $id, $key) { 
	$text_first = "First";
	$text_previous = "Previous";
	$text_next = "Next";
	$text_of = " of ";
	$text_page = "Page: ";
	$text_page_last = "Last ";
	
	$pages = $p_records / $p_break;	// max number of pages to display
	if($pages != round($pages,0)) {
		$pages = floor($pages) + 1;
	}
	$current_page = floor($p_page/$p_break); // current page number
	$prev_page = $current_page - 1;
	$next_page = $current_page + 1;
	$pages_link = ($pages - 1) * $p_break;
	$prev_link = $prev_page * $p_break;
	$next_link = $next_page * $p_break;
	$curr_text = "" . ($current_page + 1) . " ";
	
	$first_text = " <a href=\"" . $p_link . "&page=1\">&lArr;" . $text_first . "</a> ";
	$pages_text = " <a href=\"" . $p_link . "&page=" . $pages_link . "\">" . $pages . "</a> ";
	$prev_text = " &nbsp;&nbsp;&nbsp;&nbsp;<a href=\"" . $p_link . "&page=" . $prev_link . "#".$id."&key".$key."\">&lt; " . $text_previous . "</a> ";
	$next_text = " <a href=\"" . $p_link . "&page=" . $next_link . "#".$id."&key".$key."\">" . $text_next . " &gt;</a> ";	
	$last_text = "  &nbsp;&nbsp;&nbsp;&nbsp;<a href=\"" . $p_link . "&page=" . $pages_link . "#".$id."&key".$key."\">".$text_page_last . "&rArr;</a> ";
	// validate
	if($curr_text == 1) { $first_text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; }
	if($prev_page < 0) { $prev_text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; }
	if($next_page >= $pages) { $next_text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";	} 	
	if($pages < 2 || $pages_link = $p_page  ) { $last_text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";	} 	
	if($pages == 0) { $pagesx = ""; } else { $pagesx = $text_of . $pages; }
	$page_nav_text = $text_page . $curr_text . $pagesx . "&nbsp;&nbsp;&nbsp;" . $first_text . " " .  $prev_text . " " . $next_text . " " . $last_text;

	return $page_nav_text;
}


/* <<<<<<<<<<<<<<<<< ++++ CORRLINE ++++ >>>>>>>>>>>>>>>>> */	
/* 
	cat 		= 
	cat2		= 
	cat3		= 
	cat4		= 		
	status 		= 99 for all recordss
	
	
*/


// aca users
function get_aca_user_count($status) {
	// status 99 for all
	global $connection;
	$query = "SELECT count(user_id) as record_count "; 
	$query .= " FROM aca_user ";
	$query .= " WHERE user_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_user_rec($key) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_user ";
	$query .= " WHERE user_id = {$key} ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_user_list($npage, $no_of_list, $status) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_user ";
	$query .= " WHERE user_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	//$query .= " ORDER BY name ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_user_class_instructor_list($instructor, $class, $status) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_user ";
	$query .= " WHERE user_id > 0 "; 
	if ($instructor <> 0) { $query .= " AND instructor_id = {$instructor} "; }
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	if ($class <> 0) { $query .= " AND user_id <> (select aca_user_class.user_id from aca_user_class where  aca_user_class.class_id = {$class} AND aca_user_class.user_id = {$user_id} "; }
	//$query .= " ORDER BY name ASC ";
	//$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}

function get_aca_user_list_access($access, $status) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_user ";
	$query .= " WHERE user_id > 0 "; 
	if ($access <> 0) { $query .= " AND access_level = {$access} "; }
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY name ASC ";
	//$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

// aca user_exam
/*
function get_aca_user_exam_count($status) {
	// status 99 for all
	global $connection;
	$query = "SELECT count(user_exam_id) as record_count "; 
	$query .= " FROM aca_user_exam ";
	$query .= " WHERE user_exam_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_user_exam_rec($key) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_user_exam ";
	$query .= " WHERE user_exam_id = {$key} ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_user_exam_list($npage, $no_of_list, $status) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_user_exam ";
	$query .= " WHERE user_exam_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	//$query .= " ORDER BY name ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	
*/

// aca user_class
function get_aca_user_class_count($status, $thisuser, $cat7) {
	// status 99 for all
	global $connection;
	$query = "SELECT count(user_class_id) as record_count "; 
	$query .= " FROM aca_user_class ";
	$query .= " WHERE user_class_id > 0 "; 
	if ($thisuser <> 0) { $query .= " AND user_id = {$thisuser} "; }
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	if ($cat7 <> 0) { $query .= " AND class_id = {$cat7} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_user_class_rec($key) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_user_class ";
	$query .= " WHERE user_class_id = {$key} ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_user_class_list($npage, $no_of_list, $status, $thisuser, $cat7) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_user_class ";
	$query .= " WHERE user_class_id > 0 "; 
	if ($thisuser <> 0) { $query .= " AND user_id = {$thisuser} "; }
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	if ($cat7 <> 0) { $query .= " AND class_id = {$cat7} "; }
	//$query .= " ORDER BY name ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_user_class_list_assigned($status, $thisuser) {
	global $connection;
	$query = "SELECT u.user_id, u.user_class_id, c.name, c.class_id  ";
	$query .= " FROM aca_user_class u, aca_class c ";
	$query .= " WHERE u.class_id = c.class_id "; 
	if ($thisuser <> 0) { $query .= " AND u.user_id = {$thisuser} "; }
	if ($status <> 99) { $query .= " AND u.status = {$status} "; }
	$query .= " ORDER BY name ASC ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}

// aca user_class
function get_aca_class_count($status, $expiry, $cat7) {
	// status 99 for all
	global $connection;
	$query = "SELECT count(class_id) as record_count "; 
	$query .= " FROM aca_class ";
	$query .= " WHERE class_id > 0 "; 
	//if ($thisuser <> 0) { $query .= " AND user_id <> {$thisuser} "; }
	if ($expiry <> 0) { $query .= " AND expiry_date > {$expiry} "; }
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	if ($cat7 <> 0) { $query .= " AND class_id = {$cat7} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_class_rec($key) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_class ";
	$query .= " WHERE class_id = {$key} ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_class_list($npage, $no_of_list, $status, $expiry, $cat7) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_class ";
	$query .= " WHERE class_id > 0 "; 
	if ($expiry <> 0) { $query .= " AND expiry_date > {$expiry} "; }
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	//if ($cat7 <> 0) { $query .= " AND class_id = {$cat7} "; } // 20160116 sw
	if ($cat7 <> 0) { $query .= " AND instructor_id = {$cat7} "; }
	//$query .= " ORDER BY name ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	


function get_aca_userX_class_list($npage, $no_of_list, $status, $expiry, $thisuser) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_class c ";
	$query .= " WHERE c.class_id > 0 "; 
	if ($thisuser <> 0) { 
				$query .= " AND c.class_id NOT IN ( select u.class_id from aca_user_class u where u.user_id = {$thisuser} ) "; 
				
	}
	if ($expiry <> 0) { $query .= " AND c.expiry_date > {$expiry} "; }
	if ($status <> 99) { $query .= " AND c.status = {$status} "; }
	//$query .= " ORDER BY name ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	



function get_aca_userX_class_assessment_list($npage, $no_of_list, $status, $expiry, $q) {
	// select users that belongs to a class and no assessment was done for that user for that class
	global $connection;
	$query = "SELECT u.user_id, u.username, u.name as user, c.class_id, c.name as class ";
	$query .= " FROM aca_user_class uc, aca_user u, aca_class c ";
	$query .= " WHERE u.user_id = uc.user_id and uc.class_id = c.class_id "; 
	$query .= " AND uc.user_class_id NOT IN ( select uc.user_class_id from aca_user_class uc, aca_assessment a where uc.user_id = a.user_id and uc.class_id = a.class_id ) "; 
	if ($q <> 0) { $query .= " AND c.class_id = '{$q}' "; }
	if ($expiry <> 0) { $query .= " AND c.expiry_date > {$expiry} "; }
	if ($status <> 99) { $query .= " AND c.status = {$status} "; }
	//$query .= " ORDER BY name ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	


function get_aca_user_class_full_list($npage, $no_of_list, $status, $expiry, $q) {
	// select users that belongs to a class, validate expiry date, status 
	global $connection;
	$query = "SELECT u.user_id, u.username, u.name as user, c.class_id, c.name as class ";
	$query .= " FROM aca_user_class uc, aca_user u, aca_class c ";
	$query .= " WHERE u.user_id = uc.user_id and uc.class_id = c.class_id "; 
	//$query .= " AND uc.user_class_id NOT IN ( select uc.user_class_id from aca_user_class uc, aca_assessment a where uc.user_id = a.user_id and uc.class_id = a.class_id ) "; 
	if ($q <> 0) { $query .= " AND c.class_id = '{$q}' "; }
	if ($expiry <> 0) { $query .= " AND c.expiry_date > {$expiry} "; }
	if ($status <> 99) { $query .= " AND c.status = {$status} "; }
	//$query .= " ORDER BY name ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

	

function get_aca_class_list_type($npage, $no_of_list, $status, $expiry, $cat7, $class_type) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_class ";
	$query .= " WHERE class_id > 0 "; 
	if ($expiry <> 0) { $query .= " AND expiry_date > {$expiry} "; }
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	//if ($cat7 <> 0) { $query .= " AND class_id = {$cat7} "; } // 20160116 sw
	if ($class_type <> 0) { $query .= " AND class_type_id = {$class_type} "; }
	if ($cat7 <> 0) { $query .= " AND instructor_id = {$cat7} "; }
	//$query .= " ORDER BY name ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	



/*
select u.user_id, u.name as user
from
aca_user u inner join aca_user_class uc on (u.user_id = uc.user_id) inner join aca_assessment a on (uc.class_id not 
*/



/* <<<<<<<<<<<<<<<<< c_home                >>>>>>>>>>>>>>>>> */	
/*
function get_aca_user_rec($key) {
	global $connection;
	$query = "SELECT * FROM user WHERE user_id = {$key} ";	
	$query .= " LIMIT 1  ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}

function get_aca_user_count($status) {
	global $connection;
	$query = "SELECT count(user_id) as record_count "; 
	$query .= " FROM user ";
	$query .= " WHERE user_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_user_list($npage, $no_of_list, $status) {
	global $connection;
	$query = "SELECT * FROM user ";
	$query .= " WHERE user_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY user_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}	
*/

/* <<<<<<<<<<<<<<<<< instructor                >>>>>>>>>>>>>>>>> */	

function get_aca_instructor_rec($key) {
	global $connection;
	$query = "SELECT * FROM aca_instructor WHERE instructor_id = {$key} ";	
	$query .= " LIMIT 1  ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}


function get_aca_instructor_user_rec($key) {
	global $connection;
	$query = "SELECT * FROM aca_instructor WHERE user_id = {$key} ";	
	$query .= " LIMIT 1  ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}


function get_aca_instructor_count($status) {
	global $connection;
	$query = "SELECT count(instructor_id) as record_count "; 
	$query .= " FROM aca_instructor ";
	$query .= " WHERE instructor_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_instructor_list($npage, $no_of_list, $status) {
	global $connection;
	$query = "SELECT * FROM aca_instructor ";
	$query .= " WHERE instructor_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY instructor_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}

/* <<<<<<<<<<<<<<<<< representative                >>>>>>>>>>>>>>>>> */	
function get_rep_rec($key) {
	global $connection;
	$query = "SELECT * FROM representative WHERE rep_id = {$key} ";	
	$query .= " LIMIT 1  ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}

function get_rep_count($status) {
	global $connection;
	$query = "SELECT count(rep_id) as record_count "; 
	$query .= " FROM representative ";
	$query .= " WHERE rep_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	
function get_rep_list($npage, $no_of_list, $status) {
	global $connection;
	$query = "SELECT * FROM representative ";
	$query .= " WHERE rep_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY rep_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}

/* <<<<<<<<<<<<<<<<< area                >>>>>>>>>>>>>>>>> */	
function get_area_rec($key) {
	global $connection;
	$query = "SELECT * FROM area WHERE area_id = {$key} ";	
	$query .= " LIMIT 1  ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}

function get_area_count($status) {
	global $connection;
	$query = "SELECT count(area_id) as record_count "; 
	$query .= " FROM area ";
	$query .= " WHERE area_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	
function get_area_list($npage, $no_of_list, $status) {
	global $connection;
	$query = "SELECT * FROM area ";
	$query .= " WHERE area_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY country_id ASC, name ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}


/* <<<<<<<<<<<<<<<<< country                >>>>>>>>>>>>>>>>> */	
function get_country_rec($key) {
	global $connection;
	$query = "SELECT * FROM country WHERE country_id = {$key} ";	
	$query .= " LIMIT 1  ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}

function get_country_count($status) {
	global $connection;
	$query = "SELECT count(country_id) as record_count "; 
	$query .= " FROM country ";
	$query .= " WHERE country_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	
function get_country_list($npage, $no_of_list, $status) {
	global $connection;
	$query = "SELECT * FROM country ";
	$query .= " WHERE country_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY name ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}



/* <<<<<<<<<<<<<<<<< c_content               >>>>>>>>>>>>>>>>> */	
function get_content_rec($key) {
	global $connection;
	$query = "SELECT * FROM content WHERE key_id = {$key} ";	
	$query .= " LIMIT 1  ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}

/* <<<<<<<<<<<<<<<<< c_links                >>>>>>>>>>>>>>>>> */	
function get_c_links_rec($key) {
	global $connection;
	$query = "SELECT * FROM c_links WHERE key_id = {$key} ";	
	$query .= " LIMIT 1  ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}






/*\\\\\ academy  /////*/	
function get_aca_exam_count($status) {
	// status 99 for all
	// cat 999 for all
	//$today = date("Y-m-d");
	global $connection;
	$query = "SELECT count(exam_id) as record_count "; 
	$query .= " FROM aca_exam ";
	$query .= " WHERE exam_id > 0"; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_trial_subscribed($examid,$userx){
	global $connection;
	$query = "SELECT count(exam_id) as cnt "; 
	$query .= " FROM aca_user_exam ";
	$query .= " WHERE exam_id = '".$examid."' and user_id = '".$userx."'";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}

function get_aca_exam_list_admin($npage, $no_of_list, $status) {
	// get list
	global $connection;
	$query = "SELECT * FROM aca_exam ";
	$query .= " WHERE exam_id > 0"; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY exam_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_aca_exam_list($npage, $no_of_list, $status) {
	// get list
	global $connection;
	$query = "SELECT * FROM aca_exam ";
	$query .= " WHERE exam_id >= 1 and trial = 0"; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY exam_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_aca_exam_trial($npage, $no_of_list, $status, $userid) {
	// get list
	global $connection;
	$query = "SELECT * FROM aca_exam ";
	$query .= " WHERE exam_id > 0 and trial = 1"; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " and exam_id not in (select exam_id from aca_user_exam where user_id = {$userid})";
	$query .= " ORDER BY exam_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_aca_exam($status) {
	// get list
	global $connection;
	$query = "SELECT * FROM aca_exam ";
	$query .= " WHERE exam_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY exam_id DESC ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_aca_trial($status) {
	// get list
	global $connection;
	$query = "SELECT * FROM aca_exam ";
	$query .= " WHERE exam_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} and trial = '1'"; }
	$query .= " ORDER BY exam_id DESC ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_exam_count($userid){
	global $connection;
	$query = "select count(a.exam_id) as cnt from aca_user_exam a inner join aca_exam b on b.exam_id = a.exam_id where  a.user_id = {$userid}";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_trial_count($userid){
	global $connection;
	$query = "select count(a.exam_id) as cnt from aca_user_exam a inner join aca_exam b on b.exam_id = a.exam_id where b.trial = '1' and a.user_id = {$userid}";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_aca_exam_ecount($status) {
	// get list
	global $connection;
	$query = "SELECT e.exam_id, e.code, e.description, count(q.question_id) as ecount FROM aca_exam e, aca_question q ";
	$query .= " WHERE e.exam_id = q.exam_id "; 
	if ($status <> 99) { $query .= " AND e.status = {$status} "; }
	$query .= " GROUP BY e.exam_id, e.code, e.description ";
	$query .= " ORDER BY e.exam_id DESC ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}


function get_aca_exam_rec($key) {
	// get list
	global $connection;
	$query = "SELECT * FROM aca_exam WHERE exam_id = {$key} ";
	$query .= " LIMIT 1  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}


// user_exam
function get_aca_user_exam_count($status, $cat, $expiry_date) {
	// status 99 for all
	// cat 0 for all
	//$today = date("Y-m-d");
	global $connection;
	$query = "SELECT count(user_exam_id) as record_count "; 
	$query .= " FROM aca_user_exam ";
	$query .= " WHERE user_exam_id > 0"; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	if ($cat <> 0) { $query .= " AND user_id = {$cat} "; }
	if ($expiry_date <> 0) { $query .= " AND expiry_date >= {$expiry_date} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_user_exam_rec($key) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_user_exam ";
	$query .= " WHERE user_exam_id = {$key} ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_user_exam_list($npage, $no_of_list, $status, $cat, $expiry_date) {
	// get list
	global $connection;
	$query = "SELECT * FROM aca_user_exam ";
	$query .= " WHERE exam_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = '".$status."' "; }
	if ($cat <> 0) { $query .= " AND user_id = '".$cat."' "; }
	if ($expiry_date <> 0) { $query .= " AND expiry_date >= '".$expiry_date."' "; }
	$query .= " ORDER BY exam_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_aca_user_exam_list_all($npage, $no_of_list, $status, $cat, $expiry_date) {
    // get list
    global $connection;

    $query = " SELECT exam_id, code FROM v_user_exam ";
    $query .= " WHERE expiry_date >= '".$expiry_date."' ";
    if ($status <> 99) { $query .= " AND status = '".$status."' "; }
    if ($cat <> 0) { $query .= " AND user_id = '".$cat."' "; }
    $query .= " and trial = '0' ";
    $query .= " GROUP BY exam_id, code ";
    $query .= " ORDER BY code ASC ";
    $query .= " LIMIT {$npage}, {$no_of_list}  ";
    $list = mysqli_query($connection, $query);
    confirm_query($list);
    return $list;
}

function get_aca_user_trial_list_all($npage, $no_of_list, $status, $cat, $expiry_date) {
    // get list
    global $connection;

    $query = " SELECT exam_id, code FROM v_user_exam ";
    $query .= " WHERE expiry_date >= '".$expiry_date."' ";
    if ($status <> 99) { $query .= " AND status = '".$status."' "; }
    if ($cat <> 0) { $query .= " AND user_id = '".$cat."' "; }
    $query .= " and trial = '1' ";
    $query .= " GROUP BY exam_id, code ";
    $query .= " ORDER BY code ASC ";
    $query .= " LIMIT {$npage}, {$no_of_list}  ";
    $list = mysqli_query($connection, $query);
    confirm_query($list);
    return $list;
}


function get_aca_user_exam_count_available($status, $cat) {
	// status 99 for all
	// cat 0 for all
	$today = date("Y-m-d");
	// what if he subscribes again
	global $connection;
	$query = "SELECT count(a.exam_id) as record_count "; 
	$query .= " FROM aca_exam a";
	$query .= " WHERE a.trial = 0 and a.exam_id NOT IN (select b.exam_id from aca_user_exam b where b.user_id = {$cat} and b.expiry_date >= '{$today}' ) ";
	//	if ($cat <> 0) { $query .= " AND b.user_id = {$cat} "; }
	//	if ($status <> 99) { $query .= " AND a.status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_user_exam_list_available($npage, $no_of_list, $status, $cat, $expiry_date) {
	// get list
	$today = date("Y-m-d");
	global $connection;
	$query = "SELECT *,DATE_FORMAT(update_date,'%Y-%m-%d') as 'update_date_custom' FROM aca_exam a WHERE a.trial = 0 and a.exam_id NOT IN (select exam_id from aca_user_exam where user_id = '".$cat."' and expiry_date >= '".$today."')";
	if ($status <> 99) { $query .= " AND a.status = {$status} "; }
	//if ($cat <> 0) { $query .= " AND a.user_id = {$cat} "; }
	//if ($expiry_date <> 0) { $query .= " AND expiry_date >= {$expiry_date} "; }
	$query .= " ORDER BY a.exam_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}


// class_exam
function get_aca_class_exam_list_available($npage, $no_of_list, $status, $cat7, $expiry_date) {
	// get list
	$today = date("Y-m-d");
	global $connection;
	$query = "SELECT * FROM aca_exam a ";
	$query .= " WHERE a.exam_id NOT IN (select exam_id from aca_class_exam where class_id = {$cat7} and expiry_date >= '{$today}' )"; 
	if ($status <> 99) { $query .= " AND a.status = {$status} "; }
	//if ($cat <> 0) { $query .= " AND a.user_id = {$cat} "; }
	//if ($expiry_date <> 0) { $query .= " AND expiry_date >= {$expiry_date} "; }
	$query .= " ORDER BY a.exam_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}


function get_aca_class_exam_list($npage, $no_of_list, $status, $cat7, $expiry_date) {
	// get list
	global $connection;
	$query = "SELECT * FROM aca_class_exam ";
	$query .= " WHERE class_exam_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	if ($cat7 <> 0) { $query .= " AND class_id = {$cat7} "; }
	if ($expiry_date <> 0) { $query .= " AND expiry_date >= {$expiry_date} "; }
	$query .= " ORDER BY exam_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}


function get_aca_class_exam_count($status, $cat7, $expiry_date) {
	// status 99 for all
	// cat 0 for all
	//$today = date("Y-m-d");
	global $connection;
	$query = "SELECT count(class_exam_id) as record_count "; 
	$query .= " FROM aca_class_exam ";
	$query .= " WHERE class_exam_id > 0"; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	if ($cat7 <> 0) { $query .= " AND class_id = {$cat7} "; }
	if ($expiry_date <> 0) { $query .= " AND expiry_date >= {$expiry_date} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_class_exam_rec($key) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_class_exam ";
	$query .= " WHERE class_exam_id = {$key} ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	



function get_aca_class_exam_list_instructor($status, $user_id, $expiry_date) {
	// get list
	global $connection;
	$query = "SELECT distinct(e.code) as exam, ce.exam_id, i.instructor_id  "; // , ce.exam_id , i.instructor_id, c.class_id
	$query .= " FROM aca_instructor i, aca_class_exam ce, aca_class c, aca_exam e ";
	$query .= " WHERE i.user_id = {$user_id} and  i.instructor_id = c.instructor_id and ce.class_id = c.class_id and ce.exam_id = e.exam_id "; 
	if ($status <> 99) { $query .= " AND ce.status = {$status} "; }
	if ($instructor_id <> 0) { $query .= " AND c.class_id = {$cat7} "; }
	if ($expiry_date <> 0) { $query .= " AND ce.expiry_date >= {$expiry_date} "; }
	$query .= " GROUP BY 1 ";
	$query .= " ORDER BY e.code ASC ";
	//$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}



function get_aca_class_exam_user($status, $cat, $exam_id, $expiry_date) { // $status, $cat, $exam_id, $expiry_date
	// get the class_id of a user, exam combination
	global $connection;
	$query = " SELECT distinct(ce.class_id) FROM aca_user_class uc, aca_class_exam ce ";
	$query .= " WHERE uc.class_id = ce.class_id  "; 
	if ($exam_id <> 0) { $query .= " AND ce.exam_id = {$exam_id} "; }
	if ($cat <> 0) { $query .= " AND uc.user_id = {$cat} "; }
	if ($status <> 99) { $query .= " AND ce.status = {$status} "; }
	if ($status <> 99) { $query .= " AND uc.status = {$status} "; }
	if ($expiry_date <> 0) { $query .= " AND ce.expiry_date >= '{$expiry_date}' "; }
	if ($expiry_date <> 0) { $query .= " AND uc.expiry_date >= '{$expiry_date}' "; }
	$query .= " LIMIT 1  ";
	
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}



/*
SELECT ce.exam_id, e.code FROM aca_instructor i, aca_class c, aca_class_exam ce, aca_exam e WHERE i.user_id = 51 and i.instructor_id = c.instructor_id and ce.class_id = c.class_id and ce.exam_id = e.exam_id
*/
function get_aca_quiz_last_rec($key, $exam_id) {
	global $connection;
	$query = "SELECT max(quiz_id) as quiz_id ";
	$query .= " FROM aca_quiz ";
	$query .= " WHERE creator_id = {$key} and exam_id = {$exam_id} ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	


function get_aca_quiz_last_list($user, $status, $recs) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_quiz ";
	$query .= " WHERE creator_id = {$user} ";
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY quiz_id DESC ";
	if ($recs <> 0) { $query .= " LIMIT {$recs} "; }
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	


function get_aca_quiz_score($key, $status) {
	// status 99 for all
	global $connection;
	$query = "SELECT sum(score) as score, count(quiz_option_id) as rec_count "; 
	$query .= " FROM aca_quiz_option ";
	$query .= " WHERE quiz_option_id > 0"; 
	if ($key <> 0) { $query .= " AND quiz_id = {$key} "; }
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_quiz_option_score($key, $status) {
	// status 99 for all
	global $connection;
	$query = "SELECT sum(score) as score,  ";
	$query .= " sum(answ_a) as count_a,  ";
	$query .= " sum(answ_b) as count_b,  ";
	$query .= " sum(answ_c) as count_c,  ";
	$query .= " sum(answ_d) as count_d,  ";
	$query .= " sum(answ_e) as count_e,  ";
	$query .= " sum(answ_f) as count_f  "; 
	$query .= " FROM aca_quiz_option ";
	$query .= " WHERE quiz_option_id > 0"; 
	if ($key <> 0) { $query .= " AND quiz_id = {$key} "; }
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	


// question
function get_aca_question_count($status, $exam_id, $cat6) {
	// status 99 for all
	// cat 999 for all
	//$today = date("Y-m-d");
	global $connection;
	$query = "SELECT count(question_id) as record_count "; 
	$query .= " FROM aca_question ";
	$query .= " WHERE question_id > 0 "; 
	if ($exam_id <> 0) { $query .= " AND exam_id = {$exam_id} "; }
	if ($cat6 <> 0) { $query .= " AND tool_id = {$cat6} "; }	
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_question_list($npage, $no_of_list, $status, $exam_id, $cat6) {
	// get list
	global $connection;
	$query = "SELECT * FROM aca_question ";
	$query .= " WHERE question_id > 0 "; 
	if ($exam_id <> 0) { $query .= " AND exam_id = '{$exam_id}' "; }
	if ($cat6 <> 0) { $query .= " AND tool_id = '{$cat6}' "; }	
	if ($status <> 99) { $query .= " AND status = '{$status}' "; }
	$query .= " ORDER BY question_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_aca_question($status) {
	// get list
	global $connection;
	$query = "SELECT * FROM aca_question ";
	$query .= " WHERE question_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY exam_id DESC ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_aca_question_rec($key, $cat6) {
    // get list
    global $connection;
    $query = "SELECT * FROM aca_question WHERE question_id = '".$key."' ";
    if ($cat6 <> 0) { $query .= " AND tool_id = '".$cat6."' "; }
    $query .= " LIMIT 1  ";
    $list = mysqli_query($connection, $query);
    //confirm_query($list);
    return $list;
}

function get_aca_question_rec3($key, $cat6, $exam_id) {
    // get list
    global $connection;
    $query = "SELECT * FROM aca_question WHERE question_no = '".$key."' ";
if ($exam_id <> 0) { $query .= " AND exam_id = '".$exam_id."' "; }
    if(is_array($cat6)){
        $filterExisting = implode($cat6,"','");
        $query .= " AND tool_id in ('".$filterExisting."') ";
    } else {
        if ($cat6 <> 0) {
            $query .= " AND tool_id = '" . $cat6 . "' ";
        }
    }

    if ($exam_id <> 0) { $query .= " AND exam_id = '".$exam_id."' "; }
    $query .= " LIMIT 1  ";
    $list = mysqli_query($connection, $query);
    //confirm_query($list);
    return $list;
}

function get_aca_question_rec_all($key, $cat6)
{
    // get list
    global $connection;

    $query = "SELECT * FROM aca_question WHERE question_no = '" . $key . "' ";
	
if ($cat6 <> 0) { $query .= " AND exam_id = '".$cat6."' "; }
	
    $query .= " LIMIT 1  ";
    $list = mysqli_query($connection, $query);
    // confirm_query($list);
    return $list;
}

function get_aca_question_rec2($key, $cat6,$exam_id)
{
    // get list
    global $connection;

    $query = "SELECT * FROM aca_question WHERE question_no = '" . $key . "' ";

    $query .= " AND exam_id = '".$exam_id."' ";

    $query .= " LIMIT 1  ";
    $list = mysqli_query($connection, $query);
    // confirm_query($list);
    return $list;
}


function get_aca_question_tool($key, $q, $cat6) {
    // get list
    global $connection;
    $query = "SELECT * FROM aca_question WHERE exam_id = '".$key."'";
    if ($q <> 0) { $query .= " AND question_id = '".$q."' "; }
    if ($cat6 <> 0) { $query .= " AND tool_id = '".$cat6."' "; }
    $query .= " LIMIT 1";
    $list = mysqli_query($connection, $query);
    confirm_query($list);
    return $list;
}

function get_aca_question_tool2($key, $q, $t01, $t02, $t03, $t04, $t05, $t06, $t07, $t08, $t09, $t10, $t11, $t12, $t13, $t14, $t15, $t16, $t17, $t18, $t19, $t20) {
    // get list
    $t = 1;
    global $connection;

    $query = "SELECT * FROM aca_question WHERE exam_id = '".$key."'";
    //if ($q <> 0) { $query .= " AND question_id = '".$q."' "; }
    if ($t01 <> 0) {
        if ($t <> 0) { $query .= " and ( "; }
        if ($t01 <> 0) { $query .= " tool_id = '" . $t01 . "' "; }
        if ($t02 <> 0) { $query .= "or tool_id = '" . $t02 . "' ";}
        if ($t03 <> 0) { $query .= "or tool_id = '" . $t03 . "' ";}
        if ($t04 <> 0) { $query .= "or tool_id = '" . $t04 . "' ";}
        if ($t05 <> 0) { $query .= "or tool_id = '" . $t05 . "' ";}
        if ($t06 <> 0) { $query .= "or tool_id = '" . $t06 . "' ";}
        if ($t07 <> 0) { $query .= "or tool_id = '" . $t07 . "' ";}
        if ($t08 <> 0) { $query .= "or tool_id = '" . $t08 . "' ";}
        if ($t09 <> 0) { $query .= "or tool_id = '" . $t09 . "' ";}
        if ($t10 <> 0) { $query .= "or tool_id = '" . $t10 . "' ";}
        if ($t11 <> 0) { $query .= "or tool_id = '" . $t11 . "' ";}
        if ($t12 <> 0) { $query .= "or tool_id = '" . $t12 . "' ";}
        if ($t13 <> 0) { $query .= "or tool_id = '" . $t13 . "' ";}
        if ($t14 <> 0) { $query .= "or tool_id = '" . $t14 . "' ";}
        if ($t15 <> 0) { $query .= "or tool_id = '" . $t15 . "' ";}
        if ($t16 <> 0) { $query .= "or tool_id = '" . $t16 . "' ";}
        if ($t17 <> 0) { $query .= "or tool_id = '" . $t17 . "' ";}
        if ($t18 <> 0) { $query .= "or tool_id = '" . $t18 . "' ";}
        if ($t19 <> 0) { $query .= "or tool_id = '" . $t19 . "' ";}
        if ($t20 <> 0) { $query .= "or tool_id = '" . $t20 . "' ";}
        if ($t <> 0) { $query .= " ) "; }
    } else {
        //$query .= " and tool_id > '0' ";
    }
    $query .= " LIMIT 1";
    $list = mysqli_query($connection, $query);
    confirm_query($list);

    error_log( $query );
    return $list;
}

function get_aca_random_question_id($key, $cat6) {
    // get list
    global $connection;
    $query = "SELECT question_id FROM aca_question WHERE exam_id = '".$key."'";
    if ($cat6 <> 0) { $query .= " AND tool_id = '".$cat6."' "; }
    $query .= "order by rand() LIMIT 1";
    $list = mysqli_query($connection, $query);
    confirm_query($list);
    return $list;
}

function get_aca_random_question_id2($key, $cat6) {
    // get list
    global $connection;

    $query = "SELECT question_id FROM aca_question WHERE exam_id = '".$key."'";

        $filterExisting = implode($cat6,"','");
        $query .= " AND tool_id in ('".$filterExisting."') ";

    $query .= " order by rand() limit 1";
    $list = mysqli_query($connection, $query);
    confirm_query($list);
    ini_set("log_errors", 1);
    ini_set("error_log", "C:/Websites/Beyond-Training/php-error.log");

    error_log( $query );
    return $list;
}

function get_aca_option_duplicate($exam_id, $version, $date, $cat3, $q) {
	// get list
	global $connection;
	$query = "SELECT count(*) qexist FROM aca_option WHERE exam_id = '".$exam_id."' ";
	if ($cat3 <> 0) { $query .= " AND creator_id = '".$cat3."' "; }
	if ($q <> 0) { $query .= " AND question_id = '".$q."' "; }
	if ($version <> 0) { $query .= " AND userthis = '".$version."' "; }
	if ($date <> 0) { $query .= " AND create_date = '".$date."' "; }

	$query .= " LIMIT 1  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_aca_option_results($cat, $cat6, $cat2, $cat3, $cat4) {
	// get summary
	global $connection;
	$query = "SELECT o.username, o.creator_id, o.exam_id, o.userthis, o.create_date, count(o.question_id) as questions, sum(o.score) as score ";	
	$query .= " FROM aca_option o, aca_exam e, aca_tool t, aca_question q  ";	
	$query .= " WHERE t.tool_id = q.tool_id and (o.question_id = q.question_no and o.exam_id = q.exam_id) ";	
	if ($cat  <> 0) { $query .= " AND o.exam_id = '".$cat."' "; }
	if ($cat6 <> 0) { $query .= " AND t.tool_id = '".$cat6."' "; }
	if ($cat2 <> 0) { $query .= " AND o.userthis = '".$cat2."' "; }
	if ($cat3 <> 0) { $query .= " AND o.creator_id = '".$cat3."' "; }
	if ($cat4 <> 0) { $query .= " AND o.create_date = '".$cat4."' "; }
	$query .= " GROUP BY o.username, o.creator_id, o.exam_id, o.userthis, o.create_date ";
	$query .= " ORDER BY o.create_date DESC, o.exam_id ASC, o.username ASC, o.userthis ASC ";	
	//$query .= " LIMIT 50  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}



function get_aca_option_results_instructor($cat, $cat6, $cat2, $cat3, $cat4, $cat5, $cat7, $npage, $no_of_list) {
	// get summary
	global $connection;
	$query = "SELECT o.username, u.name, o.creator_id, o.exam_id, o.userthis, o.create_date, count(o.question_id) as questions, sum(o.score) as score ";	
	$query .= " FROM aca_option o, aca_exam e, aca_tool t, aca_question q, aca_user u, aca_user_class uc, aca_class c ";	
	$query .= " WHERE t.tool_id = q.tool_id and (o.question_id = q.question_no and o.exam_id = q.exam_id) and u.user_id = o.creator_id and e.exam_id = o.exam_id and u.user_id = uc.user_id and uc.class_id = c.class_id ";
	if ($cat  <> 0) { $query .= " AND o.exam_id = {$cat} "; }
	if ($cat6 <> 0) { $query .= " AND t.tool_id = {$cat6} "; }
	if ($cat2 <> 0 || $cat2 <> "0") { $query .= " AND trim(o.userthis) like '{$cat2}%' "; }
	if ($cat3 <> 0) { $query .= " AND o.creator_id = {$cat3} "; }
	if ($cat4 <> 0) { $query .= " AND o.create_date = '{$cat4}' "; }
	if ($cat5 <> 0) { $query .= " AND c.instructor_id = '{$cat5}' "; }
	if ($cat7 <> 0) { $query .= " AND uc.class_id = '{$cat7}' "; }
	$query .= " GROUP BY o.username, u.name, o.creator_id, o.exam_id, o.userthis, o.create_date ";
	$query .= " ORDER BY o.create_date DESC, o.exam_id ASC, o.username ASC, o.userthis ASC ";	
	$query .= " LIMIT {$npage}, {$no_of_list} ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_aca_option_results_instructor_count($cat, $cat6, $cat2, $cat3, $cat4, $cat5, $cat7) {
	// get summary
	global $connection;
	$query = "SELECT count(b.userthis) as record_count ";	
	$query .= " FROM ";	
		$query .= "( SELECT o.username, u.name, o.creator_id, o.exam_id, o.userthis, o.create_date, count(o.question_id) as questions, sum(o.score) as score ";	
		$query .= " FROM aca_option o, aca_exam e, aca_tool t, aca_question q, aca_user u, aca_user_class uc, aca_class c ";	
		$query .= " WHERE t.tool_id = q.tool_id and (o.question_id = q.question_no and o.exam_id = q.exam_id) and u.user_id = o.creator_id and e.exam_id = o.exam_id and u.user_id = uc.user_id and uc.class_id = c.class_id ";
		if ($cat  <> 0) { $query .= " AND o.exam_id = {$cat} "; }
		if ($cat6 <> 0) { $query .= " AND t.tool_id = {$cat6} "; }
		if ($cat2 <> 0 || $cat2 <> "0") { $query .= " AND trim(o.userthis) like '{$cat2}%' "; }
		if ($cat3 <> 0) { $query .= " AND o.creator_id = {$cat3} "; }
		if ($cat4 <> 0) { $query .= " AND o.create_date = '{$cat4}' "; }
		if ($cat5 <> 0) { $query .= " AND c.instructor_id = '{$cat5}' "; }
	if ($cat7 <> 0) { $query .= " AND uc.class_id = '{$cat7}' "; }
		$query .= " GROUP BY o.username, u.name, o.creator_id, o.exam_id, o.userthis, o.create_date ";
		$query .= " ORDER BY o.create_date DESC, o.exam_id ASC, o.username ASC, o.userthis ASC ) b ";	
	$query .= " LIMIT 1 ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_aca_option_results_instructor_max($cat, $cat6, $cat2, $cat3, $cat4, $cat5, $cat7) {
	// get summary
	global $connection;
	$query = "SELECT o.username, u.name, o.creator_id, o.exam_id, o.userthis, o.create_date, count(o.question_id) as questions, sum(o.score) as score ";	
	$query .= " FROM aca_option o, aca_exam e, aca_tool t, aca_question q, aca_user u, aca_user_class uc, aca_class c ";		
	$query .= " WHERE t.tool_id = q.tool_id and (o.question_id = q.question_no and o.exam_id = q.exam_id) and u.user_id = o.creator_id and u.user_id = uc.user_id and uc.class_id = c.class_id ";
	if ($cat  <> 0) { $query .= " AND o.exam_id = {$cat} "; }
	if ($cat6 <> 0) { $query .= " AND t.tool_id = {$cat6} "; }
//	if ($cat2 <> 0) { $query .= " AND o.userthis = '{$cat2}' "; }
	if ($cat2 <> 0) { $query .= " AND o.userthis like '%{$cat2}%' "; }
	if ($cat3 <> 0) { $query .= " AND o.creator_id = {$cat3} "; }
	if ($cat4 <> 0) { $query .= " AND o.create_date = '{$cat4}' "; }
	if ($cat5 <> 0) { $query .= " AND c.instructor_id = '{$cat5}' "; }
	if ($cat7 <> 0) { $query .= " AND uc.class_id = '{$cat7}' "; }
	$query .= " GROUP BY o.username, u.name, o.creator_id, o.exam_id, o.userthis, o.create_date ";
	$query .= " ORDER BY o.create_date DESC, o.exam_id ASC, o.username ASC, o.userthis ASC ";	
	//$query .= " LIMIT 50  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}



function get_aca_option_results_tool_instructor($cat, $cat6, $cat2, $cat3, $cat4, $cat5, $cat7) {
	// get summary
	global $connection;
	$query = "SELECT e.exam_id, e.code, t.description as tool, t.tool_id, count(o.question_id) as questions, sum(o.score) as score ";	
	$query .= " FROM aca_option o, aca_exam e, aca_tool t, aca_question q, aca_user u, aca_user_class uc, aca_class c ";		
	$query .= " WHERE e.exam_id = o.exam_id and (o.question_id = q.question_no and o.exam_id = q.exam_id) and t.tool_id = q.tool_id and u.user_id = o.creator_id and u.user_id = uc.user_id and uc.class_id = c.class_id ";	
	if ($cat  <> 0) { $query .= " AND o.exam_id = {$cat} "; }
	if ($cat6 <> 0) { $query .= " AND t.tool_id = {$cat6} "; }
	//if ($cat2 <> 0) { $query .= " AND o.userthis = '{$cat2}' "; }
	if ($cat2 <> 0 || $cat2 <> "0") { $query .= " AND trim(o.userthis) like '{$cat2}%' "; }
	if ($cat3 <> 0) { $query .= " AND o.creator_id = {$cat3} "; }
	if ($cat4 <> 0) { $query .= " AND o.create_date = '{$cat4}' "; }
	if ($cat5 <> 0) { $query .= " AND c.instructor_id = '{$cat5}' "; }
	if ($cat7 <> 0) { $query .= " AND uc.class_id = '{$cat7}' "; }
	$query .= " GROUP BY e.exam_id, e.code, t.description, t.tool_id ";	
	//$query .= " LIMIT 50  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_aca_option_results_tool_user($cat3) {
	// get summary
	global $connection;
	$query = "SELECT e.exam_id, e.code, t.description as tool, t.tool_id, count(o.question_id) as questions, sum(o.score) as score ";	
	$query .= " FROM aca_option o, aca_exam e, aca_tool t, aca_question q, aca_user u ";		
	$query .= " WHERE e.exam_id = o.exam_id and (o.question_id = q.question_no and o.exam_id = q.exam_id) and t.tool_id = q.tool_id and u.user_id = o.creator_id  ";	
	if ($cat3 <> 0) { $query .= " AND o.user_id = {$cat3} "; }
	$query .= " GROUP BY e.exam_id, e.code, t.description, t.tool_id ";	
	//$query .= " LIMIT 50  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}



function get_aca_option_results_tool_instructor_top10_incorrect($cat, $cat6, $cat2, $cat3, $cat4, $cat5) {
	// get summary
	global $connection;
	$query = "SELECT e.exam_id, e.code, t.description as tool, t.tool_id, q.question_text, q.question_id, count(o.question_id) as questions, sum(o.score) as score, u.instructor_id as instructor ";
	$query .= " FROM aca_option o, aca_exam e, aca_tool t, aca_question q, aca_user u  ";	
	$query .= " WHERE e.exam_id = o.exam_id and (o.question_id = q.question_no and o.exam_id = q.exam_id) and t.tool_id = q.tool_id and u.user_id = o.creator_id  ";	
	if ($cat  <> 0) { $query .= " AND e.exam_id = {$cat} "; }
	if ($cat6 <> 0) { $query .= " AND t.tool_id = {$cat6} "; }
	if ($cat2 <> 0) { $query .= " AND o.userthis = '{$cat2}' "; }
	if ($cat3 <> 0) { $query .= " AND o.creator_id = {$cat3} "; }
	if ($cat4 <> 0) { $query .= " AND o.create_date = '{$cat4}' "; }
	if ($cat5 <> 0) { $query .= " AND u.instructor_id = '{$cat5}' "; }
	$query .= " GROUP BY e.exam_id, e.code, t.description, t.tool_id, q.question_text, q.question_id ";	
	$query .= " ORDER BY 8 desc ";	
	$query .= " LIMIT 10  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}


function get_aca_option_results_trend($cat, $cat6, $cat2, $cat3, $cat4, $cat5, $cat7) {
	// get summary
	global $connection;
	$query = "SELECT  ";	
	$query .= " o.create_date as date,  ";	
	$query .= " count(o.question_id) as questions, sum(o.score) as score ";	
	$query .= " FROM aca_option o, aca_exam e, aca_tool t, aca_question q, aca_user u, aca_user_class uc, aca_class c ";	
	$query .= " WHERE e.exam_id = o.exam_id and (o.question_id = q.question_no and o.exam_id = q.exam_id) and t.tool_id = q.tool_id and u.user_id = o.user_id and u.user_id = uc.user_id and uc.class_id = c.class_id ";	
	if ($cat  <> 0) { $query .= " AND e.exam_id = {$cat} "; }
	if ($cat6 <> 0) { $query .= " AND t.tool_id = {$cat6} "; }
	if ($cat2 <> 0 || $cat2 <> "0") { $query .= " AND trim(o.userthis) like '{$cat2}%' "; }
	if ($cat3 <> 0) { $query .= " AND o.creator_id = {$cat3} "; }
	if ($cat4 <> 0) { $query .= " AND o.create_date = '{$cat4}' "; }
	if ($cat5 <> 0) { $query .= " AND c.instructor_id = '{$cat5}' "; }
	if ($cat7 <> 0) { $query .= " AND uc.class_id = '{$cat7}' "; }
	$query .= " AND e.trial != '1' ";		
	$query .= " GROUP BY o.create_date ";		
	$query .= " ORDER BY o.create_date desc ";	
	$query .= " LIMIT 30  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}
function get_aca_option_results_trend_user($cat3) {
	// get summary
	global $connection;
	$query = "SELECT  ";	
	$query .= " o.create_date as date,  ";	
	$query .= " count(o.question_id) as questions, sum(o.score) as score ";	
	$query .= " FROM aca_option o  ";	
	$query .= " WHERE o.user_id = {$cat3} ";
	$query .= " GROUP BY o.create_date ";		
	$query .= " ORDER BY o.create_date desc ";	
	$query .= " LIMIT 30  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_aca_v_results($cat, $cat6, $cat2, $cat3, $cat4, $cat5, $cat7) {
	// get summary
	global $connection;
	$query = "SELECT cdate, sum(questions) as questions, sum(score) as score ";	
	//$query = "SELECT cdate, questions, score ";	
	$query .= " FROM v_results ";	
	//$query .= " WHERE questions > 0 and questions not null ";	
	$query .= " WHERE questions > 0 "; // changed by SW on 2017/01/03
	if ($cat  <> 0) { $query .= " AND exam_id = {$cat} "; }
	if ($cat6 <> 0) { $query .= " AND tool_id = {$cat6} "; }
	if ($cat2 <> 0 || $cat2 <> "0") { $query .= " AND trim(userthis) like '{$cat2}%' "; }
	if ($cat3 <> 0) { $query .= " AND user_id = {$cat3} "; }
	if ($cat4 <> 0) { $query .= " AND cdate = '{$cat4}' "; }
	if ($cat5 <> 0) { $query .= " AND instructor_id = '{$cat5}' "; }
	if ($cat7 <> 0) { $query .= " AND class_id = {$cat7} "; }
	$query .= " GROUP BY cdate ";		
	$query .= " ORDER BY cdate desc ";			
	//$query .= " HAVING sum(questions) > 0 ";	
	$query .= " LIMIT 30  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}


function get_aca_results_eval($cat, $cat3, $cat7) {
	// get summary
	global $connection;
	$query = "SELECT o.class_id, o.user_id, o.exam_id, count(distinct o.userthis) as testcnt, count(o.option_id) as qcnt, sum(o.score) as score ";	
	//$query = "SELECT o.class_id, o.user_id, o.tool_id, count(o.option_id) as qcnt, sum(o.score) as score ";	
	$query .= " FROM aca_option o ";	
	$query .= " WHERE o.user_id >= 1 ";	
	if ($cat  <> 0) { $query .= " AND o.exam_id = {$cat} "; }
	if ($cat3 <> 0) { $query .= " AND o.user_id = {$cat3} "; }
	if ($cat7 <> 0) { $query .= " AND o.class_id = '{$cat7}' "; }
	$query .= " GROUP BY o.class_id, o.user_id, o.exam_id ";		
	$query .= " ORDER BY o.user_id asc ";	
	$query .= " LIMIT 100  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}

function get_aca_results_eval_tool($cat, $cat6, $cat3, $cat7) {
	// get summary
	global $connection;
	$query = "SELECT o.class_id, o.user_id, o.exam_id, q.tool_id, count(o.option_id) as qcnt, sum(o.score) as score ";	
	$query .= " FROM aca_option o, aca_question q ";	
	$query .= " WHERE o.question_id = q.question_id ";	
	if ($cat  <> 0) { $query .= " AND o.exam_id = {$cat} "; }
	if ($cat6 <> 0) { $query .= " AND o.tool_id = {$cat6} "; }
	if ($cat3 <> 0) { $query .= " AND o.user_id = {$cat3} "; }
	if ($cat7 <> 0) { $query .= " AND o.class_id = '{$cat7}' "; }
	$query .= " GROUP BY o.class_id, o.user_id, o.exam_id, q.tool_id ";		
	$query .= " ORDER BY o.user_id asc, o.exam_id asc ";	
	$query .= " LIMIT 100  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}


/*
select o.class_id, o.user_id, count(distinct o.userthis) as testcnt, count(o.option_id) as qcnt, sum(o.score) as score
from aca_option o
where
o.test_type = 1
group by 
o.class_id, o.user_id
order by 1, 2
*/


/*
function get_aca_question_errors($exam_id) {
	// get summary
	global $connection;
	$query = "SELECT l.exam_id, t.description as tool, q.tool_id, 1.question_id, sum(l.qerr) as errors ";	
	$query .= " FROM aca_log l, aca_tool t, aca_question q  ";	
	$query .= " WHERE  q.question_id = l.question_id and t.tool_id = q.tool_id and l.qerr = 1 ";	
	if ($exam_id <> 0) { $query .= " AND l.exam_id = {$exam_id} "; }
	$query .= " GROUP BY l.exam_id, t.description, t.tool_id, l.question_id ";
	$query .= " ORDER BY 4 ";	
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}
*/

function get_aca_question_errors($exam_id) {
	// get summary
	global $connection;
	$query = "SELECT question_id, count(log_id) as errors FROM `aca_log`   ";	
	$query .= " WHERE log_id > 0 ";	
	if ($exam_id <> 0) { $query .= " AND exam_id = {$exam_id} "; }
	$query .= " GROUP BY question_id";	
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}


function get_aca_option_results_tool($cat, $cat6, $cat2, $cat3, $cat4) {
	// get summary
	global $connection;
	$query = "SELECT e.exam_id, e.code, t.description as tool, t.tool_id, count(o.question_id) as questions, sum(o.score) as score ";	
	$query .= " FROM aca_option o, aca_exam e, aca_tool t, aca_question q  ";	
	$query .= " WHERE e.exam_id = o.exam_id and (q.question_no = o.question_id and q.exam_id = o.exam_id) and t.tool_id = q.tool_id ";	
	if ($cat  <> 0) { $query .= " AND e.exam_id = '".$cat."' "; }
	if ($cat6 <> 0) { $query .= " AND t.tool_id = '".$cat6."' "; }
	if ($cat2 <> 0) { $query .= " AND o.userthis = '".$cat2."' "; }
	if ($cat3 <> 0) { $query .= " AND o.creator_id = '".$cat3."' "; }
	if ($cat4 <> 0) { $query .= " AND o.create_date = '".$cat4."' "; }
	$query .= " GROUP BY e.exam_id, e.code, t.description, t.tool_id ";	
	//$query .= " LIMIT 50  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}


function get_aca_exam_tools($status, $exam_id) {
    // get list
    global $connection;
    $query = "SELECT distinct(t.description) as tool, t.tool_id FROM aca_question q, aca_tool t ";
    $query .= " WHERE t.tool_id = q.tool_id  ";
    if ($exam_id <> 0) { $query .= " and q.exam_id = '{$exam_id}' "; }
    if ($status <> 99) { $query .= " AND q.status = {$status} "; }
    $query .= " ORDER BY t.description ASC ";
    $list = mysqli_query($connection, $query);
    confirm_query($list);
    return $list;
}

function get_aca_exam_tools_test11($status, $exam_id) {
    // get list
    global $connection;
    $query = "SELECT t.description as tool, t.tool_id FROM aca_tool t where t.tool_id in (select distinct(tool_id) from aca_question where exam_id = '".$exam_id."')";
    //$query .= " WHERE q.exam_id = '{$exam_id}' ";
    //if ($status <> 99) { $query .= " AND q.status = {$status} "; }
    $query .= " group by t.tool_id ORDER BY t.description ASC ";
    $list = mysqli_query($connection, $query);
    confirm_query($list);
    return $list;
}

function get_aca_exam_tools_qcount($status, $exam_id) {
	// get list
	global $connection;
	$query = "SELECT distinct(t.description) as tool, t.tool_id, count(q.question_id) as qcount FROM aca_question q, aca_tool t ";
	$query .= " WHERE t.tool_id = q.tool_id  ";
	if ($exam_id <> 0) { $query .= " and q.exam_id = '{$exam_id}' "; }
	if ($status <> 99) { $query .= " AND q.status = {$status} "; }
	$query .= " GROUP BY t.description, t.tool_id ";	
	$query .= " ORDER BY t.description ASC ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}


function get_aca_option_summary($key, $username, $userthis) {
	// get summary
	global $connection;
	$query = "SELECT username, creator_id, userthis, count(question_id) as questions, sum(score) as score FROM aca_option   ";	
	$query .= " WHERE exam_id = '".$key."' and username = '".$username."' and userthis = '".$userthis."' ";	
	$query .= " GROUP BY username, creator_id, userthis ";	
	$query .= " LIMIT 1  ";
	$list = mysqli_query($connection, $query);
	ini_set("log_errors", 1);
	ini_set("error_log", "C:/Websites/Beyond-Training/php-error.log");
	
	error_log( $query );
	confirm_query($list);
	return $list;
}


function get_aca_option_this($key, $username, $userthis) {
	// get summary
	global $connection;
	$query = "SELECT userthis FROM aca_option   ";	
	$query .= " WHERE exam_id = {$key} and username = '{$username}' and userthis = '{$userthis}'  ";	
	$query .= " LIMIT 1  ";
	$list = mysqli_query($connection, $query);
	confirm_query($list);
	return $list;
}


// search
function get_aca_question_search($npage, $no_of_list, $status, $srch) {
	//$srch = "%" . $xxx . "%";
	global $connection;
	//$query = "SELECT * FROM `aca_question` WHERE question_note like '%Choose%'  ";
	$query = "SELECT * ";
	$query .= " FROM aca_question ";
	$query .= " WHERE ";
	$query .= " question_text like '%{$srch}%' or ";
	$query .= " question_note like '%{$srch}%' or ";
	$query .= " option_a_text like '%{$srch}%' or ";
	$query .= " option_b_text like '%{$srch}%' or ";
	$query .= " option_c_text like '%{$srch}%' or ";
	$query .= " option_d_text like '%{$srch}%' or ";
	$query .= " option_e_text like '%{$srch}%' ";
	$query .= " LIMIT {$npage}, {$no_of_list} ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	


function get_aca_question_search_count($status, $srch) {
	//$srch = "%" . $xxx . "%";
	global $connection;
	//$query = "SELECT * FROM `aca_question` WHERE question_note like '%Choose%'  ";
	$query = "SELECT count(question_id) as record_count ";
	$query .= " FROM aca_question ";
	$query .= " WHERE ";
	$query .= " question_text like '%{$srch}%' or ";
	$query .= " question_note like '%{$srch}%' or ";
	$query .= " option_a_text like '%{$srch}%' or ";
	$query .= " option_b_text like '%{$srch}%' or ";
	$query .= " option_c_text like '%{$srch}%' or ";
	$query .= " option_d_text like '%{$srch}%' or ";
	$query .= " option_e_text like '%{$srch}%' ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	




// get the latest record
function get_rec_next_question($exam_id) {
	
	global $connection;
	$query = " SELECT max(question_id) as next_no FROM aca_question where exam_id = '{$exam_id}' "; 
	$query .= " LIMIT 1 ";
	$result_set = mysqli_query($connection, $query);
	confirm_query($result_set);
	return $result_set;
}

// tools
function get_aca_tool_count($status, $exam_id) {
	// status 99 for all
	global $connection;
	$query = "SELECT count(tool_id) as record_count ";
	$query .= " FROM aca_tool ";
	$query .= " WHERE tool_id > 0 ";
	if ($exam_id <> 0) { $query .= " AND exam_id = {$exam_id} "; }
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_tool_rec($key) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_tool ";
	$query .= " WHERE tool_id = '{$key}' ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_tool_list($npage, $no_of_list, $status) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_tool ";
	$query .= " WHERE tool_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY description ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	




// assessment_questions
function get_aca_assessment_questions_count($status) {
	// status 99 for all
	global $connection;
	$query = "SELECT count(assessment_questions_id) as record_count ";
	$query .= " FROM aca_assessment_questions ";
	$query .= " WHERE assessment_questions_id > 0 ";
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_assessment_questions_rec($key) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_assessment_questions ";
	$query .= " WHERE assessment_questions_id = '{$key}' ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_assessment_questions_list($npage, $no_of_list, $status) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_assessment_questions ";
	$query .= " WHERE assessment_questions_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY description ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	



function get_aca_assessment_question_valid_rec($valid, $status) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_assessment_questions ";
	$query .= " WHERE assessment_questions_id >= 0 ";
	if ($valid <> 0) { $query .= " AND effective_date <= '{$valid}' AND expiry_date >= '{$valid}' "; }
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY effective_date DESC ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	


// assessment
function get_aca_assessment_count($status, $cat7) {
	// status 99 for all
	global $connection;
	$query = "SELECT count(assessment_id) as record_count ";
	$query .= " FROM aca_assessment ";
	$query .= " WHERE assessment_id > 0 ";
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_assessment_rec($key) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_assessment ";
	$query .= " WHERE assessment_id = '{$key}' ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_assessment_list($npage, $no_of_list, $status, $cat7) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_assessment ";
	$query .= " WHERE assessment_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY class_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	


// evaluation
function get_aca_evaluation_count($status, $cat7) {
	// status 99 for all
	global $connection;
	$query = "SELECT count(e.eval_id) as record_count ";
	$query .= " FROM aca_evaluation e ";
	if ($cat7 <> 0) { $query .= " , aca_class c "; }
	$query .= " WHERE e.eval_id > 0 ";
	if ($cat7 <> 0) { $query .= " AND e.class_id = c.class_id AND c.instructor_id= {$cat7} "; }
	if ($status <> 99) { $query .= " AND e.status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	


function get_aca_evaluation_count_search($status, $cat7, $class_id, $user_id) {
	// status 99 for all
	global $connection;
	$query = "SELECT count(e.eval_id) as record_count ";
	$query .= " FROM aca_evaluation e ";
	if ($cat7 <> 0) { $query .= " , aca_class c "; }
	$query .= " WHERE e.eval_id > 0 ";
	if ($cat7 <> 0) { $query .= " AND e.class_id = c.class_id AND c.instructor_id= {$cat7} "; }
	if ($class_id <> 0) { $query .= " AND e.class_id = {$class_id} "; }
	if ($user_id <> 0) { $query .= " AND e.user_id = {$user_id} "; }
	if ($status <> 99) { $query .= " AND e.status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_evaluation_rec($user) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_evaluation ";
	$query .= " WHERE user_id = '{$user}' ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_evaluation_list($npage, $no_of_list, $status, $cat7) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_evaluation e ";
	if ($cat7 <> 0) { $query .= " , aca_class c "; }
	$query .= " WHERE e.eval_id > 0 ";
	if ($cat7 <> 0) { $query .= " AND e.class_id = c.class_id AND c.instructor_id= {$cat7} "; }
	if ($status <> 99) { $query .= " AND e.status = {$status} "; }
	$query .= " ORDER BY e.class_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_evaluation_list_search($npage, $no_of_list, $status, $cat7, $class_id, $user_id) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_evaluation e ";
	if ($cat7 <> 0) { $query .= " , aca_class c "; }
	$query .= " WHERE e.eval_id > 0 ";
	if ($cat7 <> 0) { $query .= " AND e.class_id = c.class_id AND c.instructor_id= {$cat7} "; }
	if ($class_id <> 0) { $query .= " AND e.class_id = {$class_id} "; }
	if ($user_id <> 0) { $query .= " AND e.user_id = {$user_id} "; }
	if ($status <> 99) { $query .= " AND e.status = {$status} "; }
	$query .= " ORDER BY e.class_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_evaluation_user($key) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_class c, aca_user_class u ";
	$query .= " WHERE u.user_id = '{$key}' and c.class_id = u.class_id and c.class_type_id = 2 ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	


// course
function get_aca_course_count($status) {
	// status 99 for all
	global $connection;
	$query = "SELECT count(course_id) as record_count ";
	$query .= " FROM aca_course ";
	$query .= " WHERE course_id > 0 ";
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_course_rec($key) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_course ";
	$query .= " WHERE course_id = '{$key}' ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_course_list($npage, $no_of_list, $status) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_course ";
	$query .= " WHERE course_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY name ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	


function get_aca_course_exam($key) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_course c, aca_exam e ";
	$query .= " WHERE e.exam_id = '{$key}' and e.exam_id = c.exam_id ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	


// errors / log

function get_aca_log_count($status, $exam_id) {
	// status 99 for all
	global $connection;
	$query = "SELECT count(log_id) as record_count ";
	$query .= " FROM aca_log ";
	$query .= " WHERE log_id > 0 ";
	if ($exam_id <> 0) { $query .= " AND exam_id = {$exam_id} "; }
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_log_rec($key) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_log ";
	$query .= " WHERE log_id = '{$key}' ";
	$query .= " LIMIT 1 ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

function get_aca_log_list($npage, $no_of_list, $status) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM aca_log ";
	$query .= " WHERE log_id > 0 "; 
	if ($status <> 99) { $query .= " AND status = {$status} "; }
	$query .= " ORDER BY question_id ASC ";
	$query .= " LIMIT {$npage}, {$no_of_list}  ";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	



/* \\\\\\\\\\\\\\\\\\\\\\\\ END FUNCTIONS /////////////////////////// */


function generateInvoice($transdate,$reference,$type){

	ini_set("log_errors", 1);
	ini_set("error_log", "php-error.log");
	$_SESSION["invpdf"] = $reference;
	error_log( $reference );
    
require_once "../lib/fpdf/fpdf.php";

if(class_exists(PDF)) {
    class PDF2 extends PDF
    {
		// Page header
		function Header()
		{
			global $connection;
                global $reference;
                $sql = "SELECT * FROM aca_transaction where paymentreference = '".$_SESSION["invpdf"]."'";

                $result_set = mysqli_query($connection, $sql);

                while ($row = mysqli_fetch_array($result_set)) {
                    $quote_nr = '';

                    // Arial bold 15
                    $this->SetFont('Arial', 'B', 16);
                    // Move to the right
                    $this->Cell(50, 10, 'Tax Invoice', 0, 0, 'L');
                    $this->Cell(130, 10, '', 0, 0, 'L');
                    $this->Ln(10);

                    // Arial bold 12
                    $this->SetFont('Arial', '', 8);
                    $this->SetTextColor(102, 102, 102);
                    // Move to the right
                    $this->Cell(25, 4, 'NUMBER:', 0, 0, 'L');

                    $this->Cell(25, 4, $row["paymentreference"], 0, 1, 'R');
                    // Logo
                    $this->Image('../images/logo_blackboardtraining_200x62.png', 150, 10, 50);
                    $this->Cell(25, 4, 'REFERENCE:', 0, 0, 'L');
                    $this->Cell(25, 4, $row["paymentreference"], 0, 1, 'R');
                    $this->Cell(25, 4, 'DATE:', 0, 0, 'L');
                    $this->Cell(25, 4, substr($row["transactiondate"],0,10), 0, 1, 'R');
                }
		}

		// Page footer
		function Footer()
		{
			global $connection;
                global $reference;
                $sql = "SELECT * FROM aca_transaction where paymentreference = '".$_SESSION["invpdf"]."'";

                $result_set = mysqli_query($connection, $sql);

                while ($row = mysqli_fetch_array($result_set)) {
                    // Position at 1.5 cm from bottom
                    $this->SetY(-90);
                    // Arial italic 8

                    $this->SetFont('Arial', '', 8);
                    $this->SetFillColor(255, 255, 255);
                    $this->SetDrawColor(222, 222, 222);
                    $this->__currentY=$this->GetY();
                    $this->Cell(190, 2, '', 'T', 1, 'L');
                    $this->SetFont('Arial', '', 8);
                    $this->MultiCell(130,4,'', 0, 'L',false);
                    $this->SetFont('Arial', '', 8);
                    $this->SetXY($this->GetX()+130, $this->__currentY+5);
                    $this->MultiCell(35, 4,"Total Exclusive:\n\tTotal VAT;\n\tSub Total:", 0, 'L',false);
                    $this->SetXY($this->GetX()+165, $this->__currentY+5);
                    $this->SetFont('Arial', 'B', 8);
                    $this->MultiCell(25, 4,$_SESSION["currencySymbol"]." ".$row["totalpayable"]."\n\t".$_SESSION["currencySymbol"]." 0.00\n\t".$_SESSION["currencySymbol"]." ".$row["totalpayable"], 0, 'R',false);

                    $this->Ln(20);
                    $this->Cell(140, 4, '', 0, 0, 'L');
                    $this->Cell(25, 4, 'Grand Total', 0, 0, 'L');
                    $this->Cell(25, 4, $_SESSION["currencySymbol"].' '.$row["totalpayable"], 0, 1, 'R');

                    $this->Ln(20);
                    $this->SetFont('Arial', 'B', 9);
                    $this->Cell(140, 7, '', 0, 0, 'L');
                    $this->Cell(50, 7, 'BALANCE DUE', 0, 1, 'R');

                    $this->SetFont('Arial', 'B', 12);
                    $this->Cell(140, 7, '', 0, 0, 'L');
                    $this->Cell(50, 7, $_SESSION["currencySymbol"].' '.$row["totalpayable"], 0, 1, 'R');
                }
		}
	}
} else {
    class PDF2 extends FPDF
    {
        // Page header
        function Header()
        {
			global $connection;
                global $reference;
                $sql = "SELECT * FROM aca_transaction where paymentreference = '".$_SESSION["invpdf"]."'";

                $result_set = mysqli_query($connection, $sql);

                while ($row = mysqli_fetch_array($result_set)) {
                    $quote_nr = '';

                    // Arial bold 15
                    $this->SetFont('Arial', 'B', 16);
                    // Move to the right
                    $this->Cell(50, 10, 'Tax Invoice', 0, 0, 'L');
                    $this->Cell(130, 10, '', 0, 0, 'L');
                    $this->Ln(10);

                    // Arial bold 12
                    $this->SetFont('Arial', '', 8);
                    $this->SetTextColor(102, 102, 102);
                    // Move to the right
                    $this->Cell(25, 4, 'NUMBER:', 0, 0, 'L');

                    $this->Cell(25, 4, $row["paymentreference"], 0, 1, 'R');
                    // Logo
                    $this->Image('../images/logo_blackboardtraining_200x62.png', 150, 10, 50);
                    $this->Cell(25, 4, 'REFERENCE:', 0, 0, 'L');
                    $this->Cell(25, 4, $row["paymentreference"], 0, 1, 'R');
                    $this->Cell(25, 4, 'DATE:', 0, 0, 'L');
                    $this->Cell(25, 4, substr($row["transactiondate"],0,10), 0, 1, 'R');
                }
        }

        // Page footer
        function Footer()
        {
            global $connection;
                global $reference;
                $sql = "SELECT * FROM aca_transaction where paymentreference = '".$_SESSION["invpdf"]."'";

                $result_set = mysqli_query($connection, $sql);

                while ($row = mysqli_fetch_array($result_set)) {
                    // Position at 1.5 cm from bottom
                    $this->SetY(-90);
                    // Arial italic 8

                    $this->SetFont('Arial', '', 8);
                    $this->SetFillColor(255, 255, 255);
                    $this->SetDrawColor(222, 222, 222);
                    $this->__currentY=$this->GetY();
                    $this->Cell(190, 2, '', 'T', 1, 'L');
                    $this->SetFont('Arial', '', 8);
                    $this->MultiCell(130,4,'', 0, 'L',false);
                    $this->SetFont('Arial', '', 8);
                    $this->SetXY($this->GetX()+130, $this->__currentY+5);
                    $this->MultiCell(35, 4,"Total Exclusive:\n\tTotal VAT;\n\tSub Total:", 0, 'L',false);
                    $this->SetXY($this->GetX()+165, $this->__currentY+5);
                    $this->SetFont('Arial', 'B', 8);
                    $this->MultiCell(25, 4,$_SESSION["currencySymbol"]." ".$row["totalpayable"]."\n\t".$_SESSION["currencySymbol"]." 0.00\n\t".$_SESSION["currencySymbol"]." ".$row["totalpayable"], 0, 'R',false);

                    $this->Ln(20);
                    $this->Cell(140, 4, '', 0, 0, 'L');
                    $this->Cell(25, 4, 'Grand Total', 0, 0, 'L');
                    $this->Cell(25, 4, $_SESSION["currencySymbol"].' '.$row["totalpayable"], 0, 1, 'R');

                    $this->Ln(20);
                    $this->SetFont('Arial', 'B', 9);
                    $this->Cell(140, 7, '', 0, 0, 'L');
                    $this->Cell(50, 7, 'BALANCE DUE', 0, 1, 'R');

                    $this->SetFont('Arial', 'B', 12);
                    $this->Cell(140, 7, '', 0, 0, 'L');
                    $this->Cell(50, 7, $_SESSION["currencySymbol"].' '.$row["totalpayable"], 0, 1, 'R');
                }
        }
    }
}


// Instanciation of inherited class
    global $connection;

    $sql2 = "select * from aca_invoice_config limit 1";

    $result2 = mysqli_query($connection, $sql2);
    while ($row = mysqli_fetch_array($result2)) {
        $company_name = $row["company_name"];
        $vat_number = $row["vat_number"];
        $address1 = $row["postal_address1"];
        $address2 = $row["postal_address2"];
        $address3 = $row["postal_address3"];
        $address4 = $row["postal_address4"];
    }
    $sql = "SELECT * FROM aca_transaction where paymentreference = '".$reference."'";

    $result_set = mysqli_query($connection, $sql);

    while ($row = mysqli_fetch_array($result_set)) {

        $s_details = unserialize($row["s_details"]);
        $s_transaction = unserialize($row["s_transaction"]);

        $arr = explode("\n", $s_details["postal"]);

        $quote_nr = $row["paymentreference"];
        $pdf = new PDF2();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->Ln(10);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(95, 5, 'FROM:', 0, 0, 'L');
        $pdf->Cell(95, 5, 'TO:', 0, 1, 'L');
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->SetTextColor(102, 102, 102);
        $pdf->Cell(95, 5, $company_name, 0, 0, 'L');
		if($s_details["invoiceto"] == "company"){
        $pdf->Cell(95, 5, $row["ccompanyname"], 0, 1, 'L');
		} else {
        $pdf->Cell(95, 5, $row["cname"].' '.$row["csurname"], 0, 1, 'L');
		}

        $pdf->SetFont('Arial', 'B', 9);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(48, 5, 'VAT NO:', 0, 0, 'L');
        $pdf->SetFont('Arial', 'I', 9);
		$pdf->SetTextColor(102, 102, 102);
        $pdf->Cell(47, 5, ($vat_number != '' ? $vat_number : ''), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
		$pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(48, 5, 'CUSTOMER VAT NO:', 0, 0, 'L');
		$pdf->SetTextColor(102, 102, 102);
		$pdf->SetFont('Arial', '', 9);
        $pdf->Cell(47, 5, $s_details["vatnumber"], 0, 1, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(48, 5, 'POSTAL ADDRESS:', 0, 0, 'L');
		$pdf->SetFont('Arial', '', 9);
        $pdf->SetTextColor(102, 102, 102);
		$pdf->Cell(47, 5, $address1, 0, 0, 'L');
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(48, 5, 'POSTAL ADDRESS:', 0, 0, 'L');
		$pdf->SetFont('Arial', '', 9);
		$pdf->SetTextColor(102, 102, 102);
        $pdf->Cell(47, 5, (isset($arr[0]) ? $arr[0] : ''), 0, 1, 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->SetTextColor(102, 102, 102);
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, $address2, 0, 0, 'L');
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, (isset($arr[1]) ? $arr[1] : ''), 0, 1, 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, $address3, 0, 0, 'L');
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, (isset($arr[2]) ? $arr[2] : ''), 0, 1, 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, $address4, 0, 0, 'L');
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, (isset($arr[3]) ? $arr[3] : ''), 0, 1, 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, '', 0, 0, 'L');
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, (isset($arr[4]) ? $arr[4] : ''), 0, 1, 'L');


        $pdf->Ln(10);
        $pdf->SetFont('Arial', 'IB', 8);
        $pdf->SetTextColor(159, 159, 159);
        $pdf->SetFillColor(250, 250, 250);
        $pdf->SetDrawColor(222, 222, 222);
        $pdf->Cell(75, 5, 'Description', 'TB', 0, 'L', 1);
        $pdf->Cell(20, 5, 'Qty', 'TB', 0, 'L', 1);
        $pdf->Cell(25, 5, 'Excl. Price', 'TB', 0, 'L', 1);
        $pdf->Cell(25, 5, 'VAT %', 'TB', 0, 'L', 1);
        $pdf->Cell(25, 5, 'Excl. Total', 'TB', 0, 'L', 1);
        $pdf->Cell(25, 5, 'Incl. Total', 'TB', 1, 'L', 1);
		$pdf->SetFont('Arial', '', 8);
		$pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetDrawColor(255, 255, 255);
		$pdf->__currentY=$pdf->GetY();
		$pdf->MultiCell(75, 5, $s_transaction[0]["description"], 1, 'L',false);
		$pdf->SetXY($pdf->GetX()+75, $pdf->__currentY);
		$pdf->MultiCell(20, 5, '1', 1, 'L',false);
		$pdf->SetXY($pdf->GetX()+95, $pdf->__currentY);
		$pdf->MultiCell(25, 5, $_SESSION["currencySymbol"].' '.$s_transaction[0]["amount"], 1, 'L',false);
		$pdf->SetXY($pdf->GetX()+120, $pdf->__currentY);
		$pdf->MultiCell(25, 5, '0', 0, 'L',false);
		$pdf->SetXY($pdf->GetX()+145, $pdf->__currentY);
		$pdf->MultiCell(25, 5, $_SESSION["currencySymbol"].' '.$s_transaction[0]["amount"], 0, 'L',false);
		$pdf->SetXY($pdf->GetX()+170, $pdf->__currentY);
		$pdf->MultiCell(25, 5, $_SESSION["currencySymbol"].' '.$s_transaction[0]["amount"], 0, 'L',false);
		
		$out = $pdf->Output('../pdf/' . date("ymd",strtotime($transdate)) . '_'.$reference.'.pdf', 'F');
	}

		

}

function generateInvoice2($transdate,$reference,$type){

	ini_set("log_errors", 1);
	ini_set("error_log", "php-error.log");
	$_SESSION["invpdf"] = $reference;
	error_log( $reference );
    
require_once "lib/fpdf/fpdf.php";

if(class_exists(PDF)) {
    class PDF2 extends PDF
    {
		// Page header
		function Header()
		{
			global $connection;
                global $reference;
                $sql = "SELECT * FROM aca_transaction where paymentreference = '".$_SESSION["invpdf"]."'";

                $result_set = mysqli_query($connection, $sql);

                while ($row = mysqli_fetch_array($result_set)) {
                    $quote_nr = '';

                    // Arial bold 15
                    $this->SetFont('Arial', 'B', 16);
                    // Move to the right
                    $this->Cell(50, 10, 'Tax Invoice', 0, 0, 'L');
                    $this->Cell(130, 10, '', 0, 0, 'L');
                    $this->Ln(10);

                    // Arial bold 12
                    $this->SetFont('Arial', '', 8);
                    $this->SetTextColor(102, 102, 102);
                    // Move to the right
                    $this->Cell(25, 4, 'NUMBER:', 0, 0, 'L');

                    $this->Cell(25, 4, $row["paymentreference"], 0, 1, 'R');
                    // Logo
                    $this->Image('images/logo_blackboardtraining_200x62.png', 150, 10, 50);
                    $this->Cell(25, 4, 'REFERENCE:', 0, 0, 'L');
                    $this->Cell(25, 4, $row["paymentreference"], 0, 1, 'R');
                    $this->Cell(25, 4, 'DATE:', 0, 0, 'L');
                    $this->Cell(25, 4, substr($row["transactiondate"],0,10), 0, 1, 'R');
                }
		}

		// Page footer
		function Footer()
		{
			global $connection;
                global $reference;
                $sql = "SELECT * FROM aca_transaction where paymentreference = '".$_SESSION["invpdf"]."'";

                $result_set = mysqli_query($connection, $sql);

                while ($row = mysqli_fetch_array($result_set)) {
                    // Position at 1.5 cm from bottom
                    $this->SetY(-90);
                    // Arial italic 8

                    $this->SetFont('Arial', '', 8);
                    $this->SetFillColor(255, 255, 255);
                    $this->SetDrawColor(222, 222, 222);
                    $this->__currentY=$this->GetY();
                    $this->Cell(190, 2, '', 'T', 1, 'L');
                    $this->SetFont('Arial', '', 8);
                    $this->MultiCell(130,4,'', 0, 'L',false);
                    $this->SetFont('Arial', '', 8);
                    $this->SetXY($this->GetX()+130, $this->__currentY+5);
                    $this->MultiCell(35, 4,"Total Exclusive:\n\tTotal VAT;\n\tSub Total:", 0, 'L',false);
                    $this->SetXY($this->GetX()+165, $this->__currentY+5);
                    $this->SetFont('Arial', 'B', 8);
                    $this->MultiCell(25, 4,$_SESSION["currencySymbol"]." ".$row["totalpayable"]."\n\t".$_SESSION["currencySymbol"]." 0.00\n\t".$_SESSION["currencySymbol"]." ".$row["totalpayable"], 0, 'R',false);

                    $this->Ln(20);
                    $this->Cell(140, 4, '', 0, 0, 'L');
                    $this->Cell(25, 4, 'Grand Total', 0, 0, 'L');
                    $this->Cell(25, 4, $_SESSION["currencySymbol"].' '.$row["totalpayable"], 0, 1, 'R');

                    $this->Ln(20);
                    $this->SetFont('Arial', 'B', 9);
                    $this->Cell(140, 7, '', 0, 0, 'L');
                    $this->Cell(50, 7, 'BALANCE DUE', 0, 1, 'R');

                    $this->SetFont('Arial', 'B', 12);
                    $this->Cell(140, 7, '', 0, 0, 'L');
                    $this->Cell(50, 7, $_SESSION["currencySymbol"].' '.$row["totalpayable"], 0, 1, 'R');
                }
		}
	}
} else {
    class PDF2 extends FPDF
    {
        // Page header
        function Header()
        {
			global $connection;
                global $reference;
                $sql = "SELECT * FROM aca_transaction where paymentreference = '".$_SESSION["invpdf"]."'";

                $result_set = mysqli_query($connection, $sql);

                while ($row = mysqli_fetch_array($result_set)) {
                    $quote_nr = '';

                    // Arial bold 15
                    $this->SetFont('Arial', 'B', 16);
                    // Move to the right
                    $this->Cell(50, 10, 'Tax Invoice', 0, 0, 'L');
                    $this->Cell(130, 10, '', 0, 0, 'L');
                    $this->Ln(10);

                    // Arial bold 12
                    $this->SetFont('Arial', '', 8);
                    $this->SetTextColor(102, 102, 102);
                    // Move to the right
                    $this->Cell(25, 4, 'NUMBER:', 0, 0, 'L');

                    $this->Cell(25, 4, $row["paymentreference"], 0, 1, 'R');
                    // Logo
                    $this->Image('images/logo_blackboardtraining_200x62.png', 150, 10, 50);
                    $this->Cell(25, 4, 'REFERENCE:', 0, 0, 'L');
                    $this->Cell(25, 4, $row["paymentreference"], 0, 1, 'R');
                    $this->Cell(25, 4, 'DATE:', 0, 0, 'L');
                    $this->Cell(25, 4, substr($row["transactiondate"],0,10), 0, 1, 'R');
                }
        }

        // Page footer
        function Footer()
        {
            global $connection;
                global $reference;
                $sql = "SELECT * FROM aca_transaction where paymentreference = '".$_SESSION["invpdf"]."'";

                $result_set = mysqli_query($connection, $sql);

                while ($row = mysqli_fetch_array($result_set)) {
                    // Position at 1.5 cm from bottom
                    $this->SetY(-90);
                    // Arial italic 8

                    $this->SetFont('Arial', '', 8);
                    $this->SetFillColor(255, 255, 255);
                    $this->SetDrawColor(222, 222, 222);
                    $this->__currentY=$this->GetY();
                    $this->Cell(190, 2, '', 'T', 1, 'L');
                    $this->SetFont('Arial', '', 8);
                    $this->MultiCell(130,4,'', 0, 'L',false);
                    $this->SetFont('Arial', '', 8);
                    $this->SetXY($this->GetX()+130, $this->__currentY+5);
                    $this->MultiCell(35, 4,"Total Exclusive:\n\tTotal VAT;\n\tSub Total:", 0, 'L',false);
                    $this->SetXY($this->GetX()+165, $this->__currentY+5);
                    $this->SetFont('Arial', 'B', 8);
                    $this->MultiCell(25, 4,$_SESSION["currencySymbol"]." ".$row["totalpayable"]."\n\t".$_SESSION["currencySymbol"]." 0.00\n\t".$_SESSION["currencySymbol"]." ".$row["totalpayable"], 0, 'R',false);

                    $this->Ln(20);
                    $this->Cell(140, 4, '', 0, 0, 'L');
                    $this->Cell(25, 4, 'Grand Total', 0, 0, 'L');
                    $this->Cell(25, 4, $_SESSION["currencySymbol"].' '.$row["totalpayable"], 0, 1, 'R');

                    $this->Ln(20);
                    $this->SetFont('Arial', 'B', 9);
                    $this->Cell(140, 7, '', 0, 0, 'L');
                    $this->Cell(50, 7, 'BALANCE DUE', 0, 1, 'R');

                    $this->SetFont('Arial', 'B', 12);
                    $this->Cell(140, 7, '', 0, 0, 'L');
                    $this->Cell(50, 7, $_SESSION["currencySymbol"].' '.$row["totalpayable"], 0, 1, 'R');
                }
        }
    }
}


// Instanciation of inherited class
    global $connection;

    $sql2 = "select * from aca_invoice_config limit 1";

    $result2 = mysqli_query($connection, $sql2);
    while ($row = mysqli_fetch_array($result2)) {
        $company_name = $row["company_name"];
        $vat_number = $row["vat_number"];
        $address1 = $row["postal_address1"];
        $address2 = $row["postal_address2"];
        $address3 = $row["postal_address3"];
        $address4 = $row["postal_address4"];
    }
    $sql = "SELECT * FROM aca_transaction where paymentreference = '".$reference."'";

    $result_set = mysqli_query($connection, $sql);

    while ($row = mysqli_fetch_array($result_set)) {

        $s_details = unserialize($row["s_details"]);
        $s_transaction = unserialize($row["s_transaction"]);

        $arr = explode("\n", $s_details["postal"]);

        $quote_nr = $row["paymentreference"];
        $pdf = new PDF2();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->Ln(10);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(95, 5, 'FROM:', 0, 0, 'L');
        $pdf->Cell(95, 5, 'TO:', 0, 1, 'L');
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->SetTextColor(102, 102, 102);
        $pdf->Cell(95, 5, $company_name, 0, 0, 'L');
		if($s_details["invoiceto"] == "company"){
        $pdf->Cell(95, 5, $row["ccompanyname"], 0, 1, 'L');
		} else {
        $pdf->Cell(95, 5, $row["cname"].' '.$row["csurname"], 0, 1, 'L');
		}

        $pdf->SetFont('Arial', 'B', 9);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(48, 5, 'VAT NO:', 0, 0, 'L');
        $pdf->SetFont('Arial', 'I', 9);
		$pdf->SetTextColor(102, 102, 102);
        $pdf->Cell(47, 5, ($vat_number != '' ? $vat_number : ''), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
		$pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(48, 5, 'CUSTOMER VAT NO:', 0, 0, 'L');
		$pdf->SetTextColor(102, 102, 102);
		$pdf->SetFont('Arial', '', 9);
        $pdf->Cell(47, 5, $s_details["vatnumber"], 0, 1, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(48, 5, 'POSTAL ADDRESS:', 0, 0, 'L');
		$pdf->SetFont('Arial', '', 9);
        $pdf->SetTextColor(102, 102, 102);
		$pdf->Cell(47, 5, $address1, 0, 0, 'L');
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(48, 5, 'POSTAL ADDRESS:', 0, 0, 'L');
		$pdf->SetFont('Arial', '', 9);
		$pdf->SetTextColor(102, 102, 102);
        $pdf->Cell(47, 5, (isset($arr[0]) ? $arr[0] : ''), 0, 1, 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->SetTextColor(102, 102, 102);
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, $address2, 0, 0, 'L');
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, (isset($arr[1]) ? $arr[1] : ''), 0, 1, 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, $address3, 0, 0, 'L');
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, (isset($arr[2]) ? $arr[2] : ''), 0, 1, 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, $address4, 0, 0, 'L');
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, (isset($arr[3]) ? $arr[3] : ''), 0, 1, 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, '', 0, 0, 'L');
        $pdf->Cell(48, 5, '', 0, 0, 'L');
        $pdf->Cell(47, 5, (isset($arr[4]) ? $arr[4] : ''), 0, 1, 'L');


        $pdf->Ln(10);
        $pdf->SetFont('Arial', 'IB', 8);
        $pdf->SetTextColor(159, 159, 159);
        $pdf->SetFillColor(250, 250, 250);
        $pdf->SetDrawColor(222, 222, 222);
        $pdf->Cell(75, 5, 'Description', 'TB', 0, 'L', 1);
        $pdf->Cell(20, 5, 'Qty', 'TB', 0, 'L', 1);
        $pdf->Cell(25, 5, 'Excl. Price', 'TB', 0, 'L', 1);
        $pdf->Cell(25, 5, 'VAT %', 'TB', 0, 'L', 1);
        $pdf->Cell(25, 5, 'Excl. Total', 'TB', 0, 'L', 1);
        $pdf->Cell(25, 5, 'Incl. Total', 'TB', 1, 'L', 1);
		$pdf->SetFont('Arial', '', 8);
		$pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetDrawColor(255, 255, 255);
		$pdf->__currentY=$pdf->GetY();
		$pdf->MultiCell(75, 5, $s_transaction[0]["description"], 1, 'L',false);
		$pdf->SetXY($pdf->GetX()+75, $pdf->__currentY);
		$pdf->MultiCell(20, 5, '1', 1, 'L',false);
		$pdf->SetXY($pdf->GetX()+95, $pdf->__currentY);
		$pdf->MultiCell(25, 5, $_SESSION["currencySymbol"].' '.$s_transaction[0]["amount"], 1, 'L',false);
		$pdf->SetXY($pdf->GetX()+120, $pdf->__currentY);
		$pdf->MultiCell(25, 5, '0', 0, 'L',false);
		$pdf->SetXY($pdf->GetX()+145, $pdf->__currentY);
		$pdf->MultiCell(25, 5, $_SESSION["currencySymbol"].' '.$s_transaction[0]["amount"], 0, 'L',false);
		$pdf->SetXY($pdf->GetX()+170, $pdf->__currentY);
		$pdf->MultiCell(25, 5, $_SESSION["currencySymbol"].' '.$s_transaction[0]["amount"], 0, 'L',false);
		
		$out = $pdf->Output('pdf/' . date("ymd",strtotime($transdate)) . '_'.$reference.'.pdf', 'F');
	}

		

}

function generateExamPDF($transdate,$exam_id,$nrquestions,$reference){

	$_SESSION["pdfeid"] = $exam_id;
    require_once "../lib/fpdf/fpdf.php";

    class PDF extends FPDF
    {
// Page header
        function Header()
        {
			if ( $this->PageNo() == 1 ) {

                global $connection;

                $sql = "select * from aca_exam where exam_id = '".$_SESSION["pdfeid"]."'";

                $result_set = mysqli_query($connection, $sql);

                while ($row = mysqli_fetch_array($result_set)) {
                    $this->SetFont('Arial', 'B', 12);
                    $this->SetTextColor(0, 0, 0);
                    $this->Cell(20,5,'',0,'C',false);
                    $this->MultiCell(150,5,$row["code"].' - '.$row["description"],0,'C',false);
                    $this->Cell(20,5,'',0,'C',false);
                    $this->Ln(2);
                    $this->SetFont('Arial', 'B', 9);
                    $this->Cell(20,5,'',0,'C',false);
                    $this->MultiCell(150,5,'Version: '.$row["versions"],0,'C',false);
                    $this->Cell(20,5,'',0,'C',false);
                }
            }
            $this->Image('../images/pdf-watermark.png', 30, 40, 190);
            $this->Image('../images/logo_blackboardtraining_200x62.png', 10, 283,40);
        }

// Page footer
        function Footer()
        {
// Go to 1.5 cm from bottom
            $this->SetY(-15);
// Select Arial italic 8
            $this->SetFont('Arial','I',8);
// Print centered page number

            $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
            $this->Cell(0,10,'https://training.blackboardbi.com',0,0,'R');
        }
    }
    global $connection;
    $sql = "SELECT * FROM aca_question where exam_id = '".$exam_id."' order by rand() limit ".$nrquestions;

    $result_set = mysqli_query($connection, $sql);


    $pdf = new PDF();
    $pdf->AliasNbPages();
    $pdf->AddPage();

    $pdf->Ln(10);


    $i = 1;
    $questions = array();
    while ($row = mysqli_fetch_array($result_set)) {
        $questions[] = $row["question_id"];
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(10,5,$i.'.)',0,'L',false);
        $pdf->MultiCell(180, 5, $row["question_text"], 0, 'L', false);
        $pdf->Ln(3);
        $pdf->SetFont('Arial', '', 6);
        $pdf->SetTextColor(0, 0, 0);
        if(isset($row["option_a_text"]) && $row["option_a_text"] != ''){
            $pdf->Cell(10,3,'',0,'L', false);
            $pdf->Cell(3,3,'',1,'L', false);
            $pdf->Cell(5,3,'A. ',0,'R', false);
            $pdf->MultiCell(167, 3, $row["option_a_text"], 0, 'L', false);
            $pdf->Ln(3);
        }
        if(isset($row["option_b_text"]) && $row["option_b_text"] != ''){
            $pdf->Cell(10,3,'',0,'L', false);
            $pdf->Cell(3,3,'',1,'L', false);
            $pdf->Cell(5,3,'B. ',0,'R', false);
            $pdf->MultiCell(167, 3, $row["option_b_text"], 0, 'L', false);
            $pdf->Ln(3);
        }
        if(isset($row["option_c_text"]) && $row["option_c_text"] != ''){
            $pdf->Cell(10,3,'',0,'L', false);
            $pdf->Cell(3,3,'',1,'L', false);
            $pdf->Cell(5,3,'C. ',0,'R', false);
            $pdf->MultiCell(180, 3, $row["option_c_text"], 0, 'L', false);
            $pdf->Ln(3);
        }
        if(isset($row["option_d_text"]) && $row["option_d_text"] != ''){
            $pdf->Cell(10,3,'',0,'L', false);
            $pdf->Cell(3,3,'',1,'L', false);
            $pdf->Cell(5,3,'D. ',0,'R', false);
            $pdf->MultiCell(180, 3, $row["option_d_text"], 0, 'L', false);
            $pdf->Ln(3);
        }
        if(isset($row["option_e_text"]) && $row["option_e_text"] != ''){
            $pdf->Cell(10,3,'',0,'L', false);
            $pdf->Cell(3,3,'',1,'L', false);
            $pdf->Cell(5,3,'E. ',0,'R', false);
            $pdf->MultiCell(180, 3, $row["option_e_text"], 0, 'L', false);
            $pdf->Ln(3);
        }
        $pdf->Ln(3);
        $i++;
        /*if(is_float($i/7) == false){
        $pdf->AddPage();
        }*/
    }

    $pdf->AddPage();

    $pdf->SetFont('Arial', 'B', 12);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell(20,5,'',0,'C',false);
    $pdf->MultiCell(150,5,'Answers',0,'C',false);
    $pdf->Ln(10);

    $i = 1;

    $col = 0;

    foreach($questions as $key=>$value) {
        $sql2 = "SELECT * FROM aca_question where question_id = '".$value."'";

        $result_set2 = mysqli_query($connection, $sql2);


        while ($row = mysqli_fetch_array($result_set2)) {
            $answers = array();
            if($col == '4'){
                $pdf->Ln(3);
                $pdf->Ln(3);
                $col = 0;
            }
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(10, 5, $i . '.)', 0, 'L', false);
            $pdf->SetFont('Arial', '', 8);
            $pdf->SetTextColor(0, 0, 0);
            if (isset($row["option_a_answer"]) && $row["option_a_answer"] != '') {
                if ($row["option_a_answer"] == '1') {
                    array_push($answers,'a');
                }

//$pdf->Ln(3);
            }
            if (isset($row["option_b_answer"]) && $row["option_b_answer"] != '') {
                if ($row["option_b_answer"] == '1') {
                    array_push($answers,'b');
                }

//$pdf->Ln(3);
            }
            if (isset($row["option_c_answer"]) && $row["option_c_answer"] != '') {
                if ($row["option_c_answer"] == '1') {
                    array_push($answers,'c');
                }

//$pdf->Ln(3);
            }
            if (isset($row["option_d_answer"]) && $row["option_d_answer"] != '') {
                if ($row["option_d_answer"] == '1') {
                    array_push($answers,'d');
                }

//$pdf->Ln(3);
            }
            if (isset($row["option_e_answer"]) && $row["option_e_answer"] != '') {
                if ($row["option_e_answer"] == '1') {
                    array_push($answers,'e');
                }

//$pdf->Ln(3);
            }
            $answers_str = implode(',',$answers);
            $pdf->Cell(40, 5, $answers_str, 0, 'L', false);
//$pdf->Ln(3);
            $i++;
            /*if(is_float($i/7) == false){
            $pdf->AddPage();
            }*/
            $col++;
        }
    }
    $pdf->Output('../pdf/exams/'.date("ymd",strtotime($transdate)).'_'.$reference.'.pdf', 'F');
}

function generateExamPDF2($transdate,$exam_id,$nrquestions,$reference){

	$_SESSION["pdfeid"] = $exam_id;
    require_once "lib/fpdf/fpdf.php";

    class PDF extends FPDF
    {
// Page header
        function Header()
        {
			if ( $this->PageNo() == 1 ) {

                global $connection;

                $sql = "select * from aca_exam where exam_id = '".$_SESSION["pdfeid"]."'";

                $result_set = mysqli_query($connection, $sql);

                while ($row = mysqli_fetch_array($result_set)) {
                    $this->SetFont('Arial', 'B', 12);
                    $this->SetTextColor(0, 0, 0);
                    $this->Cell(20,5,'',0,'C',false);
                    $this->MultiCell(150,5,$row["code"].' - '.$row["description"],0,'C',false);
                    $this->Cell(20,5,'',0,'C',false);
                    $this->Ln(2);
                    $this->SetFont('Arial', 'B', 9);
                    $this->Cell(20,5,'',0,'C',false);
                    $this->MultiCell(150,5,'Version: '.$row["versions"],0,'C',false);
                    $this->Cell(20,5,'',0,'C',false);
                }
            }
            $this->Image('images/pdf-watermark.png', 30, 40, 190);
            $this->Image('images/logo_blackboardtraining_200x62.png', 10, 283,40);
        }

// Page footer
        function Footer()
        {
// Go to 1.5 cm from bottom
            $this->SetY(-15);
// Select Arial italic 8
            $this->SetFont('Arial','I',8);
// Print centered page number

            $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
            $this->Cell(0,10,'https://training.blackboardbi.com',0,0,'R');
        }
    }
    global $connection;
    $sql = "SELECT * FROM aca_question where exam_id = '".$exam_id."' order by rand() limit ".$nrquestions;

    $result_set = mysqli_query($connection, $sql);


    $pdf = new PDF();
    $pdf->AliasNbPages();
    $pdf->AddPage();

    $pdf->Ln(10);


    $i = 1;
    $questions = array();
    while ($row = mysqli_fetch_array($result_set)) {
        $questions[] = $row["question_id"];
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(10,5,$i.'.)',0,'L',false);
        $pdf->MultiCell(180, 5, $row["question_text"], 0, 'L', false);
        $pdf->Ln(3);
        $pdf->SetFont('Arial', '', 6);
        $pdf->SetTextColor(0, 0, 0);
        if(isset($row["option_a_text"]) && $row["option_a_text"] != ''){
            $pdf->Cell(10,3,'',0,'L', false);
            $pdf->Cell(3,3,'',1,'L', false);
            $pdf->Cell(5,3,'A. ',0,'R', false);
            $pdf->MultiCell(167, 3, $row["option_a_text"], 0, 'L', false);
            $pdf->Ln(3);
        }
        if(isset($row["option_b_text"]) && $row["option_b_text"] != ''){
            $pdf->Cell(10,3,'',0,'L', false);
            $pdf->Cell(3,3,'',1,'L', false);
            $pdf->Cell(5,3,'B. ',0,'R', false);
            $pdf->MultiCell(167, 3, $row["option_b_text"], 0, 'L', false);
            $pdf->Ln(3);
        }
        if(isset($row["option_c_text"]) && $row["option_c_text"] != ''){
            $pdf->Cell(10,3,'',0,'L', false);
            $pdf->Cell(3,3,'',1,'L', false);
            $pdf->Cell(5,3,'C. ',0,'R', false);
            $pdf->MultiCell(180, 3, $row["option_c_text"], 0, 'L', false);
            $pdf->Ln(3);
        }
        if(isset($row["option_d_text"]) && $row["option_d_text"] != ''){
            $pdf->Cell(10,3,'',0,'L', false);
            $pdf->Cell(3,3,'',1,'L', false);
            $pdf->Cell(5,3,'D. ',0,'R', false);
            $pdf->MultiCell(180, 3, $row["option_d_text"], 0, 'L', false);
            $pdf->Ln(3);
        }
        if(isset($row["option_e_text"]) && $row["option_e_text"] != ''){
            $pdf->Cell(10,3,'',0,'L', false);
            $pdf->Cell(3,3,'',1,'L', false);
            $pdf->Cell(5,3,'E. ',0,'R', false);
            $pdf->MultiCell(180, 3, $row["option_e_text"], 0, 'L', false);
            $pdf->Ln(3);
        }
        $pdf->Ln(3);
        $i++;
        /*if(is_float($i/7) == false){
        $pdf->AddPage();
        }*/
    }

    $pdf->AddPage();

    $pdf->SetFont('Arial', 'B', 12);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell(20,5,'',0,'C',false);
    $pdf->MultiCell(150,5,'Answers',0,'C',false);
    $pdf->Ln(10);

    $i = 1;

    $col = 0;

    foreach($questions as $key=>$value) {
        $sql2 = "SELECT * FROM aca_question where question_id = '".$value."'";

        $result_set2 = mysqli_query($connection, $sql2);


        while ($row = mysqli_fetch_array($result_set2)) {
            $answers = array();
            if($col == '4'){
                $pdf->Ln(3);
                $pdf->Ln(3);
                $col = 0;
            }
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(10, 5, $i . '.)', 0, 'L', false);
            $pdf->SetFont('Arial', '', 8);
            $pdf->SetTextColor(0, 0, 0);
            if (isset($row["option_a_answer"]) && $row["option_a_answer"] != '') {
                if ($row["option_a_answer"] == '1') {
                    array_push($answers,'a');
                }

//$pdf->Ln(3);
            }
            if (isset($row["option_b_answer"]) && $row["option_b_answer"] != '') {
                if ($row["option_b_answer"] == '1') {
                    array_push($answers,'b');
                }

//$pdf->Ln(3);
            }
            if (isset($row["option_c_answer"]) && $row["option_c_answer"] != '') {
                if ($row["option_c_answer"] == '1') {
                    array_push($answers,'c');
                }

//$pdf->Ln(3);
            }
            if (isset($row["option_d_answer"]) && $row["option_d_answer"] != '') {
                if ($row["option_d_answer"] == '1') {
                    array_push($answers,'d');
                }

//$pdf->Ln(3);
            }
            if (isset($row["option_e_answer"]) && $row["option_e_answer"] != '') {
                if ($row["option_e_answer"] == '1') {
                    array_push($answers,'e');
                }

//$pdf->Ln(3);
            }
            $answers_str = implode(',',$answers);
            $pdf->Cell(40, 5, $answers_str, 0, 'L', false);
//$pdf->Ln(3);
            $i++;
            /*if(is_float($i/7) == false){
            $pdf->AddPage();
            }*/
            $col++;
        }
    }
    $pdf->Output('pdf/exams/'.date("ymd",strtotime($transdate)).'_'.$reference.'.pdf', 'F');
}

function currencyConverter($currency_from,$currency_to,$currency_input){
    $yql_base_url = "http://query.yahooapis.com/v1/public/yql";
    $yql_query = 'select * from yahoo.finance.xchange where pair in ("'.$currency_from.$currency_to.'")';
    $yql_query_url = $yql_base_url . "?q=" . urlencode($yql_query);
    $yql_query_url .= "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
    $yql_session = curl_init($yql_query_url);
    curl_setopt($yql_session, CURLOPT_RETURNTRANSFER,true);
    $yqlexec = curl_exec($yql_session);
    $yql_json =  json_decode($yqlexec,true);
    $currency_output = (float) $currency_input*$yql_json['query']['results']['rate']['Rate'];

    return $currency_output;
}


function googleCurrencyConverter($from_Currency,$to_Currency,$amount) {
    $from_Currency = urlencode($from_Currency);
    $to_Currency = urlencode($to_Currency);
    $encode_amount = 1;
    $get = file_get_contents("https://www.google.com/finance/converter?a=$encode_amount&from=$from_Currency&to=$to_Currency");
    $get = explode("<span class=bld>",$get);
    $get = explode("</span>",$get[1]);
    $converted_currency = preg_replace("/[^0-9\.]/", null, $get[0]);
    return $converted_currency;
}

function formatMoney($number, $cents = 2) { // cents: 0=never, 1=if needed, 2=always
    if (is_numeric($number)) { // a number
        if (!$number) { // zero
            $money = ($cents == 2 ? '0.00' : '0'); // output zero
        } else { // value
            if (floor($number) == $number) { // whole number
                $money = number_format($number, ($cents == 2 ? 2 : 0)); // format
            } else { // cents
                $money = number_format(round($number, 2), ($cents == 0 ? 0 : 2)); // format
            } // integer or decimal
        } // value
        return $money;
    } // numeric
} // formatMoney

function mysqli_result($result,$row,$field=0) {
    if ($result===false) return false;
    if ($row>=mysqli_num_rows($result)) return false;
    if (is_string($field) && !(strpos($field,".")===false)) {
        $t_field=explode(".",$field);
        $field=-1;
        $t_fields=mysqli_fetch_fields($result);
        for ($id=0;$id<mysqli_num_fields($result);$id++) {
            if ($t_fields[$id]->table==$t_field[0] && $t_fields[$id]->name==$t_field[1]) {
                $field=$id;
                break;
            }
        }
        if ($field==-1) return false;
    }
    mysqli_data_seek($result,$row);
    $line=mysqli_fetch_array($result);
    return isset($line[$field])?$line[$field]:false;
}

// dimention records for all languages
function get_test_exam($userid) {
	global $connection;
	$query = "SELECT * FROM aca_user WHERE user_id = '{$userid}'";
	$records = mysqli_query($connection, $query);
	confirm_query($records);
	return $records;
}	

?>