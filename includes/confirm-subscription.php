<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/07/06
 * Time: 3:00 AM
 */
if(!isset($_SESSION["paygateDetails"])){
    echo "No paygate details";
}

if(isset($_SESSION["payeeDetails"]["paymethod"]) && $_SESSION["payeeDetails"]["paymethod"] == "paypal"){

    $querystring = '';

    // Firstly Append paypal account to querystring
    $querystring .= "?business=".urlencode($paypal_email)."&";

    // Append amount& currency (£) to quersytring so it cannot be edited in html

    //The item name and amount can be brought in dynamically by querying the $_POST['item_number'] variable.
    $querystring .= "item_name=".urlencode($_SESSION["transactionDetails"][0]["code"])."&";
    $querystring .= "amount=".formatMoney($_SESSION["transactionDetails"][0]["amount"],2)."&";

    //loop for posted values and append to querystring
    foreach($_POST as $key => $value){
        $value = urlencode(stripslashes($value));
        $querystring .= "$key=$value&";
    }

    // Append paypal return addresses
    $querystring .= "return=".urlencode(stripslashes($paypal_return))."&";
    $querystring .= "cancel_return=".urlencode(stripslashes($paypal_cancel))."&";
    $querystring .= "notify_url=".urlencode($paypal_notify);

    // Append querystring with custom field
    //$querystring .= "&custom=".USERID;

    // Redirect to paypal IPN
    //$posturl = 'https://www.sandbox.paypal.com/cgi-bin/webscr'.$querystring;
    $posturl = 'https://www.paypal.com/cgi-bin/webscr'.$querystring;
	$btntext = "Continue";
}

if($_SESSION["transactionDetails"][0]["amount"] < '1'){
	$sql = "Update aca_transaction set paymentcomplete = '1' where reference = '".$_SESSION["paygateDetails"]["reference"]."'";
	mysqli_query( $connection, $sql);
	
	$query = "update aca_user_exam set status='1' where user_id = '".$userx."' and exam_id = '".$_SESSION["payeeDetails"]["examid"]."'" ;
	$result = mysqli_query( $connection, $query);
						 
	$posturl = '?id=21&login=1&opt=0&key=0&set=0';
	$btntext = "Continue to Exams";
}
	
?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>Confirm Subscription</h2><div class="colored-line-left"></div></div>
<div class="clearfix"></div>
<div class="row content-pad">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

<form action="<?php echo $posturl ?>" method="post" name="paygate_process_form" id="confirmpaymentmethod">
    <table class="table" border="0" cellspacing="0" cellpadding="0">
        <thead>
        <tr>
            <th style="background:#58595b;color:#FFFFFF;">Payment and Invoice summary</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                To continue your subscription please confirm the details below.<br />
                <br /></p>
                <div class="form-group">
                    <label class="control-label col-sm-4">Invoice to:</label>
                    <div class="col-sm-4"><?php echo ($_SESSION["payeeDetails"]["invoiceto"] == "company" ? 'Company' : 'Individual' ); ?></div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Firstname:</label>
                    <div class="col-sm-4"><?php echo $_SESSION["payeeDetails"]["firstname"]; ?></div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Surname:</label>
                    <div class="col-sm-4"><?php echo $_SESSION["payeeDetails"]["surname"]; ?></div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">ID Number:</label>
                    <div class="col-sm-4"><?php echo $_SESSION["payeeDetails"]["company"]; ?></div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Postal Address:</label>
                    <div class="col-sm-4"><?php echo nl2br($_SESSION["payeeDetails"]["postal"]); ?></div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Country:</label>
                    <div class="col-sm-4"><?php echo $_SESSION["payeeDetails"]["country"]; ?></div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Email:</label>
                    <div class="col-sm-4"><?php echo $_SESSION["payeeDetails"]["email"]; ?></div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Contact Number:</label>
                    <div class="col-sm-4"><?php echo $_SESSION["payeeDetails"]["contactnumber"]; ?></div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Order Number:</label>
                    <div class="col-sm-4"><?php echo $_SESSION["payeeDetails"]["ordernumber"]; ?></div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">VAT Number:</label>
                    <div class="col-sm-4"><?php echo $_SESSION["payeeDetails"]["vatnumber"]; ?></div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Amount:</label>
                    <div class="col-sm-4"><?php echo $_SESSION["currencySymbol"].' '.formatMoney($_SESSION["payeeDetails"]["amount"],2); ?></div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Payment Method:</label>
                    <div class="col-sm-4"><?php

                        if(isset($_SESSION["payeeDetails"]["paymethod"]) && $_SESSION["payeeDetails"]["paymethod"] == "paypal"){
                            echo '<input type="hidden" name="confirmpaymethod" class="confirmpaymentmethod" value="paypal" />PayPal';
                        }

                        ?></div>
                    <div class="clearfix"></div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <p>&nbsp;</p>
		<table class="table table-striped">
                                 <tr>
                                    <th><strong>Description</strong></th>
                                    <th><strong>Cost</strong></th>
                                 </tr>
							<?php
								for($x = 0; $x < count($_SESSION["transactionDetails"]); $x++) {
								echo '<tr>
										<td> '.$_SESSION["transactionDetails"][$x]["description"].'</td>
										<td> '.$_SESSION["currencySymbol"].' '.formatMoney($_SESSION["transactionDetails"][$x]["amount"],2).' </td>
									</tr>';
                            }
							?>
                              </table>
		
    <?php

    if(isset($_SESSION["payeeDetails"]["paymethod"]) && $_SESSION["payeeDetails"]["paymethod"] == "paypal") {
    ?>


    <!-- Identify your business so that you can collect the payments. -->
    <input type="hidden" name="business" value="<?php echo $paypal_email; ?>">

    <!-- Specify a Buy Now button. -->
    <input type="hidden" name="cmd" value="_xclick">

    <!-- Specify details about the item that buyers will purchase. -->
    <input type="hidden" name="reference" value="<?php echo $_SESSION["paygateDetails"]["reference"]; ?>">
    <input type="hidden" name="item_name" value="<?php echo $_SESSION["transactionDetails"][0]["code"]; ?>">
    <input type="hidden" name="item_number" value="<?php echo $_SESSION["transactionDetails"][0]["examid"]; ?>">
    <input type="hidden" name="amount" value="<?php echo formatMoney($_SESSION["transactionDetails"][0]["amount"],2); ?>">
    <input type="hidden" name="currency_code" value="<?php echo $_SESSION["paygateDetails"]["currency"]; ?>">

    <!-- Specify URLs -->
    <input type='hidden' name='cancel_return' value='<?php echo $paypal_cancel; ?>'>
    <input type='hidden' name='return' value='<?php echo $paypal_return; ?>'>
    <input type='hidden' name='notify_url' value='<?php echo $paypal_notify; ?>'>



    <?php

    }



    ?>
    <div class="form-group text-center">
    <input class="btn btn-default" type="submit" name="btnSubmit" value="<?php echo $btntext; ?>" />
        </div>
<br>
</form>
    </div>
</div>
