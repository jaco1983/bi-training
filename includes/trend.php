
<?php	
// results id = 42


		// exam - key
		// tool / subject - cat6
		// version - cat2
		// user - cat3
		// create_date - test date - cat4
		
		
//
if ($id == 42) {
	
	if ($set == 0) {	
		// apply filters to the result se
		
		if ($cat <> 0) { // exam
			$c1 = $cat;
			$result_set1 = get_aca_exam_rec($cat);
			while ($row = mysqli_fetch_array($result_set1)) {
				$c1 = $row["code"];
			} 			 
		} else { $c1 = ""; }
		
		if ($cat6 <> 0) { // subject / tool
			$c6 = $cat6;
			$result_set6 = get_aca_tool_rec($cat6);
			while ($row = mysqli_fetch_array($result_set6)) {
				$c6 = $row["description"];
			} 
		} else { $c6 = ""; }
		
		if ($cat2 <> "0" || $cat2 <> 0) { // subject / tool
			$c2 = $cat2;
			//$result_set2 = get_aca_tool_rec($cat2);
			//while ($row = mysqli_fetch_array($result_set2)) {
			//	$c2 = $row["userthis"];
			//} 
		} else { $c2 = ""; }
		
		//if ($cat2 <> 0) { $c2 = $cat2; } else { $c2 = ""; } // version
		
		if ($cat3 <> 0) { // user name
			$c3 = $cat3;
			$result_set3 = get_aca_user_rec($cat3);
			while ($row = mysqli_fetch_array($result_set3)) {
				$c3 = $row["username"];
			} 
		} else { $c3 = ""; }
		
		if ($cat4 <> 0) { // create date 
			$c4 = $cat4; 
		} else { $c4 = ""; }
		
		if ($cat5 <> 0) { // create date 
			$c5 = $cat5; 
		} else { $c5 = ""; }
		
		// headers and filters  -------------------------------
		
		$filterExam = "<a href=\"?id=".$id."&set=0&opt=0&key=0&login=1&cat6=".$cat6."&cat5=".$cat5."&cat=0&cat7=".$cat7."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."\">[Exam]</a> ";
		$filterSubject = "<a href=\"?id=".$id."&set=0&opt=0&key=".$key."&cat=".$cat."&cat5=".$cat5."&cat6=0&login=1&cat2="."&cat7=".$cat7.$cat2."&cat3=".$cat3."&cat4=".$cat4."\">[Subject]</a> ";
		$filterVersion = "<a href=\"?id=".$id."&set=0&opt=0&key=".$key."&cat=".$cat."&cat5=".$cat5."&cat6=".$cat6."&cat7=".$cat7."&login=1&cat2=0&cat3=".$cat3."&cat4=".$cat4."\">[Version]</a> ";
		$filterUser = "<a href=\"?id=".$id."&set=0&opt=0&key=".$key."&cat=".$cat."&cat5=".$cat5."&cat6=".$cat6."&cat7=".$cat7."&login=1&cat2=".$cat2."&cat3=0&cat4=".$cat4."\">[User]</a> ";
		$filterTestDate = "<a href=\"?id=".$id."&set=0&opt=0&key=".$key."&cat=".$cat."&cat5=".$cat5."&cat6=".$cat6."&cat7=".$cat7."&login=1&cat2=".$cat2."&cat3=".$cat3."&cat4=0\">[Test Date]</a> ";
		$filterAll = "<a href=\"?id=".$id."&set=0&opt=0&key=".$key."&cat=".$cat."&cat5=0&cat6=0&login=1&cat2=0&cat7=0&cat3=0&cat4=0\">[All]";
		$filterInstructor = "<a href=\"?id=".$id."&set=0&opt=0&key=".$key."&cat=".$cat."&cat5=0&cat6=".$cat6."&cat7=".$cat7."&login=1&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."\">[Instructor]</a> ";
		
		// instructor list
		$inst = "";
		$result_set = get_aca_instructor_list(0, 10, 1);
		while ($row = mysqli_fetch_array($result_set)) {		
			$instructor_id = $row["instructor_id"];
			$name = $row["name"];
			$email = $row["email"];		
			$status = $row["status"];
			$d_status = $status;
			$inst .= "&nbsp;<a href=\"?id=".$id."&set=0&opt=0&key=".$key."&cat=".$cat."&cat5=".$cat5."&cat6=".$cat6."&cat7=".$instructor_id."&login=1&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."\">[ ".$name." ]</a>&nbsp;";
		}
				
		echo "<div class=\"container\">";
			echo "<h2>Test Results</h2>";
			//echo "<p>Filter Exam and Subjects and select a Question to maintain: ". $c1 . " " .$join. " ".$c6."</p> ";           
	
		echo "<div class=\"panel panel-primary\"> "; 
		  echo "<div class=\"panel-heading\">Filters (Click to reset)</div> "; 
		  echo "<div class=\"panel-body\">";
		  	
		echo "<table class=\"table\">";
		echo "<thead>";
		echo "<tr>";
			echo "<th>".$filterExam."</th>";
			echo "<th>".$filterSubject."</th>";
			echo "<th>".$filterVersion."</th>";
			echo "<th>".$filterUser."</th>";
			echo "<th>".$filterTestDate."</th>";
			echo "<th>".$filterAll."</th>";
		echo "</tr>";		
		echo "</thead>";
		echo "<tbody>";
		echo "<tr>";
			echo "<td>".$c1."</td>";
			echo "<td>".$c6."</td>";
			echo "<td>".$c2."</td>";
			echo "<td>".$c3."</td>";
			echo "<td>".$c4."</td>";
			echo "<td></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td colspan=\"6\">".$filterInstructor."&nbsp;&nbsp;&nbsp;".$inst."</td>";
			echo "<td></td>";
		echo "</tr>";
		echo "</tbody>";
		echo "</table>";
		
		
		  echo "</div> "; 
	   echo " </div> "; 	
		
		// --- end of Filters -----------
		// print menu options - top 10 incorrect answers
		
		$butTopTenIncorrect = "<button name=\"TopBut\" type=\"button\" value=\"Top 10 incorrect answers\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&set=0&opt=5&key=".$key."&cat=".$cat."&login=1&cat6=".$cat6."&cat7=".$cat7."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."');return document.MM_returnValue\"/>Top 10 incorrect answers</button>";
		$butQuestionsErrors = "<button name=\"TopBut\" type=\"button\" value=\"Questions with Errors\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&set=0&opt=6&key=".$key."&cat=".$cat."&login=1&cat6=".$cat6."&cat7=".$cat7."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."');return document.MM_returnValue\"/>Questions with Errors</button>";
		
		

		// --- Summary by Subject ------------------------
		
		echo "<div class=\"panel panel-primary\"> "; 
		  echo "<div class=\"panel-heading\">Question Trend</div> "; 
		  echo "<div class=\"panel-body\">";
		  	
	//	echo "<table class=\"\">";
	//	echo "<tbody>";
			//echo "<td align=\"center\" valign=\"bottom\">Date: </br>Questions</br>%</td>";
	//		echo "<td>";
		// get data
		$tq = 0;
		$ts = 0;
		$tp = 0;
		$barh = 0;
		$style_row = "progress-bar-striped";
		$pb = "progress-bar progress-bar-success " . $style_row;
		// get data	
		$result_set = get_aca_option_results_trend($key, $cat6, $cat2, $cat3, $cat4, $cat5);
		while ($row = mysqli_fetch_array($result_set)) {		
			$date = $row["date"];
			$da = substr($date,5,2)."/".substr($date,7,5);
			$tq = $row["questions"];		
			$ts = $row["score"];
			$tp = ($ts / $tq)*100;
			if ($tp < 50) { $pb = "progress-bar progress-bar-danger " . $style_row; }
			if ($tp >= 50 && $tp < 60) { $pb = "progress-bar progress-bar-warning " . $style_row; }
			if ($tp >= 60) { $pb = "progress-bar progress-bar-success " . $style_row; }
			echo "<div class=\"progress\">";
  				echo "&nbsp;" . $date . " <div class=\"".$pb."\" role=\"progressbar\"
  aria-valuenow=\"".$tp."\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:".$tp."%\">".number_format($tp,0,",",",")."% (".$tq.")";
  				echo "</div>";
			echo "</div>";
		}
		  echo "</div> "; 
	   echo " </div> "; 	
	
	// ------ end Summary by Subject ------------------------------

		
	// Test
	if ($test_aca == 1) {
		echo "Test before question form</br>";
		echo "Set: ".$set. "</br>";
		echo "Q: ".$q. "</br>";
		//echo "Qno: ".$aca_qno. "</br>";
		//echo "Q Tot: ".$q_tot. "</br>";
		//echo "Q Cnt: ".$q_count. "</br>";
		echo "Login: ".$login. "</br>";
		echo "Cat: ".$cat. "</br>";
		echo "Cat2: ".$cat2. "</br>";
		echo "Cat3: ".$cat3. "</br>";
		echo "Cat4: ".$cat4. "</br>";
		echo "Cat5: ".$cat5. "</br>";
		echo "Cat6: ".$cat6. "</br>";
		echo "Cat7: ".$cat7. "</br>";
	}
		
	// ------- Summary by Test -----------------------
		echo "<div class=\"panel panel-primary\"> "; 
		  echo "<div class=\"panel-heading\">Summary by Test</div> "; 
		  echo "<div class=\"panel-body\">";
		  	
		echo "<table class=\"table table-striped\">";
		echo "<thead>";
		echo "<tr>";
			echo "<th>User</th>";
			echo "<th>Name</th>";
			echo "<th>Exam</th>";
			echo "<th>Version</th>";
			echo "<th>Date</th>";
			echo "<th>Questions</th>";
			echo "<th>Correct</th>";
			echo "<th>%</th>";
		echo "</tr>";		
		echo "</thead>";
		echo "<tbody>";
				// get data
		$tt_qtot = 0;
		$tt_score = 0;
		$tt_ps = 0;	
		$result_set = get_aca_option_results_instructor($cat, $cat6, $cat2, $cat3, $cat4, $cat5);
		while ($row = mysqli_fetch_array($result_set)) {		
			$user = $row["username"];			
			$name = $row["name"];		
			$creator_id = $row["creator_id"];		
			$exam_id = $row["exam_id"];
			$ver = trim($row["userthis"]);	
			$cdate = $row["create_date"];	
			$qtot = $row["questions"];		
			$score = $row["score"];
			$ps = ($score / $qtot)*100;
			$tt_qtot = $tt_qtot + $qtot;
			$tt_score = $tt_score + $score;
			$result_setXX = get_aca_exam_rec($exam_id);
			while ($row = mysqli_fetch_array($result_setXX)) {
				$exam_code = $row["code"];
			} 
			
			$resultUser = "<a href=\"?id=".$id."&set=0&opt=0&login=1&key=".$key."&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=".$creator_id."&cat4=".$cat4."&cat5=".$cat5."\">".$user."</a>";
			$resultName = "<a href=\"?id=".$id."&set=0&opt=0&login=1&key=".$key."&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=".$creator_id."&cat4=".$cat4."&cat5=".$cat5."\">" .$name. "</a>";
			$resultExam = "<a href=\"?id=".$id."&set=0&opt=0&login=1&cat=".$exam_id."&cat6=".$cat6."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$exam_code."</a>";
			$resultVersion = "<a href=\"?id=".$id."&set=0&opt=0&login=1&key=".$key."&cat=".$cat."&cat6=".$cat6."&cat2=".$ver."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$ver."</a>";
			$resultDate = "<a href=\"?id=".$id."&set=0&opt=0&login=1&key=".$key."&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cdate."&cat5=".$cat5."\">".$cdate."</a>";	 
		echo "<tr>";
			echo "<td>".$resultUser."</td>";
			echo "<td>".$resultName."</td>";
			echo "<td>".$resultExam."</td>";
			echo "<td>".$resultVersion."</td>";
			echo "<td>".$resultDate."</td>";
			echo "<td>".$qtot."</td>";
			echo "<td>".$score."</td>";
			echo "<td>".number_format($ps,0,",",",")."%</td>";
		echo "</tr>";
		}	
		$tt_ps = ($tt_score / $tt_qtot)*100;
		echo "<tr>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td>".$tt_qtot."</td>";
			echo "<td>".$tt_score."</td>";
			echo "<td>".number_format($tt_ps,0,",",",")."%</td>";
		echo "</tr>";
		echo "</tbody>";
		echo "</table>";
		  echo "</div> "; 
	   echo " </div> "; 	
	
	// ------ end Summary by Test ------------------------------

		  
	} // end of set = 0

} // end of id = 42
?>
 