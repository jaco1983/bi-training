<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/09/28
 * Time: 12:41 AM
 */

echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>FAQ Maintenance</h2><div class="colored-line-left"></div></div>
          <div class="clearfix"></div>
          <div class="row content-pad">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

if($set == 2){

    $sql = "select * from aca_faq where id = '".$key."'";

    $result = mysqli_query($connection, $sql);

    while ($row = mysqli_fetch_array($result)) {
        echo '<table class="table table-striped" width="100%">
                    <thead>
                    <tr><th colspan="2" style="background:#58595b;color:#FFFFFF;line-height:2em;">Edit FAQ</th></tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Question</td>
                        <td>
                            <input type="hidden" name="faqid reqd" id="faqeid" class="form-control" value="'.$row["id"].'">
                            <input type="text" name="faqq reqd" id="faqeq" class="form-control" value="'.$row["faqq"].'">
                        </td>
                    </tr>
                    <tr>
                        <td>Answer</td>
                        <td>
                            <textarea name="faqa reqd" id="faqea" class="form-control" cols="40" rows="5">"'.$row["faqa"].'"</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Order</td>
                        <td>
                            <input type="text" name="faqo" id="faqeo" class="form-control" value="'.$row["faqo"].'">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center"><input type="submit" name="faqsubmit" id="faqesubmit" value="Save" class="btn btn-default" />&nbsp;<a href="?id=111&set=0" class="btn btn-default">Cancel</a></td>
                    </tr>
                    </tbody>
                    </table>';
    }
}

if($set == 0) {
    echo '<table class="table table-striped" width="100%">
                    <thead>
                    <tr><th colspan="5" style="background:#58595b;color:#FFFFFF;line-height:2em;">List of FAQs<div style="float:right"><a href="?id=111&set=1" class="btn btn-primary">New</a></div></th></tr>
                    
                <tr>
                    <th>ID</th>
                    <th>Question</th>
                    <th>Answer</th>
                    <th>Ordering</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>';

    $sql = "select * from aca_faq order by id asc";

    $result = mysqli_query($connection, $sql);

    while ($row = mysqli_fetch_array($result)) {
        echo '<tr>
                    <td>'.$row["id"].'</td>
                    <td>'.$row["faqq"].'</td>
                    <td>'.$row["faqa"].'</td>
                    <td align="center">'.$row["faqo"].'</td>
                    <td><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=111&set=2&key='.$row["id"].'\');return document.MM_returnValue" class="btn btn-success">Edit</a></td>
                </tr>';
    }

    echo '</tbody></table>';
}

if($set == 1){
    echo '<table class="table table-striped" width="100%">
                    <thead>
                    <tr><th colspan="2" style="background:#58595b;color:#FFFFFF;line-height:2em;">Add FAQ</th></tr>
                    </thead>
                    <tbody>
                    
                <tr>
                    <td>Question</td>
                    <td>
                        <input type="text" name="faqq reqd" id="faqq" class="form-control">
                    </td>
                </tr>
            <tr>
                <td>Answer</td>
                <td>
                    <textarea name="faqa reqd" id="faqa" class="form-control" cols="40" rows="5"></textarea>
                </td>
            </tr>
            <tr>
                <td>Order</td>
                <td>
                    <input type="text" name="faqo" id="faqo" class="form-control">
                </td>
            </tr>
            <tr>
                <td colspan="2" class="text-center"><input type="submit" name="faqsubmit" id="faqsubmit" value="Save" class="btn btn-default" />&nbsp;<a href="?id=111&set=0" class="btn btn-default">Cancel</a></td>
            </tr>
            </tbody>
            </table>';
}
echo '</div>
        </div>
        <script>
            $(document).on("click","#faqsubmit",function(e){
                var bool = 0;

                if ($("#faqq").val().length < 3) {
                    $("#faqq").removeClass("alert-success").addClass("alert-danger");
                    $("#faqq").effect("shake", {times: 3}, 50);
                    bool++;
                } else {
                    $("#faqq").removeClass("alert-danger").addClass("alert-success");
                }

                if ($("#faqa").val().length < 3) {
                    $("#faqa").removeClass("alert-success").addClass("alert-danger");
                    $("#faqa").effect("shake", {times: 3}, 50);
                    bool++;
                } else {
                    $("#faqa").removeClass("alert-danger").addClass("alert-success");
                }

                if(bool == 0){

                    var faqq = $("#faqq").val();
                    var faqa = $("#faqa").val();
                    var faqo = $("#faqo").val();

                    $.post( "ajax/ajax-faq.php", { action:"SaveFAQ",faqq:faqq,faqa:faqa,faqo:faqo
                     }, function(data){
                     if(data.result == 1){
                        var $elm = $(data.html);
                        $(".main-content").prepend($elm);
                        setTimeout(function() {
                            $elm.remove();
                        }, 3000);
                        window.location.href = "?id=111&set=0";
                     }
                     },
                     "json");
                }


            });

            $(document).on("click","#faqesubmit",function(e){
                var bool = 0;

                if ($("#faqeq").val().length < 3) {
                    $("#faqeq").removeClass("alert-success").addClass("alert-danger");
                    $("#faqeq").effect("shake", {times: 3}, 50);
                    bool++;
                } else {
                    $("#faqeq").removeClass("alert-danger").addClass("alert-success");
                }

                if ($("#faqea").val().length < 3) {
                    $("#faqea").removeClass("alert-success").addClass("alert-danger");
                    $("#faqea").effect("shake", {times: 3}, 50);
                    bool++;
                } else {
                    $("#faqea").removeClass("alert-danger").addClass("alert-success");
                }

                if(bool == 0){

                    var faqid = $("#faqeid").val();
                    var faqq = $("#faqeq").val();
                    var faqa = $("#faqea").val();
                    var faqo = $("#faqeo").val();

                        $.post( "ajax/ajax-faq.php", { action:"UpdateFAQ",faqid:faqid,faqq:faqq,faqa:faqa,faqo:faqo
                     }, function(data){
                     if(data.result == 1){
                        var $elm = $(data.html);
                        $(".main-content").prepend($elm);
                        setTimeout(function() {
                            $elm.remove();
                        }, 3000);
                        window.location.href = "?id=111&set=0";
                     }
                     },
                     "json");
                }


            });
        </script>';