<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/08/23
 * Time: 8:45 AM
 */

echo 'Identify your strengths: Before writing a certification first know your strengths. If you want to write a certification in a particular area, you should know whether you have worked in any of those areas and gained any functional knowledge / domain knowledge / end user experience / configuration experience / coding experience / training etc. Note that everybody cannot get all the opportunities in all the above areas. But, you should able to Identify your strengths with respect to these areas, then decide which Certification you are required to attempt.

Understand SAP Certification Policies: Unfortunately, SAP is having different type of policies with reference to SAP Certification in different countries. I had to travel from India to Malaysia in order to write my SAP BW Certification . Dictionary meaning says Policy means, the rules are same for every one. But, unfortunately, SAP Education policy change from country to country, person to person and organisation to organisation. They are very complex, layman like me cannot understand . First contact your local SAP Education Team and understand what is their policy applicable for your case and get a written confirmation that you are eligible to write the certification. Sometimes, it may so happen that you are eligible to write SAP Certification on day 1 and surprisingly may not be eligible on day 2.

Formal Training: If you are really new to an area, but intended to write SAP Certification, you need to attend the formal training. In some countries, SAP says, you must get trained from SAP Education / SAP Authorised Training Partner. But, in other countries, you can register yourself and write the exam. If you are fortunate enough to afford the money, then, it is recommended to get some sort of formal training. There may be some exceptional people who has written some Certifications without formal training. But, please note all the certification you cannot adopt the same approach. Some of the Certifications really test your ability and conceptual knowledge. Please note that unless you are formally trained, you cannot reach the level of passing the Certification Exam. If you pass such certifications without having any knowledge and training, then you are a star

Plan in advance: You must plan in advance. At least couple of months are really required from the date of start of your formal training and attempting the certification. If you are privileged to get a formal training from SAP, you would get lot of new concepts every day and will have less time to practice and study, then your next day starts for new movie . Therefore, hardly you get any time to master the things. Since, you have gone through every page in detail, probably, you may in a better position position to attempt the examination.

Exercises: At the end of the each chapter, there used to be exercises, but do not worry if you are not able to complete them. In my whole life, I could not complete all the exercises in a book (little incompetent !!! LOL ). Please note that the exercises are given only to get clarity on the theoretical and conceptual knowledge. They are not intended to make you master or test your hands on exercises. Therefore, do not put much stress even if you cannot complete them.

After Training - Before Examination: I would say this is the real crucial stage. During the training period, you never had chance to revise anything. But, this is the period where you will really put the concepts into your mind.

    Read each Certification Curriculum book for not less than FIVE times, very RELIGIOUSLY, before going to examination. First time, it would take longer period, but the more number of times you read, the time would be reduced. You must keep a notes and note down the important points. Meanwhile, you also highlight any important points with marker and note some questions with pencil. But, all my Certification books are filled with different marker colours, so could not read anything
    Give more importance to chapters with +++ or ++ ratings in the syllabus. You may expect more number of questions out of these chapters.
    You are not advised to memorise the things, but, try to read in order to understand the concepts. Because, in certification, SAP is trying to test your conceptual knowledge, but not your memory.
    Try to answer the questions given at the end of the each chapter.
    Practice number of questions during this period. There are number of yahoo / google groups, where you can find number of sample questions. Try to attempt them. Do not worry, if you are only able to correct few in your first attempt. Even me used to get few correct answers only. If it is wrong, understand why it is wrong, if it is correct, why it is correct, this is the only way, you can clear the conceptual understanding, rather than blindly remembering the answers.
    There are number of pictures / graphs that are being given in the Certification material. Please do not look them as nice looking coloured pictures. Try to analyse them in functional / technical perspective. Hopefully, you will find lot of meaning from them. Then read the underlying text in conjunction with those pictures. You will see much and better confidence view.
    If you have any time try to go through help.sap.com, where the concepts are written in crystal clear language. If you find any time and also go through some of the threads on SDN, that would be a bonus.

Day before examination:

    Know the address of your examination center. Otherwise, you may get panic in searching for the center tomorrow morning
    Go to bed little early.
    Do not read too much the whole night, it may not help, moreover, you may be sleepy in the examination.

On the day of examination:

    Get up little early and have relaxed bath, take adequate breakfast.
    See if you have time and can rush through pages, but not spend too much time here, you will forget going to exam.
    Ensure that you have identity card (passport / driving license) or admission ticket, pen / pencil etc.
    Ensure that you reach half an hour in advance to the examination time. One time, I could not reach the examination center due to traffic and I am forced to reschedule my examination.
    Whoever be the person, there will be definitely some tension boiling inside. But, try to keep yourself as much relaxed as possible. Just think, even if you do not pass, it is not the end of the world, still there is a tomorrow. Be calm.

In the Examination Hall:

Before coming to the examination, you must have enquired the pattern of the examination. In earlier pattern, you can still get partial credit for partially correct questions. However, for new pattern, SAP only will award  marks if all your answers / sub answers are correct in a question. Even if you make one sub-question incorrect, you will not get any marks for the total question. Therefore, your concepts must be crystal clear, you cannot take chances to put lotteries in the examination center , whereas in old pattern, many people used to put lotteries for unknown questions (including me ), as you were not going to loose anything. No negative marking is there in either current or old pattern of valuation.

    Ensure you have completed all the formalities, like filling the form containing the name, contact details, address (where you should get your certificate) etc.
    Use elimination technique - First try to identify the wrong questions, which you can identify very easily, then finally you will reach the right answers.
    Concentrate on Single Answered Questions - For certain questions are called single answered questions, which will have radio buttons, only one question will is the correct answer. I heard that they have higher weightage, but not sure. Ensure that you reach the correct answer by using elimination technique.
    Try to attempt the easy questions first - Try to attempt the easy questions first, then this will give you confidence and keep you in better frame of mind.
    Mark the questions - If you cannot answer the question completely, then put them as "Marked question", then at the end you can easily identify such questions and complete the answers. Even if you do not put as "Marked Questions", still you can come back and complete them, it would waste your valuable time.
    Take few minutes break - Go to wash room and come back, your mind may be recharged.
    Do not worry much about the time - You should usually get ample time. Every time, I used to get time to go through all the questions 3 to 4 times. Therefore, in my opinion, time will not be a constraint, unless somebody spends more than an hour on a single question
    Give last ten minutes for re-look at all questions - At this point of time, you do not have the time to read the question, only look at the question with your eyes and ensure that you have answered all questions. So, go through in this fashion for all the questions.
    Relax - If you are confident, then you can click submit button, if you are shivering, just sit, relax and close your eyes, the system will take your session and issue the results. If you pass you will see Congratulations, otherwise, you will see your failed message. You can note down the marks obtained chapter wise on a plain paper.
    Even if you pass or fail, do not much worry about it, calm / relax and come out of the examination hall.

After the Examination:

    You should get your hard copy of the certificate within 4 - 6 weeks of time from the date of your Certification.
    If not contact your Local / Global SAP Education Team, they should be able to help you out.
    Contact SAP Education Team for password of your S ID. This you can use in order visit Service Market Place (https://service.sap.com). Do not share this User ID and Password with others.
    Contact SAP Education Team for your Certification Logo, which you can use on your CV.
    Earlier SAP used to send S ID cards. I got for my three certifications, but not for other three. But, I do not know where I misplaced';