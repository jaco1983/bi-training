<?php	
// help  id = 99

if ($id == 99) {
// display help text		
	
	echo "<div class=\"alert alert-info\">";
  	echo "<strong>Student</strong> Indicates a registered user of the application.";
	echo "</div>";
	
	echo "<div class=\"alert alert-info\">";
  	echo "<strong>Student Type</strong> Individual or Class.";
	echo "</div>";	
	
	echo "<div class=\"alert alert-info\">";
  	echo "<strong>Class</strong> A grouping of Students. A class will have an expiry date and an instructor.";
	echo "</div>";	
	
	echo "<div class=\"alert alert-info\">";
  	echo "<strong>Class-Exam</strong> The relationship between a class and an Exam.";
	echo "</div>";	
	
	echo "<div class=\"alert alert-info\">";
  	echo "<strong>Class-Exam</strong> The relationship between a class and an Exam.";
	echo "</div>";
	
	echo "<div class=\"alert alert-info\">";
  	echo "<strong>Exam</strong> Indicates an Exam that could be assigned to a user or a class.";
	echo "</div>";
	
	echo "<div class=\"alert alert-info\">";
  	echo "<strong>Question</strong> Indicates a question and answer that is linked to an Exam and categorised in Subjects.";
	echo "</div>";
		
	echo "<div class=\"alert alert-info\">";
  	echo "<strong>Test</strong> Indicates a random number of questions for a specific Exam and could be limited to a Subject.";
	echo "</div>";
		
	echo "<div class=\"alert alert-info\">";
  	echo "<strong>Option</strong> Indicates a recorded question for a Test or practice.";
	echo "</div>";
	
	
	
	
} // end of user id = 99
?>
 