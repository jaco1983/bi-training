<?php
// Database variables
/*$host = "197.242.70.201"; //database location
$user = "test"; //database username
$pass = "k1llsw1tch"; //database password
$db_name = "test_paypal"; //database name

ini_set("log_errors", 1);
ini_set("error_log", "php-error.log");
error_log( $reference );*/
// PayPal settings

$return_url = $paypal_return;
$cancel_url = $paypal_cancel;
$notify_url = $paypal_notify;

$item_name = $_SESSION["transactionDetails"][0]["code"];;
$item_amount = formatMoney($_SESSION["transactionDetails"][0]["amount"],2);;

// Include Functions
include("functions.php");

// Check if paypal request or response
if (!isset($_POST["txn_id"]) && !isset($_POST["txn_type"])){
    $querystring = '';

    // Firstly Append paypal account to querystring
    $querystring .= "?business=".urlencode($paypal_email)."&";

    // Append amount& currency (£) to quersytring so it cannot be edited in html

    //The item name and amount can be brought in dynamically by querying the $_POST['item_number'] variable.
    $querystring .= "item_name=".urlencode($item_name)."&";
    $querystring .= "amount=".urlencode($item_amount)."&";

    //loop for posted values and append to querystring
    foreach($_POST as $key => $value){
        $value = urlencode(stripslashes($value));
        $querystring .= "$key=$value&";
    }

    // Append paypal return addresses
    $querystring .= "return=".urlencode(stripslashes($return_url))."&";
    $querystring .= "cancel_return=".urlencode(stripslashes($cancel_url))."&";
    $querystring .= "notify_url=".urlencode($notify_url);

    // Append querystring with custom field
    //$querystring .= "&custom=".USERID;

    // Redirect to paypal IPN
    header('location:https://www.sandbox.paypal.com/cgi-bin/webscr'.$querystring);
    exit();
} else {
    // Response from Paypal

    // read the post from PayPal system and add 'cmd'
    $req = 'cmd=_notify-validate';
    foreach ($_POST as $key => $value) {
        $value = urlencode(stripslashes($value));
        $value = preg_replace('/(.*[^%^0^D])(%0A)(.*)/i','${1}%0D%0A${3}',$value);// IPN fix
        $req .= "&$key=$value";
    }

    // assign posted variables to local variables
    $data['item_name']			= $_POST['item_name'];
    $data['item_number'] 		= $_POST['item_number'];
    $data['payment_status'] 	= $_POST['payment_status'];
    $data['payment_amount'] 	= $_POST['mc_gross'];
    $data['payment_currency']	= $_POST['mc_currency'];
    $data['txn_id']				= $_POST['txn_id'];
    $data['receiver_email'] 	= $_POST['receiver_email'];
    $data['payer_email'] 		= $_POST['payer_email'];
    $data['custom'] 			= $_POST['custom'];

    var_dump($data);
    // post back to PayPal system to validate
    $header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
    $header .= "Host: www.sandbox.paypal.com\r\n";
    $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
    $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";

    $fp = @fsockopen ('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);


}
?>
