<?php	
// user id = 33

if ($id == 33) {

    echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>Instructor Maintenance</h2><div class="colored-line-left"></div>
<div class="clearfix"></div>
<div class="row content-pad">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

	if ($set == 2) { // insert				
		if (isset($_POST['submit'])) {
			$name = trim(mysql_prep($_POST['name']));
			$email = trim(mysql_prep($_POST['email']));
			$user_id = trim(mysql_prep($_POST['user_id']));
			$creator_id = $userx;
			$status = '1';	
		}
		
		if ($test_aca == 1) {
			$msg = "set 2 Post opt=".$opt."</br>";
			$msg .= "creator_id: ".$userx."</br>";
			echo $msg;
		}

		$query = "INSERT INTO aca_instructor( ";
		$query .= " name, email, user_id, "; 
		$query .= " status, creator_id ) ";
		$query .= " VALUES (   ";
		$query .= " '{$name}', '{$email}', '{$user_id}', ";  
		$query .= " '{$status}', {$userx} ) " ;
	
		$result = mysqli_query( $connection, $query);
		if (mysqli_affected_rows($connection) == 1) {	
			// Success	
			echo "<div class=\"alert alert-success\">";
				echo "Record Created";
			echo "</div>";
			$set = 0; // set action back to listing
			} else {
			echo "<div class=\"alert alert-danger\">";
				echo "Record creation failed";
			echo "</div>";
		}
		$set = 0; // set action back to form
	//} // end opt = 1
		
	} // end of set = 2
	


	if ($set == 4 && $key <> 0) { // update record													
		if (isset($_POST['submit'])) {
			$name = trim(mysql_prep($_POST['name']));
			$email = trim(mysql_prep($_POST['email']));
			$user_id = trim(mysql_prep($_POST['user_id']));
			$status = trim(mysql_prep($_POST['status']));
			$creator_id = $userx;
		}
		
			
			if ($test_aca == 1) {
				$msg = "set 2 Post</br>";
				$msg .= "status: ".$status."</br>";
				$msg .= "creator_id: ".$userx."</br>";
				//echo $msg;
			}
			
			// Update
			$query = "UPDATE aca_instructor SET ";
			$query .= " name = '{$name}', ";
			$query .= " email = '{$email}', ";
			$query .= " user_id = '{$user_id}', ";
			$query .= " status = '{$status}', ";
			$query .= " creator_id = '{$userx}' ";
			$query .= " WHERE instructor_id = {$key} ";
			$query .= " LIMIT 1 ";
		
			$result = mysqli_query( $connection, $query);
			if (mysqli_affected_rows($connection) == 1) {	
				// Success	
				echo "<div class=\"alert alert-success\">";
					echo "The record was updated successfully.";
				echo "</div>";
				$set = 0; // set action back to listing
				} else {
				echo "<div class=\"alert alert-danger\">";
					echo "The record was not updated. Please try again. Make sure that all fields are completed.";
				echo "</div>";
				$set = 3; // set action back to form
			}
				
	} // end of set = 4 update




	if ($set == 3) { // edit
			/*echo "<div class=\"panel panel-primary\"> ";
		  echo "<div class=\"panel-heading\">Instructor Maintenance</div> "; 
		  echo "<div class=\"panel-body\">";*/
				
		$result_set = get_aca_instructor_rec($key);
		while ($row = mysqli_fetch_array($result_set)) {		
			$key = $row["instructor_id"];
			$name = $row["name"];
			$email = $row["email"];		
			$user_id = $row["user_id"];	
			$status = $row["status"];
			$user_name = "";
			$result_setI = get_aca_user_rec($user_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$user_name = $row["name"];		
			}
			$d_status = $status;
				$result_set74 = get_dim("status", $status);
				while ($row = mysqli_fetch_array($result_set74)) {
					$dim_key = $row["dim_key"]; 
					$d_status = $row["description"];
				}
		}
		
		echo "<form name=\"aca_instructor\" method=\"post\" action=\"?id=".$id."&set=4&opt=" . $opt . "&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat5=".$cat5." \"enctype=\"multipart/form-data\">";

        echo "<table class=\"table table-striped\">";
        echo "<thead>";
        echo "<tr>";
        echo "<th colspan=\"2\" style=\"background:#58595b;color:#FFFFFF;\">Edit Instructor</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
		echo "<tr>";
		  echo "<td>Name:</td>";
		  echo "<td><input type=\"text\" name=\"name\" class=\"form-control reqd\" id=\"name\" placeholder=\"Enter name\" value=\"".$name."\"></td>";
		echo "</tr>";
		echo "<tr>";
		  echo "<td>Email:</td>";
		  echo "<td><input type=\"email\" name=\"email\" class=\"form-control reqd\" id=\"email\" placeholder=\"Enter Email\" value=\"".$email."\"></td>";
		echo "</tr>";
		
		echo "<tr>";
		  echo "<td>User:</td>";
			echo "<td><select name=\"user_id\" class=\"form-control reqd\" id=\"user_id\">";
						  echo "<option value=\"".$user_id."\">".$user_name." (".$user_id.")</option>";
								$result_set211 = get_aca_user_list_access(5, 1); // get_aca_user_list_access($access, $status)
								while ($row = mysqli_fetch_array($result_set211)) {
									$dim_key = $row["user_id"]; 
									$display_name = $row["name"];
									$display_email = $row["email"];
									echo "<option value=\"" . $dim_key . "\">" . $display_name . " (" . $dim_key . " / ".$display_email.")</option>";
								}
			echo "</select></td>";
		echo "</tr>";
		
		echo "<tr>";
		  echo "<td>Status</td>";
			echo "<td><select name=\"status\" class=\"form-control reqd\" onchange=\"showTool(this.value)\" id=\"status\">";
						  echo "<option value=\"".$status."\">".$d_status."</option>";
								  $result_set9 = get_dim_list("status");
								  while ($row = mysqli_fetch_array($result_set9)) {
									  $dim_key = $row["dim_key"]; 
									  $display_name = $row["description"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }
								  echo "</select></td>";
		echo "</tr>";
										
			// buttons
			$update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Submit</button>";			
			$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";
			
		  echo "<tr><td colspan=\"2\" class=\"text-center\">". $update . " " . $cancel . "</td></tr>";
		  echo "</tbody>";
		  echo "</table>";

		echo "</form>";			

		  /*echo "</div> ";
	   echo " </div> "; 	
		
		
		echo "<p>&nbsp;</p>";*/
		
	} // end of edit




	if ($set == 0) {										

		// get record count
		$p_records = 0;
		$result_setA = get_aca_instructor_count(99, 0); // status 1 = valid, access 0 for all
		while ($row = mysqli_fetch_array($result_setA)) {
			$p_records = $row["record_count"]; 
		}
		$anchor = "x";
		$p_link = "?id=".$id."&opt=".$opt."&cat2=0&cat3=0&cat6=".$cat6."&key=".$key."&cat=".$cat."&cat4=".$cat4."&set=".$set;
		$p = page_nav_last($page, $page_break_d, $p_records, $p_link, $anchor);
		$n = "<a href=\"?id=".$id."&set=1&key=".$key."\">New</a>";
		$c = " ";
		$n =  "<button name=\"NewBut\" type=\"button\" value=\"New\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=1&opt=0&key=".$key."');return document.MM_returnValue\"/>New</button>&nbsp;&nbsp;";
		//echo "<p align=\"right\">" . $n." ".$p . "</p></br>";
		
		
		
		/*echo "<div class=\"panel panel-primary\"> ";
		  echo "<div class=\"panel-heading\">Subject Maintenance</div> "; 
		  echo "<div class=\"panel-body\">";*/
		  	
		echo "<table class=\"table table-striped\">";
		echo "<thead>";
		echo "<tr>";
		echo "<th colspan=\"6\" style=\"background:#58595b;color:#FFFFFF;line-height:2em;\">List of instructors<div style=\"float:right\">" . $n." ".$p . "</div> </th>";
		echo "</tr>";
		echo "<tr>";
			echo "<th>ID</th>";
			echo "<th>Description</th>";
			echo "<th>Exam</th>";
			echo "<th>Status</th>";
			echo "<th>User ID</th>";
			echo "<th></th>";
		echo "</tr>";		
		echo "</thead>";
		echo "<tbody>";
		// get data
		$npage = $page;
		$result_set = get_aca_instructor_list($npage, $page_break_f, 99);
		while ($row = mysqli_fetch_array($result_set)) {		
			$key = $row["instructor_id"];
			$name = $row["name"];
			$email = $row["email"];	
			$user_id = $row["user_id"];		
			$status = $row["status"];
			$d_status = $status;
				$result_set74 = get_dim("status", $status);
				while ($row = mysqli_fetch_array($result_set74)) {
					$dim_key = $row["dim_key"]; 
					$d_status = $row["description"];
				}
			
			$un = "<a href=\"?id=".$id."&set=3&opt=0&login=1&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&key=".$key."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$name."</a>";
			$e =  "<button name=\"EditBut\" type=\"button\" value=\"Edit\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat7=".$cat7."&cat=".$cat."&login=1&set=3&opt=0&key=".$key."');return document.MM_returnValue\"/>Edit</button>&nbsp;&nbsp;";
			
		echo "<tr>";
			echo "<td>".$key."</td>";
			echo "<td>".$un."</td>";
			echo "<td>".$email."</td>";
			echo "<td>".$d_status."</td>";
			echo "<td>".$user_id."</td>";
			echo "<td>".$e."</td>";
		echo "</tr>";
		}	
		echo "<tr>";
			echo "<td align=\"left\" colspan=\"4\">".$c."</td>"; // close, new, page nav
			echo "<td align=\"right\" colspan=\"4\">".$n."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;".$p."</td>";
		echo "</tr>";


		echo "</tbody>";
		echo "</table>";
		  /*echo "</div> ";
	   echo " </div> "; */
	
		
	} // end of set = 0




	if ($set == 1) { // create new		
		
		/*echo "<div class=\"panel panel-primary\"> ";
		  echo "<div class=\"panel-heading\">Instructor Maintenance - Add an instructor</div> "; 
		  echo "<div class=\"panel-body\">";*/
				
		
		echo "<form name=\"aca_instructor\" method=\"post\" action=\"?id=".$id."&set=2&opt=" . $opt . "&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat5=".$cat5." \"enctype=\"multipart/form-data\">";
        echo "<table class=\"table table-striped\">";
        echo "<thead>";
        echo "<tr>";
        echo "<th colspan=\"2\" style=\"background:#58595b;color:#FFFFFF;\">Add Instructor</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
		echo "<tr>";
		  echo "<td>Name:</td>";
		  echo "<td><input type=\"text\" name=\"name\" class=\"form-control reqd\" id=\"name\" placeholder=\"Enter name\" value=\"\"></td>";
		echo "</tr>";
		echo "<tr>";
		  echo "<td>Email:</td>";
		  echo "<td><input type=\"email\" name=\"email\" class=\"form-control reqd\" id=\"email\" placeholder=\"Enter Email\" value=\"\"></td>";
		echo "</tr>";
		
		echo "<tr>";
		  echo "<td>User:</td>";
			echo "<td><select name=\"user_id\" class=\"form-control reqd\" id=\"user_id\">";
						  echo "<option value=\"\"></option>";
								$result_set21 = get_aca_user_list_access(5, 1); // get_aca_user_list_access($access, $status)
								while ($row = mysqli_fetch_array($result_set21)) {
									$dim_key = $row["user_id"]; 
									$display_name = $row["name"];
									echo "<option value=\"" . $dim_key . "\">" . $display_name . " (" . $dim_key . " / ".$display_email.")</option>";
								}
			echo "</select></td>";
		echo "</tr>";

										
			// buttons
			$update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Submit</button>";			
			$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";
			
		  echo "<tr><td colspan=\"2\" class=\"text-center\">" . $update . " " . $cancel . "</td></tr>";
		  echo "</tbody>";
		  echo "</table>";

		echo "</form>";			
		
		
		
		
		  /*echo "</div> ";
	   echo " </div> "; 	
		
		
		echo "<p>&nbsp;</p>";*/
		
	} // end of set = 1


echo '</div>';
echo '</div>';
echo '</div>';


} // end of user id = 33
?>
 