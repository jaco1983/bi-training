<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>Invoice Configuration</h2><div class="colored-line-left"></div>
<div class="clearfix"></div>
<div class="row content-pad">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="invoiceconfig1">
            <table class="table table-striped">
                <tr>
                    <th colspan="2" style="background:#58595b;color:#FFFFFF;">Invoice Configuration</th>
                </tr>
                        <?php

                        $query = "select * from aca_invoice_config";

                        $result = mysqli_query( $connection, $query);

                        while ($row = mysqli_fetch_array($result)) {
                            echo '<tr>
                                <td><strong>Company Name:</strong></td>
                                <td>'.$row["company_name"].'</td>
                            </tr>
                            <tr>
                                <td><strong>VAT Number:</strong></td>
                                <td>'.$row["vat_number"].'</td>
                            </tr>
                            <tr>
                                <td><strong>Postal Address</strong></td>
                                <td>'.$row["postal_address1"].'</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>'.$row["postal_address2"].'</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>'.$row["postal_address3"].'</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>'.$row["postal_address4"].'</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="2"><input type="button" class="btn btn-default col-sm-2 col-sm-offset-5" onclick="editInvoiceConfig()" value="Edit" /></td>
                            </tr>';
                        }
                        ?>
                    </table>
        </div>
        <div id="invoiceconfig2">
            <table class="table table-striped">
                <tr>
                    <th colspan="2" style="background:#58595b;color:#FFFFFF;">Edit Invoice Configuration</th>
                </tr>
                        <?php

                        $query = "select * from aca_invoice_config";

                        $result = mysqli_query( $connection, $query);

                        while ($row = mysqli_fetch_array($result)) {
                            echo '<tr>
                                    <td><strong>Company Name:</strong></td>
                                    <td><input type="text" name="company_name" class="form-control" id="company_name" value="'.$row["company_name"].'" /></td>
                                </tr>
                                <tr>
                                    <td><strong>VAT Number:</strong></td>
                                    <td><input type="text" name="vat_number" class="form-control" id="vat_number" value="'.$row["vat_number"].'" /></td>
                                </tr>
                                <tr>
                                    <td><strong>Postal Address</strong></td>
                                    <td><input type="text" name="postal_address1" class="form-control" id="postal_address1"  value="'.$row["postal_address1"].'" /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><input type="text" name="postal_address2" class="form-control" id="postal_address2" value="'.$row["postal_address2"].'" /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><input type="text" name="postal_address3" class="form-control" id="postal_address3" value="'.$row["postal_address3"].'" /></td>
                                </tr>
                                 <tr>
                                    <td></td>
                                    <td><input type="text" name="postal_address4" class="form-control" id="postal_address4" value="'.$row["postal_address4"].'" /></td>
                                </tr>
                                <tr>
                                <td></td>
                                <td></td>
                            </tr>
                                <tr>
                                <td colspan="2"><input type="button" class="btn btn-default col-sm-2 col-sm-offset-5" onclick="saveInvoiceConfig()" value="Save" /></td>
                            </tr>';
                        }
                        ?>
                    </table>
        </div>
    </div>
</div>
</div>

<script>
    $(document).ready(function() {
        $("#invoiceconfig1").show();
        $("#invoiceconfig2").hide();
    })
</script>