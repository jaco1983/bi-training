<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/09/28
 * Time: 12:41 AM
 */

echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>Video Maintenance</h2><div class="colored-line-left"></div></div>
          <div class="clearfix"></div>
          <div class="row content-pad">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
if($set == 2){

    $sql = "select * from aca_video where id = '".$key."' limit 1";

    $result = mysqli_query($connection, $sql);

    while ($row = mysqli_fetch_array($result)) {
        echo '<form id="upload" method="post" action="" enctype="multipart/form-data" autocomplete="off">
                    <table class="table table-striped" width="100%">
                    <thead>
                    <tr><th colspan="2" style="background:#58595b;color:#FFFFFF;">Edit Videos</th></tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Video Title</td>
                        <td>
                            <input type="hidden" name="evid" id="evid" class="form-control" value="'.$row["id"].'">
                            <input type="text" name="evtitle" id="evtitle" class="form-control" value="'.$row["video_title"].'">
                        </td>
                    </tr>
                    <tr>
                        <td>Video Description</td>
                        <td>
                            <textarea name="evdesc" id="evdesc" class="form-control">'.$row["video_description"].'</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>MP4 Video</td>
                        <td>
                            <input type="file" name="file1[]" id="emp4file" accept=".mp4" value="'.$row["mp4_file"].'" />
                        </td>
                    </tr>
                    <tr>
                        <td>OGG Video</td>
                        <td>
                            <input type="file" name="file1[]" id="eoggfile" accept=".ogv" value="'.$row["ogv_file"].'" />
                        </td>
                    </tr>
                    <tr>
                        <td>WEBM Video</td>
                        <td>
                            <input type="file" name="file1[]" id="ewebmfile" accept=".webm" value="'.$row["webm_file"].'" />
                        </td>
                    </tr>
                    <tr>
                        <td>Active</td>
                        <td>
                            <input type="checkbox" name="evstatus" id="evstatus" '.($row["status"] == '1' ? 'checked' : '').' />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center"><input type="submit" name="evsubmit" id="evsubmit" value="Save" class="btn btn-default" /><img src="images/busy_icon_sm.gif" id="loading" style="display: none;" /></td>
                    </tr>
                    </tbody>
                    </table>
                </form>';
    }

}

if($set == 1){
    echo '<form id="upload" method="post" action="" enctype="multipart/form-data" autocomplete="off">
                <table class="table table-striped" width="100%">
                    <thead>
                    <tr><th colspan="2" style="background:#58595b;color:#FFFFFF;">Add Videos</th></tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Video Title</td>
                        <td>
                            <input type="text" name="vtitle" id="vtitle" class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td>Video Description</td>
                        <td>
                            <textarea name="vdesc" id="vdesc" class="form-control"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>MP4 Video</td>
                        <td>
                            <input type="file" name="file1[]" id="mp4file" accept=".mp4" />
                        </td>
                    </td>
                    <tr>
                        <td>OGG Video</td>
                        <td>
                            <input type="file" name="file1[]" id="oggfile" accept=".ogv" />
                        </td>
                    </tr>
                    <tr>
                        <td>WEBM Video</td>
                        <td>
                            <input type="file" name="file1[]" id="webmfile" accept=".webm" />
                        </td>
                    </tr>
                    <tr>
                        <td>Active</td>
                        <td>
                            <input type="checkbox" name="vstatus" id="vstatus" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center"><input type="submit" name="vsubmit" id="vsubmit" value="Save" class="btn btn-default" /><img src="images/busy_icon_sm.gif" id="loading" style="display: none;" /></td>
                    </tr>
                    </tbody>
                    </table>
                </form>';
}



                if($set == 0){

                    $sql = "select * from aca_video";

                    $result = mysqli_query($connection, $sql);

                    echo '<table class="table table-striped" width="100%">';

			echo '<thead>
            <tr><th colspan="7" style="background:#58595b;color:#FFFFFF;line-height:2em;">List of Videos<div style="float:right;"><a href="?id=110&set=1" class="btn btn-primary">New</a></div></th></tr>
			<tr>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>MP4</th>
                    <th>OGV</th>
                    <th>WEBM</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>';

                    $sql = "select * from aca_video order by id asc";

                    $result = mysqli_query($connection, $sql);

                    while ($row = mysqli_fetch_array($result)) {
                        echo '<tr>
                    <td>'.$row["id"].'</td>
                    <td>'.$row["video_title"].'</td>
                    <td>'.$row["video_description"].'</td>
                    <td align="center">'.($row["mp4_file"] != '' ? '<span style="color:#0f0" class="glyphicon glyphicon-ok"></span>' : '<span style="color:#f00" class="glyphicon glyphicon-remove"></span>').'</td>
                    <td align="center">'.($row["ogv_file"] != '' ? '<span style="color:#0f0" class="glyphicon glyphicon-ok"></span>' : '<span style="color:#f00" class="glyphicon glyphicon-remove"></span>').'</td>
                    <td align="center">'.($row["webm_file"] != '' ? '<span style="color:#0f0" class="glyphicon glyphicon-ok"></span>' : '<span style="color:#f00" class="glyphicon glyphicon-remove"></span>').'</td>
                    <td align="right"><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=110&set=2&key='.$row["id"].'\');return document.MM_returnValue" class="btn btn-success">Edit</a></td>
                </tr>';
                    }

                    echo '</tbody></table>
                          </div>';
                }
            echo '</div>
        </div>
        <script>

$(document).ajaxSend(function () {
      $("#loading").show();
      $("#vsubmit").hide();
  });

  // invoked when sending ajax completed
  $(document).ajaxComplete(function () {
      $("#loading").hide();
  });

        $(":file").filestyle({ buttonName:"btn-primary"});

        $("#vsubmit").click(function(e){
            e.stopPropagation(); // Stop stuff happening
            e.preventDefault(); // Totally stop stuff happening

            var data;

            data = new FormData();
            data.append( "file0", $( "#mp4file" )[0].files[0] );
            data.append( "file1", $( "#oggfile" )[0].files[0] );
            data.append( "file2", $( "#webmfile" )[0].files[0] );

            $.ajax({
                            url: "includes/video_upload.php?files",
                            type: "POST",
                            data: data,
                            cache: false,
                            dataType: "json",
                            processData: false,
                            contentType: false,
                            success: function(data, textStatus, jqXHR)
                            {
                                if(typeof data.error === "undefined")
                                {
                                    submitForm(e, data);
                                }
                                else
                                {
                                    console.log("ERRORS: " + data.error);
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown)
                            {
                                console.log("ERRORS: " + textStatus);
                            }
                        });

            function submitForm(event, data)
                    {
                        $form = $(event.target);

                        var formData = $form.serialize();

                        $.each(data.files, function(key, value)
                        {
                            formData = formData + "&filenames[]=" + value;
                        });

                        var vtitle = $("#vtitle").val();
                        var vdesc = $("#vdesc").val();
                        var vstatus = $("#vstatus").val();

                        $.post( "ajax/ajax-video.php", { action:"SaveVideo",vtitle:vtitle,vdesc:vdesc,vstatus:vstatus,data:formData }, function(data){
                            if(data.result == 1){

                                $("#vtitle").val("");
                                $("#vdesc").val("");
;
                                $("input:file").val("");

                                var $elm = $(data.html);
                                $(".content-pad").prepend($elm);
                                setTimeout(function() {
                                    $elm.remove();
                                }, 3000);
                                window.location.href = "?id=110&set=0";
                            } else {
                                var $elm = $(data.html);
                                $(".content-pad").prepend($elm);
                                setTimeout(function() {
                                    $elm.remove();
                                }, 3000);

                                $("#vsubmit").show();
                            }
                        },
                        "json");

                    }

        });

        $("#evsubmit").click(function(e){
            e.stopPropagation(); // Stop stuff happening
            e.preventDefault(); // Totally stop stuff happening

            var data;

            data = new FormData();
            data.append( "file0", $( "#emp4file" )[0].files[0] );
            data.append( "file1", $( "#eoggfile" )[0].files[0] );
            data.append( "file2", $( "#ewebmfile" )[0].files[0] );

            $.ajax({
                            url: "includes/video_upload.php?files",
                            type: "POST",
                            data: data,
                            cache: false,
                            dataType: "json",
                            processData: false,
                            contentType: false,
                            success: function(data, textStatus, jqXHR)
                            {
                                if(typeof data.error === "undefined")
                                {
                                    submitForm(e, data);
                                }
                                else
                                {
                                    console.log("ERRORS: " + data.error);
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown)
                            {
                                console.log("ERRORS: " + textStatus);
                            }
                        });

            function submitForm(event, data)
                    {
                        $form = $(event.target);

                        var formData = $form.serialize();

                        $.each(data.files, function(key, value)
                        {
                            formData = formData + "&filenames[]=" + value;
                        });

                        var vid = $("#evid").val();
                        var vtitle = $("#evtitle").val();
                        var vdesc = $("#evdesc").val();
                        var vstatus = $("#evstatus").val();

                        $.post( "ajax/ajax-video.php", { action:"UpdateVideo",vid:vid,vtitle:vtitle,vdesc:vdesc,vstatus:vstatus,data:formData }, function(data){
                            if(data.result == 1){

                                $("#evtitle").val("");
                                $("#evdesc").val("");
;
                                $("input:file").val("");

                                var $elm = $(data.html);
                                $(".content-pad").prepend($elm);
                                setTimeout(function() {
                                    $elm.remove();
                                }, 3000);
                                window.location.href = "?id=110&set=0";
                            } else {
                                var $elm = $(data.html);
                                $(".content-pad").prepend($elm);
                                setTimeout(function() {
                                    $elm.remove();
                                }, 3000);

                                $("#evsubmit").show();
                            }
                        },
                        "json");

                    }

        });


        </script>';