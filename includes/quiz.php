<?php	
// quiz id = 12
$test_aca = 0;
/*
The instructor will select an exam and question.
the question will be displayed and a session number will be created and displayed with the question.

The students will select the Quiz from the menu and enter the session number.
the question and options will be displayed and the student can answer the question.

The instructor can refresh the question to see the answers supplied by the students.

The question, session and answeres is stored in the DB.


Create two tables
quiz
quiz_options


Instructor
1. setect exam.
2. select question no
3. display question
4. mark question


set
---
0	display form and select a exam
2	display question, mark and display result if opt = 1
10	get next question
30
90	display summary when test complete

	
*/

if ($id == 12) {
    if (logged_in() && $aca_accesslevel == 1){
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Instructor Quiz</h2><div class=\"colored-line-left\"></div></div>";
    } else {
		echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Instructor Quiz</h2><div class=\"colored-line-left\"></div></div>";
    }
        echo '<div class="clearfix"></div>';
        echo '<div class="row content-pad">';
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";

        // quiz selected, Instructor create session, Student validate session
        if ($set == 1) {
            $validquiz = 0;

            if ($opt == 1) {
              // validate session number
              if (isset($_POST['submit'])) {
                $session_id = trim(mysql_prep($_POST['quiz_id']));
               } // end of post
                // test if exam and question is valid
                $query = "SELECT quiz_id, exam_id, question_id ";
                $query .= " FROM aca_quiz";
                $query .= " WHERE quiz_id = '{$session_id}' ";
                $query .= " AND status = '1' ";
                $query .= " LIMIT 1";
                $result_setU = mysqli_query($connection,$query);
                confirm_query($result_setU);
                if (mysqli_num_rows($result_setU) == 1) {
                    $validquiz = 1;
                    while ($row = mysqli_fetch_array($result_setU)) {
                        $quiz_id = $row["quiz_id"];
                        $exam_id = $row["exam_id"];
                        $question_id = $row["question_id"];
                    }

                    // include test to see if user already answered this question
                    $query = "SELECT quiz_option_id, quiz_id, exam_id, question_id ";
                    $query .= " FROM aca_quiz_option ";
                    $query .= " WHERE quiz_id = '{$session_id}' AND user_id = '{$userx}' ";
                    //$query .= " AND status = '1' ";
                    $query .= " LIMIT 1";
                    $result_setY = mysqli_query($connection,$query);
                    confirm_query($result_setY);
                    if (mysqli_num_rows($result_setY) == 1) {
                        $validquiz = 0;
                    }

                    // redirect to question for this session
                    $key = $quiz_id;
                    $qno = $question_id;
                    $set = 22;
                }

                if ($validquiz <> 1) {
                        echo "<div class=\"alert alert-danger\">";
                            echo "Invalid Session or question already answered. Please select a different Session.";
                        echo "</div>";
                        $set = 0;
                }


            } // end of opt = 1





            if ($opt >= 51) {
            if (isset($_POST['submit'])) {
              $userexam = trim(mysql_prep($_POST['exam_id']));
              $qno = trim(mysql_prep($_POST['qno']));
              $gip = get_ip(0);
             // echo "<p>Posted - User_ID: ".$userx."</p>";
             // echo "<p>Posted UserExam: ".$userexam."</p>";
             // echo "<p>Posted Q No: ".$qno."</p>";
             } // end of post

              // test if exam and question is valid
                $query = "SELECT exam_id, question_id ";
                $query .= " FROM aca_question";
                $query .= " WHERE exam_id = '{$userexam}' ";
                $query .= " AND question_id = '{$qno}' ";
                $query .= " AND status = '1' ";
                $query .= " LIMIT 1";
                $result_setZ = mysqli_query($connection,$query);
                confirm_query($result_setZ);
                if (mysqli_num_rows($result_setZ) == 1) {
                    $validquiz = 1;
                    $status = 1;
                    while ($row = mysqli_fetch_array($result_setZ)) {
                        $exam_id = $row["exam_id"];
                        $question_id = $row["question_id"];
                    }
                    // get the instructor_id
                    $result_set1 = get_aca_instructor_user_rec($userx);
                    while ($row = mysqli_fetch_array($result_set1)) {
                        $instructor_id = $row["instructor_id"];
                        $instructor_name = $row["name"];
                    }
                    //$instructor_id = $row["instructor_id"];

                      //echo "<p>One record check - Q: ".$question_id." / ".$qno."</p>";
                    $class_id = 0; // not in use

                    // create quiz session id
                    $query = "INSERT INTO aca_quiz( ";
                    $query .= " exam_id, question_id, instructor_id, class_id, ";
                    $query .= " status, creator_id ) ";
                    $query .= " VALUES (   ";
                    $query .= " '{$exam_id}', '{$question_id}', '{$instructor_id}', '{$class_id}', ";
                    $query .= " '{$status}', {$userx} ) " ;

                    $result = mysqli_query( $connection, $query);
                    if (mysqli_affected_rows($connection) == 1) {
                        // Success
                        echo "<div class=\"alert alert-success\">";
                            echo "Record Created";
                              // display the Quiz Session ID
                            // get the last record
                            $result_set1 = get_aca_quiz_last_rec($userx, $exam_id);
                            while ($row = mysqli_fetch_array($result_set1)) {
                                $session_id = $row["quiz_id"];
                            }
                            echo "<h2>Quiz</h2>";
                            echo "Session ID: ".$session_id. "</br>";
                            echo "Select the Instructor Quiz option from the Main menu and enter this ID: <strong>".$session_id."</strong></br>";
                            echo "</div>";

                            // Display the selected Question
                            $key = $question_id;
                            $set = 22; // get and display question and options

                            //$set = 0; // set action back to listing
                        } else {
                        echo "<div class=\"alert alert-danger\">";
                            echo "Record creation failed";
                        echo "</div>";
                        $set = 0;
                    }
                }

                if ($validquiz <> 1) {
                        echo "<div class=\"alert alert-danger\">";
                            echo "Invalid Question for the selected Exam. Please select a different question.";
                        echo "</div>";
                        $set = 0;
                }

            } // end op opt 51
        } // end of set = 1




        // display session id and question + results
        if ($set == 22) {
            if ($opt >= 51) {
            // instructor view question and option for explanation. Also number of answers and mark exam.

            // get question
            $fa = "alert alert-info";
            $fb = "alert alert-warning";
            $fc = "alert alert-info";
            $fd = "alert alert-warning";
            $fe = "alert alert-info";
            $ff = "alert alert-warning";
            $dis = "";
            $oac = "";
            $obc = "";
            $occ = "";
            $odc = "";
            $oec = "";
            $ofc = "";
            $oa = "";
            $ob = "";
            $oc = "";
            $od = "";
            $oe = "";
            $of = "";
            $upErr = "";
            $up = "";
            $cora = "";
            // print the question form

            echo "<form name=\"academy\" method=\"post\" action=\"?id=".$id."&qno=".$qno."&set=3&opt=".$opt."&login=1&key=".$key." \"enctype=\"multipart/form-data\">";

            echo "<table class=\"table\">";
            // get data
            //$npage = $page;
            $result_set = get_aca_question_rec($qno, 0); // question number and subject/tool or 0 for all
            while ($row = mysqli_fetch_array($result_set)) {
                $question_id = $row["question_id"];
                $qno = $question_id;
                $exam_id = $row["exam_id"];
                $question_no = $row["question_no"];
                $question_text = $row["question_text"];
                $question_note = $row["question_note"];
                $option_a_text = $row["option_a_text"];
                $option_b_text = $row["option_b_text"];
                $option_c_text = $row["option_c_text"];
                $option_d_text = $row["option_d_text"];
                $option_e_text = $row["option_e_text"];
                $option_f_text = $row["option_f_text"];
                $option_a_answer = $row["option_a_answer"];
                $option_b_answer = $row["option_b_answer"];
                $option_c_answer = $row["option_c_answer"];
                $option_d_answer = $row["option_d_answer"];
                $option_e_answer = $row["option_e_answer"];
                $option_f_answer = $row["option_f_answer"];
                $tool_id = $row["tool_id"];
                $qhelp = $row["question_help"];
                $create_date = $row["create_date"];

                $cla = "alert alert-info";
                $clb = "alert alert-warning";
                $clc = "alert alert-info";
                $cld = "alert alert-warning";
                $cle = "alert alert-info";
                $clf = "alert alert-warning";

                if ($cat7 == 1) { // display the answer if "Mark" selected
                  $cla = "alert alert-danger";
                  $clb = "alert alert-danger";
                  $clc = "alert alert-danger";
                  $cld = "alert alert-danger";
                  $cle = "alert alert-danger";
                  $clf = "alert alert-danger";
                  $fa = "alert alert-danger";
                  $fb = "alert alert-danger";
                  $fc = "alert alert-danger";
                  $fd = "alert alert-danger";
                  $fe = "alert alert-danger";
                  $ff = "alert alert-danger";
                  if ($option_a_answer == 1) {
                      $fa = "alert alert-success";
                      $cla = "alert alert-success";
                      $cora .= "A "; }
                  if ($option_b_answer == 1) {
                      $fb = "alert alert-success";
                      $clb = "alert alert-success";
                      $cora .= "B "; }
                  if ($option_c_answer == 1) {
                      $fc = "alert alert-success";
                      $clc = "alert alert-success";
                      $cora .= "C "; }
                  if ($option_d_answer == 1) {
                      $fd = "alert alert-success";
                      $cld = "alert alert-success";
                      $cora .= "D "; }
                  if ($option_e_answer == 1) {
                      $fe = "alert alert-success";
                      $cle = "alert alert-success";
                      $cora .= "E "; }
                }
                  if ($option_f_answer == 1) {
                      $ff = "alert alert-success";
                      $clf = "alert alert-success";
                      $cora .= "F "; }
                }

                $exam_code = "";
                $result_setI = get_aca_exam_rec($exam_id);
                while ($row = mysqli_fetch_array($result_setI)) {
                    $exam_code = $row["code"];
                }

                $subject = "";
                $result_setI = get_aca_tool_rec($tool_id);
                while ($row = mysqli_fetch_array($result_setI)) {
                    $subject = $row["description"];
                }

                $result_set80 = get_aca_quiz_option_score($key, 1); // quiz_id, status
                while ($row = mysqli_fetch_array($result_set80)) {
                    $score = $row["score"];
                    $oac = $row["count_a"];
                    $obc = $row["count_b"];
                    $occ = $row["count_c"];
                    $odc = $row["count_d"];
                    $oec = $row["count_e"];
                    $ofc = $row["count_f"];
                }

            $oa = "&nbsp;<span>".$oac."</span>";
            $ob = "&nbsp;<span>".$obc."</span>";
            $oc = "&nbsp;<span>".$occ."</span>";
            $od = "&nbsp;<span>".$odc."</span>";
            $oe = "&nbsp;<span>".$oec."</span>";
            $of = "&nbsp;<span>".$ofc."</span>";
            /*
            $oa = "<h4>&nbsp;<span class=\"".$cla."\">".$oac."</span></h4>";
            $ob = "<h4>&nbsp;<span class=\"".$clb."\">".$obc."</span></h4>";
            $oc = "<h4>&nbsp;<span class=\"".$clc."\">".$occ."</span></h4>";
            $od = "<h4>&nbsp;<span class=\"".$cld."\">".$odc."</span></h4>";
            $oe = "<h4>&nbsp;<span class=\"".$cle."\">".$oec."</span></h4>";
            $of = "<h4>&nbsp;<span class=\"".$clf."\">".$ofc."</span></h4>";
            */
			
		// print list detail	
		echo "<tr>";
				echo "<td align=\"left\"><h2><strong>Quiz ".$key."</strong></h2></br><em>Question: ".$question_id." of Exam: ".$exam_code."</em></td>"; // 
		echo "</tr>";	
		echo "<tr>";
			echo "<td align=\"right\" class=\"topline\">&nbsp;</td>";
		echo "</tr>";
		echo "<tr>";
				echo "<td align=\"left\"><strong>".$question_text."</strong></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td align=\"right\">&nbsp;</td>";
		echo "</tr>";
		echo "<tr>";
				echo "<td align=\"left\">".$question_note."</td>";
			echo "</tr>";			
		echo "</table>";
		
		if ($cat7 == 1) { // display the answer if "Mark" selected
			echo "<p class=\"valid\">The correct answer is ".$cora."</p>";	
		}
		
		
		echo "<table width=\"580\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";	
		echo "<tr>";
			echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
		echo "</tr>";	
		echo "<tr>";
				echo "<td class=\"qu\" align=\"left\" width=\"30\">A</td>";
				echo "<td class=\"qu ".$fa."\" align=\"left\" width=\"500\">".nl2br($option_a_text)."</td>";
				echo "<td class=\"qu ".$cla."\" align=\"left\">&nbsp;&nbsp;".$oa."</td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
		echo "</tr>";
		echo "<tr>";
				echo "<td class=\"qn\" align=\"left\">B</td>";
				echo "<td class=\"qn ".$fb."\" align=\"left\">".nl2br($option_b_text)."</td>";
				echo "<td class=\"qn ".$clb."\" align=\"left\">&nbsp;&nbsp;".$ob."</td>";
		echo "</tr>";
		if ($option_c_text <> "") {
		  echo "<tr>";
			  echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
		  echo "</tr>";
		  echo "<tr>";
				  echo "<td class=\"qu\" align=\"left\">C</td>";
				  echo "<td class=\"qu ".$fc."\" align=\"left\">".nl2br($option_c_text)."</td>";
				  echo "<td class=\"qu ".$clc."\" align=\"left\">&nbsp;&nbsp;".$oc."</td>";
		  echo "</tr>";
		}
		if ($option_d_text <> "") {
		  echo "<tr>";
			  echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
		  echo "</tr>";
		  echo "<tr>";
				  echo "<td class=\"qn\" align=\"left\">D</td>";
				  echo "<td class=\"qn ".$fd."\" align=\"left\">".nl2br($option_d_text)."</td>";
				  echo "<td class=\"qn ".$cld."\" align=\"left\">&nbsp;&nbsp;".$od."</td>";
		  echo "</tr>";
		}
		if ($option_e_text <> "") {
		  echo "<tr>";
			  echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
		  echo "</tr>";
		  echo "<tr>";
				  echo "<td class=\"qu\" align=\"left\">E</td>";
				  echo "<td class=\"qu ".$fe."\" align=\"left\">".nl2br($option_e_text)."</td>";
				  echo "<td class=\"qu ".$cle."\" align=\"left\">&nbsp;&nbsp;".$oe."</td>";
		  echo "</tr>";
		}
		if ($option_f_text <> "") {
		  echo "<tr>";
			  echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
		  echo "</tr>";
		  echo "<tr>";
				  echo "<td class=\"qu\" align=\"left\">E</td>";
				  echo "<td class=\"qu ".$ff."\" align=\"left\">".nl2br($option_f_text)."</td>";
				  echo "<td class=\"qu ".$clf."\" align=\"left\">&nbsp;&nbsp;".$of."</td>";
		  echo "</tr>";
		}
		echo "<tr>";
			echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
		echo "</tr>";
		//} // end of result_set
		$refresh = "<button name=\"RefreshBut\" type=\"button\" value=\"Refresh\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&qno=".$qno."&login=1&set=22&opt=".$opt."&key=".$key."');return document.MM_returnValue\"/>Refresh</button>";		
		$mark = "<button name=\"MarkBut\" type=\"button\" value=\"Refresh\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&qno=".$qno."&login=1&set=22&cat7=1&opt=".$opt."&key=".$key."');return document.MM_returnValue\"/>Mark Question</button>";	
		
		$explain = " <button type=\"button\" class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#myModal\">Open Question Help</button>";
		if ($qhelp == "") { $explain = ""; }
		
		$new = "<button name=\"NewBut\" type=\"button\" value=\"New\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&qno=0&login=1&set=0&opt=".$opt."&key=0');return document.MM_returnValue\"/>New</button>";		

	
		// table footer	
		echo "</tr>";
		echo "<tr>";
			echo "<td align=\"right\" colspan=\"3\" class=\"topline\">&nbsp;</td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td align=\"right\" colspan=\"3\">".$refresh." " .$mark.$explain." " .$new. "</td>";
		echo "</tr>";
		// end of table
		echo "</table>";	
		echo "</form>";


		
		
		// <!-- Question Help -->
		echo "<div class=\"modal fade\" id=\"myModal\" role=\"dialog\">";
		  echo "<div class=\"modal-dialog\">";
		  
		   //  <!-- Modal content-->
			echo "<div class=\"modal-content\">";
			  echo "<div class=\"modal-header\">";
				echo "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>";
				echo "<h4 class=\"modal-title\">Question Help ".$qno." / ".$exam_code."</h4>";
			  echo "</div>";
			  echo "<div class=\"modal-body\">";
			   echo " <p>".$qhelp."</p>";
			   if ($subject <> "") { echo " <p>".$subject."</p>"; }
			 echo "</div>";
			  echo "<div class=\"modal-footer\">";
				echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>";
			  echo "</div>";
			echo "</div>";
			
		  echo "</div>";
		echo "</div>";

		} // end of opt >= 51
		
		
		
		
		
		
		if ($opt == 1) {
			// display question
			// option to submit
			
			
			$up = "<button name=\"submit\" type=\"submit\" value=\"Submit\" class=\"btn btn-default\">Submit</button>";	
			$upErr = "";
			$dis = "";
				  
			// print the question form												
			echo "<form name=\"academy\" method=\"post\" action=\"?id=".$id."&qno=".$qno."&set=13&opt=".$opt."&login=1&key=".$key." \"enctype=\"multipart/form-data\">";
				  
			echo "<table width=\"580\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";	
			// get data	
				  		
			$fa = "alert alert-info";
			$fb = "alert alert-warning";
			$fc = "alert alert-info";
			$fd = "alert alert-warning";
			$fe = "alert alert-info";
			$ff = "alert alert-warning";
			$oac = "";
			$obc = "";
			$occ = "";
			$odc = "";
			$oec = "";
			$ofc = "";
			$oa = "";
			$ob = "";
			$oc = "";
			$od = "";
			$oe = "";
			$of = "";
		
			$result_set = get_aca_question_rec($qno, 0); // question number and subject/tool or 0 for all
			while ($row = mysqli_fetch_array($result_set)) {		
				$question_id = $row["question_id"];
				$exam_id = $row["exam_id"];		
				$question_no = $row["question_no"];	
				$question_text = $row["question_text"];	
				$question_note = $row["question_note"];
				$option_a_text = $row["option_a_text"];
				$option_b_text = $row["option_b_text"];	
				$option_c_text = $row["option_c_text"];	
				$option_d_text = $row["option_d_text"];
				$option_e_text = $row["option_e_text"];
				$option_f_text = $row["option_f_text"];
				$option_a_answer = $row["option_a_answer"];	
				$option_b_answer = $row["option_b_answer"];	
				$option_c_answer = $row["option_c_answer"];	
				$option_d_answer = $row["option_d_answer"];	
				$option_e_answer = $row["option_e_answer"];	
				$option_f_answer = $row["option_f_answer"];	
				$create_date = $row["create_date"];	
			
				$exam_code = "";
				$result_setI = get_aca_exam_rec($exam_id);
				while ($row = mysqli_fetch_array($result_setI)) {		
					$exam_code = $row["code"];		
				}
					  
				  // print list detail	
				  echo "<tr>";
					  echo "<td align=\"left\"><h2><strong>Quiz ".$key."</strong></h2></br><em>Question: ".$question_id." of Exam: ".$exam_code."</em></td>"; // 
				  echo "</tr>";	
				  echo "<tr>";
					  echo "<td align=\"right\" class=\"topline\">&nbsp;</td>";
				  echo "</tr>";
				  echo "<tr>";
						  echo "<td align=\"left\"><strong>".$question_text."</strong></td>";
				  echo "</tr>";
				  echo "<tr>";
					  echo "<td align=\"right\">&nbsp;</td>";
				  echo "</tr>";
				  echo "<tr>";
						  echo "<td align=\"left\">".$question_note."</td>";
					  echo "</tr>";			
				  echo "</table>";
				  
				  $oa = "<input class=\"qu\" type=\"checkbox\" value=\"1\" name=\"answ_a\" id=\"answ_a\" ".$oac.$dis."  />";
				  $ob = "<input class=\"qn\" type=\"checkbox\" value=\"1\" name=\"answ_b\" id=\"answ_b\" ".$obc.$dis." />";
				  $oc = "<input class=\"qu\" type=\"checkbox\" value=\"1\" name=\"answ_c\" id=\"answ_c\" ".$occ.$dis." />";
				  $od = "<input class=\"qn\" type=\"checkbox\" value=\"1\" name=\"answ_d\" id=\"answ_d\" ".$odc.$dis." />";
				  $oe = "<input class=\"qu\" type=\"checkbox\" value=\"1\" name=\"answ_e\" id=\"answ_e\" ".$oec.$dis." />";
				  $of = "<input class=\"qn\" type=\"checkbox\" value=\"1\" name=\"answ_f\" id=\"answ_f\" ".$ofc.$dis." />";
					  
				  
				  echo "<table width=\"580\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";	
				  echo "<tr>";
					  echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
				  echo "</tr>";	
				  echo "<tr>";
						  echo "<td class=\"qu\" align=\"left\" width=\"30\">A</td>";
						  echo "<td class=\"qu ".$fa."\" align=\"left\" width=\"500\">".nl2br($option_a_text)."</td>";
						  echo "<td class=\"qu\" align=\"left\">&nbsp;&nbsp;".$oa."</td>";
				  echo "</tr>";
				  echo "<tr>";
					  echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
				  echo "</tr>";
				  echo "<tr>";
						  echo "<td class=\"qn\" align=\"left\">B</td>";
						  echo "<td class=\"qn ".$fb."\" align=\"left\">".nl2br($option_b_text)."</td>";
						  echo "<td class=\"qn\" align=\"left\">&nbsp;&nbsp;".$ob."</td>";
				  echo "</tr>";
				  if ($option_c_text <> "") {
					echo "<tr>";
						echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
					echo "</tr>";
					echo "<tr>";
							echo "<td class=\"qu\" align=\"left\">C</td>";
							echo "<td class=\"qu ".$fc."\" align=\"left\">".nl2br($option_c_text)."</td>";
							echo "<td class=\"qu\" align=\"left\">&nbsp;&nbsp;".$oc."</td>";
					echo "</tr>";
				  }
				  if ($option_d_text <> "") {
					echo "<tr>";
						echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
					echo "</tr>";
					echo "<tr>";
							echo "<td class=\"qn\" align=\"left\">D</td>";
							echo "<td class=\"qn ".$fd."\" align=\"left\">".nl2br($option_d_text)."</td>";
							echo "<td class=\"qn\" align=\"left\">&nbsp;&nbsp;".$od."</td>";
					echo "</tr>";
				  }
				  if ($option_e_text <> "") {
					echo "<tr>";
						echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
					echo "</tr>";
					echo "<tr>";
							echo "<td class=\"qu\" align=\"left\">E</td>";
							echo "<td class=\"qu ".$fe."\" align=\"left\">".nl2br($option_e_text)."</td>";
							echo "<td class=\"qu\" align=\"left\">&nbsp;&nbsp;".$oe."</td>";
					echo "</tr>";
				  }
				  if ($option_f_text <> "") {
					echo "<tr>";
						echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
					echo "</tr>";
					echo "<tr>";
							echo "<td class=\"qn\" align=\"left\">F</td>";
							echo "<td class=\"qn ".$ff."\" align=\"left\">".nl2br($option_f_text)."</td>";
							echo "<td class=\"qn\" align=\"left\">&nbsp;&nbsp;".$of."</td>";
					echo "</tr>";
				  }
				  echo "<tr>";
					  echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
				  echo "</tr>";
				  } // end of result_set
						  
				  // table footer	
				  echo "</tr>";
				  echo "<tr>";
					  echo "<td align=\"right\" colspan=\"3\" class=\"topline\">&nbsp;</td>";
				  echo "</tr>";
				  echo "<tr>";
					  echo "<td align=\"right\" colspan=\"3\">".$upErr." ".$up."</td>";
				  echo "</tr>";
				  // end of table
				  echo "</table>";	
				  echo "</form>";	
				  
				  	
		} // end of if opt = 1
		
		
	} // end of 22
	
	
	
		
		
	// mark question and update stats - do this when question is answered (on Submit)
	if ($set == 13 && $key <> 0 && $qno <> 0 && $opt == 1) {		
		$answ_a = 0;			
		$answ_b = 0;				
		$answ_c = 0;				
		$answ_d = 0;				
		$answ_e = 0;				
		$answ_f = 0;		
		$correct_cnt = 0;
		$answer_cnt = 0;			
		
		// set questions variables							
		$oac = "";
		$obc = "";
		$occ = "";
		$odc = "";
		$oec = "";
		$ofc = "";
		$msg = "";
		$cora = "";
		$but = "but";
		$up = "";
		$upErr = "";
		$fa = "alert alert-info";
		$fb = "alert alert-info";
		$fc = "alert alert-info";
		$fd = "alert alert-info";
		$fe = "alert alert-info";
		$ff = "alert alert-info";


					
		if (isset($_POST['submit'])) {
			if(isset($_POST['answ_a'])) {
                $answ_a = trim(mysql_prep($_POST['answ_a']));
            }
            if(isset($_POST['answ_b'])) {
                $answ_b = trim(mysql_prep($_POST['answ_b']));
            }
            if(isset($_POST['answ_c'])) {
                $answ_c = trim(mysql_prep($_POST['answ_c']));
            }
            if(isset($_POST['answ_d'])) {
                $answ_d = trim(mysql_prep($_POST['answ_d']));
            }
            if(isset($_POST['answ_e'])) {
                $answ_e = trim(mysql_prep($_POST['answ_e']));
            }
            if(isset($_POST['answ_f'])) {
                $answ_f = trim(mysql_prep($_POST['answ_f']));
            }
			$tot_answ = $answ_a + $answ_b + $answ_c + $answ_d + $answ_e + $answ_f; // total number of questions answered
		}
		// get record results and print answers			
		$result_set = get_aca_question_rec($qno, 0); 
		while ($row = mysqli_fetch_array($result_set)) {
			$option_a_answer = $row["option_a_answer"];	
			$option_b_answer = $row["option_b_answer"];	
			$option_c_answer = $row["option_c_answer"];	
			$option_d_answer = $row["option_d_answer"];	
			$option_e_answer = $row["option_e_answer"];
			$option_f_answer = $row["option_f_answer"];
			$exam_id = $row["exam_id"];
			$correct_cnt = $option_a_answer + $option_b_answer + $option_c_answer + $option_d_answer + $option_e_answer + $option_f_answer;
			if ($option_a_answer == 1) { 
				$fa = "alert alert-success"; 
				$cora .= "A "; }
			if ($option_b_answer == 1) { 
				$fb = "alert alert-success"; 
				$cora .= "B "; }
			if ($option_c_answer == 1) { 
				$fc = "alert alert-success"; 
				$cora .= "C "; }
			if ($option_d_answer == 1) { 
				$fd = "alert alert-success"; 
				$cora .= "D "; }
			if ($option_e_answer == 1) { 
				$fe = "alert alert-success"; 
				$cora .= "E "; }
			if ($option_f_answer == 1) { 
				$ff = "alert alert-success"; 
				$cora .= "F "; }
		}
		
		if ($test_aca == 1) {
		echo "Test</br>";
		  echo "A: ".$answ_a." - " .$option_a_answer. "</br>";
		  echo "B: ".$answ_b." - " .$option_b_answer. "</br>";
		  echo "C: ".$answ_c." - " .$option_c_answer. "</br>";
		  echo "D: ".$answ_d." - " .$option_d_answer. "</br>";
		  echo "E: ".$answ_e." - " .$option_e_answer. "</br>";
		  echo "F: ".$answ_f." - " .$option_f_answer. "</br>";
		  echo "Set: ".$set. "</br>";
		  echo "Q: ".$q. "</br>";
		  echo "Login: ".$login. "</br>";
		}
		
		if ($answ_a == 1 && $option_a_answer == 1) { $answer_cnt = $answer_cnt + 1; }
		if ($answ_b == 1 && $option_b_answer == 1) { $answer_cnt = $answer_cnt + 1; }
		if ($answ_c == 1 && $option_c_answer == 1) { $answer_cnt = $answer_cnt + 1; }
		if ($answ_d == 1 && $option_d_answer == 1) { $answer_cnt = $answer_cnt + 1; }
		if ($answ_e == 1 && $option_e_answer == 1) { $answer_cnt = $answer_cnt + 1; }	
		if ($answ_f == 1 && $option_f_answer == 1) { $answer_cnt = $answer_cnt + 1; }	
		
		if ($answ_a == 1) {  $oac = "checked=\"checked\""; } else { $oac = ""; }
		if ($answ_b == 1) {  $obc = "checked=\"checked\""; } else { $obc = ""; }
		if ($answ_c == 1) {  $occ = "checked=\"checked\""; } else { $occ = ""; }
		if ($answ_d == 1) {  $odc = "checked=\"checked\""; } else { $odc = ""; }
		if ($answ_e == 1) {  $oec = "checked=\"checked\""; } else { $oec = ""; }
		if ($answ_f == 1) {  $oec = "checked=\"checked\""; } else { $ofc = ""; }		
			
				
		if ( $correct_cnt == $answer_cnt && $tot_answ == $correct_cnt) { 
			// update table
			$score = 1;
			$create_date = $today;
			$query = "INSERT INTO aca_quiz_option( ";
			$query .= " quiz_id, exam_id, question_id, user_id, score, "; 
			$query .= " answ_a, answ_b, answ_c, answ_d, answ_e, answ_f, "; 
			$query .= " create_date, status, creator_id ) ";
			$query .= " VALUES (   ";
			$query .= " '{$key}', '{$exam_id}', '{$qno}', '{$userx}', {$score}, ";  
			$query .= " '{$answ_a}', '{$answ_b}', '{$answ_c}', '{$answ_d}', '{$answ_e}', '{$answ_f}', "; 
			$query .= " '{$create_date}', 1, '{$userx}' ) " ;		
			$result = mysqli_query( $connection, $query);
			if (mysqli_affected_rows($connection) == 1) {	
					echo "<div class=\"alert alert-success\">";
						echo "The answer was submitted. Please wait for the instructor.";
					echo "</div>";	
			} else {
					echo "<div class=\"alert alert-danger\">";
						echo "An error occurred. Please try again.";
					echo "</div>";	
			}
		} else { 
			// update table
			$score = 0;
			$create_date = $today;
			$query = "INSERT INTO aca_quiz_option( ";
			$query .= " quiz_id, exam_id, question_id, user_id, score, "; 
			$query .= " answ_a, answ_b, answ_c, answ_d, answ_e, answ_f, "; 
			$query .= " create_date, status, creator_id ) ";
			$query .= " VALUES (   ";
			$query .= " '{$key}', '{$exam_id}', '{$qno}', '{$userx}', {$score}, ";  
			$query .= " '{$answ_a}', '{$answ_b}', '{$answ_c}', '{$answ_d}', '{$answ_e}', '{$answ_f}', "; 
			$query .= " '{$create_date}', 1, '{$userx}' ) " ;		
			$result = mysqli_query( $connection, $query);
			if (mysqli_affected_rows($connection) == 1) {	
					echo "<div class=\"alert alert-success\">";
						echo "The answer was submitted. Please wait for the instructor.";
					echo "</div>";	
			} else {
					echo "<div class=\"alert alert-danger\">";
						echo "An error occurred. Please try again.";
					echo "</div>";	
			}
		}
		
		$set = 0; // question was tested and results updated
	
		
		if ($test_aca == 1) {
		echo "Test set=2 opt=1 mark question</br>";
		echo "Q: ".$q. "</br>";
		echo "Opt: ".$opt. "</br>";
		echo "Set: ".$set. "</br></br>";
	}
					
	} // end of set == 13 and opt == 1 question marked
	
	
	
	if ($test_aca == 1) {
		echo "Test before set 10</br>";
		echo "Q: ".$q. "</br>";
		echo "Opt: ".$opt. "</br>";
		echo "Set: ".$set. "</br></br>";
	}
		
			  

	
	if ($set == 0) { // select a question	
		if ($test_aca == 1) {
				  echo "Options to select Quiz Session</br>";
				  echo "Set: ".$set. "</br>";
				  echo "Opt: ".$opt. "</br>";
				  //echo "Qno: ".$aca_qno. "</br>";
				  echo "UserX: ".$userx. "</br>";
				  //echo "Q Cnt: ".$q_count. "</br>";
				  echo "Login: ".$login. "</br>";
			  }
	
		
		if ($opt == 1) {
			// display session number form
            /*if (logged_in() && $aca_accesslevel >= 1) {
                echo "<h2 style=\"display:block;padding:1em;background: url(images/logo_beyondbi_253x67.png);background-position: 0em 0.6em;background-repeat: no-repeat;text-align: center\">Instructor Quiz</h2>";
            }*/
		//echo "<h2>Select a Quiz Session</h2>";
		echo "<form name=\"test\" method=\"post\" action=\"?id=".$id."&set=1&opt=".$opt."&login=1\" role=\"form\">";

            echo "<table class=\"table table-striped\">";
            echo "<thead>";
            echo '<tr><th colspan="2" style="background:#58595b;color:#FFFFFF;">Select a Question</th></tr>';
            echo "</thead>";
            echo "<tbody>";

		echo "<tr>";
		echo "<td>Quiz Session ID</td>";
		echo "<td><input name=\"quiz_id\" type=\"\" class=\"form-control isNum\" id=\"quiz_id\" placeholder=\"\" value=\"\"></td>";
		echo "</tr>";
		echo "<tr><td colspan=\"2\" class=\"text-center\"><button name=\"submit\" type=\"submit\" class=\"btn btn-default\"> Display Quiz </button></td></tr>";
		echo "</tbody>";
		echo "</table>";
		echo "</form>";
	
		} // end if opt = 1
	
		if ($opt >= 51) {					

		echo "<form name=\"test\" method=\"post\" action=\"?id=".$id."&set=1&opt=".$opt."&login=1\" role=\"form\">";
            echo "<table class=\"table table-striped\">";
            echo "<thead>";
            echo '<tr><th colspan="2" style="background:#58595b;color:#FFFFFF;">Select a Question</th></tr>';
            echo "</thead>";
            echo "<tbody>";
		echo "<tr>";
		echo "<td class=\"col-sm-2\">Exam</td>";
			echo "<td><select name=\"exam_id\" class=\"form-control reqd\" id=\"pracexam_id\">";
						  //echo "<option value=\"".$exam_id."\">".$exam."</option>";					
// TEMP update with DB lookup to only display exams that the user has access to
						  echo "<option value=\"\"></option>";
								$result_set19 = get_aca_class_exam_list_instructor(1,$userx,$today); 
								// get_aca_user_exam_list($npage, $no_of_list, $status, $cat, $expiry_date) {
								// get_aca_class_exam_list_instructor($status, $user_id, $expiry_date) {
								while ($row = mysqli_fetch_array($result_set19)) {
									$dim_key = $row["exam_id"]; 
									$display_name = $row["exam"];
									//	$result_setI = get_aca_exam_rec($dim_key);
									//	while ($row = mysqli_fetch_array($result_setI)) {		
									//		$display_name = $row["code"];		
									//	}
									echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								}
			echo "</select></td>";
		echo "</tr>";
		
		echo "<tr>";
		echo "<td>Question Number</td>";
		echo "<td><input name=\"qno\" type=\"text\" class=\"form-control isNum\" id=\"pracquestion\" placeholder=\"\" value=\"\"></td>";
		echo "</tr>";
		
		
		echo "<tr><td colspan=\"2\" class=\"text-center\"><button name=\"submit\" type=\"submit\" class=\"btn btn-default\"> Display Question </button></td></tr>";
		echo "</tbody>";
		echo "</table>";
		//$lastquiz = "<button name=\"LastBut\" type=\"button\" value=\"Last\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&q=0&qq=".$q."&login=1&set=0&opt=0&key=".$key."');return document.MM_returnValue\"/>Display Last Quiz</button>";
		
		
		//echo $lastquiz;
		
		echo "</form>";
		
		} // end if opt >= 51
			
		
		echo "<p>&nbsp;</p>";	
		
		
		if ($opt >= 51) {
		// display the last 20 sessions for this instructor

		  	
		echo "<table class=\"table table-striped\">";
		echo "<thead>";
            echo '<tr><th colspan="6" style="background:#58595b;color:#FFFFFF;">Recent Sessions</th></tr>';
		echo "<tr>";
			echo "<th>Session</th>";
			echo "<th>Exam</th>";
			echo "<th>Question</th>";
			echo "<th>Score</th>";
			echo "<th>Count</th>";
			echo "<th>%</th>";
		echo "</tr>";		
		echo "</thead>";
		echo "<tbody>";
		// get data
		$result_setS = get_aca_quiz_last_list($userx, 1, 20); // instructor's user id, status, no of recs
		while ($row = mysqli_fetch_array($result_setS)) {		
			$key = $row["quiz_id"];
			$exam_id = $row["exam_id"];		
			$question_id = $row["question_id"];	
			$qno = $question_id;
			$instructor_id = $row["instructor_id"];	
			$class_id = $row["class_id"];
			$status = $row["status"];
			$score = 0;
			$score_p = 0;
				$result_set74 = get_aca_quiz_score($key, 1); // quiz_id, status
				while ($row = mysqli_fetch_array($result_set74)) {
					$score = $row["score"]; 
					$rec_count = $row["rec_count"]; 
				}
			if ($rec_count == 0) { $score_p = 0; } else { $score_p = ($score / $rec_count) *100; }	
			$sp = number_format($score_p,0,",",",")."%";	
			$exam = "";
			$result_setI = get_aca_exam_rec($exam_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$exam_code = $row["code"];		
			}
			$d_status = $status;
				$result_set74 = get_dim("status", $status);
				while ($row = mysqli_fetch_array($result_set74)) {
					$dim_key = $row["dim_key"]; 
					$d_status = $row["description"];
				}
			
			$un = "<a href=\"?id=".$id."&set=22&opt=".$opt."&login=1&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&qno=".$qno."&key=".$key."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$key."</a>";
			
		echo "<tr>";
			echo "<td>".$un."</td>";
			echo "<td>".$exam_code."</td>";
			echo "<td>".$question_id."</td>";
			echo "<td>".$score."</td>";
			echo "<td>".$rec_count."</td>";
			echo "<td>".$sp."</td>";
		echo "</tr>";
		}	


		echo "</tbody>";
		echo "</table>";
		  echo "</div> "; 
	   echo " </div> ";

	   echo "<p></p>";
	   
	   } // end if opt >= 51
	  
	} // end of set = 0

    echo "</div>";
    echo "</div>";
	
} // end of quiz id = 12
?>
 