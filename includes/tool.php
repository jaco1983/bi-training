<?php	
// user id = 31

if ($id == 31) {

    echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Subject Maintenance</h2><div class=\"colored-line-left\"></div></div>
          <div class=\"clearfix\"></div>
          <div class=\"row content-pad\">
          <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";

	if ($set == 2) { // insert																
		if (isset($_POST['submit'])) {
			$description = trim(mysql_prep($_POST['description']));
			$exam_id = trim(mysql_prep($_POST['exam_id']));
			$create_date = $today;	
			$creator_id = $userx;
			$status = '1';	
		}
		
		if ($test_aca == 1) {
			$msg = "set 2 Post opt=".$opt."</br>";
			$msg .= "create_date: ".$create_date."</br>";
			$msg .= "creator_id: ".$userx."</br>";
			echo $msg;
		}

		$query = "INSERT INTO aca_tool( ";
		$query .= " description, create_date, "; 
		$query .= " status, creator_id, exam_id ) ";
		$query .= " VALUES (   ";
		$query .= " '{$description}', '{$create_date}', ";  
		$query .= " '{$status}', {$userx}, {$exam_id} ) " ;
	
		$result = mysqli_query( $connection, $query);
		if (mysqli_affected_rows($connection) == 1) {	
			// Success	
			echo "<div class=\"alert alert-success\">";
				echo "Record Created";
			echo "</div>";
			$set = 0; // set action back to listing
			} else {
			echo "<div class=\"alert alert-danger\">";
				echo "Record creation failed";
			echo "</div>";
		}
		$set = 0; // set action back to form
	//} // end opt = 1
		
	} // end of set = 2
	


	if ($set == 4 && $key <> 0) { // update record													
		if (isset($_POST['submit'])) {
			$description = trim(mysql_prep($_POST['description']));
			$exam_id = trim(mysql_prep($_POST['exam_id']));
			$status = trim(mysql_prep($_POST['status']));
			$create_date = $today;	
			$creator_id = $userx;
		}
		
			
			if ($test_aca == 1) {
				$msg = "set 2 Post</br>";
				$msg .= "create_date: ".$create_date."</br>";
				$msg .= "status: ".$status."</br>";
				$msg .= "creator_id: ".$userx."</br>";
				//echo $msg;
			}
			
			// Update
			$query = "UPDATE aca_tool SET ";
			$query .= " description = '{$description}', ";
			$query .= " status = '{$status}', ";
			$query .= " creator_id = '{$userx}', ";
			$query .= " exam_id = '{$exam_id}' ";
			$query .= " WHERE tool_id = {$key} ";
			$query .= " LIMIT 1 ";
		
			$result = mysqli_query( $connection, $query);
			if (mysqli_affected_rows($connection) == 1) {	
				// Success	
				echo "<div class=\"alert alert-success\">";
					echo "The record was updated successfully.";
				echo "</div>";
				$set = 0; // set action back to listing
				} else {
				echo "<div class=\"alert alert-danger\">";
					echo "The record was not updated. Please try again. Make sure that all fields are completed.";
				echo "</div>";
				$set = 3; // set action back to form
			}
				
	} // end of set = 4 update


	if ($set == 3) { // edit																
			/*echo "<div class=\"panel panel-primary\"> ";
		  echo "<div class=\"panel-heading\">Subject Maintenance</div> "; 
		  echo "<div class=\"panel-body\">";*/
				
		$result_set = get_aca_tool_rec($key);
		while ($row = mysqli_fetch_array($result_set)) {	
			$key = $row["tool_id"];
			$description = $row["description"];
			$exam_id = $row["exam_id"];		
			$status = $row["status"];
					
			$exam_code = "";
			$result_setI = get_aca_exam_rec($exam_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$exam_code = $row["code"];		
			}
			$d_status = $status;
				$result_set74 = get_dim("status", $status);
				while ($row = mysqli_fetch_array($result_set74)) {
					$dim_key = $row["dim_key"]; 
					$d_status = $row["description"];
				}
		}
		
		echo "<form name=\"aca_tool\" method=\"post\" action=\"?id=".$id."&set=4&opt=" . $opt . "&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat5=".$cat5." \"enctype=\"multipart/form-data\">";

		echo '<table class="table table-striped">
		      <thead>
		        <tr>
		            <th colspan="2" style="background:#58595b;color:#FFFFFF;">Edit Subject</th>
                </tr>
              </thead>
              <tbody>';

		echo "<tr>";
		  echo "<td>Description:</td>";
		  echo "<td><input type=\"text\" name=\"description\" class=\"form-control reqd\" id=\"description\" placeholder=\"Enter description\" value=\"".$description."\"></td>";
		echo "</tr>";
		echo "<tr>";
		  echo "<td>Exam:</td>";
			echo "<td><select name=\"exam_id\" class=\"form-control reqd\" onchange=\"showTool(this.value)\" id=\"exam_id\">";
						  echo "<option value=\"".$exam_id."\">".$exam_code."</option>";
								$result_set19 = get_aca_exam(1);
								while ($row = mysqli_fetch_array($result_set19)) {
									$dim_key = $row["exam_id"]; 
									$display_name = $row["code"];
									echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								}
			echo "</select></td>";
		echo "</tr>";
		echo "<tr>";
		  echo "<td>Status</td>";
			echo "<td><select name=\"status\" class=\"form-control reqd\" onchange=\"showTool(this.value)\" id=\"status\">";
						  echo "<option value=\"".$status."\">".$d_status."</option>";
								  $result_set9 = get_dim_list("status");
								  while ($row = mysqli_fetch_array($result_set9)) {
									  $dim_key = $row["dim_key"]; 
									  $display_name = $row["description"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }
								  echo "</select></td>";
		echo "</tr>";
										
			// buttons
			$update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Submit</button>";			
			$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";
			
		  echo "<tr><td colspan=\"2\" class=\"text-center\">" . $update . " " . $cancel . "</td></tr>";
		  echo "</tbody>";
		  echo "</table>";
		echo "</form>";
		
		
		
		
		  echo "</div> "; 
	   echo " </div> "; 	
		
		
		echo "<p>&nbsp;</p>";	
		
	} // end of edit




	if ($set == 0) {										

		// get record count
		$p_records = 0;
		$result_setA = get_aca_tool_count(99, 0); // status 1 = valid, access 0 for all
		while ($row = mysqli_fetch_array($result_setA)) {
			$p_records = $row["record_count"]; 
		}
		$page_break = $page_break_d;
		$anchor = "x";
		$p_link = "?id=".$id."&opt=".$opt."&cat2=0&cat3=0&cat6=".$cat6."&key=".$key."&cat=".$cat."&cat4=".$cat4."&set=".$set;
		$p = page_nav_last($page, $page_break, $p_records, $p_link, $anchor);
		$n = "<a href=\"?id=".$id."&set=1&key=".$key."\">New</a>";
		$c = " ";
		$n =  "<button name=\"NewBut\" type=\"button\" value=\"New\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=1&opt=0&key=".$key."');return document.MM_returnValue\"/>New</button>&nbsp;&nbsp;";
		//echo "<p align=\"right\">" . $n." ".$p . "</p></br>";
		
		
		
		/*echo "<div class=\"panel panel-primary\"> ";
		  echo "<div class=\"panel-heading\">Subject Maintenance</div> "; 
		  echo "<div class=\"panel-body\">";*/
		  	
		echo "<table class=\"table table-striped\" style=\"width:100%\">";
		echo "<thead>";
		echo "<tr>";
		echo "<th colspan=\"5\" style=\"background:#58595b;color:#FFFFFF;line-height:2em;\">List of subjects<div style=\"float:right;\">" . $n." ".$p . "</div></th>";
		echo "</tr>";
		echo "<tr>";
			echo "<th>ID</th>";
			echo "<th>Description</th>";
			echo "<th>Exam</th>";
			echo "<th>Status</th>";
			echo "<th></th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		// get data
		$npage = $page;
		$result_set = get_aca_tool_list($npage, $page_break, 99);
		while ($row = mysqli_fetch_array($result_set)) {		
			$key = $row["tool_id"];
			$description = $row["description"];
			$exam_id = $row["exam_id"];		
			$status = $row["status"];
					
			$exam = "";
			$result_setI = get_aca_exam_rec($exam_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$exam_code = $row["code"];		
			}
			$d_status = $status;
				$result_set74 = get_dim("status", $status);
				while ($row = mysqli_fetch_array($result_set74)) {
					$dim_key = $row["dim_key"]; 
					$d_status = $row["description"];
				}
			
			$un = "<a href=\"?id=".$id."&set=3&opt=0&login=1&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&key=".$key."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$description."</a>";
			
		echo "<tr>";
			echo "<td>".$key."</td>";
			echo "<td>".$un."</td>";
			echo "<td>".$exam_code."</td>";
			echo "<td>".$d_status."</td>";
			echo "<td><a href=\"?id=".$id."&set=3&opt=0&login=1&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&key=".$key."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\" class=\"btn\">Edit</a></td>";
		echo "</tr>";
		}	
		echo "<tr>";
			echo "<td align=\"right\" colspan=\"5\">".$c." ".$n."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;".$p."</td>";
		echo "</tr>";


		echo "</tbody>";
		echo "</table>";
		  /*echo "</div> ";
	   echo " </div> "; */
	
		
	}


	if ($set == 1) { // create new		
		
		/*echo "<div class=\"panel panel-primary\"> ";
		  echo "<div class=\"panel-heading\">Subject Maintenance - Add a subject</div> "; 
		  echo "<div class=\"panel-body\">";*/
				
		
		echo "<form name=\"aca_tool\" method=\"post\" action=\"?id=".$id."&set=2&opt=" . $opt . "&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat5=".$cat5." \"enctype=\"multipart/form-data\">";
        echo '<table class="table table-striped">
		      <thead>
		        <tr>
		            <th colspan="2" style="background:#58595b;color:#FFFFFF;">Add Subject</th>
                </tr>
              </thead>
              <tbody>';
		echo "<tr>";
		  echo "<td>Description:</td>";
		  echo "<td><input type=\"text\" name=\"description\" class=\"form-control reqd\" id=\"description\" placeholder=\"Enter description\" value=\"\"></td>";
		echo "</tr>";
		  echo "<tr>";
		  echo "<td>Exam:</td>";
			echo "<td><select name=\"exam_id\" class=\"form-control reqd\" placeholder=\"Select an exam code\" onchange=\"showTool(this.value)\" id=\"exam_id\">";
						  echo "<option value=\"\"></option>";
								$result_set19 = get_aca_exam(1);
								while ($row = mysqli_fetch_array($result_set19)) {
									$dim_key = $row["exam_id"]; 
									$display_name = $row["code"];
									echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								}
			echo "</select></td>";
		echo "</tr>";
										
			// buttons
			$update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Submit</button>";			
			$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";
			
		  echo "<tr><td colspan=\"2\" class=\"text-center\">" . $update . " " . $cancel . "</td></tr>";
		  echo "</tbody>";
		  echo "</table>";
		echo "</form>";
		
		
		

	} // end of set = 1

echo '</div></div>';
	
} // end of user id = 31
?>
 