<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/09/18
 * Time: 11:06 PM
 */

echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>FAQ</h2><div class="colored-line-left"></div></div>
          <div class="clearfix"></div>
          <div class="row content-pad">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="alert alert-info">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;margin-top: .3em;"></span><strong>Feel free to send any questions we haven&apos;t answered in the section above to <a href="'.$support_email.'">'.$support_email.'</a>.</strong></p>
                </div>
            </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel-group" id="accordion">';

$sql = "select * from aca_faq order by faqo asc";

$result = mysqli_query($connection, $sql);

while ($row = mysqli_fetch_array($result)) {
    echo '<div class="panel panel-default" style="margin-bottom:20px !important;">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            '.$row["faqq"].'
                        </h4>
                    </div>
                    <div id="collapse'.$row["id"].'" class="panel-collapse">
                        <div class="panel-body">
                            '.$row["faqa"].'
                        </div>
                    </div>
                </div>';
}
            echo '</div>
            </div>
        </div>
        ';