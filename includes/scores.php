
<?php	
// student test scores id = 41


// exam - cat (key)
// tool / subject - cat6
// version - cat2
// user - cat3
// create_date - test date - cat4
// instructor - cat5

// set variables
$cat3 = $userx;		
$page_break = $page_break_d;
$no_of_list = $page_break_d;		
//if ($id == 41 || (logged_in() && $aca_accesslevel >= 1)) {


    if (logged_in() && $aca_accesslevel >= 1) {
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Dashboard</h2><div class=\"colored-line-left\"></div></div>
			<div class=\"clearfix\"></div>";
        echo "<div class=\"row content-pad\">";

    }
		
	if ($trial_expire >= date("Y-m-d")){
		$resultt = get_aca_exam_trial(0,12,1,$userx);
		
		echo '<input type="hidden" id="trialuserid" value="'.$_SESSION["aca_userx"].'" />';
		
		echo "<div class=\"alert alert-info\" role=\"alert\">";
        echo "<span class=\"alert-link\">Welcome to the BlackboardBI Training platform. You can subscribe to a Trial exam until ".$trial_expire." by clicking <a href=\"?id=118&login=1&opt=1&set=4\" style=\"color:#245269;text-decoration:underline;\">here</a>.</span>";
        echo "</div>";
		/*echo '<table class="table table-striped">';
		echo '<tr><th colspan="4" style="background:#58595b;color:#FFFFFF;">Available Trial Exams</th></tr>';
		while ($row = mysqli_fetch_array($resultt))
		{
			$trials = get_aca_trial_subscribed($row["exam_id"],$userx);
			while($rowt = mysqli_fetch_array($trials)){
				$trialcnt = $rowt["cnt"];
			}
			$key = $row["exam_id"];
			$exam_image = $row["exam_image"];
			$code = $row["code"];
			$description= $row["description"];
			$cost = $row["cost"];
			$exam_id = $row["exam_id"];
				echo '<tr>
					<td><img src="' . $exam_image . '" height="20" /></td>
					<td><strong>' . $code . '</strong> - ' . $description . '</td>
					<td><a href="?id=2&key='.$exam_id.'" class="btn btn-default">Read more</a>&nbsp;';
					if($trialcnt > 0){
						echo '<a href="javascript:void(0)" class="btn">Take Test</a>';
					} else {
						echo '<a href="javascript:void(0)" onclick="TrialSubscribe('.$exam_id.')" class="btn">Subscribe</a>';
					}
				echo '</tr>';
		}
		echo '</table>';*/
	}

	$exam_set = get_exam_count($_SESSION["aca_userx"]);
	while($row = mysqli_fetch_array($exam_set)){
		$examcnt = $row["cnt"];
		
	}
	
	if($examcnt === "0"){
		$result1 = get_aca_exam_list(0,12,1);
		
		echo '<table class="table table-striped">';
		echo '<tr><th colspan="4" style="background:#58595b;color:#FFFFFF;">Available Exams</th></tr>';
		while ($row = mysqli_fetch_array($result1))
		{
			$key = $row["exam_id"];
			$exam_image = $row["exam_image"];
			$code = $row["code"];
			$description= $row["description"];
			$cost = $row["cost"];
			$exam_id = $row["exam_id"];
				echo '<tr>
					<td><img src="' . $exam_image . '" height="20" /></td>
					<td><strong>' . $code . '</strong> - ' . $description . '</td>
					<td>'.$_SESSION["currencySymbol"].' '.formatMoney(($_SESSION["currencyconverter"] * $cost),2).'</td>
					<td><a href="?id=2&key='.$exam_id.'" class="btn btn-default">Read more</a>&nbsp;<a href="?id=21&cat='.$cat.'&login=1&set=11&opt=0&key='.$key.'" class="btn">Subscribe</a>
					</tr>';
		}
		echo '</table>';
	} else {
    // lookup descriptions for selected filters
    if ($cat <> 0) { // exam
        $c1 = $cat;
        $result_set1 = get_aca_exam_rec($cat);
        while ($row = mysqli_fetch_array($result_set1)) {
            $c1 = $row["code"];
        }
    } else { $c1 = ""; }

    if ($cat6 <> 0) { // subject / tool
        $c6 = $cat6;
        $result_set6 = get_aca_tool_rec($cat6);
        while ($row = mysqli_fetch_array($result_set6)) {
            $c6 = $row["description"];
        }
    } else { $c6 = ""; }

    if ($cat2 <> "0" || $cat2 <> 0) { // subject / tool
        $c2 = $cat2;
    } else { $c2 = ""; }

    // force user to the signed-in student number
    $cat3 = $userx;
    if ($cat3 <> 0) { // user name
        $c3 = $cat3;
        $result_set3 = get_aca_user_rec($cat3);
        while ($row = mysqli_fetch_array($result_set3)) {
            $c3 = $row["username"];
        }

    } else { $c3 = ""; }
	
    if ($cat4 <> 0) { // create date
        $c4 = $cat4;
    } else { $c4 = ""; }

    // report output  -------------------------------
    echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";
echo "<table class=\"table table-striped\">";

echo "<thead>";

echo "<tr><th colspan=\"5\" style=\"background:#58595b;color:#FFFFFF;\">Question Trend</th></tr></thead><tbody><tr><td>";

	// SW added 2017/01/15
	
	
	if($aca_accesslevel == 1){
		  $cat3 = $_SESSION['aca_userx']; 
		  $m = "";
		  $m .= "Key: ".$key."</br>";
		  $m .= "CAT: ".$cat."</br>";
		  $m .= "CAT2: ".$cat2."</br>";
		  $m .= "CAT3: ".$cat3."</br>";
		  $m .= "CAT4: ".$cat4."</br>";
		  $m .= "CAT5: ".$cat5."</br>";
		  $m .= "CAT6: ".$cat6."</br>";
		  $m .= "CAT7: ".$cat7."</br>";
		  $m .= "Access Level: ".$aca_accesslevel."</br>";
		  $m .= "UL: ".$cat3."</br>";
      }
	 
		
	  
    // get data
    $tq = 0;
    $ts = 0;
    $tp = 0;
    $barh = 0;
    $style_row = " progress-bar-striped ";
    $pb = "progress-bar progress-bar-success " . $style_row;
    // get data
    if ($aca_accesslevel == 1) { $result_setXX = get_aca_option_results_trend_user($cat3);
	} else {
		$result_setXX = get_aca_option_results_trend($key, $cat6, $cat2, $cat3, $cat4, $cat5, $cat7);
		
	}
    while ($row = mysqli_fetch_array($result_setXX)) {
        $tq = $row["questions"];
        $ts = $row["score"];
        $tp = ($ts / $tq)*100;
        $date = $row["date"];
        $da = substr($date,5,2)."/".substr($date,7,5);
        if ($tp < 50) { $pb = "progress-bar progress-bar-danger " . $style_row; }
        if ($tp >= 50 && $tp < 60) { $pb = "progress-bar progress-bar-warning " . $style_row; }
        if ($tp >= 60) { $pb = "progress-bar progress-bar-success " . $style_row; }
        echo "<div class=\"progress\">";
        echo "&nbsp;" . $date . " <div class=\"".$pb."\" role=\"progressbar\"
  aria-valuenow=\"".$tp."\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:".$tp."%\">".number_format($tp,0,",",",")."% (".$tq.")";
        echo "</div>";
        echo "</div>";
    }
    echo "</td></tr></tbody></table></div>";

    echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";			
			
    // --- Summary by Subject -----------------------

echo "<table class=\"table table-striped\" width=\"100%\">";

echo "<thead>";

echo "<tr><th colspan=\"5\" style=\"background:#58595b;color:#FFFFFF;\">Summary by subject</th></tr>";
    echo "<tr>";
    echo "<th>Exam</th>";
    echo "<th>Subject</th>";
    echo "<th>Questions</th>";
    echo "<th>Correct</th>";
    echo "<th>%</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    // set variables
    $tt_qtot = 0;
    $tt_score = 0;
    $tt_ps = 0;

    // get data
    if ($aca_accesslevel == 1) { $result_setX1 = get_aca_option_results_tool_user($cat3); } else {
		$result_setX1 = get_aca_option_results_tool_instructor($key, $cat6, $cat2, $cat3, $cat4, $cat5, $cat7);
	}    
    while ($row = mysqli_fetch_array($result_setX1)) {
        $exam_id = $row["exam_id"];
        //$instructor = $row["instructor"];
        $exam_code = $row["code"];
        $tool = $row["tool"];
        $tool_id = $row["tool_id"];
        $t_qtot = $row["questions"];
        $t_score = $row["score"];
        $t_ps = ($t_score / $t_qtot)*100;
        $tt_qtot = $tt_qtot + $t_qtot;
        $tt_score = $tt_score + $t_score;

        $resultSExam = "<a href=\"?id=".$id."&set=0&opt=0&login=1&cat=".$exam_id."&cat6=".$cat6."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$exam_code."</a>";
        $resultSSubject = "<a href=\"?id=".$id."&set=0&opt=".$opt."&key=".$key."&login=1&cat=".$cat."&cat6=".$tool_id."&cat2=".$cat2."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$tool."</a>";

        echo "<tr>";
        echo "<td>".$resultSExam."</td>";
        echo "<td>".$resultSSubject."</td>";
        echo "<td>".$t_qtot."</td>";
        echo "<td>".$t_score."</td>";
        echo "<td>".number_format($t_ps,0,",",",")."%</td>";
        echo "</tr>";
    }
    if ($tt_qtot == 0) { $tt_ps = 0; } else { $tt_ps = ($tt_score / $tt_qtot)*100; }
    echo "<tr>";
    echo "<td></td>";
    echo "<td></td>";
    echo "<td>".$tt_qtot."</td>";
    echo "<td>".$tt_score."</td>";
    echo "<td>".number_format($tt_ps,0,",",",")."%</td>";
    echo "</tr>";
    echo "</tbody>";
    echo "</table>";
    echo "</div> ";
    echo " </div> ";
    echo " </div> ";

    // ------ end Summary by Subject ------------------------------

//} // end of id = 41
}
?>
 </div>
 </div>
 </div>
 <script>
 
 function TrialSubscribe(id){
		 var id = id;
		 var userid = $("#trialuserid").val();
		 var userx = $("#trialuserid").val();
		 $.post( "ajax/ajax_trial.php", { action:"TrialSubscribe",exam_id:id,user_id:userid,userx:userx }, function(data){
					if(data.result == 1){
						console.log("Success");
					} else {
						
					}
				},
				"json");
	 }
	 
 </script>