<?php

 
 $display_footer_widgets    = onetone_option('enable_footer_widget_area',1); 
 $footer_columns            = onetone_option('footer_columns','4'); 
 $copyright_text            = onetone_option('copyright',''); 
 $display_copyright_bar     = onetone_option('display_copyright_bar','yes'); 
 
?>
<!--Footer-->
		<footer>
            <div class="footer-widget-area">
                <div class="container">
                    <div class="row">
                    <?php 
					for( $i=1;$i<=$footer_columns; $i++ ){
					?>
                    <div class="col-md-<?php echo 12/$footer_columns; ?>">
                    <?php
							if(is_active_sidebar("footer_widget_".$i)){
	                           dynamic_sidebar("footer_widget_".$i);
                               }
							?>
                    </div>
                    
                    <?php }?>
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<a href="">facebook</a>
						</div>
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<a href="">facebook</a>
						</div>
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<a href="">facebook</a>
						</div>
                    </div>
                </div>
            </div>
            <?php if( $display_copyright_bar == 'yes' ):?>
			<div class="footer-info-area" role="contentinfo">
				<div class="container">	
                
					<div class="site-info">
					  <?php 
							
							echo do_shortcode($copyright_text);
							?>
					</div>
                     
				</div>
			</div>	
            
            <?php endif; ?>		
		</footer>
	</div>
	<!-- jQuery JS -->
<script src="<?php echo esc_url( get_template_directory_uri().'/js/jquery-1.11.1.js'); ?>"></script>

<!-- Modernizr JS --> 
<script src="<?php echo esc_url( get_template_directory_uri().'/js/modernizr-2.6.2.min.js'); ?>"></script>

<!--Bootatrap JS-->
<script src="<?php echo esc_url( get_template_directory_uri().'/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>

<!-- REVOLUTION Slider  -->
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri().'/vendor/rs-plugin/js/jquery.themepunch.plugins.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri().'/vendor/rs-plugin/js/jquery.themepunch.revolution.js'); ?>"></script>

<!-- Shuffle JS -->
<script src="<?php echo esc_url( get_template_directory_uri().'/js/jquery.shuffle.min.js'); ?>"></script>

<!-- mmenu -->
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri().'/vendor/mmenu/js/jquery.mmenu.min.js'); ?>"></script>

<!-- Owl Carosel -->
<script src="<?php echo esc_url( get_template_directory_uri().'/vendor/owl/js/owl.carousel.min.js'); ?>"></script>
<script src="<?php echo esc_url( get_template_directory_uri().'/js/wow.min.js'); ?>"></script>

<!-- waypoints JS-->
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>

<!-- Counterup JS -->
<script src="<?php echo esc_url( get_template_directory_uri().'/js/jquery.counterup.min.js'); ?>"></script>

<!-- Easing JS -->
<script src="<?php echo esc_url( get_template_directory_uri().'/js/jquery.easing.min.js'); ?>"></script>

<!-- Smooth Scroll JS -->
<script src="<?php echo esc_url( get_template_directory_uri().'js/scrolling-nav.js'); ?>"></script>
<script src="<?php echo esc_url( get_template_directory_uri().'/js/smoothscroll.min.js'); ?>"></script>

<!-- Custom Script JS -->
<script src="<?php echo esc_url( get_template_directory_uri().'/js/script.js'); ?>"></script>



</body>
</html>