<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/09/18
 * Time: 11:06 PM
 */

echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>Demo</h2><div class="colored-line-left"></div></div>
          <div class="clearfix"></div>
          <div class="row content-pad">';

            $sql = "select * from aca_video where status = '1'";

            $result = mysqli_query($connection, $sql);

            while ($row = mysqli_fetch_array($result)) {
                echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="row" style="border-bottom:1px solid #777777;padding:20px 0px 20px;">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div style="width:100%;height:auto;overflow:hidden;">
                         <video controls style="width:100%">
                          <source src="' . str_replace('../','',$row["mp4_file"]) . '" type="video/mp4" />
                          <source src="' . str_replace('../','',$row["ogv_file"]) . '" type="video/webm" />
                          <source src="' . str_replace('../','',$row["webm_file"]) . '" type="video/ogg" />
                          Your browser does not support the video tag.
                        </video>
                        </div>
                    </div>
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <h4>' . $row["video_title"] . '</h4>
                        <p>' . $row["video_description"] . '</p>
                    </div>
                </div>
            </div>';
            }
        echo '</div>';