<?php	
// practice id = 11

/*
1. setect exam.
2. select question no
3. display question
4. mark question


set
---
0	display form and select a exam
2	display question, mark and display result if opt = 1
10	get next question
30
90	display summary when test complete

	
*/

if ($id == 11) {

    if (logged_in() && $aca_accesslevel >= 1) {
        echo "
			<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Practice a Question</h2><div class=\"colored-line-left\"></div></div>
			<div class=\"clearfix\"></div>
	  ";
    }
    echo "<div class=\"row content-pad\">
<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";

    $teston = 0;
	// test selected, create session
	if ($set == 1) { 				
		if (isset($_POST['submit'])) { 
		  $userexam = trim(mysql_prep($_POST['exam_id'])); 
		  $qno = trim(mysql_prep($_POST['qno']));
		  $gip = get_ip(0);
		  
		  // set session variables								
		  $_SESSION['aca_userthis'] 	= "xxxxxx";			// practise
		  $_SESSION['aca_userexam'] 	= $userexam;		// exam id
		  $_SESSION['aca_q_count'] 		= 1;				// question count
		  $_SESSION['aca_subject'] 		= 0;				// subject / tool id
		  $_SESSION['aca_q_tot'] 		= 1;				// current question number / answered
		  $_SESSION['aca_q'] 			= $qno;				// practice question
		  $userx 			= $_SESSION['aca_userx']; 
		  $username 		= $_SESSION['aca_username']; 
		  $aca_name 		= $_SESSION['aca_name']; 
		  $userthis 		= $_SESSION['aca_userthis']; 
		  $userexam 		= $_SESSION['aca_userexam'];  
		  $qno	 			= $_SESSION['aca_q'];  
		  $q_tot 			= $_SESSION['aca_q_tot']; 
		  $aca_subject 		= $_SESSION['aca_subject']; 
		  $aca_instructor 	= $_SESSION['aca_instructor'];  // this should come from the user record by exam (user_exam table required)
		  
		  $set = 2; 	// go to display questions
		  $teston = 1;	// set the flag that test was selected
		  
		} // end of post

			if ($test_aca == 1) {
				echo "Test end of set=1 :: teston = 1 and session variables set</br>";
				echo "Username: ".$username. "</br>";
				echo "UserThis: ".$userthis. "</br>";
				echo "Snow: ".$snow. "</br>";
				echo "Set     : ".$set. "</br>";
				//echo "Q       : ".$q. "</br>";
				echo "Instruct: ".$aca_instructor. "</br>";
				//echo "QCnt    : ".$q_count. "</br>";
				echo "Login   : ".$login. "</br>";
				echo "Teston  : ".$teston. "</br></br>";
			}

		
	} // end of set = 1 
	
			
	// users must be logged in and selected a test
	if (test_on() || $teston == 1) {
	  
	  	// set session variables														
		$username 		= $_SESSION['aca_username']; 	// 
		$aca_name 		= $_SESSION['aca_name']; 		// 
		$aca_instructor = $_SESSION['aca_instructor']; 	// 
		$userx 			= $_SESSION['aca_userx'];  		// user_id
		$userthis 		= $_SESSION['aca_userthis'];  	// user_id
		$userexam 		= $_SESSION['aca_userexam'];  	// user_id
		$q_count 		= $_SESSION['aca_q_count'];  	// number of questions to answer
		$q_tot 			= $_SESSION['aca_q_tot'];  		// current number of questions answered
		$qno 			= $_SESSION['aca_q'];  			// practice questions
		$aca_subject 	= $_SESSION['aca_subject'];  	// current number of questions answered
		$q_display 		= $q_tot;
		$key			= $userexam;
			
		if ($test_aca == 1) {
			echo "Test set=2 :: session variables set</br>";
			echo "Username: ".$username. "</br>";
			echo "UserThis: ".$userthis. "</br>";
			echo "Set: ".$set. "</br>";
			echo "Q: ".$q. "</br>";
			echo "QNo: ".$qno. "</br>";
			echo "QCnt: ".$q_count. "</br></br>";
		}
	
		// get exam heading							
		$result_set1 = get_aca_exam_rec($key);
		while ($row = mysqli_fetch_array($result_set1)) {		
			$key = $row["exam_id"];
			$code = $row["code"];	
			$description = $row["description"];	
			$versions = $row["versions"];
			$report_question = $row["report_question"];
		}
		// get total number of questions
		$p_records = 1; 
		
		
		// set questions variables							
		$oac = "";
		$obc = "";
		$occ = "";
		$odc = "";
		$oec = "";
		$ofc = "";
		$msg = "";
		$cora = "";
		$but = "but";
		$up = "";
		$upErr = "";
		$fa = "alert alert-info";
		$fb = "alert alert-info";
		$fc = "alert alert-info";
		$fd = "alert alert-info";
		$fe = "alert alert-info";
		$ff = "alert alert-info";
		
		
		
		
		// mark question and update stats - do this when question is answered (on Submit)
		if ($set == 3 && $key <> 0 && $opt == 1) {		
			$answ_a = 0;			
			$answ_b = 0;				
			$answ_c = 0;				
			$answ_d = 0;				
			$answ_e = 0;				
			$answ_f = 0;	
			$correct_cnt = 0;
			$answer_cnt = 0;			
			
						
			if (isset($_POST['submit'])) {
				$answ_a = (isset($_POST['answ_a']) ? trim(mysql_prep($_POST['answ_a'])) : 0);
				$answ_b = (isset($_POST['answ_b']) ? trim(mysql_prep($_POST['answ_b'])) : 0);
				$answ_c = (isset($_POST['answ_c']) ? trim(mysql_prep($_POST['answ_c'])) : 0);
				$answ_d = (isset($_POST['answ_d']) ? trim(mysql_prep($_POST['answ_d'])) : 0);
				$answ_e = (isset($_POST['answ_e']) ? trim(mysql_prep($_POST['answ_e'])) : 0);
				$answ_f = (isset($_POST['answ_f']) ? trim(mysql_prep($_POST['answ_f'])) : 0);
				$tot_answ = $answ_a + $answ_b + $answ_c + $answ_d + $answ_e + $answ_f; // total number of questions answered
				
				// set session count for total number of questions presented
				$q_display = $q_tot;
				$_SESSION['aca_q_tot'] = $_SESSION['aca_q_tot'] + 1;
				$q_tot = $_SESSION['aca_q_tot'];
			}
			
			
			// get record results and print answers			
			//$result_set = get_aca_question_rec($qno, 0); // break, current page, status (99 for all)
			$result_set = get_aca_question_rec3($qno, 0, $_SESSION['aca_userexam']); // SW updated on 2017/01/03 --question_no, tool, exam
			while ($row = mysqli_fetch_array($result_set)) {
				$option_a_answer = $row["option_a_answer"];	
				$option_b_answer = $row["option_b_answer"];	
				$option_c_answer = $row["option_c_answer"];	
				$option_d_answer = $row["option_d_answer"];	
				$option_e_answer = $row["option_e_answer"];	
				$option_f_answer = $row["option_f_answer"];
				$correct_cnt = $option_a_answer + $option_b_answer + $option_c_answer + $option_d_answer + $option_e_answer + $option_f_answer;
				if ($option_a_answer == 1) { 
					$fa = "alert alert-success"; 
					$cora .= "A "; }
				if ($option_b_answer == 1) { 
					$fb = "alert alert-success"; 
					$cora .= "B "; }
				if ($option_c_answer == 1) { 
					$fc = "alert alert-success"; 
					$cora .= "C "; }
				if ($option_d_answer == 1) { 
					$fd = "alert alert-success"; 
					$cora .= "D "; }
				if ($option_e_answer == 1) { 
					$fe = "alert alert-success"; 
					$cora .= "E "; }
				if ($option_f_answer == 1) { 
					$ff = "alert alert-success"; 
					$cora .= "F "; }
					
			}
			
			//echo "<p>TEST^^^^^^^^^^^^^^: Qtot: ".$q_tot."  ::: QNO: ".$qno." -- ".$correct_cnt."</p>";		
			
			// get the first valid class for this user and exam - for reporting on class		
			$class_id = 0;			
			$result_setQ = get_aca_class_exam_user(1, $userx, $key, $today); // $status, $cat, $exam_id, $expiry_date  1, $userx, $key, $today)
			while ($row = mysqli_fetch_array($result_setQ)) {
				$class_id = $row["class_id"];	
			}			
			//echo "<p>TEST^^^^^^^^^^^^^^: Class: ".$class_id."</p>";
			if ($test_aca == 1) {
			echo "Test</br>";
			  echo "A: ".$answ_a." " .$option_a_answer. "</br>";
			  echo "B: ".$answ_b." " .$option_b_answer. "</br>";
			  echo "C: ".$answ_c." " .$option_c_answer. "</br>";
			  echo "D: ".$answ_d." " .$option_d_answer. "</br>";
			  echo "E: ".$answ_e." " .$option_e_answer. "</br>";
			  echo "F: ".$answ_f." " .$option_f_answer. "</br>";
			  echo "Set: ".$set. "</br>";
			  echo "Q: ".$q. "</br>";
			  echo "Login: ".$login. "</br>";
			}
			
			if ($answ_a == 1 && $option_a_answer == 1) { $answer_cnt = $answer_cnt + 1; }
			if ($answ_b == 1 && $option_b_answer == 1) { $answer_cnt = $answer_cnt + 1; }
			if ($answ_c == 1 && $option_c_answer == 1) { $answer_cnt = $answer_cnt + 1; }
			if ($answ_d == 1 && $option_d_answer == 1) { $answer_cnt = $answer_cnt + 1; }
			if ($answ_e == 1 && $option_e_answer == 1) { $answer_cnt = $answer_cnt + 1; }	
			if ($answ_f == 1 && $option_f_answer == 1) { $answer_cnt = $answer_cnt + 1; }	
			
			if ($answ_a == 1) {  $oac = "checked=\"checked\""; } else { $oac = ""; }
			if ($answ_b == 1) {  $obc = "checked=\"checked\""; } else { $obc = ""; }
			if ($answ_c == 1) {  $occ = "checked=\"checked\""; } else { $occ = ""; }
			if ($answ_d == 1) {  $odc = "checked=\"checked\""; } else { $odc = ""; }
			if ($answ_e == 1) {  $oec = "checked=\"checked\""; } else { $oec = ""; }
			if ($answ_f == 1) {  $ofc = "checked=\"checked\""; } else { $ofc = ""; }	
				
				
					
			if ( $correct_cnt == $answer_cnt && $tot_answ == $correct_cnt) {
                $msg = "<div class=\"alert alert-success\"><p class=\"valid\" style=\"font-size:20px;\"><span class=\"glyphicon glyphicon-ok\"></span> Correct</p></div>";
				// update table
				$score = 1;
				//$class_id = 0;
				$test_type = 2; // practice
				$create_date = $today;
				$query = "INSERT INTO aca_option( ";
				$query .= " exam_id, question_id, score, "; 
				$query .= " create_date, creator_id, username, userthis, user_id, test_type, class_id ) ";
				$query .= " VALUES (   ";
				$query .= " '{$key}', '{$q}', '{$score}', ";  
				$query .= " '{$create_date}', '{$userx}', '{$username}', '{$userthis}', '{$userx}', '{$test_type}', {$class_id} ) " ;		
				$result = mysqli_query( $connection, $query);
				if (mysqli_affected_rows($connection) == 1) {	
					$mmm = 1;
					$but = "but_g";
				}
			} else {
                $msg = "<div class=\"alert alert-danger\"><p class=\"invalid\" style=\"font-size:20px;\"><span class=\"glyphicon glyphicon-remove\"></span> Incorrect. The correct answer is ".$cora."</p></div>";
				// update table
				$score = 0;
				//$class_id = 0;
				$test_type = 2; // practice
				$create_date = $today;
				$query = "INSERT INTO aca_option( ";
				$query .= " exam_id, question_id, score, "; 
				$query .= " create_date, creator_id, username, userthis, user_id, test_type, class_id ) ";
				$query .= " VALUES (   ";
				$query .= " '{$key}', '{$q}', '{$score}', ";  
				$query .= " '{$create_date}', '{$userx}', '{$username}', '{$userthis}', '{$userx}', '{$test_type}', {$class_id}  ) " ;		
				$result = mysqli_query( $connection, $query);
				if (mysqli_affected_rows($connection) == 1) {	
					$mmm = 0;
					$but = "but_r";
				}
			}
		
			// update recorded question error if exists	
			if ($cat7 <> 0 && $qq <> 0) {
				// insert into error log
				$create_date = $today;
				$query = "INSERT INTO aca_log( ";
				$query .= " exam_id, question_id, qerr, "; 
				$query .= " create_date, creator_id, username, userthis) ";
				$query .= " VALUES (   ";
				$query .= " '{$key}', '{$qq}', '{$cat7}', ";  
				$query .= " '{$create_date}', '{$userx}', '{$username}', '{$userthis}' ) " ;		
				$result = mysqli_query( $connection, $query);
				if (mysqli_affected_rows($connection) == 1) {	
					$mmm = 1;
					$but = "but_g";
				}
				
			}
			
			$set = 2; // question was tested and results updated
			$opt = 3;  // reset opt to 3 - question marked and answer must be displayed
		
			
			if ($test_aca == 1) {
			echo "Test set=2 opt=1 mark question</br>";
			echo "Q: ".$q. "</br>";
			echo "Opt: ".$opt. "</br>";
			echo "Set: ".$set. "</br></br>";
		}
						
		} // end of set == 3 and opt == 1 question marked
		
		
		
		if ($test_aca == 1) {
			echo "Test before set 10</br>";
			echo "Q: ".$q. "</br>";
			echo "Opt: ".$opt. "</br>";
			echo "Set: ".$set. "</br></br>";
		}
		
			  
			  
 
		// display question
		if ($set == 2) {	
		
			  
		  // set buttons						
		  if ($opt == 3) { 
		  // results updated, view and go to more practice	
			  $dis = "disabled=\"disabled\"";
			  		
			  // if no more questions
			  if ($q_tot > $p_records || $q_tot > $q_count) { 
				 // $up = "<button name=\"FinishBut\" type=\"button\" value=\"More\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&q=0&qq=".$q."&login=1&set=0&opt=0&key=".$key."');return document.MM_returnValue\"/>Practice more</button>";
				  $up = "<button name=\"FinishBut\" type=\"button\" value=\"More\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&q=0&qq=".$q."&login=1&set=0&testout=1&opt=99&key=0');return document.MM_returnValue\"/>Practice more</button>";
				  //&testout=1&opt=99&key=0
                  if($report_question == "0"){
                      $upErr = "";
                  } else {
                      $upErr = "<button name=\"FinishBut\" type=\"button\" value=\"Report Question and more\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=" . $id . "&q=0&login=1&set=0&qq=" . $q . "&cat7=1&opt=0&key=" . $key . "');return document.MM_returnValue\"/>Report Question and more</button>";
                  }
			  }	
		  } // end if opt = 3
		  
			  
		  // get questions	if new / next question selected	
		  if ($opt == 0) {
				  $cat6 = 0;
				  
				  $sub = 0;
				  $up = "<button name=\"submit\" type=\"submit\" value=\"Submit\" class=\"btn btn-default\">Submit</button>";	
				  $upErr = "";
				  $dis = "";
					  
			} // end if opt = 0
			
		 //}
			  
			  
			  
			  
			  
			  

			  
			  if ($test_aca == 1) {
				  echo "Test before question form</br>";
				  echo "Set: ".$set. "</br>";
				  echo "Q: ".$q. "</br>";
				  //echo "Qno: ".$aca_qno. "</br>";
				  echo "Q Tot: ".$q_tot. "</br>";
				  echo "Q Cnt: ".$q_count. "</br>";
				  echo "Login: ".$login. "</br>";
			  }
				  
				  
			  // print the question form												
                    if(isset($msg) && $msg != '') {
                        echo $msg;
                    }
                    echo "<form name=\"academy\" method=\"post\" action=\"?id=".$id."&q=".$qno."&set=3&opt=1&login=1&key=".$key." \"enctype=\"multipart/form-data\">";
				  
				  echo "<table class=\"table\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
				  // get data			
				  //$npage = $page;
				  $result_set = get_aca_question_rec3($qno, 0, $_SESSION['aca_userexam']); // question number and subject/tool or 0 for all
				  while ($row = mysqli_fetch_array($result_set)) {		
					  $question_id = $row["question_id"];
					  $exam_id = $row["exam_id"];		
					  $question_no = $row["question_no"];	
					  $question_text = $row["question_text"];	
					  $question_note = $row["question_note"];
					  $option_a_text = $row["option_a_text"];
					  $option_b_text = $row["option_b_text"];	
					  $option_c_text = $row["option_c_text"];	
					  $option_d_text = $row["option_d_text"];
					  $option_e_text = $row["option_e_text"];
					  $option_f_text = $row["option_f_text"];
					  $option_a_answer = $row["option_a_answer"];	
					  $option_b_answer = $row["option_b_answer"];	
					  $option_c_answer = $row["option_c_answer"];	
					  $option_d_answer = $row["option_d_answer"];	
					  $option_e_answer = $row["option_e_answer"];		
					  $option_f_answer = $row["option_f_answer"];
					  $question_help = $row["question_help"];	
					  $create_date = $row["create_date"];			
					  
					  
				  // print list detail	
				  echo "<tr>";
						  echo "<th align=\"left\" style=\"background:#58595b;color:#FFFFFF;\"><strong>Question ".$q_display."</strong></th>"; // $question_id
				  echo "</tr>";
				  if(isset($question_text) && $question_text != '') {
                      echo "<tr>";
                      echo "<td align=\"left\"><strong>Q. " . $question_text . "</strong></td>";
                      echo "</tr>";
                  }
                  if(isset($question_note) && $question_note != '') {
                      echo "<tr>";
                      echo "<td align=\"left\">" . $question_note . "</td>";
                      echo "</tr>";
                  }
				  echo "</table>";
				  
				  $oa = "<input class=\"qu\" type=\"checkbox\" value=\"1\" name=\"answ_a\" id=\"answ_a\" ".$oac.$dis."  />";
				  $ob = "<input class=\"qn\" type=\"checkbox\" value=\"1\" name=\"answ_b\" id=\"answ_b\" ".$obc.$dis." />";
				  $oc = "<input class=\"qu\" type=\"checkbox\" value=\"1\" name=\"answ_c\" id=\"answ_c\" ".$occ.$dis." />";
				  $od = "<input class=\"qn\" type=\"checkbox\" value=\"1\" name=\"answ_d\" id=\"answ_d\" ".$odc.$dis." />";
				  $oe = "<input class=\"qu\" type=\"checkbox\" value=\"1\" name=\"answ_e\" id=\"answ_e\" ".$oec.$dis." />";
				  $of = "<input class=\"qn\" type=\"checkbox\" value=\"1\" name=\"answ_f\" id=\"answ_f\" ".$ofc.$dis." />";
					  
				  
				  echo "<table class=\"table questions\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout: fixed\">";
				  /*echo "<tr>";
					  echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
				  echo "</tr>";	*/
				  echo "<tr>";
						  echo "<td class=\"qu ".$fa."\" align=\"center\" style=\"width:4% !important;max-width:4% !important;min-width:4% !important;\">A</td>";
                      echo "<td class=\"qu ".$fa."\" align=\"center\" style=\"width:4% !important;max-width:4% !important;min-width:4% !important;\">&nbsp;&nbsp;".$oa."</td>";
                      echo "<td class=\"qu ".$fa."\" align=\"left\">".nl2br($option_a_text)."</td>";

				  echo "</tr>";
				  /*echo "<tr>";
					  echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
				  echo "</tr>";*/
				  echo "<tr>";
						  echo "<td class=\"qn ".$fb."\" align=\"center\">B</td>";
                      echo "<td class=\"qn ".$fb."\" align=\"center\">&nbsp;&nbsp;".$ob."</td>";
                      echo "<td class=\"qn ".$fb."\" align=\"left\">".nl2br($option_b_text)."</td>";

				  echo "</tr>";
				  if ($option_c_text <> "") {
					/*echo "<tr>";
						echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
					echo "</tr>";*/
					echo "<tr>";
							echo "<td class=\"qu ".$fc."\" align=\"center\">C</td>";
                      echo "<td class=\"qu ".$fc."\" align=\"center\">&nbsp;&nbsp;".$oc."</td>";
                      echo "<td class=\"qu ".$fc."\" align=\"left\">".nl2br($option_c_text)."</td>";

					echo "</tr>";
				  }
				  if ($option_d_text <> "") {
					/*echo "<tr>";
						echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
					echo "</tr>";*/
					echo "<tr>";
							echo "<td class=\"qn ".$fd."\" align=\"center\">D</td>";
                      echo "<td class=\"qn ".$fd."\" align=\"center\">&nbsp;&nbsp;".$od."</td>";
                      echo "<td class=\"qn ".$fd."\" align=\"left\">".nl2br($option_d_text)."</td>";

					echo "</tr>";
				  }
				  if ($option_e_text <> "") {
					/*echo "<tr>";
						echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
					echo "</tr>";*/
					echo "<tr>";
							echo "<td class=\"qu ".$fe."\" align=\"center\">E</td>";
                      echo "<td class=\"qu ".$fe."\" align=\"center\">&nbsp;&nbsp;".$oe."</td>";
                      echo "<td class=\"qu ".$fe."\" align=\"left\">".nl2br($option_e_text)."</td>";

					echo "</tr>";
				  }
				  if ($option_f_text <> "") {
					/*echo "<tr>";
						echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
					echo "</tr>";*/
					echo "<tr>";
							echo "<td class=\"qn ".$ff."\" align=\"center\">F</td>";
                      echo "<td class=\"qn ".$ff."\" align=\"center\">&nbsp;&nbsp;".$of."</td>";
                      echo "<td class=\"qn ".$ff."\" align=\"left\">".nl2br($option_f_text)."</td>";

					echo "</tr>";
				  }
				  } // end of result_set
						  
				  // table footer	
				  echo "</tr>";
				  echo "<tr>";
					  echo "<td align=\"left\" class=\"text-center\" colspan=\"3\">".$upErr." ".$up."</td>";
				  echo "</tr>";
				  // end of table
				  echo "</table>";	
				  echo "</form>";			
				  
		  
	  
	  } // end of set = 1 and create session
	  
	  
	  
	
	if ($set <> 0) { // select a test		  				
	  // print reset button and question number	
	  $reset = "<div class=\"col-sm-12 text-center\"><button type=\"button\" class=\"btn btn-danger\" onclick=\"MM_goToURL('parent','?id=11&login=1&opt=0&key=0&set=0&testout=1');return document.MM_returnValue\"/>Reset Test</button></div>";
		if ($set == 2) {		
			$qnprint = 	"";
			//$qnprint = 	"[".$question_id."]";
			if ($question_id == 0) { $qnprint = ""; }
		  	echo "<p>".$reset." ".$qnprint." </p>";
		} else { echo "<p>".$reset."</p>"; }
		
		
	} // end of set <> 0 reset button
			
			
	} // end of test_on()	
	
	
	if ($set == 0) { // select a question

		echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
        if (isset($_GET['testout']) && $_GET['testout'] == 1) {
            $message = "Your test has been cancelled!";
            $usernamelog = $username;
            $username = "";
            $password = "";
            $userx = 0;
            $login = 0;
            $q_count = 0;
            $q_tot = 0;
            $aca_qno = 0;
            $_SESSION['aca_userthis'] = NULL;
            $_SESSION['aca_userexam'] = NULL;
            $_SESSION['aca_q_count'] = NULL;
            $_SESSION['aca_q'] = NULL;
            $_SESSION['aca_q_tot'] = NULL;
            $_SESSION['aca_subject'] = NULL;
            unset($_SESSION['aca_userthis']);
            unset($_SESSION['aca_userexam']);
            unset($_SESSION['aca_q_count']);
            unset($_SESSION['aca_q']);
            unset($_SESSION['aca_subject']);

            /*echo "<div class=\"alert alert-success\" role=\"alert\">";
                  echo "<a href=\"#\" class=\"alert-link\">".$message."</a>";
            echo "</div>";*/
        }
        if(isset($message) && $message != ''){
            echo '<div class="alert alert-info">'.$message.'</div>';
        }
		/*echo "<div class=\"panel panel-primary\"> ";

			  echo "<div class=\"panel-heading\">Select a question</div> "; 

			  echo "<div class=\"panel-body table-responsive\">";*/
		echo "<form name=\"test\" method=\"post\" action=\"?id=".$id."&opt=0&set=1&login=1\" role=\"form\" autocomplete=\"off\">";

        echo "<table class=\"table table-striped\">";
        echo "<thead>";
        echo '<tr><th colspan="2" style="background:#58595b;color:#FFFFFF;">Select a Question</th></tr>';
        echo "</thead>";
        echo "<tbody>";

		echo "<tr>";
		echo "<td class=\"col-sm-2\">Exam</td>";
			echo "<td><select name=\"exam_id\" class=\"pracexamid form-control placeholder reqd\" id=\"pracexam_id\" required>";
						  //echo "<option value=\"".$exam_id."\">".$exam."</option>";					
// TEMP update with DB lookup to only display exams that the user has access to
						  echo "<option value=\"\" selected hidden>Select Exam</option>";
								$result_set19 = get_aca_user_exam_list_all(0,999,1,$userx,$today); // get_aca_user_exam_list($npage, $no_of_list, $status, $cat, $expiry_date) {
								while ($row = mysqli_fetch_array($result_set19)) {
									$dim_key = $row["exam_id"]; 
									$display_name = $row["code"];
										//$result_setI = get_aca_exam_rec($dim_key);
										//while ($row = mysqli_fetch_array($result_setI)) {		
										//	$display_name = $row["code"];		
										//}
									echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								}
			echo "</select></td>";
		echo "</tr>";
		
		echo "<tr>";
		echo "<td>Question:</td>";

		echo '<td><input type="text" name="qno" value="" placeholder="Type question or question no" id="pracquestion" class="form-control" /></td>';
		//echo '<div id="pracsuggestionbox"></div>';
		echo "</tr>";
		echo "<tr><td colspan=\"2\" class=\"text-center\"><button name=\"submit\" type=\"submit\" id=\"pracsubmit\" class=\"btn btn-default\"> Display Question </button></td></tr>";
		echo "</tbody>";
		echo "</table>";
		echo "</form>";
        /*echo "</div></div>";*/
        echo "</div>";



	  
	} // end of set = 0
?>

    <script>

        /*$(document).ready(function() {
            $.post("ajax/ajax-test.php", { action:"CountExams"
                }, function(data){
                    if(data.result == 1){
                        if(data.cnt == "0") {
                            $(".content-pad").prepend(data.html);
                            $("#pracexam_id").prop("disabled",true);
                            $("#pracquestion").prop("disabled",true);
                            $("#pracsubmit").prop("disabled",true);
                        }
                    } else {

                    }
                },
                "json");
        })*/
    </script>

<?php

    echo "</div>";
    echo "</div>";
} // end of test id = 10
?>
 