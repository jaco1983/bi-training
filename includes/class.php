<?php	
// class  id = 34

if ($id == 34) {


	if ($set == 2) { // insert				
		if (isset($_POST['submit'])) {
			$name = trim(mysql_prep($_POST['name']));
			$note = trim(mysql_prep($_POST['note']));
			$password = trim(mysql_prep($_POST['password']));
			$expiry_date = trim(mysql_prep($_POST['expiry_date']));
			$instructor_id = trim(mysql_prep($_POST['instructor_id']));
			$class_type_id = trim(mysql_prep($_POST['class_type_id']));
			$create_date = $today;	
			$creator_id = $userx;
			$status = '1';	
		}
		
		if ($test_aca == 1) {
			$msg = "set 2 Post opt=".$opt."</br>";
			$msg .= "create_date: ".$create_date."</br>";
			$msg .= "creator_id: ".$userx."</br>";
			echo $msg;
		}

		$query = "INSERT INTO aca_class( ";
		$query .= " name, password, instructor_id, expiry_date, note, "; 
		$query .= " class_type_id, create_date, status, creator_id ) ";
		$query .= " VALUES (   ";
		$query .= " '{$name}', '{$password}', {$instructor_id}, '{$expiry_date}', '{$note}', "; 
		$query .= " '{$class_type_id}', '{$create_date}', '{$status}', {$userx} ) " ;
	
		$result = mysqli_query( $connection, $query);
		if (mysqli_affected_rows($connection) == 1) {	
			// Success	
			echo "<div class=\"alert alert-success\">";
				echo "Record Created";
			echo "</div>";
			$set = 0; // set action back to listing
			} else {
			echo "<div class=\"alert alert-danger\">";
				echo "Record creation failed";
			echo "</div>";
		}
		$set = 0; // set action back to form
	//} // end opt = 1
		
	} // end of set = 2
	


	if ($set == 4 && $key <> 0) { // update record													
		if (isset($_POST['submit'])) {
			$name = trim(mysql_prep($_POST['name']));
			$password = trim(mysql_prep($_POST['password']));
			$expiry_date = trim(mysql_prep($_POST['expiry_date']));
			$instructor_id = trim(mysql_prep($_POST['instructor_id']));
			$class_type_id = trim(mysql_prep($_POST['class_type_id']));
			$note = trim(mysql_prep($_POST['note']));
			$status = trim(mysql_prep($_POST['status']));
			$create_date = $today;	
			$creator_id = $userx;
		}
		
			
			if ($test_aca == 1) {
				$msg = "set 2 Post</br>";
				$msg .= "class_type_id: ".$class_type_id."</br>";
				$msg .= "create_date: ".$create_date."</br>";
				$msg .= "status: ".$status."</br>";
				$msg .= "creator_id: ".$userx."</br>";
				echo $msg;
			}
			
			// Update
			$query = "UPDATE aca_class SET ";
			$query .= " name = '{$name}', ";
			$query .= " password = '{$password}', ";
			$query .= " instructor_id = '{$instructor_id}', ";
			$query .= " expiry_date = '{$expiry_date}', ";
			$query .= " note = '{$note}', ";
			$query .= " class_type_id = '{$class_type_id}', ";
			$query .= " status = '{$status}', ";
			$query .= " creator_id = '{$userx}' ";
			$query .= " WHERE class_id = {$key} ";
			$query .= " LIMIT 1 ";
		
			$result = mysqli_query( $connection, $query);
			if (mysqli_affected_rows($connection) == 1) {	
				// Success	
				echo "<div class=\"alert alert-success\">";
					echo "The record was updated successfully.";
				echo "</div>";
				$set = 0; // set action back to listing
				} else {
				echo "<div class=\"alert alert-danger\">";
					echo "The record was not updated. Please try again. Make sure that all fields are completed.";
				echo "</div>";
				$set = 3; // set action back to form
			}
				
	} // end of set = 4 update


	if ($set == 3) { // edit
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Class Maintenance</h2><div class=\"colored-line-left\"></div></div>";
        echo '<div class="clearfix"></div>';
        echo '<div class="row content-pad">';
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";
				
		$result_set = get_aca_class_rec($key);
		while ($row = mysqli_fetch_array($result_set)) {		
			$class_id = $row["class_id"];
			$cat7 = $class_id;
			$name = $row["name"];	
			$password = $row["password"];		
			$instructor_id = $row["instructor_id"];	
			$expiry_date = $row["expiry_date"];				
			$note = $row["note"];	
			$class_type_id = $row["class_type_id"];
			$status = $row["status"];
			$instructor = "";
			$result_setI = get_aca_instructor_rec($instructor_id);
			while ($row = mysqli_fetch_array($result_setI)) {		
				$instructor = $row["name"];		
			}
			$d_status = $status;
			$result_set74 = get_dim("status", $status);
			while ($row = mysqli_fetch_array($result_set74)) {
				$dim_key = $row["dim_key"]; 
				$d_status = $row["description"];
			}
			$class_type = "";
			$result_set75 = get_dim_rec_z("aca_class_type", "class_type_id", $class_type_id);
			while ($row = mysqli_fetch_array($result_set75)) {
				$class_type = $row["description"];
			}
		}
		//$startdate = strtotime("Saturday");
		//$d = (strtotime("+18 weeks",$startdate));
		//$expiry_date = date("Y-m-d", $d);
		
		echo "<form name=\"aca_class\" method=\"post\" action=\"?id=".$id."&set=4&opt=" . $opt . "&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat5=".$cat5." \"enctype=\"multipart/form-data\">";

        echo '<table class="table table-striped">
		      <thead>
		        <tr>
		            <th colspan="2" style="background:#58595b;color:#FFFFFF;">Edit Class</th>
                </tr>
              </thead>
              <tbody>';


		echo "<tr>";
		  echo "<td>Class Name:</td>";
		  echo "<td><input type=\"text\" name=\"name\" class=\"form-control reqd\" id=\"name\" placeholder=\"Enter name\" value=\"".$name."\"></td>";
		echo "</tr>";
		echo "<tr>";
		  echo "<td>Password:</td>";
		  echo "<td><input type=\"text\" name=\"password\" class=\"form-control reqd\" id=\"password\" placeholder=\"Enter password\" value=\"".$password."\"></td>";
		echo "</tr>";
			echo "<tr>";
			  echo "<td>Expiry Date:</td>";
			  echo "<td><input type=\"date\" name=\"expiry_date\" class=\"form-control reqd\" id=\"expiry_date\" placeholder=\"Expiry Date\" value=\"".$expiry_date."\"></td>";
			echo "</tr>";
		echo "<tr>";
		  echo "<td>Instructor</td>";
			echo "<td><select name=\"instructor_id\" class=\"form-control reqd\" onchange=\"showTool(this.value)\" id=\"instructor_id\">";
								  echo "<option value=\"".$instructor_id."\">".$instructor."</option>";	
								  $result_set96 = get_aca_instructor_list(0, 25, 99);
								  while ($row = mysqli_fetch_array($result_set96)) {
									  $dim_key = $row["instructor_id"]; 
									  $display_name = $row["name"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }
								  echo "<option value=\"0\">Not Assigned</option>";
								  echo "</select></td>";
		echo "</tr>";
		echo "<tr>";
		  echo "<td>Note:</td>";
		  echo "<td><input type=\"text\" name=\"note\" class=\"form-control reqd\" id=\"note\" placeholder=\"Enter note\" value=\"".$note."\"></td>";
		echo "</tr>";
		echo "<tr>";
		  echo "<td>Class Type</td>";
			echo "<td><select name=\"class_type_id\" class=\"form-control reqd\" id=\"class_type_id\">";
								  echo "<option value=\"".$class_type_id."\">".$class_type."</option>";	
								  $result_set96 = get_dim_z("aca_class_type", "description", "ASC", 1); // $table, $order_field, $direction, $status
								  while ($row = mysqli_fetch_array($result_set96)) {
									  $dim_key = $row["class_type_id"]; 
									  $display_name = $row["description"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }
								  echo "<option value=\"\"></option>";
								  echo "</select></td>";
		echo "</tr>";
		echo "<tr>";
		  echo "<td>Status</td>";
			echo "<td><select name=\"status\" class=\"form-control reqd\" onchange=\"showTool(this.value)\" id=\"status\">";
						  echo "<option value=\"".$status."\">".$d_status."</option>";
								  $result_set9 = get_dim_list("status");
								  while ($row = mysqli_fetch_array($result_set9)) {
									  $dim_key = $row["dim_key"]; 
									  $display_name = $row["note"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }
								  echo "</select></td>";
		echo "</tr>";
										
			// buttons
			$update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Submit</button>";			
			$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";
			
		  echo "<tr><td colspan=\"2\" class=\"text-center\">" . $update . " " . $cancel . "</td></tr>";
		  echo "</tbody>";
		  echo "</table>";

		echo "</form>";			
		
		echo "<p>&nbsp;</p>";
		
		
		// view class exams
		// get record count
		$p_records = 0;
		$result_setA = get_aca_class_exam_count(99, $cat7, 0); // status 1 = valid, access 0 for all
		while ($row = mysqli_fetch_array($result_setA)) {
			$p_records = $row["record_count"]; 
		}
		if ($p_records > 0) {
			
			$anchor = "x";
			$p_link = "?id=".$id."&opt=".$opt."&cat2=0&cat3=0&cat6=".$cat6."&key=".$key."&cat=".$cat."&cat4=".$cat4."&set=".$set;
			$p = page_nav_last($page, $page_break_d, $p_records, $p_link, $anchor);
			$c = " ";
	
			//echo "Select a user to maintain or create a new user</br>";
			//$n =  "<button name=\"NewBut\" type=\"button\" value=\"New\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=1&opt=0&key=".$key."');return document.MM_returnValue\"/>New</button>&nbsp;&nbsp;";
			$n = "";
			
			echo "<table class=\"table table-striped\">";
			echo "<thead>";
            echo '<tr><th colspan="5" style="background:#58595b;color:#FFFFFF;">Exams Subscribed To<div style="float:right;">' . $n.' '.$p . '</div></th></tr>';
			echo "<tr>";
				//echo "<th>ID</th>";
				echo "<th>Exam Code</th>";
				echo "<th>Description</th>";
				echo "<th>Versions</th>";
				echo "<th>Subscription End</th>";
				echo "<th></th>";
				//echo "<th></th>";
			echo "</tr>";		
			echo "</thead>";
			echo "<tbody>";
			
		// get data		
		$npage = $page;
		$un = "";
		$e =  "";
		$result_set = get_aca_class_exam_list($npage, $page_break_d, 99, $cat7, 0);
		while ($row = mysqli_fetch_array($result_set)) {		
			$class_exam_id = $row["class_exam_id"];		
			$class_id = $row["class_id"];
			$cat7 = $class_id;	
			$exam_id = $row["exam_id"];
			$expiry_date = $row["expiry_date"];		
			//$description = $row["description"];	
			$status = $row["status"];
			$d_status = $status;
				$result_set74 = get_dim("status", $status);
				while ($row = mysqli_fetch_array($result_set74)) {
					$dim_key = $row["dim_key"]; 
					$d_status = $row["description"];
				}
				$result_set76 = get_aca_exam_rec($exam_id);
				while ($row = mysqli_fetch_array($result_set76)) {
					$code = $row["code"]; 
					$description = $row["description"];	
					$versions = $row["versions"];	
					$un = "<a href=\"?id=".$id."&set=3&opt=0&login=1&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&key=".$exam_id."&cat3=".$cat3."&cat7=".$cat7."&cat4=".$cat4."&cat5=".$cat5."\">".$code."</a>";
					$e =  "<button name=\"EditBut\" type=\"button\" value=\"Edit\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat7=".$cat7."&cat=".$cat."&login=1&set=3&opt=0&key=".$class_exam_id."');return document.MM_returnValue\"/>Edit</button>&nbsp;&nbsp;";
				}
			
			
		echo "<tr>";
			//echo "<td>".$exam_id."</td>";
			echo "<td>".$code."</td>";
			echo "<td>".$description."</td>";
			echo "<td>".$versions."</td>";
			echo "<td>".$expiry_date."</td>";
			echo "<td>".$e."</td>";
		echo "</tr>";
		}
		echo "<tr>";
			echo "<td align=\"left\" colspan=\"5\">".$c." ".$n."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;".$p."</td>";
		echo "</tr>";
		
		echo "</tbody>";
		echo "</table>";
		
	} // end of if p_records > 0

		echo "</div>";
		echo "</div>";

	} // end of edit



	if ($set == 0) { // list
		// get record count
		$p_records = 0;
		$result_setA = get_aca_class_count(99, 0, $cat7); // status 1 = valid, access 0 for all
		while ($row = mysqli_fetch_array($result_setA)) {
			$p_records = $row["record_count"]; 
		}
		$anchor = "x";
		$p_link = "?id=".$id."&opt=".$opt."&cat2=0&cat3=0&cat6=".$cat6."&key=".$key."&cat=".$cat."&cat4=".$cat4."&set=".$set;
		$p = page_nav_last($page, $page_break_d, $p_records, $p_link, $anchor);
		$c = " ";

		//echo "Select a user to maintain or create a new user</br>";
		$n =  "<button name=\"NewBut\" type=\"button\" value=\"New\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=1&opt=0&key=".$key."');return document.MM_returnValue\"/>New</button>&nbsp;&nbsp;";


        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Class Maintenance</h2><div class=\"colored-line-left\"></div></div>";
        echo '<div class="clearfix"></div>';
        echo '<div class="row content-pad">';
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";
		  	
		echo "<table class=\"table table-striped\">";
		echo "<thead>";
        echo '<tr>
		            <th colspan="6" style="background:#58595b;color:#FFFFFF;line-height: 2em;">List of Classes<div style="float:right;">' . $n.' '.$p . '</div> </th>
                </tr>';
		echo "<tr>";
			echo "<th>ID</th>";
			echo "<th>Name</th>";
			echo "<th>Expiry</th>";
			echo "<th>Instructor</th>";
			echo "<th>Status</th>";
			echo "<th></th>";
		echo "</tr>";		
		echo "</thead>";
		echo "<tbody>";
		// get data		
		$npage = $page;
		$result_set = get_aca_class_list($npage, $page_break_d, 99, 0, $cat7);
		while ($row = mysqli_fetch_array($result_set)) {		
			$class_id = $row["class_id"];
			$name = $row["name"];		
			$note = $row["note"];		
			$password = $row["password"];		
			$expiry_date = $row["expiry_date"];	
			$instructor_id = $row["instructor_id"];	
			$status = $row["status"];
			$d_status = $status;
				$result_set74 = get_dim("status", $status);
				while ($row = mysqli_fetch_array($result_set74)) {
					$dim_key = $row["dim_key"]; 
					$d_status = $row["description"];
				}
				$instructor = "";
				$result_setIS = get_aca_instructor_rec($instructor_id);
				while ($row = mysqli_fetch_array($result_setIS)) {		
					$instructor = $row["name"];		
				}
			
			$un = "<a href=\"?id=".$id."&set=3&opt=0&login=1&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&key=".$class_id."&cat3=".$cat3."&cat4=".$cat4."&cat5=".$cat5."\">".$name."</a>";
			$e =  "<button name=\"EditBut\" type=\"button\" value=\"Edit\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&cat=".$cat."&login=1&set=3&opt=0&key=".$class_id."');return document.MM_returnValue\"/>Edit</button>&nbsp;&nbsp;";
			$ex =  "<button name=\"ExamBut\" type=\"button\" value=\"Exam\" class=\"btn btn-warning\" onclick=\"MM_goToURL('parent','?id=23&cat=".$cat."&login=1&set=1&opt=0&key=".$class_id."');return document.MM_returnValue\"/>Exam</button>&nbsp;&nbsp;";
			
		echo "<tr>";
			echo "<td>".$class_id."</td>";
			echo "<td>".$un."</td>";
			echo "<td>".$expiry_date."</td>";
			echo "<td>".$instructor."</td>";
			echo "<td>".$d_status."</td>";
			echo "<td>".$e." " .$ex."</td>";
		echo "</tr>";
		}	
		echo "<tr>";
			echo "<td align=\"left\" colspan=\"3\">".$c."</td>"; // close, new, page nav
			echo "<td align=\"right\" colspan=\"3\">".$n."&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;".$p."</td>";
		echo "</tr>";

		echo "</tbody>";
		echo "</table>";
		  echo "</div> "; 
	   echo " </div> "; 	
	
		
	} // end of list set = 0




	if ($set == 1) { // create new	
		$startdate = strtotime("Saturday");
		$d = (strtotime("+18 weeks",$startdate));
		$expiry_date = date("Y-m-d", $d);

        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Class Maintenance</h2><div class=\"colored-line-left\"></div></div>";
        echo '<div class="clearfix"></div>';
        echo '<div class="row content-pad">';
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";
				
		
		echo "<form name=\"aca_class\" method=\"post\" action=\"?id=".$id."&set=2&opt=" . $opt . "&key=" . $key . "&cat=".$cat."&cat6=".$cat6."&cat2=".$cat2."&cat3=" . $cat3 . "&cat4=".$cat4."&cat5=".$cat5." \"enctype=\"multipart/form-data\">";

        echo "<table class=\"table table-striped\">";
        echo "<thead>";
        echo '<tr>
		            <th colspan="2" style="background:#58595b;color:#FFFFFF;line-height: 2em;">Add a Class</th>
                </tr>';
        echo "<tr>";
		echo "</thead>";
		echo "<tbody>";
		echo "<tr>";
		  echo "<td>Class Name:</td>";
		  echo "<td><input type=\"text\" name=\"name\" class=\"form-control reqd\" id=\"name\" placeholder=\"Enter name\" value=\"\"></td>";
		echo "</tr>";
		echo "<tr>";
		  echo "<td>Password:</td>";
		  echo "<td><input type=\"text\" name=\"password\" class=\"form-control reqd\" id=\"password\" placeholder=\"Enter password\" value=\"\"></td>";
		echo "</tr>";
		echo "<tr>";
			  echo "<td>Expiry Date:</td>";
			  echo "<td><input type=\"date\" name=\"expiry_date\" class=\"form-control reqd\" id=\"expiry_date\" placeholder=\"Expiry Date\" value=\"".$expiry_date."\"></td>";
			echo "</tr>";
		echo "<tr>";
		  echo "<td>Instructor</td>";
			echo "<td><select name=\"instructor_id\" class=\"form-control reqd\" onchange=\"showTool(this.value)\" id=\"instructor_id\">";
								  echo "<option value=\"\"></option>";	
								  $result_set96 = get_aca_instructor_list(0, 25, 99);
								  while ($row = mysqli_fetch_array($result_set96)) {
									  $dim_key = $row["instructor_id"]; 
									  $display_name = $row["name"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }
								  echo "<option value=\"0\">Not Assigned</option>";
								  echo "</select></td>";
		echo "</tr>";
		echo "<tr>";
		  echo "<td>Note:</td>";
		  echo "<td><input type=\"text\" name=\"note\" class=\"form-control reqd\" id=\"note\" placeholder=\"Enter note\" value=\"\"></td>";
		echo "</tr>";
		echo "<tr>";
		  echo "<td>Class Type</td>";
			echo "<td><select name=\"class_type_id\" class=\"form-control reqd\" id=\"class_type_id\">";
								  echo "<option value=\"\"></option>";	
								  $result_set96 = get_dim_z("aca_class_type", "description", "ASC", 1); // $table, $order_field, $direction, $status
								  while ($row = mysqli_fetch_array($result_set96)) {
									  $dim_key = $row["class_type_id"]; 
									  $display_name = $row["description"];
									  echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								  }
								  echo "</select></td>";
		echo "</tr>";

										
			// buttons
			$update = "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\">Submit</button>";			
			$cancel = "<button name=\"CancelBut\" type=\"button\" value=\"Cancel\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&opt=0&key=0');return document.MM_returnValue\"/>Cancel</button>";
			
		  echo "<tr><td colspan=\"2\" class=\"text-center\">" . $update . " " . $cancel . "</td></tr>";
		  echo "</tbody>";
		  echo "</table>";

		echo "</form>";			

		  echo "</div> ";
	   echo " </div> ";

	} // end of set = 1
	
} // end of user id = 30
?>
 