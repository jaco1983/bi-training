<?php 
// connection.php
/* this file contains the connection information. 
set the connection parameters in constants.php */
?>

<?php 
require("constants.php");
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
 if (!$connection) {
     die('Could not connect: ' . mysqli_error($connection));
 }
$db_select =  mysqli_select_db($connection, DB_NAME);
?>