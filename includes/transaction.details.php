<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/07/21
 * Time: 9:49 PM
 */

$sql = "select * from aca_transaction where id = '".$_GET["trans"]."'";

$result_set = mysqli_query($connection, $sql);
echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Transaction Details</h2><div class=\"colored-line-left\"></div></div>
<div class=\"clearfix\"></div>
<div class=\"row content-pad\">
<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";

                    while ($row = mysqli_fetch_array($result_set)) {
                        $payeeDetails = unserialize($row["s_details"]);
                        $transDetails = unserialize($row["s_transaction"]);

                        echo '<table class="table">
                                <tr>
                                    <td><strong>Invoice to:</strong></td>
                                    <td>'.($payeeDetails["invoiceto"] == "company" ? 'Company' : 'Individual' ).'</td>
                                </tr>
                                <tr>
                                    <td><strong>Firstname:</strong></td>
                                    <td>'.$payeeDetails["firstname"].'</td>
                                </tr>
								<tr>
                                    <td><strong>Surname:</strong></td>
                                    <td>'.$payeeDetails["surname"].'</td>
                                </tr>
                                <tr>
                                    <td><strong>Company:</strong></td>
                                    <td>'.$payeeDetails["company"].'</td>
                                </tr>
								<tr>
                                    <td><strong>Postal Address:</strong></td>
                                    <td>'.nl2br($payeeDetails["postal"]).'</td>
                                </tr>
                                <tr>
                                    <td><strong>Country:</strong></td>
                                    <td>'.$payeeDetails["country"].'</td>
                                </tr>
                                <tr>
                                    <td><strong>Email:</strong></td>
                                    <td>'.$payeeDetails["email"].'</td>
                                </tr>
                                <tr>
                                    <td><strong>Contact number:</strong></td>
                                    <td>'.$payeeDetails["contactnumber"].'</td>
                                </tr>
								<tr>
                                    <td><strong>Order Number:</strong></td>
                                    <td>'.$payeeDetails["ordernumber"].'</td>
                                </tr>
								<tr>
                                    <td><strong>VAT Number:</strong></td>
                                    <td>'.$payeeDetails["vatnumber"].'</td>
                                </tr>
                                <tr>
                                    <td><strong>Payment Method:</strong></td>
                                    <td>';

                        if($row["paymentmethod"] == "eft"){
                            echo 'EFT';
                            if($row["paymentcomplete"] == "0" || $row["paymentcomplete"] == "-"){
                                echo '<a href="javascript:void(0)" onclick="approveTransaction('.$_GET["trans"].')" class="btn btn-primary">Approve</a>';
                            }
                        }
						if($row["paymentmethod"] == "cc"){
                            echo 'CC';
                            if($row["paymentcomplete"] == "0" || $row["paymentcomplete"] == "-"){
                                echo '<a href="javascript:void(0)" onclick="approveTransaction('.$_GET["trans"].')" class="btn btn-primary">Approve</a>';
                            }
                        }
						if($row["paymentmethod"] == "paypal"){
                            echo 'PayPal';
                            if($row["paymentcomplete"] == "0" || $row["paymentcomplete"] == "-"){
                                echo '<a href="javascript:void(0)" onclick="approveTransaction('.$_GET["trans"].')" class="btn btn-primary">Approve</a>';
                            }
                        }
                        echo '</td>
                                </tr>
                              </table>

                              <table class="table">
                                 <tr bgcolor="#ccc">
                                    <th><strong>Description</strong></th>
                                    <th><strong>Cost</strong></th>
                                 </tr>';
                        for($x = 0; $x < count($transDetails); $x++) {
                            echo '<tr>
                                    <td> '.$transDetails[$x]["description"].'</td>
                                    <td> '.$transDetails[$x]["amount"].' </td>
                                </tr>';
                            }
                              echo '</table>';
                        //var_dump($transDetails);
                    }
            echo '</div>
            </div>';