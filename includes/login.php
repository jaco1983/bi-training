<?php

echo '<div id="LoginModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Log In</h4>
      </div>
      <div class="modal-body">';

    echo '<form name="login" method="post" action="?id=0&opt='.$opt.'&set='.$set.'&login=3&testout=1" role="form" autocomplete="off">
            <div class="form-group">
                <label for="aemail" class="label-control col-sm-3">Email:</label>
                <div class="col-sm-9">
                    <input name="aemail" type="email" class="form-control" id="aemail" placeholder="Email" value="'.(isset($_POST["aemail2"]) ? $_POST["aemail2"] : '' ).'">
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label for="apwd" class="label-control col-sm-3">Password:</label>
                <div class="col-sm-9">
                    <input name="apwd" type="password" class="form-control" id="apwd" placeholder="Password" value="'.(isset($_POST["apwd2"]) ? $_POST["apwd2"] : '' ).'">
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label for="captcha" class="label-control col-sm-3">Captcha:</label>
                <div class="col-sm-9">
                    <input name="captcha" type="text" class="form-control" placeholder="Verification Code" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    &nbsp;
                </div>
                <div class="col-sm-9">
                    <img src="img/refresh.jpg" alt="refresh captcha" onclick="captchaRefresh()" id="refresh-captcha" /> <img src="php/newCaptcha.php" alt="" id="captcha2" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <div class="col-sm-12 text-center">
                    <button type="submit" name="submit" class="btn btn-default">Log in</button>&nbsp;
                    <a href="" name="forgotpass" class="btn btn-default">Forgot Password</a>
                </div>
                <div class="clearfix"></div>
            </div>
          </form>';



    echo '</div>
      <div class="modal-footer">

      </div>
    </div>

  </div>
</div>';

?>