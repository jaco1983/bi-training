<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/07/21
 * Time: 2:33 AM
 */

if($id == 108){

    $sql = "select * from aca_voucher order by voucher_id desc";

    $result_set = mysqli_query($connection, $sql);

    echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>Voucher Maintenance</h2><div class="colored-line-left"></div></div>
<div class="clearfix"></div>
<div class="row content-pad">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main-content">
            <div class="col-sm-12 text-right"><input type="button" name="new_voucher" class="btn btn-success" value="New" onclick="NewVoucher()"></div>
                
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Voucher Number</th>
                                <th>Emailed to</th>
                                <th>% Discount</th>
                                <th>Date Created</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>';
                            while ($row = mysqli_fetch_array($result_set)) {
                                echo '<tr>
                                        <td>'.$row["voucher_id"].'</td>
                                        <td>'.$row["voucher_number"].'</td>
                                        <td>'.$row["user_email"].'</td>
                                        <td>'.$row["discount"].'</td>
                                        <td>'.$row["date_created"].'</td>
                                        <td align="center">';

                                        switch($row["status"]){
                                            case "-1":
                                                echo 'Cancelled';
                                                break;
                                            case "0":
                                                echo 'Available';
                                                break;
                                            case "1":
                                                echo 'Used';
                                                break;
                                        }

                                        echo '</td>
                                        <td><a href="javascript:void(0)" class="btn btn-primary" data-id="' . $row["voucher_id"] . '" onclick="EditVoucher('.$row["voucher_id"].')">Details</a></td>
                                    </tr>';
                             }
                    echo '</tbody>
                        </table>';

}

        echo '</div></div>';