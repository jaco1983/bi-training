<?php
/**
 * Created by PhpStorm.
 * User: Jaco
 * Date: 2016/07/21
 * Time: 2:33 AM
 */

if($id == 104){

    echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"><h2>Transaction Maintenance</h2><div class=\"colored-line-left\"></div></div>
<div class=\"clearfix\"></div>
<div class=\"row content-pad\">
<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">";

    $sql = "select * from aca_transaction order by id desc";

    $result_set = mysqli_query($connection, $sql);

    echo '
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Date</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Reference</th>
                                <th>Payment Type</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>';
                            while ($row = mysqli_fetch_array($result_set)) {
                                echo '<tr>
                                        <td>'.$row["id"].'</td>
                                        <td>'.$row["transactiondate"].'</td>
                                        <td>'.$row["cname"].'</td>
                                        <td>'.$row["csurname"].'</td>
                                        <td>'.$row["paymentreference"].'</td>
                                        <td align="center">'.$row["paymentmethod"].'</td>
                                        <td align="center">';

                                            switch($row["paymentcomplete"]){
                                                case "0":
                                                    echo 'Pending';
                                                    break;
                                                case "1":
                                                    echo 'Paid';
                                                    break;
												case "-":
                                                    echo 'Cancelled';
                                                    break;
                                            }

                                        echo '</td>
                                        <td><a href="?id=105&trans='.$row["id"].'" class="btn btn-primary">Details</a></td>
                                    </tr>';
                            }
                   echo '</tbody>
                        </table>';

}

echo '</div>';
echo '</div>';