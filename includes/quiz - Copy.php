<?php	
// quiz id = 12

/*
The instructor will select an exam and question.
the question will be displayed and a session number wil be created and displayed with the question.

The students will select the Quiz from the menu and enter the session number.
the question and options will be displayed and the student can answer the question.

The instructor can refresh the question to see the answers supplied by the students.

The question, session and answeres is stored in the DB.

Instructor
1. setect exam.
2. select question no
3. display question
4. mark question


set
---
0	display form and select a exam
2	display question, mark and display result if opt = 1
10	get next question
30
90	display summary when test complete

	
*/

if ($id == 12) {
	
	// test selected, create session
	if ($set == 1) { 	
					
		if (!test_on() && isset($_POST['submit'])) { 
		  $userexam = trim(mysql_prep($_POST['exam_id'])); 
		  $qno = trim(mysql_prep($_POST['qno'])); 
		  $gip = get_ip(0);
		  
		  // set session variables								
		  $_SESSION['aca_userthis'] 	= "xxxxxx";			// practise
		  $_SESSION['aca_userexam'] 	= $userexam;		// exam id
		  $_SESSION['aca_q_count'] 		= 1;				// question count
		  $_SESSION['aca_subject'] 		= 0;				// subject / tool id
		  $_SESSION['aca_q_tot'] 		= 1;				// current question number / answered
		  $_SESSION['aca_q'] 			= $qno;				// quiz question
		  $userx 			= $_SESSION['aca_userx']; 
		  $username 		= $_SESSION['aca_username']; 
		  $aca_name 		= $_SESSION['aca_name']; 
		  $userthis 		= $_SESSION['aca_userthis']; 
		  $userexam 		= $_SESSION['aca_userexam'];  
		  $qno	 			= $_SESSION['aca_q'];  
		  $q_tot 			= $_SESSION['aca_q_tot']; 
		  $aca_subject 		= $_SESSION['aca_subject']; 
		  $aca_instructor 	= $_SESSION['aca_instructor'];  // this should come from the user record by exam (user_exam table required)
		  
		  $set = 2; 	// go to display questions
		  $teston = 1;	// set the flag that test was selected
		  
		} // end of post
		

			if ($test_aca == 1) {
				echo "Test end of set=1 :: teston = 1 and session variables set</br>";
				echo "Username: ".$username. "</br>";
				echo "UserThis: ".$userthis. "</br>";
				echo "Snow: ".$snow. "</br>";
				echo "Set     : ".$set. "</br>";
				//echo "Q       : ".$q. "</br>";
				echo "Instruct: ".$aca_instructor. "</br>";
				//echo "QCnt    : ".$q_count. "</br>";
				echo "Login   : ".$login. "</br>";
				echo "Teston  : ".$teston. "</br></br>";
			}

		
	} // end of set = 1 
	
			
	// users must be logged in and selected a test
	if (test_on() || $teston == 1) {
	  
	  	// set session variables														
		$username 		= $_SESSION['aca_username']; 	// 
		$aca_name 		= $_SESSION['aca_name']; 		// 
		$aca_instructor = $_SESSION['aca_instructor']; 	// 
		$userx 			= $_SESSION['aca_userx'];  		// user_id
		$userthis 		= $_SESSION['aca_userthis'];  	// user_id
		$userexam 		= $_SESSION['aca_userexam'];  	// user_id
		$q_count 		= $_SESSION['aca_q_count'];  	// number of questions to answer
		$q_tot 			= $_SESSION['aca_q_tot'];  		// current number of questions answered
		$qno 			= $_SESSION['aca_q'];  			// quiz questions
		$aca_subject 	= $_SESSION['aca_subject'];  	// current number of questions answered
		$q_display 		= $q_tot;
		$key			= $userexam;
			
		if ($test_aca == 1) {
			echo "Test set=2 :: session variables set</br>";
			echo "Username: ".$username. "</br>";
			echo "UserThis: ".$userthis. "</br>";
			echo "Set: ".$set. "</br>";
			echo "Q: ".$q. "</br>";
			echo "QNo: ".$qno. "</br>";
			echo "QCnt: ".$q_count. "</br></br>";
		}
	
		// get exam heading							
		$result_set1 = get_aca_exam_rec($key);
		while ($row = mysqli_fetch_array($result_set1)) {		
			$key = $row["exam_id"];
			$code = $row["code"];	
			$description = $row["description"];	
			$versions = $row["versions"];
		}			
		// get total number of questions
		$p_records = 1; 
		
		
		// set questions variables							
		$oac = "";
		$obc = "";
		$occ = "";
		$odc = "";
		$oec = "";
		$msg = "";
		$cora = "";
		$but = "but";
		$up = "";
		$upErr = "";
		$fa = "alert alert-info";
		$fb = "alert alert-info";
		$fc = "alert alert-info";
		$fd = "alert alert-info";
		$fe = "alert alert-info";
		
		
		
		
		// mark question and update stats - do this when question is answered (on Submit)
		if ($set == 3 && $key <> 0 && $opt == 1) {		
			$answ_a = 0;			
			$answ_b = 0;				
			$answ_c = 0;				
			$answ_d = 0;				
			$answ_e = 0;	
			$correct_cnt = 0;
			$answer_cnt = 0;			
			
						
			if (isset($_POST['submit'])) {
				$answ_a = trim(mysql_prep($_POST['answ_a']));
				$answ_b = trim(mysql_prep($_POST['answ_b']));
				$answ_c = trim(mysql_prep($_POST['answ_c']));
				$answ_d = trim(mysql_prep($_POST['answ_d']));
				$answ_e = trim(mysql_prep($_POST['answ_e']));
				$tot_answ = $answ_a + $answ_b + $answ_c + $answ_d + $answ_e; // total number of questions answered
				
				// set session count for total number of questions presented
				$q_display = $q_tot;
				$_SESSION['aca_q_tot'] = $_SESSION['aca_q_tot'] + 1;
				$q_tot = $_SESSION['aca_q_tot'];
			}
			// get record results and print answers			
			$result_set = get_aca_question_rec($qno, 0); // break, current page, status (99 for all)
			while ($row = mysqli_fetch_array($result_set)) {
				$option_a_answer = $row["option_a_answer"];	
				$option_b_answer = $row["option_b_answer"];	
				$option_c_answer = $row["option_c_answer"];	
				$option_d_answer = $row["option_d_answer"];	
				$option_e_answer = $row["option_e_answer"];
				$correct_cnt = $option_a_answer + $option_b_answer + $option_c_answer + $option_d_answer + $option_e_answer;
				if ($option_a_answer == 1) { 
					$fa = "alert alert-success"; 
					$cora .= "A "; }
				if ($option_b_answer == 1) { 
					$fb = "alert alert-success"; 
					$cora .= "B "; }
				if ($option_c_answer == 1) { 
					$fc = "alert alert-success"; 
					$cora .= "C "; }
				if ($option_d_answer == 1) { 
					$fd = "alert alert-success"; 
					$cora .= "D "; }
				if ($option_e_answer == 1) { 
					$fe = "alert alert-success"; 
					$cora .= "E "; }
			}
			
			if ($test_aca == 1) {
			echo "Test</br>";
			  echo "A: ".$answ_a." " .$option_a_answer. "</br>";
			  echo "B: ".$answ_b." " .$option_b_answer. "</br>";
			  echo "C: ".$answ_c." " .$option_c_answer. "</br>";
			  echo "D: ".$answ_d." " .$option_d_answer. "</br>";
			  echo "E: ".$answ_e." " .$option_e_answer. "</br>";
			  echo "Set: ".$set. "</br>";
			  echo "Q: ".$q. "</br>";
			  echo "Login: ".$login. "</br>";
			}
			
			if ($answ_a == 1 && $option_a_answer == 1) { $answer_cnt = $answer_cnt + 1; }
			if ($answ_b == 1 && $option_b_answer == 1) { $answer_cnt = $answer_cnt + 1; }
			if ($answ_c == 1 && $option_c_answer == 1) { $answer_cnt = $answer_cnt + 1; }
			if ($answ_d == 1 && $option_d_answer == 1) { $answer_cnt = $answer_cnt + 1; }
			if ($answ_e == 1 && $option_e_answer == 1) { $answer_cnt = $answer_cnt + 1; }	
			
			if ($answ_a == 1) {  $oac = "checked=\"checked\""; } else { $oac = ""; }
			if ($answ_b == 1) {  $obc = "checked=\"checked\""; } else { $obc = ""; }
			if ($answ_c == 1) {  $occ = "checked=\"checked\""; } else { $occ = ""; }
			if ($answ_d == 1) {  $odc = "checked=\"checked\""; } else { $odc = ""; }
			if ($answ_e == 1) {  $oec = "checked=\"checked\""; } else { $oec = ""; }	
				
					
			if ( $correct_cnt == $answer_cnt && $tot_answ == $correct_cnt) { 
				$msg = "<img src=\"images/correct.png\" width=\"50\" height=\"48\" alt=\"Correct\" /><p class=\"valid\">Correct</p>"; 
				// update table
				$score = 1;
				$class_id = 0;
				$test_type = 2; // quiz
				$create_date = $today;
				$query = "INSERT INTO aca_option( ";
				$query .= " exam_id, question_id, score, "; 
				$query .= " create_date, creator_id, username, userthis, test_type, class_id ) ";
				$query .= " VALUES (   ";
				$query .= " '{$key}', '{$q}', '{$score}', ";  
				$query .= " '{$create_date}', '{$userx}', '{$username}', '{$userthis}', '{$test_type}', {$class_id} ) " ;		
				$result = mysqli_query( $connection, $query);
				if (mysqli_affected_rows($connection) == 1) {	
					$mmm = 1;
					$but = "but_g";
				}
			} else { 
				$msg = "<img src=\"images/wrong.png\" width=\"50\" height=\"57\" alt=\"Wrong\" /><p class=\"invalid\">Incorrect. The correct answer is ".$cora."</p>"; 
				// update table
				$score = 0;
				$class_id = 0;
				$test_type = 2; // quiz
				$create_date = $today;
				$query = "INSERT INTO aca_option( ";
				$query .= " exam_id, question_id, score, "; 
				$query .= " create_date, creator_id, username, userthis, test_type, class_id ) ";
				$query .= " VALUES (   ";
				$query .= " '{$key}', '{$q}', '{$score}', ";  
				$query .= " '{$create_date}', '{$userx}', '{$username}', '{$userthis}', '{$test_type}', {$class_id}  ) " ;		
				$result = mysqli_query( $connection, $query);
				if (mysqli_affected_rows($connection) == 1) {	
					$mmm = 0;
					$but = "but_r";
				}
			}
		
			// update recorded question error if exists	
			if ($cat7 <> 0 && $qq <> 0) {
				// insert into error log
				$create_date = $today;
				$query = "INSERT INTO aca_log( ";
				$query .= " exam_id, question_id, qerr, "; 
				$query .= " create_date, creator_id, username, userthis) ";
				$query .= " VALUES (   ";
				$query .= " '{$key}', '{$qq}', '{$cat7}', ";  
				$query .= " '{$create_date}', '{$userx}', '{$username}', '{$userthis}' ) " ;		
				$result = mysqli_query( $connection, $query);
				if (mysqli_affected_rows($connection) == 1) {	
					$mmm = 1;
					$but = "but_g";
				}
				
			}
			
			$set = 2; // question was tested and results updated
			$opt = 3;  // reset opt to 3 - question marked and answer must be displayed
		
			
			if ($test_aca == 1) {
			echo "Test set=2 opt=1 mark question</br>";
			echo "Q: ".$q. "</br>";
			echo "Opt: ".$opt. "</br>";
			echo "Set: ".$set. "</br></br>";
		}
						
		} // end of set == 3 and opt == 1 question marked
		
		
		
		if ($test_aca == 1) {
			echo "Test before set 10</br>";
			echo "Q: ".$q. "</br>";
			echo "Opt: ".$opt. "</br>";
			echo "Set: ".$set. "</br></br>";
		}
		
			  
			  
 
		// display question
		if ($set == 2) {	
		
			  
		  // set buttons						
		  if ($opt == 3) { 
		  // results updated, view and go to more quiz	
			  $dis = "disabled=\"disabled\"";
			  		
			  // if no more questions
			  if ($q_tot > $p_records || $q_tot > $q_count) { 
				  $up = "<button name=\"FinishBut\" type=\"button\" value=\"More\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&q=0&qq=".$q."&login=1&set=0&opt=0&key=".$key."');return document.MM_returnValue\"/>quiz more</button>";
				  $upErr = "<button name=\"FinishBut\" type=\"button\" value=\"Report Question and more\" class=\"btn btn-default\" onclick=\"MM_goToURL('parent','?id=".$id."&q=0&login=1&set=0&qq=".$q."&cat7=1&opt=0&key=".$key."');return document.MM_returnValue\"/>Report Question and more</button>";
			  }	
		  } // end if opt = 3
		  
			  
		  // get questions	if new / next question selected	
		  if ($opt == 0) {
				  $cat6 = 0;
				  
				  $sub = 0;
				  $up = "<button name=\"submit\" type=\"submit\" value=\"Submit\" class=\"btn btn-default\">Submit</button>";	
				  $upErr = "";
				  $dis = "";
					  
			} // end if opt = 0
			
		 //}
			  
			  
			  
			  
			  
			  

			  
			  if ($test_aca == 1) {
				  echo "Test before question form</br>";
				  echo "Set: ".$set. "</br>";
				  echo "Q: ".$q. "</br>";
				  //echo "Qno: ".$aca_qno. "</br>";
				  echo "Q Tot: ".$q_tot. "</br>";
				  echo "Q Cnt: ".$q_count. "</br>";
				  echo "Login: ".$login. "</br>";
			  }
				  
				  
			  // print the question form												
				  echo "<form name=\"academy\" method=\"post\" action=\"?id=".$id."&q=".$qno."&set=3&opt=1&login=1&key=".$key." \"enctype=\"multipart/form-data\">";
				  
				  echo "<table width=\"580\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";	
				  // get data			
				  //$npage = $page;
				  $result_set = get_aca_question_rec($qno, 0); // question number and subject/tool or 0 for all
				  while ($row = mysqli_fetch_array($result_set)) {		
					  $question_id = $row["question_id"];
					  $exam_id = $row["exam_id"];		
					  $question_no = $row["question_no"];	
					  $question_text = $row["question_text"];	
					  $question_note = $row["question_note"];
					  $option_a_text = $row["option_a_text"];
					  $option_b_text = $row["option_b_text"];	
					  $option_c_text = $row["option_c_text"];	
					  $option_d_text = $row["option_d_text"];
					  $option_e_text = $row["option_e_text"];
					  $option_a_answer = $row["option_a_answer"];	
					  $option_b_answer = $row["option_b_answer"];	
					  $option_c_answer = $row["option_c_answer"];	
					  $option_d_answer = $row["option_d_answer"];	
					  $option_e_answer = $row["option_e_answer"];	
					  $create_date = $row["create_date"];			
					  
					  
				  // print list detail	
				  echo "<tr>";
						  echo "<td align=\"left\"><h2><strong>Question ".$q_display.$msg."</strong></h2></td>"; // $question_id
				  echo "</tr>";	
				  echo "<tr>";
					  echo "<td align=\"right\" class=\"topline\">&nbsp;</td>";
				  echo "</tr>";
				  echo "<tr>";
						  echo "<td align=\"left\"><strong>".$question_text."</strong></td>";
				  echo "</tr>";
				  echo "<tr>";
					  echo "<td align=\"right\">&nbsp;</td>";
				  echo "</tr>";
				  echo "<tr>";
						  echo "<td align=\"left\">".$question_note."</td>";
					  echo "</tr>";			
				  echo "</table>";
				  
				  $oa = "<input class=\"qu\" type=\"checkbox\" value=\"1\" name=\"answ_a\" id=\"answ_a\" ".$oac.$dis."  />";
				  $ob = "<input class=\"qn\" type=\"checkbox\" value=\"1\" name=\"answ_b\" id=\"answ_b\" ".$obc.$dis." />";
				  $oc = "<input class=\"qu\" type=\"checkbox\" value=\"1\" name=\"answ_c\" id=\"answ_c\" ".$occ.$dis." />";
				  $od = "<input class=\"qn\" type=\"checkbox\" value=\"1\" name=\"answ_d\" id=\"answ_d\" ".$odc.$dis." />";
				  $oe = "<input class=\"qu\" type=\"checkbox\" value=\"1\" name=\"answ_e\" id=\"answ_e\" ".$oec.$dis." />";
					  
				  
				  echo "<table width=\"580\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";	
				  echo "<tr>";
					  echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
				  echo "</tr>";	
				  echo "<tr>";
						  echo "<td class=\"qu\" align=\"left\" width=\"30\">A</td>";
						  echo "<td class=\"qu ".$fa."\" align=\"left\" width=\"500\">".nl2br($option_a_text)."</td>";
						  echo "<td class=\"qu\" align=\"left\">&nbsp;&nbsp;".$oa."</td>";
				  echo "</tr>";
				  echo "<tr>";
					  echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
				  echo "</tr>";
				  echo "<tr>";
						  echo "<td class=\"qn\" align=\"left\">B</td>";
						  echo "<td class=\"qn ".$fb."\" align=\"left\">".nl2br($option_b_text)."</td>";
						  echo "<td class=\"qn\" align=\"left\">&nbsp;&nbsp;".$ob."</td>";
				  echo "</tr>";
				  if ($option_c_text <> "") {
					echo "<tr>";
						echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
					echo "</tr>";
					echo "<tr>";
							echo "<td class=\"qu\" align=\"left\">C</td>";
							echo "<td class=\"qu ".$fc."\" align=\"left\">".nl2br($option_c_text)."</td>";
							echo "<td class=\"qu\" align=\"left\">&nbsp;&nbsp;".$oc."</td>";
					echo "</tr>";
				  }
				  if ($option_d_text <> "") {
					echo "<tr>";
						echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
					echo "</tr>";
					echo "<tr>";
							echo "<td class=\"qn\" align=\"left\">D</td>";
							echo "<td class=\"qn ".$fd."\" align=\"left\">".nl2br($option_d_text)."</td>";
							echo "<td class=\"qn\" align=\"left\">&nbsp;&nbsp;".$od."</td>";
					echo "</tr>";
				  }
				  if ($option_e_text <> "") {
					echo "<tr>";
						echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
					echo "</tr>";
					echo "<tr>";
							echo "<td class=\"qu\" align=\"left\">E</td>";
							echo "<td class=\"qu ".$fe."\" align=\"left\">".nl2br($option_e_text)."</td>";
							echo "<td class=\"qu\" align=\"left\">&nbsp;&nbsp;".$oe."</td>";
					echo "</tr>";
				  }
				  echo "<tr>";
					  echo "<td align=\"right\" colspan=\"3\">&nbsp;</td>";
				  echo "</tr>";
				  } // end of result_set
						  
				  // table footer	
				  echo "</tr>";
				  echo "<tr>";
					  echo "<td align=\"right\" colspan=\"3\" class=\"topline\">&nbsp;</td>";
				  echo "</tr>";
				  echo "<tr>";
					  echo "<td align=\"right\" colspan=\"3\">".$upErr." ".$up."</td>";
				  echo "</tr>";
				  // end of table
				  echo "</table>";	
				  echo "</form>";			
				  
		  
	  
	  } // end of set = 1 and create session
	  
	  
	  
	
	if ($set <> 0) { // select a test		  				
	  // print reset button and question number	
	  $reset = "<button type=\"button\" class=\"btn btn-warning\" onclick=\"MM_goToURL('parent','?id=0&q=0&set=0&login=1&testout=1&opt=99&key=0');return document.MM_returnValue\"/>Reset Test</button>";
		if ($set == 2) {		
			$qnprint = 	"[".$question_id."]";
			if ($question_id == 0) { $qnprint = ""; }																			  
		  	echo "<p>".$reset." ".$qnprint." </p>";
		} else { echo "<p>".$reset."</p>"; }
		
		
	} // end of set <> 0 reset button
			
			
	} // end of test_on()	
	
	
	if ($set == 0) { // select a question		
		if ($opt == 1) {
			// display session number form
		echo "<h2>Select a Quiz Session</h2>";
		echo "<form name=\"test\" method=\"post\" action=\"?id=".$id."&opt=0&set=1&opt=".$opt."&login=1\" role=\"form\">";
		
		echo "<div class=\"form-group\">";
		echo "<label for=\"quiz_id\">Quiz Session ID</label>";
		echo "<input name=\"quiz_id\" type=\"\" class=\"form-control isNum\" id=\"quiz_id\" placeholder=\"\" value=\"\">";
		echo "</div>";
		echo "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\"> Display Quiz </button>";
		echo "</form>";	
	
		}
	
		if ($opt >= 51) {					
		echo "<h2>Select a Question</h2>";
		echo "<form name=\"test\" method=\"post\" action=\"?id=".$id."&opt=0&set=1&opt=".$opt."&login=1\" role=\"form\">";
		
		echo "<div class=\"form-group\">";
		echo "<label for=\"exam_id\">Exam</label>";
			echo "<select name=\"exam_id\" class=\"form-control reqd\" onchange=\"showTool(this.value)\" id=\"exam_id\">";					
						  //echo "<option value=\"".$exam_id."\">".$exam."</option>";					
// TEMP update with DB lookup to only display exams that the user has access to
						  echo "<option value=\"\"></option>";
								$result_set19 = get_aca_user_exam_list(0,999,1,$userx,$today); // get_aca_user_exam_list($npage, $no_of_list, $status, $cat, $expiry_date) {
								while ($row = mysqli_fetch_array($result_set19)) {
									$dim_key = $row["exam_id"]; 
									$display_name = $row["code"];
										$result_setI = get_aca_exam_rec($dim_key);
										while ($row = mysqli_fetch_array($result_setI)) {		
											$display_name = $row["code"];		
										}
									echo "<option value=\"" . $dim_key . "\">" . $display_name . "</option>";
								}
			echo "</select>";
		echo "</div>";
		
		echo "<div class=\"form-group\">";
		echo "<label for=\"qno\">Question Number</label>";
		echo "<input name=\"qno\" type=\"\" class=\"form-control isNum\" id=\"qno\" placeholder=\"\" value=\"\">";
		echo "</div>";
		echo "<button name=\"submit\" type=\"submit\" class=\"btn btn-default\"> Display Question </button>";
		echo "</form>";
		
		}
		
		echo "<p>&nbsp;</p>";	
	  
	} // end of set = 0
	
	
	
} // end of test id = 10
?>
 