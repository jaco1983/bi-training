<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>Subscription Summary</h2><div class="colored-line-left"></div></div>
<div class=\"clearfix\"></div>
<div class="row content-pad" style="padding-top: 10px;">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 10px;">
			<?php
			
				$sqldate = "select transactiondate from aca_transaction where reference = '".$_SESSION["paygateDetails"]["reference"]."'";
				$dateRow = mysqli_query($connection,$sqldate);
				
				while($rowdate = mysqli_fetch_array($dateRow)){
					$transdate = $rowdate["transactiondate"];
				}				
				
				//Get payment information from PayPal
				$item_number = $_GET['item_number']; 
				$txn_id = $_GET['tx'];
				$payment_gross = $_GET['amt'];
				$currency_code = $_GET['cc'];
				$payment_status = $_GET['st'];

				if($payment_status == 'Completed'){
					$status = '1';
				} else {
					$status = '0';
				}
				//Get product price from database
				$query = "select a.*,b.* from aca_exam a inner join aca_user_exam b on a.exam_id=b.exam_id where a.exam_id = '".$_SESSION["payeeDetails"]["examid"]."' limit 1" ;

                $productRow = mysqli_query( $connection, $query);
				//$productResult = $db->query("SELECT cost FROM aca_exam WHERE id = ".$item_number);
				//$productRow = $productResult->fetch_assoc();
				while ($row = mysqli_fetch_array($productRow)) {
					$productPrice = $row['cost'];
					$code = $row["code"];
                    $expiry_date = $row["expiry_date"];
					$pdfq = $row['pdf'];
                    if($pdfq != 0) {
                        $num_questions = $row["pdf_nr_questions"];
                        generateExamPDF2($transdate,$_SESSION["payeeDetails"]["examid"],$num_questions,$_SESSION["paygateDetails"]["reference"]);
						//echo $item_number;
                    }
				}

				if(!empty($txn_id)){
					//Check if payment data exists with the same TXN ID.
					$sql = "SELECT reference,paygatetransactionid FROM aca_transaction WHERE paygatetransactionid = '".$txn_id."' and reference = '".$_SESSION["paygateDetails"]["reference"]."'";
					$prevPaymentResult = mysqli_query( $connection, $sql);

					if(mysqli_num_rows($prevPaymentResult) > 0){
						while ($row = mysqli_fetch_array($prevPaymentResult)) {
                            $last_insert_id = $row['reference'];
						}
					}else{
                        $t = currencyConverter('USD', 'ZAR', '1');
                        //Insert tansaction data into the database
						$sql = "Update aca_transaction set paygatetransactionid = '".$txn_id."',paymentcomplete = '1',conversion = '".formatMoney($t,2)."' where reference = '".$_SESSION["paygateDetails"]["reference"]."'";
						mysqli_query( $connection, $sql);
						//$insert = $db->query("INSERT INTO payments(item_number,txn_id,payment_gross,currency_code,payment_status) VALUES('".$item_number."','".$txn_id."','".$payment_gross."','".$currency_code."','".$payment_status."')");
						$last_insert_id = $_SESSION["paygateDetails"]["reference"];
					}
			?>
                <table class="table" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th style="background:#58595b;color:#FFFFFF;">Payment and Invoice summary</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Thank you. Your payment was successful.<br />
                        <br />
                        <div class="form-group">
                            <label class="control-label col-sm-4">Reference:</label>
                            <div class="col-sm-4"><?php echo $last_insert_id; ?></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4">Exam:</label>
                            <div class="col-sm-4"><?php echo $_GET["item_name"]; ?></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4">Amount:</label>
                            <div class="col-sm-4">$ <?php echo $_GET["amt"]; ?></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4">Payment Status:</label>
                            <div class="col-sm-4"><font color="#0F0"><?php echo $_GET["st"]; ?></font></div>
                            <div class="clearfix"></div>
                        </div>
                    <p>For any queries please contact us via email <a href="mailto:<?php echo $support_email; ?>"><?php echo $support_email; ?></a>.</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center"><a href="?id=21&login=1&opt=0&key=0&set=0" class="btn btn-default">Back to Exam List</a></td>
                    </tr>
                    </tbody>
                </table>
                    <?php

                         $reference = $_SESSION["paygateDetails"]["reference"];
                         $paymentcomplete = 1;
                         $paygatestatus = "Successful";



                         $query = "update aca_user_exam set status='1' where user_id = '".$userx."' and exam_id = '".$_SESSION["payeeDetails"]["examid"]."'" ;
                         $result = mysqli_query( $connection, $query);

                         if($result){
                         include 'lib/swift_required.php';

                         $msgcontent = '';
                         $output = '';

                         $message = Swift_Message::newInstance();
                         $message->setSubject("BlackboardBI Training Exam Subscription");
                         $message->setFrom(array($admin_email => $admin_email));
                         $message->setTo(array($_SESSION["payeeDetails"]["email"] => $_SESSION["payeeDetails"]["firstname"]." ".$_SESSION["payeeDetails"]["surname"]));
                         $message->setBcc(array("jaco@itechnologiessolutions.co.za" => "Jaco Ferreira"));
                         $message->setBody('This email requires HTML to view.');

                         $msgcontent.= "<p>Dear ".$_SESSION["payeeDetails"]["firstname"]." ".$_SESSION["payeeDetails"]["surname"].", </p>";

                         $msgcontent.= "<p>&nbsp;</p>";

                             $msgcontent.= "<p>Thank you for subscribing to the <strong>".$code."</strong> exam. Your payment reference is <strong>".$_SESSION["paygateDetails"]["reference"]."</strong>.</p>";
                             $msgcontent.= "<p>Your payment was successful. You'll have access to the exam until <strong>".$expiry_date."</strong>.</p>";
                             $msgcontent.= "<p>&nbsp;</p>";
                             $msgcontent.= "<p>Regards</p>";
                             $msgcontent.= "<p>The Webmaster</p>";

                        $pdf = generateInvoice2($transdate,$_SESSION["paygateDetails"]["reference"],"paypal");


                        $message->addPart($msgcontent, 'text/html');
                        $attachment = Swift_Attachment::fromPath('pdf/'.date("ymd",strtotime($transdate)).'_'.$_SESSION["paygateDetails"]["reference"].'.pdf','application/pdf');
                        $message->attach($attachment);
                        if($pdfq == "1"){


                                $attachment2 = Swift_Attachment::fromPath('pdf/exams/'.date("ymd",strtotime($transdate)).'_'.$_SESSION["paygateDetails"]["reference"].'.pdf','application/pdf');
                                $message->attach($attachment2);

                        }

                        $transport = Swift_MailTransport::newInstance();
                        $mailer = Swift_Mailer::newInstance($transport);
                        $result = $mailer->send($message);
                        }
                 }else{ ?>
                    <table class="table" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                        <tr>
                            <th style="background:#58595b;color:#FFFFFF;">Payment and Invoice summary</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Payment and Invoice summary<br />
                            <br />
                            Thank you. Your payment was unsuccessful.<br />
                            <br />
                            <div class="form-group">
                                    <label class="control-label col-sm-4">Reference:</label>
                                    <div class="col-sm-4"><?php echo $last_insert_id; ?></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Exam:</label>
                                    <div class="col-sm-4"><?php echo $_GET["item_name"]; ?></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Amount:</label>
                                    <div class="col-sm-4">$ <?php echo $_GET["amt"]; ?></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Payment Status:</label>
                                    <div class="col-sm-4"><font color="#F00"><?php echo $_GET["st"]; ?></font></div>
                                    <div class="clearfix"></div>
                                </div>
                                <p>If you have any queries please send us an email with detailed information.</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
			<?php } 

			?>

</div>
</div>