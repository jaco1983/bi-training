/**
 * Created by JacoF on 11/14/2015.
 */

function getCustomerPage(){
    $.post( "ajax/customer-ajax.php", { action:"GetPage" }, function(data){
            if(data.result == 1){
                $("#main-content").html(data.html);
            }
        },
        "json");
}

function addCustomer(){
    $("#addCustomer").modal("show");
}

function listCustomer(){

    $.post( "ajax/customer-ajax.php", { action:"ListCustomer"}, function(data){
            if(data.result == 1){
                $(".customer-table").html(data.html);
            }
        },
        "json");

}

function addCustomerSave(){
    var customername = $("#addCustomer #addCustomerName").val();
    var customercategory = $("#addCustomer #addCustomerCategory").val();
    var customervat = $("#addCustomer #addCustomerVat").val();
    var salesrep = $("#addCustomer #addCustomerSalesRep").val();
    var customerstatus = $("#addCustomer #addCustomerStatus").val();
    var customertel = $("#addCustomer #addCustomerTelW").val();
    var customerfax = $("#addCustomer #addCustomerFax").val();
    var customerphysical1 = $("#addCustomer #addCustomerPhysicalAddress1").val();
    var customerphysical2 = $("#addCustomer #addCustomerPhysicalAddress2").val();
    var customerphysical3 = $("#addCustomer #addCustomerPhysicalAddress3").val();
    var customerphysical4 = $("#addCustomer #addCustomerPhysicalAddress4").val();
    var customerphysical5 = $("#addCustomer #addCustomerPhysicalAddress5").val();
    var customerpostal1 = $("#addCustomer #addCustomerPostalAddress1").val();
    var customerpostal2 = $("#addCustomer #addCustomerPostalAddress2").val();
    var customerpostal3 = $("#addCustomer #addCustomerPostalAddress3").val();
    var customerpostal4 = $("#addCustomer #addCustomerPostalAddress4").val();
    var customerpostal5 = $("#addCustomer #addCustomerPostalAddress5").val();
    var customercontactname = $("#addCustomer #addCustomerContactName").val();
    var customercontactemail = $("#addCustomer #addCustomerContactEmail").val();
    var customercontacttel = $("#addCustomer #addCustomerContactTel").val();
    var customercontactmobile = $("#addCustomer #addCustomerContactMobile").val();
    var customercontactfax = $("#addCustomer #addCustomerContactFax").val();

    $.post( "ajax/customer-ajax.php", { action:"AddCustomer",customername:customername,customercategory:customercategory,customervat:customervat,salesrep:salesrep,
        customerstatus:customerstatus,customertel:customertel,customerfax:customerfax,customerphysical1:customerphysical1,customerphysical2:customerphysical2,
            customerphysical3:customerphysical3,customerphysical4:customerphysical4,customerphysical5:customerphysical5,customerpostal1:customerpostal1,customerpostal2:customerpostal2,
            customerpostal3:customerpostal3,customerpostal4:customerpostal4,customerpostal5:customerpostal5,customercontactname:customercontactname,customercontactemail:customercontactemail,
            customercontacttel:customercontacttel,customercontactmobile:customercontactmobile,customercontactfax:customercontactfax}, function(data){
            if(data.result == 1){
                customername.val('');
                customercategory.val('');
                customervat.val('');


                customertel.val('');
                customerfax.val('');
                customerphysical1.val('');
                customerphysical2.val('');
                customerphysical3.val('');
                customerphysical4.val('');
                customerphysical5.val('');
                customerpostal1.val('');
                customerpostal2.val('');
                customerpostal3.val('');
                customerpostal4.val('');
                customerpostal5.val('');
                customercontactname.val('');
                customercontactemail.val('');
                customercontacttel.val('');
                customercontactmobile.val('');
                customercontactfax.val('');

                $("#addCustomer").modal("hide");
            }
        },
        "json");

}