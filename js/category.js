/**
 * Created by JacoF on 11/10/2015.
 */
$(document).ready(function() {

});

function getCategoryPage(){
    $.post( "ajax/category-ajax.php", { action:"GetPage" }, function(data){
            if(data.result == 1){
                $("#main-content").html(data.html);
            }
        },
        "json");
}

function addCategory(){
    $("#addCategory").modal("show");
}

function listCategory(){

    var amount = inte;

    $.post("ajax/category-ajax.php", {action: "CategoryNum"}, function (data) {
            if (data.result == 1) {
                var number = data.rows;
                $('#page-selection').bootpag({
                    total: number
                }).on("page", function (event, /* page number here */ num) {

                    $.post("ajax/category-ajax.php", {action: "ListCategory", page: num, amount:amount}, function (data) {
                            if (data.result == 1) {
                                $(".category-table").html(data.html);
                            }
                        },
                        "json");
                });
            }
        },
        "json");
}

function addCategorySave(){
    var category = $("#addCategory #addCategoryName").val();

    $.post( "ajax/category-ajax.php", { action:"AddCategory",category:category }, function(data){
            if(data.result == 1){
                $("#addCategory").modal("hide");
            }
        },
        "json");
}
