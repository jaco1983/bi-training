/**
 * Created by Jaco on 2016/09/24.
 */
$(document).ready(function() {

});


function getAddExpensePage(){
    $.post( "ajax/expense-ajax.php", { action:"GetAddPage" }, function(data){
            if(data.result == 1){
                $("#pleaseWaitDialog").modal("hide");
                $("#main-content").html(data.html);
            }
        },
        "json");
}

function getExpensePage(){
    $.post( "ajax/expense-ajax.php", { action:"GetPage" }, function(data){
            if(data.result == 1){
                $("#main-content").html(data.html);
            }
        },
        "json");
}

function listExpense(){
    var amount = inte;

    $.post("ajax/expense-ajax.php", {action: "ExpenseNum"}, function (data) {
            if (data.result == 1) {
                var number = data.rows;
                $('#page-selection').bootpag({
                    total: number
                }).on("page", function (event, /* page number here */ num) {

                    $.post("ajax/expense-ajax.php", {action: "ListExpense", page: num, amount:amount}, function (data) {
                            if (data.result == 1) {
                                $(".expense-table").html(data.html);
                            }
                        },
                        "json");
                });
            }
        },
        "json");
}

function printInvoice(id){
    $.post( "ajax/invoice-ajax.php", { action:"PrintInvoice",id:id}, function(data){
            if(data.result == 1){
                //$("#main-content").html(data.html);
                window.open(data.url, '_blank');
            }
        },
        "json");
}
