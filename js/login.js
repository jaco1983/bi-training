/**
 * Created by JacoF on 11/10/2015.
 */

function login(){

    var username = $("#login-username").val();
    var password = $("#login-password").val();

    $.post( "ajax/login-ajax.php", { action:"LogIn",username:username,password:password }, function(data){
            if(data.result == 1){
                location.reload();
            } else {

            }
        },
        "json");
}

function logout(){
    $.post( "ajax/login-ajax.php", { action:"LogOut" }, function(data){
            if(data.result == 1){
                location.reload();
            }
        },
        "json");
}