/**
 * Created by JacoF on 12/1/2015.
 */
$(document).ready(function() {

});


function getAddInvoicePage(){
    $.post( "ajax/invoice-ajax.php", { action:"GetAddPage" }, function(data){
            if(data.result == 1){
                $("#main-content").html(data.html);
            }
        },
        "json");
}

function getInvoicePage(){
    $.post( "ajax/invoice-ajax.php", { action:"GetPage" }, function(data){
            if(data.result == 1){
                $("#main-content").html(data.html);
            }
        },
        "json");
}

function listInvoice(){
    var amount = inte;

    $.post("ajax/invoice-ajax.php", {action: "InvoiceNum"}, function (data) {
            if (data.result == 1) {
                var number = data.rows;
                $('#page-selection').bootpag({
                    total: number
                }).on("page", function (event, /* page number here */ num) {

                    $.post("ajax/invoice-ajax.php", {action: "ListInvoice", page: num, amount:amount}, function (data) {
                            if (data.result == 1) {
                                $(".invoice-table").html(data.html);
                            }
                        },
                        "json");
                });
            }
        },
        "json");
}

function printInvoice(id){
    $.post( "ajax/invoice-ajax.php", { action:"PrintInvoice",id:id}, function(data){
            if(data.result == 1){
                //$("#main-content").html(data.html);
                window.open(data.url, '_blank');
            }
        },
        "json");
}
