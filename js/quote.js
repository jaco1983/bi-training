/**
 * Created by JacoF on 12/1/2015.
 */

$(document).ready(function() {

});

function getAddQuotePage(){
    $.post( "ajax/quote-ajax.php", { action:"GetAddPage" }, function(data){
            if(data.result == 1){
                $("#main-content").html(data.html);
            }
        },
        "json");
}

function getQuotePage(){
    $.post( "ajax/quote-ajax.php", { action:"GetPage" }, function(data){
            if(data.result == 1){
                $("#main-content").html(data.html);
            }
        },
        "json");
}

function listQuote(inte){
        var amount = inte;

    $.post("ajax/quote-ajax.php", {action: "QuoteNum"}, function (data) {
            if (data.result == 1) {
                var number = data.rows;
                $('#page-selection').bootpag({
                    total: number
                }).on("page", function (event, /* page number here */ num) {

                    $.post("ajax/quote-ajax.php", {action: "ListQuote", page: num, amount:amount}, function (data) {
                            if (data.result == 1) {
                                $(".quote-table").html(data.html);
                            }
                        },
                        "json");
                });
            }
        },
        "json");
}

function printQuote(id){
    $.post( "ajax/quote-ajax.php", { action:"PrintQuote",id:id}, function(data){
            if(data.result == 1){
                //$("#main-content").html(data.html);
                window.open(data.url, '_blank');
            }
        },
        "json");
}

function editQuote(id){
    $.post( "ajax/quote-ajax.php", { action:"GetEditPage" }, function(data){
            if(data.result == 1){
                $("#main-content").html(data.html);
            }
        },
        "json");
}

