/**
 * Created by JacoF on 12/1/2015.
 */
$(document).ready(function() {

});

function getItemAddPage(){
    $.post( "ajax/item-ajax.php", { action:"GetAddPage" }, function(data){
            if(data.result == 1){
                $("#main-content").html(data.html);
            }
        },
        "json");
}

function getItemPage(){
    $.post( "ajax/item-ajax.php", { action:"GetPage" }, function(data){
            if(data.result == 1){
                $("#main-content").html(data.html);
            }
        },
        "json");
}

function listItem(){
    var amount = inte;

    $.post("ajax/item-ajax.php", {action: "ItemNum"}, function (data) {
            if (data.result == 1) {
                var number = data.rows;
                $('#page-selection').bootpag({
                    total: number
                }).on("page", function (event, /* page number here */ num) {

                    $.post("ajax/item-ajax.php", {action: "ListItem", page: num, amount:amount}, function (data) {
                            if (data.result == 1) {
                                $(".item-table").html(data.html);
                            }
                        },
                        "json");
                });
            }
        },
        "json");
}
