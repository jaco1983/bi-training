<?php
/**
 * Created by PhpStorm.
 * User: JacoF
 * Date: 11/9/2015
 * Time: 11:49 PM
 */

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

@session_start();

ini_set("display_errors", 1);
$action = $_REQUEST['action'];

switch($action) {
    case "DownloadPDF":

        require_once("../includes/connection.php");

        require_once("../includes/functions.php");

        require_once("../includes/form_functions.php");

        require_once("../includes/config.php");
		
		$sql = "select * from aca_transaction where id = '".$_POST["transactionid"]."'";

		$result_set2 = mysqli_query($connection, $sql);

            while ($row = mysqli_fetch_array($result_set2)) {
				$file = date("ymd",strtotime($row["transactiondate"])).'_'.$row["reference"].'.pdf';
				$reference = $row["reference"];
				$transdate = $row["transactiondate"];
				$transDetails = unserialize($row["s_transaction"]);
			}
		
		//$file = $_POST["transdate"].'-'.$_POST["transactionid"].'-'.$_POST["reference"].'.pdf';
		
		$path = 'https://training.blackboardbi.com/pdf/exams/';
		
		$data_link = $path.''.$file;
		
		if(file_exists($data_link)){
			$file = $data_link;
		} else {
			
			$sqlpdf = "select pdf,pdf_nr_questions from aca_exam where exam_id = '".$transDetails[0]["examid"]."'";
				
				$pdfresult_set = mysqli_query($connection, $sqlpdf);
				
				while ($row4 = mysqli_fetch_array($pdfresult_set)) {
					$pdf = $row4["pdf"];
					$num_questions = $row4["pdf_nr_questions"];
					
					if($pdf == "1"){
						generateExamPDF($transdate,$transDetails[0]["examid"],$num_questions,$reference);
					}
				}
			$file = $path.''.$file;
		}
		
		$json = array("result" => 1,"link" => $file);
        
        echo json_encode($json);
        break;
	case "ApproveTransaction":

        require_once("../includes/connection.php");

        require_once("../includes/functions.php");

        require_once("../includes/form_functions.php");

        require_once("../includes/config.php");

        $sql = "update aca_transaction set paymentcomplete = '1' where id = '".$_POST["id"]."'";

        $result_set = mysqli_query($connection, $sql);

        if (mysqli_affected_rows($connection) <= 1) {

            $sql2 = "select * from aca_transaction where id = '".$_POST["id"]."'";

            $result_set2 = mysqli_query($connection, $sql2);

            while ($row = mysqli_fetch_array($result_set2)) {
                $payeeDetails = unserialize($row["s_details"]);
                $transDetails = unserialize($row["s_transaction"]);
				$transdate = $row["transactiondate"];

                $startdate = strtotime("Saturday");
                $d = (strtotime("+6 weeks", $startdate));
                $expiry_date = date("Y-m-d", $d);

                $sqlu = "update aca_user_exam set status = '1', expiry_date = '" . $expiry_date . "' where user_exam_id = '" . $payeeDetails["userexamid"] . "'";

                $result_setu = mysqli_query($connection, $sqlu);
				
				//$sqlpdf = "select pdf,pdf_nr_questions from aca_exam where exam_id = '".$payeeDetails["userexamid"]."'";
				$sqlpdf = "select pdf,pdf_nr_questions from aca_exam where exam_id = '".$transDetails[0]["examid"]."'";
				
				$pdfresult_set = mysqli_query($connection, $sqlpdf);
				
				while ($row4 = mysqli_fetch_array($pdfresult_set)) {
					$pdf = $row4["pdf"];
					$num_questions = $row4["pdf_nr_questions"];
					
					if($pdf == "1"){
						generateExamPDF($transdate,$transDetails[0]["examid"],$num_questions,$row["reference"]);
					}
				}

                include '../lib/swift_required.php';

                $msgcontent = '';
                $output = '';

                $message = Swift_Message::newInstance();
                $message->setSubject("BlackboardBI Training Exam Subscription");
                $message->setFrom(array($admin_email => "BlackboardBI Training"));
                $message->setTo(array($payeeDetails["email"] => $payeeDetails["firstname"] . " " . $payeeDetails["surname"]));
                $message->setBcc(array("jaco@itechnologiessolutions.co.za" => "Jaco Ferreira"));
                $message->setBody('This email requires HTML to view.');

                $msgcontent .= "<p>Dear " . $payeeDetails["firstname"] . " " . $payeeDetails["surname"] . ", </p>";

                $msgcontent .= "<p>&nbsp;</p>";

                $msgcontent .= "This email serves to notify you that your EFT payment for the " . $transDetails[0]["description"] . " exam was successfull.";
                $msgcontent .= "You'll have access to the exam until <strong>" . $expiry_date . "</strong>.";
                $msgcontent .= "<p>&nbsp;</p>";
                $msgcontent .= "<p>Regards</p>";
                $msgcontent .= "<p>The Webmaster</p>";
                $pdf2 = generateInvoice($transdate,$row["paymentreference"],"paypal");

                $message->addPart($msgcontent, 'text/html');
                $attachment = Swift_Attachment::fromPath('../pdf/'.date("ymd",strtotime($transdate)).'_'.$row["paymentreference"].'.pdf','application/pdf');
                $message->attach($attachment);
				$attachment2 = Swift_Attachment::fromPath('../pdf/exams/'.date("ymd",strtotime($transdate)).'_'.$row["paymentreference"].'.pdf','application/pdf');
                $message->attach($attachment2);

                $transport = Swift_MailTransport::newInstance();
                $mailer = Swift_Mailer::newInstance($transport);
                $result = $mailer->send($message);
                $result = true;

                if($result){
                    $json = array("result" => 1);
                    unset($_SESSION["invpdf"]);
                } else {
                    $json = array("result" => 0);
                }
            }

        } else {
            $json = array("result" => 0);
        }

        echo json_encode($json);
        break;
    case "ConfirmTransaction":

        require_once("../includes/connection.php");

        require_once("../includes/functions.php");

        require_once("../includes/form_functions.php");

        require_once("../includes/config.php");

        $query = "select a.*,b.* from aca_exam a inner join aca_user_exam b on a.exam_id=b.exam_id where a.exam_id = '".$_SESSION["payeeDetails"]["examid"]."'";

        $result = mysqli_query( $connection, $query);

        while ($row = mysqli_fetch_array($result)) {
            $examdesc = $row["code"];
            $expire = $row["expiry_date"];
        }

        include '../lib/swift_required.php';

        $msgcontent = '';
        $output = '';

        $message = Swift_Message::newInstance();
        $message->setSubject("BlackboardBI Training Exam Subscription");
        $message->setFrom(array($admin_email => "BlackboardBI Training"));
        $message->setTo(array($_SESSION["payeeDetails"]["email"] => $_SESSION["payeeDetails"]["firstname"]." ".$_SESSION["payeeDetails"]["surname"]));
        $message->setBcc(array("jaco@itechnologiessolutions.co.za" => "Jaco Ferreira"));
        $message->setBody('This email requires HTML to view.');

        $msgcontent.= "<p>Dear ".$_SESSION["payeeDetails"]["firstname"]." ".$_SESSION["payeeDetails"]["surname"].", </p>";

        $msgcontent.= "<p>&nbsp;</p>";

        $msgcontent.= "Thank you for subscribing to the <strong>".$examdesc."</strong> exam.<br />";
        $msgcontent.= "<br />";
        $msgcontent.= "You will be notified and gain access to the exam once your EFT payment has been received.<br />";
        $msgcontent.= "<br />";
        $msgcontent.= "Please use <strong>".$_SESSION["paygateDetails"]["reference"]."</strong> as reference and email your proof of payment to <strong>".$admin_email."</strong>.<br />";
        $msgcontent.= '<br />';
        $msgcontent.= '<strong>Banking Details:</strong><br />';
        $msgcontent.= 'Bank : First National Bank<br />';
        $msgcontent.= 'Branch : FNB Private Clients Pretoria (South Africa)<br />';
        $msgcontent.= 'Branch Code : 259-745<br />';
        $msgcontent.= 'Account No : 62624724375<br />';
        $msgcontent.= 'Account Type : Cheque Account<br />';
        $msgcontent.= 'Swift code : FIRNZAJJ<br />';
        $msgcontent.= '<br />';
        $msgcontent.= 'For any queries please send an email to <a href="mailto:'.$support_email.'">'.$support_email.'</a>.<br />';
        $msgcontent.= '<br />';
        $msgcontent.= "<strong>Regards</strong><br />";
        $msgcontent.= "The Webmast";

        $pdf = generateInvoice($_SESSION["paygateDetails"]["reference"], "cc");

        $message->addPart($msgcontent, 'text/html');
        $attachment = Swift_Attachment::fromPath('pdf/' . date("ymd") . '_' . $_SESSION["paygateDetails"]["reference"] . '.pdf', 'application/pdf');
        $message->attach($attachment);

        $transport = Swift_MailTransport::newInstance();
        $mailer = Swift_Mailer::newInstance($transport);
        $result = $mailer->send($message);
        $result = true;

        if($result){
            $json = array("result" => "1");
        } else {
            $json = array("result" => "0");
        }

        echo json_encode($json);
		break;
    default:
        echo json_encode(array('message' => 'Unknown action'));
        break;
}

die();