<?php
/**
 * Created by PhpStorm.
 * User: JacoF
 * Date: 11/9/2015
 * Time: 11:49 PM
 */

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

@session_start();

ini_set("display_errors", 1);
$action = $_REQUEST['action'];

switch($action) {
    case "NewRegistration":

        $html = '';

        require_once("../includes/connection.php");
        require_once("../includes/functions.php");
        require_once("../includes/form_functions.php");
        require_once("../includes/config.php");
        require_once("../class/encryption.php");

        $instructor_id = 0;
        $userx = 0;
        $newpassword = "";
        // create a password

        $newpassword = "T";
        $newpassword .= rand(1,9);
        $newpassword .= rand(10,16);
        $newpassword .= "x";
        $newpassword .= rand(11,77);
        $newpassword .= "V";
        $newpassword .= rand(20,29);

        $encryption = new Encryption;

        $password = $encryption->encrypt_request($newpassword);

            $query = "INSERT INTO aca_user(username, password, email, name, instructor_id, access_level,status, creator_id, origpass, trial)  VALUES ( '".$_POST["r_username"]."', '".$password."', '".$_POST["r_email"]."', '".$_POST["r_name"]."', '".$instructor_id."','1','1','".$userx."', '".$newpassword."','".$_POST["r_trial"]."')";

            $result = mysqli_query( $connection, $query);
            $newuid = $connection->insert_id;


            if (mysqli_affected_rows($connection) == 1) {

                // get this user_id

                $query = "SELECT user_id from aca_user WHERE email = '".$_POST["r_email"]."' LIMIT 1 ";

                $result = mysqli_query( $connection, $query);

                if (mysqli_affected_rows($connection) == 1) {
                    while ($row = mysqli_fetch_array($result)) {
                        $newuid = $row["user_id"];
                    }
                }
                // send a mail to user

                include '../lib/swift_required.php';

                $msgcontent = '';
                $output = '';

                $message = Swift_Message::newInstance();
                $message->setSubject("BlackboardBI Training Registration");
                $message->setFrom(array($admin_email => "BlackboardBI Training"));
                $message->setTo(array($_POST["r_email"] => $_POST["r_name"]));
                $message->setBcc(array("jaco@itechnologiessolutions.co.za" => "Jaco Ferreira"));
                $message->setBody('This email requires HTML to view.');

                $msgcontent.= '<p>Dear '.$_POST["r_name"].', </p><br />
                                Thank you for registering on '.$site_name.'.<br />
                                &nbsp;<br />
                                You access the '.$site_name.' educational platform right now by clicking <a href="https://www.blackboardbi.com/training">here</a> and using the details below to log in.<br />
                                &nbsp;<br />
                                Here you\'ll be able to update your details, join a class, subscribe to an exam, complete an instructor quiz, and take a test.<br />
                                &nbsp;<br />
                                <strong>Here are your login details:</strong><br />
                                &nbsp;<br />
                                <strong>Username: '.$_POST["r_email"].'</strong><br />
                                <strong>Password: '.$newpassword.'</strong>
                                &nbsp;<br />
                                &nbsp;<br />
                                Regards<br />
                                The Webmaster<br />';

                $message->addPart($msgcontent, 'text/html');

                $transport = Swift_MailTransport::newInstance();
                $mailer = Swift_Mailer::newInstance($transport);
                $result = $mailer->send($message);


                // send a mail to admin

                $msgcontent = '';
                $output = '';

                $message = Swift_Message::newInstance();
                $message->setSubject("BlackboardBI Training Registration");
                $message->setFrom(array($admin_email => "BlackboardBI Training"));
                $message->setTo(array("jaco@itechnologiessolutions.co.za" => "Jaco Ferreira"));
                $message->setBody('This email requires HTML to view.');

                $msgcontent.= '<p>Attention, </p><br />
                                A registration was submitted from '.$site_name.'<br />
                                &nbsp;<br />
                                Name: '.$_POST["r_name"].'<br />
                                Create Date: '.date("Y-m-d H:i:s").'<br />
                                E-Mail: '.$_POST["r_email"].'<br />
                                &nbsp;<br />
                                &nbsp;<br />
                                Regards<br />
                                The Webmaster<br />';

                $message->addPart($msgcontent, 'text/html');

                $transport = Swift_MailTransport::newInstance();
                $mailer = Swift_Mailer::newInstance($transport);
                $result = $mailer->send($message);

                // Success

                $html .= "<div class=\"alert alert-success\">";
                $html .= "Your account has been created successfully, please refer to your email for your login details.";
                $html .= "</div>";

                $json = array("html" => $html,"result" => 1,"pass" => $password);

            } else {
                $html .= "<div class=\"alert alert-danger\">";
                $html .= "Registration failed.";
                $html .= "</div>";

                $json = array("html" => $html,"result" => 0);
            }

        echo json_encode($json);
        break;
    case "NewUserRegister":

        require_once("../includes/connection.php");
        require_once("../includes/functions.php");
        require_once("../includes/form_functions.php");
        require_once("../includes/config.php");
        require_once("../class/encryption.php");

        $html = '';

        $instructor_id = 0;
        $userx = 0;
        $newpassword = "";
        // create a password

        $newpassword = "T";
        $newpassword .= rand(1,9);
        $newpassword .= rand(10,16);
        $newpassword .= "x";
        $newpassword .= rand(11,77);
        $newpassword .= "V";
        $newpassword .= rand(20,29);

        $plain_password = $newpassword;

        $encryption = new Encryption;

        $query = "INSERT INTO aca_user( ";
        $query .= " username, password, email, name, idnumber, gender, phone, mobile,";
        $query .= " instructor_id, access_level,  ";
        $query .= " status, creator_id, origpass ) ";
        $query .= " VALUES (";
        $query .= " '".$_POST["username"]."', '".$encryption->encrypt_request($newpassword)."', '".$_POST["email"]."', '".$_POST["fullname"]."', '".$_POST["idnumber"]."', '".$_POST["gender"]."', '".$_POST["phone"]."', '".$_POST["mobile"]."',";
        $query .= " '".$instructor_id."', '1',  ";
        $query .= " '1', '".$userx."', '".$newpassword."' ) " ;

        $ins = $query;
        $result = mysqli_query( $connection, $query);
        $newuid = $connection->insert_id;


        if (mysqli_affected_rows($connection) == 1) {

            // get this user_id

            $query = "SELECT user_id from aca_user WHERE email = '".$_POST["email"]."' LIMIT 1 ";

            $result = mysqli_query( $connection, $query);

            if (mysqli_affected_rows($connection) == 1) {
                while ($row = mysqli_fetch_array($result)) {
                    $newuid = $row["user_id"];
                }
            }
            // send a mail to user

            include '../lib/swift_required.php';

            $msgcontent = '';
            $output = '';

            $message = Swift_Message::newInstance();
            $message->setSubject("BlackboardBI Training Registration");
            $message->setFrom(array($admin_email => "BlackboardBI Training"));
            $message->setTo(array($_POST["email"] => $_POST["fullname"]));
            $message->setBcc(array("jaco@itechnologiessolutions.co.za" => "Jaco Ferreira"));
            $message->setBody('This email requires HTML to view.');

            $msgcontent.= '<p>Dear '.$_POST["fullname"].', </p><br />
                                Thank you for registering on '.$site_name.'.<br />
                                &nbsp;<br />
                                You access the '.$site_name.' educational platform right now by clicking <a href="https://www.blackboardbi.com/training">here</a> and using the details below to log in.<br />
                                &nbsp;<br />
                                Here you\'ll be able to update your details, join a class, subscribe to an exam, complete an instructor quiz, and take a test.<br />
                                &nbsp;<br />
                                <strong>Here are your login details:</strong><br />
                                &nbsp;<br />
                                <strong>Username: '.$_POST["email"].'</strong><br />
                                <strong>Password: '.$newpassword.'</strong>
                                &nbsp;<br />
                                &nbsp;<br />
                                Regards<br />
                                The Webmaster<br />';

            $message->addPart($msgcontent, 'text/html');

            $transport = Swift_MailTransport::newInstance();
            $mailer = Swift_Mailer::newInstance($transport);
            $result = $mailer->send($message);


            // send a mail to admin

            $msgcontent = '';
            $output = '';

            $message = Swift_Message::newInstance();
            $message->setSubject("BlackboardBI Training Registration");
            $message->setFrom(array($admin_email => "BlackboardBI Training"));
            $message->setTo(array("jaco@itechnologiessolutions.co.za" => "Jaco Ferreira"));
            $message->setBody('This email requires HTML to view.');

            $msgcontent.= '<p>Attention, </p><br />
                                A registration was submitted from '.$site_name.'<br />
                                &nbsp;<br />
                                Name: '.$_POST["fullname"].'<br />
                                Create Date: '.date("Y-m-d H:i:s").'<br />
                                E-Mail: '.$_POST["email"].'<br />
                                &nbsp;<br />
                                &nbsp;<br />
                                Regards<br />
                                The Webmaster<br />';

            $message->addPart($msgcontent, 'text/html');

            $transport = Swift_MailTransport::newInstance();
            $mailer = Swift_Mailer::newInstance($transport);
            $result = $mailer->send($message);

            // Success

            $html .= "<div class=\"alert alert-success\">";
            $html .= "Registration successful. An email was send to the registered email address for validation.";
            $html .= "</div>";

            $json = array("html" => $html,"result" => 1,"pass" => $newpassword);

        } else {
            $html .= "<div class=\"alert alert-danger\">";
            $html .= "Registration failed.";
            $html .= "</div>";

            $json = array("html" => $html,"result" => 0);
        }

        echo json_encode($json);
        break;
    case "CheckEmail":

        $html = '';

        require_once("../includes/connection.php");
        require_once("../includes/functions.php");
        require_once("../includes/form_functions.php");
        require_once("../includes/config.php");

        $query = "SELECT email from aca_user WHERE email = '".$_POST["r_email"]."'";

        $result = mysqli_query( $connection, $query);

        if (mysqli_num_rows($result) > 0) {
            $html .= '<div class="alert alert-danger" role="alert">
                                        <a href="#" class="alert-link">Email already exists. You can reset your password.</a></div>';
            $json = array("html" => $html,"result" => 0);
        } else {
            $json = array("html" => $html,"result" => 1);
        }

        echo json_encode($json);
        break;
    case "EmailCompare":
        $html = '';
        if($_POST["nemail2"] == $_POST["ncemail2"]){
            $json = array("result" => 1, "html" => $html);
        } else {
            $html .= '<div class="alert alert-danger" role="alert">
                                        <a href="#" class="alert-link">Emails do not match. Please try again.</a>
                                  </div>';

            $json = array("result" => 0, "html" => $html);
        }

        echo json_encode($json);
        break;
    default:
        echo json_encode(array('message' => 'Unknown action'));
        break;
}

die();