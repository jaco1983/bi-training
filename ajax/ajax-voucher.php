<?php
/**
 * Created by PhpStorm.
 * User: JacoF
 * Date: 11/9/2015
 * Time: 11:49 PM
 */

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

@session_start();

ini_set("display_errors", 1);
$action = $_REQUEST['action'];

switch($action) {
    case "CalculateCost":
        require_once("../includes/connection.php");

        require_once("../includes/functions.php");

        require_once("../includes/form_functions.php");

        require_once("../includes/config.php");

        $key = $_POST["key"];
        $email = $_POST["email"];
        
		$html = '';
		
		$cost = formatMoney(($_SESSION["currencyconverter"] * $_POST["amount"]),2);
        if(isset($_POST["sub_voucher"]) && $_POST["sub_voucher"] != "") {
            $sql = "select * from aca_voucher where voucher_number = '".$_POST["sub_voucher"]."' and user_email = '".$_POST["email"]."'";

            $result = mysqli_query($connection, $sql);
            if(mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_array($result)) {
					if($row["expiry_date"] >= date("Y-m-d") && $row["status"] == "0"){
						$discount = ($_POST["amount"] / 100) * $row["discount"];
						$cost = formatMoney(($_SESSION["currencyconverter"] * ($_POST["amount"] - $discount)),2);
						if ($row["exam"] > 0 && $row["exam"] == $key) {                        
							$paygateamount = $cost;
							$json = array("result" => "1","amount" => $paygateamount);
						}
						if ($row["exam"] == "0") {
							$paygateamount = $cost;
							$json = array("result" => "1","amount" => $paygateamount);
						}
						if($row["discount"] == "100"){
							$paygateamount = "0";
							$json = array("result" => "1","amount" => $paygateamount);
						}
					} else {
						if($row["status"] == "1"){
							$html .= '<p>The voucher code you entered has already been used. </p>';
						}
						if($row["expiry_date"] < date("Y-m-d")){
							$html .= '<p>The voucher code you&apos;ve entered has expired and is no longer valid. </p>';
						}
						$paygateamount = $cost;

						$json = array("result" => "0","amount" => $paygateamount,"html" => $html);
						
					}
                }

            } else {
                $paygateamount = $cost;

                $json = array("result" => "1","amount" => $paygateamount,"html" => $html);
            }
        } else {
            $paygateamount = $cost;
            $json = array("result" => "1","amount" => $paygateamount,"html" => $html);
        }

        echo json_encode($json);
        break;
    case "SaveVoucher":

        $html = '';

        require_once("../includes/connection.php");

        require_once("../includes/functions.php");

        require_once("../includes/form_functions.php");

        require_once("../includes/config.php");

        if($_POST["voucherstatus"] == "on"){
            $status = 1;
        } else {
            $status = 0;
        }

        $sql = "insert into aca_voucher (voucher_number,user_email,discount,date_created,expiry_date,status,exam) values ('".$_POST["vouchernum"]."','".$_POST["voucheremail"]."','".$_POST["voucherdisc"]."','".date("Y-m-d H:i:s")."','".$_POST["voucherexpiry"]."','".$status."','".$_POST["voucherexam"]."')";

        $result_set = mysqli_query($connection, $sql);

        if (mysqli_affected_rows($connection) == 1) {

            include '../lib/swift_required.php';

            $msgcontent = '';
            $output = '';
			
            $message = Swift_Message::newInstance();
            $message->setSubject("BlackboardBI Training Voucher");
            $message->setFrom(array($admin_email => "BlackboardBI Training"));
            $message->setTo(array($_POST["voucheremail"] => $_POST["voucheremail"]));
            $message->setBcc(array("jaco@itechnologiessolutions.co.za" => "Jaco Ferreira"));
            $message->setBody('This email requires HTML to view.');

            $sql = "select * from aca_exam where exam_id = '".$_POST["voucherexam"]."'";

            $result_set = mysqli_query($connection, $sql);

            if (mysqli_affected_rows($connection) == 1) {
                while ($row = mysqli_fetch_array($result_set)) {
                    $msgcontent .= '<table>
                                <tr>
                                    <td><img src="https://www.blackboardbi.com/training/images/logo_blackboardtraining_200x62.png" /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><h1>' . $_POST["voucherdisc"] . '&#37; OFF </h1>
                                    <p>6 Week Exam Subscription</p>
                                    <p>Voucher code: ' . $_POST["vouchernum"] . '.</p>
                                    <p>Great news, if you sign up for a 6 week subscription to the '.$row["code"].' exam '.($_POST["voucherexpiry"] != '' ? 'before '.$_POST["voucherexpiry"] : '' ).', you\'ll get % off your exam subscription.</p>

                                    <a href="https://www.blackboardbi.com/training" style="display:block;padding:10px 30px;border-radius:5px;">SIGN UP</a></td></td>
                                </tr>

                            </table>';
                }
            } else {
                $msgcontent .= '<table>
                                <tr>
                                    <td><img src="https://www.blackboardbi.com/training/images/logo_blackboardtraining_200x62.png" /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><h1>' . $_POST["voucherdisc"] . '&#37; OFF </h1>
                                    <p>6 Week Exam Subscription</p>
                                    <p>Voucher code: ' . $_POST["vouchernum"] . '.</p>
                                    <p>Great news, if you sign up for a 6 week subscription to any of the available exams '.($_POST["voucherexpiry"] != '' ? 'before '.$_POST["voucherexpiry"] : '' ).',you\'ll get % off the exam subscription.</p>

                                    <a href="https://www.blackboardbi.com/training" style="display:block;padding:10px 30px;border-radius:5px;">SIGN UP</a></td>
                                </tr>

                            </table>';
            }

            $message->addPart($msgcontent, 'text/html');

            $transport = Swift_MailTransport::newInstance();
            $mailer = Swift_Mailer::newInstance($transport);
            $result = $mailer->send($message);

            if($result) {
                $html .= "<div class=\"alert alert-success\">
                                Voucher successfully saved and sent to the provided email address.
                          </div>";
            } else {
                $html .= '<div class="alert alert-danger" role="alert">
                                        <a href="#" class="alert-link">There was an error sending the voucher code to the provided email.</a>
                                  </div>';
            }

            $json = array("result" => "1","html" => $html);
        } else {
            $html .= '<div class="alert alert-danger" role="alert">
                                        <a href="#" class="alert-link">There was an error with the credentials provided. Please try again.</a>
                                  </div>';
            $json = array("result" => "0","html" => $html);
        }


        echo json_encode($json);
        break;
    case "UpdateVoucher":

        if($_POST["voucherstatus"] == "on"){
            $status = 1;
        } else {
            $status = 0;
        }

        $html = '';

        require_once("../includes/connection.php");

        require_once("../includes/functions.php");

        require_once("../includes/form_functions.php");

        require_once("../includes/config.php");

        $sql = "update aca_voucher set voucher_number = '".$_POST["vouchernum"]."',user_email = '".$_POST["voucheremail"]."',discount = '".$_POST["voucherdisc"]."',modified_date = '".date("Y-m-d H:i:s")."',expiry_date = '".$_POST["voucherexpiry"]."',exam = '".$_POST["voucherexam"]."',status = '".$status."' where voucher_id = '".$_POST["voucherid"]."'";

        $result_set = mysqli_query($connection, $sql);

        if (mysqli_affected_rows($connection) == 1) {
                $html .= "<div class=\"alert alert-success\">
                                Voucher successfully updated.
                          </div>";
            $json = array("result" => "1","html" => $html);
            } else {
                $html .= '<div class="alert alert-danger" role="alert">
                                        <a href="#" class="alert-link">There was an error sending the voucher code to the provided email.</a>
                                  </div>';
            $json = array("result" => "0","html" => $html);
            }

        echo json_encode($json);
        break;
    case "FindUser":

        $html = '';

        require_once("../includes/connection.php");

        require_once("../includes/functions.php");

        require_once("../includes/form_functions.php");

        require_once("../includes/config.php");

        $sql = "select * from aca_user where email like '%".$_POST["term"]."%'";

            $result_set2 = mysqli_query($connection, $sql);

            $html .= '<ul class="list-group" id="user-list">';

            foreach($result_set2 as $row) {

                $html .= "<li class=\"list-group-item\" onclick=\"selectUser('".$row["email"]."')\">".$row["email"]."</li>";
            }
            $html .= '</ul>';

            $json = array("result" => "1", "html" => $html);
        echo json_encode($json);
        break;
    case "GenerateVoucherNum":

        $voucher = '';

        $voucher .= rand(0,9);
        $voucher .= rand(10,16);
        $voucher .= rand(11,77);
        $voucher .= rand(20,29);
        $voucher .= rand(30,39);


        $json = array("result" => "1","voucher" => $voucher);

        echo json_encode($json);
        break;
    case "GetVoucher":
        require_once("../includes/connection.php");

        require_once("../includes/functions.php");

        require_once("../includes/form_functions.php");

        require_once("../includes/config.php");

        $sql = "select * from aca_voucher where voucher_id = '".$_POST["id"]."' limit 1";

        $result_set2 = mysqli_query($connection, $sql);

        foreach($result_set2 as $row) {
            $voucher_id = $row["voucher_id"];
            $voucher_number = $row["voucher_number"];
            $user_email = $row["user_email"];
            $discount = $row["discount"];
            $expiry_date = $row["expiry_date"];
            $exam = $row["exam"];
            $status = $row["status"];
        }

        $json = array("result" => "1","voucher_id" => $voucher_id,"voucher_number" => $voucher_number,"user_email" => $user_email,"discount" => $discount,"expiry_date" => $expiry_date,"exam" => $exam,"status" => $status);

        echo json_encode($json);
        break;
    default:
        echo json_encode(array('message' => 'Unknown action'));
        break;
}

die();