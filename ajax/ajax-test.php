<?php
/**
 * Created by PhpStorm.
 * User: JacoF
 * Date: 11/9/2015
 * Time: 11:49 PM
 */

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

@session_start();

ini_set("display_errors", 1);
$action = $_REQUEST['action'];

switch($action) {
    case "GetSubject":

        $html = '';

        require_once("../includes/connection.php");
        require_once("../includes/functions.php");
        require_once("../includes/form_functions.php");
        require_once("../includes/config.php");
        require_once("../class/encryption.php");

        $html = '';

        //$html .= "<label for=\"aca_subject\">Subject</label><select name=\"tool_id[]\" class=\"form-control reqd\" id=\"tool_id\"><option value=\"0\">All</option>";
        $html .= "<td>Subject</td><td><select name=\"tool_id[]\" class=\"form-control reqd\" id=\"tool_id\" multiple=\"multiple\"><option value=\"0\">All</option>";
        $result_set61 = get_aca_exam_tools_test11(1, $_POST["examid"]);
        while ($row = mysqli_fetch_array($result_set61)) {
            $tool = $row["tool"];
            $tool_id = $row["tool_id"];
            $html .= "<option value=\"".$tool_id."\">".$tool."</option>";
        }

        $html .= '</select></td><script>
        jQuery("#tool_id").multiselect({                                                                                                                                                                                                                                    

            columns: 1,

            placeholder: "Select options"
			
        });
		
		jQuery(\'[id^="ms-opt-"]\').on("click",function(){
			var sList = [];

			jQuery(\'[id^="ms-opt-"]\').each(function () {
				if($(this).is(":checked")){
				sList.push($(this).val());
				}
			});
			
			var dataArray = JSON.stringify(sList);
			var dataString = dataArray.toString();
			var exam_id = $("#exam_id").val();
			$.post( "ajax/ajax-test.php", { action:"GetQuestionCount",exam_id:exam_id,tool_id:dataString
				 }, function(data){
				 if(data.result == 1){
				 console.log(data.count);
				 console.log(data.test);
				 $("#exam_q_num").val(data.count);
				 }
				 },
				 "json");
			
		});
</script>';

        $json = array("result" => "1","html" => $html);

        echo json_encode($json);
        break;
		
		case "GetQuestionCount":

        require_once("../includes/connection.php");
        require_once("../includes/functions.php");
        require_once("../includes/form_functions.php");
        require_once("../includes/config.php");
        require_once("../class/encryption.php");

        $html = '';

		$tool_id = substr($_POST["tool_id"], 1, -1);
		$count = 0;
        //$html .= "<label for=\"aca_subject\">Subject</label><select name=\"tool_id[]\" class=\"form-control reqd\" id=\"tool_id\"><option value=\"0\">All</option>";
        $query = "select count(question_id) as cnt from aca_question where exam_id = '".$_POST["exam_id"]."' and tool_id in (".$tool_id.")";
        $result_set61 = mysqli_query($connection,$query);
        while ($row = mysqli_fetch_array($result_set61)) {
			$count = $row["cnt"];
        }

        $json = array("result" => "1","count" => $count,"test" => $tool_id);

        echo json_encode($json);
        break;

    case "GetPracExamQuestionDropdown":

        $html = '';

        require_once("../includes/connection.php");
        require_once("../includes/functions.php");
        require_once("../includes/form_functions.php");
        require_once("../includes/config.php");
        require_once("../class/encryption.php");

        $html = '';

        $query = "SELECT question_no,question_text FROM aca_question WHERE exam_id = '".$_POST["examid"]."'";

        $result_set61 = mysqli_query($connection, $query);

        $html .= '<select name="qno" id="qno" class="form-control col-sm-12">';
        while ($row = mysqli_fetch_array($result_set61)) {
            $html .= "<option value=\"".$row["question_no"]."\" style=\"max-width:50%\">".wordwrap($row["question_text"],600,"\n",true)."</option>";
        }
        $html .= '</select>';

        $json = array("result" => "1","html" => $html);

        echo json_encode($json);
        break;
    case "CountExams":
        $html = '';
        $cnt = 0;

        require_once("../includes/connection.php");
        require_once("../includes/functions.php");
        require_once("../includes/form_functions.php");
        require_once("../includes/config.php");
        require_once("../class/encryption.php");


        $query = "SELECT count(a.user_id) as `cnt`,count(b.user_id) as `cnt2` from aca_user_exam a inner join aca_user_class b on a.user_id = b.user_id where (a.user_id = '".$_SESSION['aca_userx']."' || b.user_id = '".$_SESSION['aca_userx']."') and (a.status != '0' || b.status != '0')";

        $result_set61 = mysqli_query($connection, $query);

        while ($row = mysqli_fetch_array($result_set61)) {
            $cnt1 = $row["cnt"];
            $cnt2 = $row["cnt2"];
            $cnt = $cnt1 + $cnt2;
            $html .= '<div class="alert alert-danger" role="alert">
                                        <a href="#" class="alert-link">You currently aren&apos;t subscribed to any exams.</a>
                                  </div>';
        }

        $json = array("result" => "1","html" => $html,"cnt" => $cnt);

        echo json_encode($json);
        break;
    case "FindQuestion":

        $html = '';

        require_once("../includes/connection.php");

        require_once("../includes/functions.php");

        require_once("../includes/form_functions.php");

        require_once("../includes/config.php");

        $sql = "select * from aca_question where exam_id = '".$_POST["examid"]."' and (question_no like '%".$_POST["term"]."%' or question_text like '%".$_POST["term"]."%') ";

        $result_set2 = mysqli_query($connection, $sql);

        $html .= '<ul class="list-group" id="user-list">';

        foreach($result_set2 as $row) {

            $html .= "<li class=\"list-group-item\" onclick=\"selectQuestion('".$row["question_no"]."')\">".$row["question_no"]." - ".$row["question_text"]."</li>";
        }
        $html .= '</ul>';

        $json = array("result" => "1", "html" => $html);
        echo json_encode($json);
        break;
    default:
        echo json_encode(array('message' => 'Unknown action'));
        break;
}

die();