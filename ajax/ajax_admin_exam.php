<?php

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

@session_start();

ini_set("display_errors", 1);
$action = $_REQUEST['action'];

switch($action) {
    case "AddExam":

        require_once("../includes/config.php");
        require_once("../includes/connection.php");
        require_once("../includes/functions.php");
        require_once("../includes/form_functions.php");

        $html = '';
		$exam_id = $_POST['examid'];
             
            $query = "delete from aca_exam where exam_id = '".$exam_id."'";

            $result = mysqli_query( $connection, $query);

            if (mysqli_affected_rows($connection) == 1) {
                // Success
                $html .= '<div class="alert alert-success">Record Created</div>';
                $set = 0; // set action back to listing

                $json = array("result" => "1","set" => $set);
            } else {
                $html .= '<div class="alert alert-danger">Record creation failed</div>';

                $json = array("result" => "0");
            }
        echo json_encode($json);
        break;
	case "DeleteExam":

        require_once("../includes/config.php");
        require_once("../includes/connection.php");
        require_once("../includes/functions.php");
        require_once("../includes/form_functions.php");

        $html = '';
		$exam_id = $_POST['examid'];
             
            $query = "delete from aca_exam where exam_id = '".$exam_id."'";

            $result = mysqli_query( $connection, $query);

            if (mysqli_affected_rows($connection) == 1) {
                // Success
                $html .= '<div class="alert alert-success">Record Created</div>';
                $set = 0; // set action back to listing

                $json = array("result" => "1","set" => $set);
            } else {
                $html .= '<div class="alert alert-danger">Record creation failed</div>';

                $json = array("result" => "0");
            }
        echo json_encode($json);
        break;
    default:
        echo json_encode(array('message' => 'Unknown action'));
        break;
}

die();