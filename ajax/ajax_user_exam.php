<?php

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

@session_start();

ini_set("display_errors", 1);
$action = $_REQUEST['action'];

switch($action) {
    case "Insert":

        require_once("../includes/config.php");
        require_once("../includes/connection.php");
        require_once("../includes/functions.php");
        require_once("../includes/form_functions.php");

        $html = '';

                $user_id = trim(mysql_prep($_POST["user_id"]));
                $exam_id = trim(mysql_prep($_POST['exam_id']));
                $create_date = $today;
                $expiry_date = (strtotime($today)) + (60*60*24);
                $creator_id = $_POST["userx"];
                $status = '1';

            if ($test_aca == 1) {
                $msg = "set 2 Post opt=".$opt."</br>";
                $msg .= "create_date: ".$create_date."</br>";
                $msg .= "creator_id: ".$userx."</br>";
                $html .= $msg;
            }

            $query = "INSERT INTO aca_user_exam( user_id, exam_id, expiry_date, create_date, status, creator_id ) VALUES ( '".$user_id."', '".$exam_id."', '".$expiry_date."', '".$create_date."', '".$status."', '".$userx."' )" ;

            $result = mysqli_query( $connection, $query);

            if (mysqli_affected_rows($connection) == 1) {
                // Success
                $html .= '<div class="alert alert-success">Record Created</div>';
                $set = 0; // set action back to listing

                $json = array("result" => "1","set" => $set);
            } else {
                $html .= '<div class="alert alert-danger">Record creation failed</div>';

                $json = array("result" => "0");
            }

            $set = 0;
        echo json_encode($json);
        break;
    default:
        echo json_encode(array('message' => 'Unknown action'));
        break;
}

die();