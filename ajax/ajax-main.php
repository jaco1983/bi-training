<?php
/**
 * Created by PhpStorm.
 * User: JacoF
 * Date: 11/9/2015
 * Time: 11:49 PM
 */

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

@session_start();

ini_set("display_errors", 1);
$action = $_REQUEST['action'];

switch($action) {
    case "SaveConfig":
        require_once("../includes/connection.php");

        require_once("../includes/functions.php");

        require_once("../includes/form_functions.php");

        $html = '';

        $sql = "update aca_site_config set site_title = '".$_POST["site_title"]."', site_keywords = '".$_POST["site_keywords"]."',site_description = '".$_POST["site_description"]."',site_name = '".$_POST["site_name"]."',admin_email = '".$_POST["admin_email"]."',support_email = '".$_POST["support_email"]."'";
        $result_set = mysqli_query($connection,$sql);
        confirm_query($result_set);

        if(mysqli_affected_rows($connection) > 0){
            $html .= '<div class="alert alert-success" role="alert">
                                        <a href="#" class="alert-link">Configuration successfully saved.</a>
                                  </div>';
            $json = array("result" => 1,"html" => $html);
        } else {
            $html .= '<div class="alert alert-danger" role="alert">
                                        <a href="#" class="alert-link">There was an error saving the configuration. Please try again.</a>
                                  </div>';
            $json = array("result" => 0,"html" => $html);
        }

        echo json_encode($json);
        break;
    case "SaveInvoiceConfig":
        require_once("../includes/connection.php");

        require_once("../includes/functions.php");

        require_once("../includes/form_functions.php");

        $html = '';

        $sql = "update aca_invoice_config set company_name = '".$_POST["companyname"]."', vat_number = '".$_POST["vatnumber"]."',postal_address1 = '".$_POST["postaladdress1"]."',postal_address2 = '".$_POST["postaladdress2"]."',postal_address3 = '".$_POST["postaladdress3"]."',postal_address4 = '".$_POST["postaladdress4"]."'";
        $result_set = mysqli_query($connection,$sql);
        confirm_query($result_set);

        if(mysqli_affected_rows($connection) > 0){
            $html .= '<div class="alert alert-success" role="alert">
                                        <a href="#" class="alert-link">Configuration successfully saved.</a>
                                  </div>';
            $json = array("result" => 1,"html" => $html);
        } else {
            $html .= '<div class="alert alert-danger" role="alert">
                                        <a href="#" class="alert-link">There was an error saving the configuration. Please try again.</a>
                                  </div>';
            $json = array("result" => 0,"html" => $html);
        }

        echo json_encode($json);
        break;
    case "Login":

        require_once("../includes/connection.php");

        require_once("../includes/functions.php");

        require_once("../includes/form_functions.php");

        require_once("../includes/config.php");
        require_once("../class/encryption.php");

        //login existing user
            $errors = array();
            $html = '';

            $username = trim(mysql_prep($_POST['aemail2']));

            $origpassword = trim(mysql_prep($_POST['apwd2']));

            $encryption = new Encryption();

            $password = $encryption->encrypt_request($origpassword);

            $gip = get_ip(0);

            if ($test_aca == 1) {
                $query = "SELECT user_id, username, email, `name`, password FROM aca_user WHERE status = '1' LIMIT 1 ";
                $result_set = mysqli_query($connection,$query);
                confirm_query($result_set);
                $found_user = mysqli_fetch_array($result_set);
            }

            if ( empty($errors)) {
                if ($test_aca == 1) {
                    /*echo "Test:: Do if no errors " . $password . "</br>";*/
                }

                // Check database to see if username and the hashed password exists there.
                $query = "SELECT user_id, username, email, name, mobile, access_level, instructor_id, password FROM aca_user WHERE trim(email) = '{$username}' AND trim(password) = trim('{$password}') AND status = '1' LIMIT 1";
                $result_setZ = mysqli_query($connection, $query);
                confirm_query($result_setZ);

                if ($_POST["url"] == '') {
                    if (mysqli_num_rows($result_setZ) == 1) {
                        $found_user = mysqli_fetch_array($result_setZ);
                        $_SESSION['aca_userx'] = $found_user['user_id'];
                        $_SESSION['aca_username'] = $found_user['username'];
                        $_SESSION['aca_name'] = $found_user['name'];
                        $_SESSION['aca_accesslevel'] = $found_user['access_level'];

                        $split = explode(" ",$found_user['name']);
                        $_SESSION["user_detail"]["name"] = $split[0];
                        if(isset($split[1])){
                            $_SESSION["user_detail"]["surname"] = $split[1];
                        } else {
                            $_SESSION["user_detail"]["surname"] = '';
                        }
                        $_SESSION["user_detail"]["email"] = $found_user['email'];
                        $_SESSION["user_detail"]["mobile"] = $found_user['mobile'];


                        if ($found_user['access_level'] == 9 || $found_user['access_level'] == 999) {  // instructor = 9 and administrator = 999
                            $_SESSION['aca_instructor'] = $found_user['instructor_id'];
                        } else {
                            $_SESSION['aca_instructor'] = 0;
                        }

                        $userx = $_SESSION['aca_userx'];
                        $username = $_SESSION['aca_username'];
                        $aca_accesslevel = $_SESSION['aca_accesslevel'];
                        $aca_name = $_SESSION['aca_name'];
                        $aca_instructor = $_SESSION['aca_instructor'];

                        if ($test_aca == 1) {
                            /*echo "Test:: Login:" . $login . "  after login check pass, assigned sessions</br>";
                            echo "Test:: Username:" . $username . "</br>";
                            echo "Test:: AccessLevel:" . $aca_accesslevel . "</br>";*/
                        }

                        $login = 1;
                        $json = array("result" => $login,"html" => $html);
                    } else {
                        // username / password combo was not found in the database
                        $html .= '<div class="alert alert-danger" role="alert">
                                        <a href="#" class="alert-link">There was an error with the credentials provided. Please try again.</a>
                                  </div>';

                        $login = 8;
                        $json = array("result" => $login,"html" => $html);
                        if ($test_aca == 9) {
                            /*echo 'Test:: Login:' . $login . '  after login check fail</br>
                              Test:: Username:' . $username . '</br>';*/
                        }
                    }
                } else {
                    $html .= '<div class="alert alert-danger" role="alert">
                                    <a href="#" class="alert-link">There was an error with the credentials provided. Please try again.</a>
                              </div>';

                    $login = 8;
                    $json = array("result" => $login,"html" => $html);
                    if ($test_aca == 9) {
                        /*echo 'Test:: Login:' . $login . '  after login check fail</br>
                              Test:: Username:' . $username . '</br>';*/
                    }
                }
            }


        echo json_encode($json);
        break;
    case "Logout":

        $message = "You are now logged out!";

        $usernamelog = $_SESSION['aca_username'];

        $username = "";

        $password = "";

        $userx = 0;

        $login = 0;

        $q_count = 0;

        $q_tot = 0;

        $aca_qno = 0;

        $_SESSION['aca_username'] = NULL;

        $_SESSION['aca_name'] = NULL;

        $_SESSION['aca_userx'] = NULL;

        $_SESSION['aca_userthis'] = NULL;

        $_SESSION['aca_userexam'] = NULL;

        $_SESSION['aca_q_count'] = NULL;

        $_SESSION['aca_qno'] = NULL;

        $_SESSION['aca_q_tot'] = NULL;

        $_SESSION['aca_subject'] = NULL;

        $_SESSION['aca_instructor'] = NULL;

        unset($_SESSION['aca_username']);

        unset($_SESSION['aca_name']);

        unset($_SESSION['aca_userx']);

        unset($_SESSION['aca_userthis']);

        unset($_SESSION['aca_userexam']);

        unset($_SESSION['aca_q_count']);

        unset($_SESSION['aca_qno']);

        unset($_SESSION['aca_subject']);

        unset($_SESSION['aca_instructor']);



        session_destroy();


        echo json_encode(array("result" => 1));
        break;
    case "Captcha":

        /*if($_POST["captcha"] == $_SESSION["captcha"]){
            $result = 1;
        } else {
            $result = 0;
        }*/
$result = 1;
        if($result == "1"){
            $json = array("result" => 1);
        } else {
            $json = array("result" => 0);
        }

        echo json_encode($json);
        break;
    case "refreshCaptcha":
        $string = '';
        $image ='';
        for ($i = 0; $i < 5; $i++) {
            $string .= chr(rand(97, 122));
        }

        $_SESSION['captcha'] = $string; //store the captcha

        echo json_encode(array("result" => 1,"captcha" => $string));
        break;
    case "Contact":

                $html = '';
                require_once("../includes/config.php");
                require_once("../includes/connection.php");

                require_once("../includes/functions.php");

                require_once("../includes/form_functions.php");

                $html = '';

                $sql = "insert into aca_contact_log (fname,email,subject,reference,message,client_ip) values ('".$_POST["cfname"]."','".$_POST["cemail"]."','".$_POST["csubject"]."','".$_POST["creference"]."','".$_POST["cmessage"]."','".get_ip(0)."')";
                $result_set = mysqli_query($connection,$sql);
                confirm_query($result_set);


                include '../lib/swift_required.php';

                /*$msgcontent = '';
                $output = '';

                $message = Swift_Message::newInstance();
                $message->setSubject("BlackboardBI Training Contact Form");
                $message->setFrom(array($admin_email => $admin_email));
                $message->setTo(array($_POST["cemail"] => $_POST["cfname"]));
                $message->setBcc(array("jaco@itechnologiessolutions.co.za" => "Jaco Ferreira"));
                $message->setBody('This email requires HTML to view.');

                $msgcontent.= '<p>Dear '.$_POST["r_name"].', </p><br />
                                        Thank you for registering on '.$site_name.'.<br />
                                        &nbsp;<br />
                                        You access the '.$site_name.' educational platform right now by using the details below.<br />
                                        &nbsp;<br />
                                        Here you&apos;ll be able to update your details, join a class, subscribe to an exam, complete an instructor quiz, and take a test.<br />
                                        &nbsp;<br />
                                        <strong>Here are your login details:</strong><br />
                                        &nbsp;<br />
                                        <strong>Username: '.$_POST["r_email"].'</strong><br />
                                        <strong>Password: '.$password.'</strong>
                                        &nbsp;<br />
                                        &nbsp;<br />
                                        Regards<br />
                                        The Webmaster<br />';

                $message->addPart($msgcontent, 'text/html');

                $transport = Swift_MailTransport::newInstance();
                $mailer = Swift_Mailer::newInstance($transport);
                $result = $mailer->send($message);*/


                // send a mail to admin

                $msgcontent = '';
                $output = '';

                $message = Swift_Message::newInstance();
                $message->setSubject("BlackboardBI Training Contact Form");
                $message->setFrom(array($admin_email => "BlackboardBI Training"));
                $message->setTo(array($support_email => "BlackboardBI Training"));
                $message->setBody('This email requires HTML to view.');

                $msgcontent.= '<p>Attention, </p><br />
                                        A contact us form was submitted from '.$site_name.'<br />
                                        &nbsp;<br />
                                        Name: '.$_POST["cfname"].'<br />
                                        E-Mail: '.$_POST["cemail"].'<br />
                                        Subject: '.$_POST["csubject"].'<br />
                                        Reference: '.$_POST["creference"].'<br />
                                        Message: '.$_POST["cmessage"].'<br />';

                $message->addPart($msgcontent, 'text/html');

                $transport = Swift_MailTransport::newInstance();
                $mailer = Swift_Mailer::newInstance($transport);
                $result = $mailer->send($message);

                // Success
        if($result){
                $html .= "<div class=\"alert alert-success\">";
                $html .= "Contact form was successfully submitted.";
                $html .= "</div>";

                $json = array("html" => $html,"result" => 1);

        } else {
            $html .= "<div class=\"alert alert-danger\">";
            $html .= "There was an error submitting the contact form.";
            $html .= "</div>";

            $json = array("html" => $html,"result" => 0);
        }
        echo json_encode($json);
        break;
    default:
        echo json_encode(array('message' => 'Unknown action'));
        break;
}

die();