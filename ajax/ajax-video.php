<?php
/**
 * Created by PhpStorm.
 * User: JacoF
 * Date: 11/9/2015
 * Time: 11:49 PM
 */

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

@session_start();

ini_set("display_errors", 1);
$action = $_REQUEST['action'];

switch($action) {
    case "SaveVideo":

        $html = '';

        require_once("../includes/connection.php");
        require_once("../includes/functions.php");
        require_once("../includes/form_functions.php");
        require_once("../includes/config.php");
        require_once("../class/encryption.php");

        $html = '';

        parse_str($_POST['data'], $searcharray);

        if($_POST["vstatus"] == "on"){
            $status = 1;
        } else {
            $status = 0;
        }


        if(isset($searcharray["filenames"])) {
            $mp4 = '';
            $ogv = '';
            $webm = '';
            foreach ($searcharray["filenames"] as $key => $value) {
                if (strpos($value, '.mp4') !== false) {
                    $mp4 = $value;
                }
                if (strpos($value, '.ogv') !== false) {
                    $ogv = $value;
                }
                if (strpos($value, '.webm') !== false) {
                    $webm = $value;
                }
            }
        }

        $query = "insert into aca_video (video_title,video_description,mp4_file,ogv_file,webm_file,date_created,date_modified,status) values ('".$_POST["vtitle"]."','".$_POST["vdesc"]."','".$mp4."','".$ogv."','".$webm."','".date("Y-m-d")."','".date("Y-m-d")."','".$status."')";

        $result = mysqli_query( $connection, $query);


        if (mysqli_affected_rows($connection) == 1) {
            // Success
            $html .= "<div class=\"alert alert-success\">";
            $html .= "Record Created";
            $html .= "</div>";
        } else {
            $html .= "<div class=\"alert alert-danger\">";
            $html .= "Record creation failed";
            $html .= "</div>";
        }

        $json = array("result" => "1","html" => $html);

        echo json_encode($json);
        break;
    case "UpdateVideo":

        $html = '';

        require_once("../includes/connection.php");
        require_once("../includes/functions.php");
        require_once("../includes/form_functions.php");
        require_once("../includes/config.php");
        require_once("../class/encryption.php");

        $html = '';

        parse_str($_POST['data'], $searcharray);

        if($_POST["vstatus"] == "on"){
            $status = 1;
        } else {
            $status = 0;
        }

        $mp4 = '';
        $ogv = '';
        $webm = '';
        if(isset($searcharray["filenames"])) {

            foreach ($searcharray["filenames"] as $key => $value) {
                if (strpos($value, '.mp4') !== false) {
                    $mp4 = $value;
                }
                if (strpos($value, '.ogv') !== false) {
                    $ogv = $value;
                }
                if (strpos($value, '.webm') !== false) {
                    $webm = $value;
                }
            }
        }

        $query = "update aca_video set video_title = '".$_POST["vtitle"]."',video_description = '".$_POST["vdesc"]."',date_modified = '".date("Y-m-d")."',status = '".$status."'";
        if($mp4 != '') $query .= ",mp4_file = '".$mp4."'";
        if($ogv != '') $query .= ",ogv_file = '".$ogv."'";
        if($webm != '') $query .= ",webm_file = '".$webm."'";
        $query .= " where id = '".$_POST["vid"]."'";

        $result = mysqli_query( $connection, $query);


        if (mysqli_affected_rows($connection) == 1) {
            // Success
            $html .= "<div class=\"alert alert-success\">";
            $html .= "Record Created";
            $html .= "</div>";
        } else {
            $html .= "<div class=\"alert alert-danger\">";
            $html .= "Record creation failed";
            $html .= "</div>";
        }

        $json = array("result" => "1","html" => $html);

        echo json_encode($json);
        break;
    default:
        echo json_encode(array('message' => 'Unknown action'));
        break;
}

die();