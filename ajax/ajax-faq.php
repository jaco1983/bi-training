<?php
/**
 * Created by PhpStorm.
 * User: JacoF
 * Date: 11/9/2015
 * Time: 11:49 PM
 */

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

@session_start();

ini_set("display_errors", 1);
$action = $_REQUEST['action'];

switch($action) {
    case "SaveFAQ":

        $html = '';

        require_once("../includes/connection.php");
        require_once("../includes/functions.php");
        require_once("../includes/form_functions.php");
        require_once("../includes/config.php");
        require_once("../class/encryption.php");

        $html = '';

        $query = "insert into aca_faq (faqq,faqa,faqo,date_created,date_modified,status) values ('".$_POST["faqq"]."','".$_POST["faqa"]."','".$_POST["faqo"]."','".date("Y-m-d")."','".date("Y-m-d")."','1')";

        $result = mysqli_query( $connection, $query);
        if (mysqli_affected_rows($connection) == 1) {
            // Success
            $html .= "<div class=\"alert alert-success\">";
            $html .= "Record Created";
            $html .= "</div>";
        } else {
            $html .= "<div class=\"alert alert-danger\">";
            $html .= "Record creation failed";
            $html .= "</div>";
        }

        $json = array("result" => "1","html" => $html);

        echo json_encode($json);
        break;
    case "UpdateFAQ":

        $html = '';

        require_once("../includes/connection.php");
        require_once("../includes/functions.php");
        require_once("../includes/form_functions.php");
        require_once("../includes/config.php");
        require_once("../class/encryption.php");

        $html = '';

        $query = "update aca_faq set faqq = '".$_POST["faqq"]."',faqa = '".$_POST["faqa"]."',faqo='".$_POST["faqo"]."',date_modified = '".date("Y-m-d H:i:s")."' where id = '".$_POST["faqid"]."'";

        $result = mysqli_query( $connection, $query);
        if (mysqli_affected_rows($connection) == 1) {
            // Success
            $html .= "<div class=\"alert alert-success\">";
            $html .= "Record Created";
            $html .= "</div>";
        } else {
            $html .= "<div class=\"alert alert-danger\">";
            $html .= "Record creation failed";
            $html .= "</div>";
        }

        $json = array("result" => "1","html" => $html);

        echo json_encode($json);
        break;
    default:
        echo json_encode(array('message' => 'Unknown action'));
        break;
}

die();