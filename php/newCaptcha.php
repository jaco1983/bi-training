<?php


    session_start();

if(isset($_GET["rnd"])) {

    $dir = '../fonts/';
    $images = imagecreatetruecolor(165, 30); //custom image size
    $font = "OpenSans-Regular.ttf"; // custom font style
    $color = imagecolorallocate($images, 113, 193, 217); // custom color
    $white = imagecolorallocate($images, 255, 255, 255); // custom background color
    imagefilledrectangle($images,0,0,399,99,$white);
    imagettftext ($images, 20, 0, 10, 25, $color, $dir.$font, $_GET["rnd"]);

    header("Content-type: image/png");
    imagepng($images);
    imagedestroy($images);
} else {
    $dir = '../fonts/';
    $image = imagecreatetruecolor(165, 30); //custom image size
    $font = "OpenSans-Regular.ttf"; // custom font style
    $color = imagecolorallocate($image, 113, 193, 217); // custom color
    $white = imagecolorallocate($image, 255, 255, 255); // custom background color
    imagefilledrectangle($image, 0, 0, 399, 99, $white);
    imagettftext($image, 20, 0, 10, 25, $color, $dir . $font, $_SESSION['captcha']);

    header("Content-type: image/png");
    imagepng($image);
    imagedestroy($image);
}
?>