<?php

ini_set("display_errors", 0);
    // Beyond Analysis

	require_once("includes/session.php");
	
	if(isset($_GET["sessid"]) && $_GET["sessid"] == 1){
		session_unset();
		session_destroy();
	}
	
	$actual_link = rtrim(get_base_url(),"/").''.$_SERVER["REQUEST_URI"];
	
	$parts = parse_url($actual_link);
	parse_str($parts['query'], $query);
	
	
    ob_start();
	if (logged_in()) {
		$username 			= $_SESSION['aca_username']; //
		$userx 				= $_SESSION['aca_userx'];  // user_id
		$aca_instructor 	= $_SESSION['aca_instructor'];  // instructor number if applicable
		$aca_accesslevel 	= $_SESSION['aca_accesslevel'];  // user_id
		$aca_name 			= $_SESSION['aca_name'];  // name
		$exam_description 	= "";
		$userdesc = $aca_name . " (" . $username . ")";
	} else {
		$username = "";
		$userx = 0;
		$aca_accesslevel = 0;
		$aca_name = "";
		$userthis = "";
		$userdesc = "";
		$exam_description 	= "";
		$cat6 = 0;
		$ins = 0;
		$set = 0;
	} // end of get session

	// work files
	require_once("includes/connection.php");
	require_once("includes/functions.php");
	require_once("includes/form_functions.php");
	require_once("includes/config.php");
	require_once("class/encryption.php");


    //Get location and currency
    //$ip = get_ip(0);
    //$details = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip={$ip}"));
	/*if($details->geoplugin_currencyConverter == 0){
		if($details->"geoplugin_countryCode" == "ZA"){
			$_SESSION["currency"] = $details->geoplugin_currencyCode;
			$_SESSION["currencySymbol"] = $details->geoplugin_currencySymbol_UTF8;
			$_SESSION["currencyconverter"] = $details->geoplugin_currencyConverter;
		} else {
			$_SESSION["currency"] = 'USD';
			$_SESSION["currencySymbol"] = '$';
			$_SESSION["currencyconverter"] = 1;
		}
	} else {
		$_SESSION["currency"] = 'USD';
		$_SESSION["currencySymbol"] = '$';
		$_SESSION["currencyconverter"] = 1;
	}*/

    //SET CURRENCY
    $_SESSION["currency"] = 'USD';
	$_SESSION["currencySymbol"] = '$';
	$_SESSION["currencyconverter"] = 1;

	if (logged_in() && test_on()) {
		$userthis = $_SESSION['aca_userthis'];  // user test version
		$userexam = $_SESSION['aca_userexam'];  // user_id

		// get exam heading							

			$result_set1 = get_aca_exam_rec($userexam);

			while ($row = mysqli_fetch_array($result_set1)) {		
				$key = $row["exam_id"];
				$code = $row["code"];
				$exam_description = $row["description"];
				$versions = $row["versions"];
			}
	}
	
	//trial expire date
	if(logged_in()){
		$trial_set = get_test_exam($_SESSION["aca_userx"]);
		while($row = mysqli_fetch_array($trial_set)){
			$r = strtotime($row["reg_date"])+strtotime('+2 weeks');
			$trial_expire = date("Y-m-d",mktime(0, 0, 0, date("m",strtotime($row["reg_date"])), date("d",strtotime($row["reg_date"]))+14, date("Y",strtotime($row["reg_date"]))));
		}
	}

	

 	// variables																	

	$opt = "0";

	if (isset($_GET["opt"])) { 

		$opt=$_GET["opt"];	

	}	else $opt="0";	

	

	$id = 0; 	

	if (isset($_GET["id"])) { 

		$id=$_GET["id"];

	}	else $id=0;		

	

	$idp = 0; 	

	if (isset($_GET["idp"])) { 

		$idp=$_GET["idp"];

	}	else $idp=0;		

	

	$page = 0; 	

	if (isset($_GET["page"])) { 

		$page=$_GET["page"];

	}	else $page=0;		

	

	

	if (isset($_GET['q']))  {

	  $q = $_GET['q'];

	  } else { 

	  $q = 0; 

	} 

	if (isset($_GET['qq']))  {

	  $qq = $_GET['qq'];

	  } else { 

	  $qq = 0; 

	} 

	if (isset($_GET['qno']))  {

	  $qno = $_GET['qno'];

	  } else { 

	  $qno = 0; 

	} 

	

	$login = "0"; 	

	if (isset($_GET["login"])) { 

		$login=$_GET["login"];	

	}	else $login="0";

	

	$logout = "0"; 	

	if (isset($_GET["logout"])) { 

		$logout=$_GET["logout"];	

	}	else $logout="0";

	

	$teston = "0"; 	

	if (isset($_GET["teston"])) { 

		$teston=$_GET["teston"];	

	}	else $teston="0";

	

	

	$testout = "0"; 	

	if (isset($_GET["testout"])) { 

		$testout=$_GET["testout"];	

	}	else $testout="0";

	

	

	$set = ""; 	

	if (isset($_GET["set"])) { 

		$set=$_GET["set"];	

	}	else $set="0";

	

	$key = "0"; 	

	if (isset($_GET["key"])) { 

		$key=$_GET["key"];	

	}	else $key="0";	

	

	$cat = "0"; 	

	if (isset($_GET["cat"])) { 

		$cat=$_GET["cat"];	

	}	else $cat="0";	



	$cat2 = "0"; 	

	if (isset($_GET["cat2"])) { 

		$cat2=$_GET["cat2"];	

	}	else $cat2="0";	



	$cat3 = "0"; 	

	if (isset($_GET["cat3"])) { 

		$cat3=$_GET["cat3"];	

	}	else $cat3="0";	



	$cat4 = "0"; 	

	if (isset($_GET["cat4"])) { 

		$cat4=$_GET["cat4"];	

	}	else $cat4="0";	



	$cat5 = "0"; 	

	if (isset($_GET["cat5"])) { 

		$cat5=$_GET["cat5"];	

	}	else $cat5="0";	

	

	$cat6 = "0"; 	// changed to 0 from "" by SW on 2017/01/03

	if (isset($_GET["cat6"])) { 

		$cat6=$_GET["cat6"];	

	}	else $cat6="0";	// changed to 0 from "" by SW on 2017/01/03

	

	$cat7 = 0; 	

	if (isset($_GET["cat7"])) { 

		$cat7=$_GET["cat7"];	

	}	else $cat7=0;

 

	$cat8 = 0; 	

	if (isset($_GET["cat8"])) { 

		$cat8=$_GET["cat8"];	

	}	else $cat8=0;

 

	$cat9 = 0; 	

	if (isset($_GET["cat9"])) { 

		$cat9=$_GET["cat9"];	

	}	else $cat9=0;

 

	

	$srch = ""; 	

	if (isset($_GET["srch"])) { 

		$srch=$_GET["srch"];	

	}	else $srch="";

 

	

	

	$gip = get_ip(0);

	$todaynow = date("Y-m-d");

	$snow = date("h:m:s");

	$gipp = $todaynow;

	

	// temp variables

	//$exam = "C_TBI30_74";

	//$exam_id = 1;

	//$key = 1; // this is the exam_id	

	$test_aca = 0; // 1 displays test messages

	function get_base_url() {

    $server ="";

    if(isset($_SERVER['SERVER_NAME'])){
		if (strpos($_SERVER['REQUEST_URI'], "training") !== false){
			$server = sprintf("%s://%s%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_NAME'], '/training/');
		} else {
			$server = sprintf("%s://%s%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_NAME'], '/');
		}
    }
    else{
		if (strpos($_SERVER['REQUEST_URI'], "training") !== false){
			$server = sprintf("%s://%s%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_ADDR'], '/training/');
		} else {
			$server = sprintf("%s://%s%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_ADDR'], '/');
		}
    }
    return $server;

}
?>

        

<!DOCTYPE html>																		

<html lang="en">

<head>

  <title><?php echo $site_title; ?></title>

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="<?php echo get_base_url(); ?>js/bootstrap-3.1.1.min.js"></script>

  <script src="<?php echo get_base_url(); ?>js/jquery-ui.js"></script>
<script src="<?php echo get_base_url(); ?>js/jquery.multiselect.js"></script>
  <script src="<?php echo get_base_url(); ?>scripts/public.js" type="text/javascript"></script>
  <script src="<?php echo get_base_url(); ?>js/modernizr-2.6.2.min.js"></script>
  <script type="text/javascript" src="<?php echo get_base_url(); ?>vendor/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
  <script type="text/javascript" src="<?php echo get_base_url(); ?>vendor/rs-plugin/js/jquery.themepunch.revolution.js"></script>
  <script src="<?php echo get_base_url(); ?>js/jquery.shuffle.min.js"></script>
  <script src="<?php echo get_base_url(); ?>vendor/owl/js/owl.carousel.min.js"></script>
  <script src="<?php echo get_base_url(); ?>js/wow.min.js"></script>
  <script src="<?php echo get_base_url(); ?>js/jquery.counterup.min.js"></script>
  <script src="<?php echo get_base_url(); ?>js/jquery.easing.min.js"></script>
  <script src="<?php echo get_base_url(); ?>js/scrolling-nav.js"></script>
  <script src="<?php echo get_base_url(); ?>js/smoothscroll.min.js"></script>
  <script src="<?php echo get_base_url(); ?>js/script.js"></script>
  <script src="<?php echo get_base_url(); ?>vendor/tinymce/js/tinymce/tinymce.min.js"></script>
  <script src="<?php echo get_base_url(); ?>js/fileinput.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo get_base_url(); ?>js/bootstrap-filestyle.min.js"> </script>
  <script type="text/javascript" src="<?php echo get_base_url(); ?>js/moment.min.js"> </script>
  <script type="text/javascript" src="<?php echo get_base_url(); ?>js/bootstrap-datetimepicker.min.js"> </script>



  <link rel="stylesheet" href="<?php echo get_base_url(); ?>css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo get_base_url(); ?>css/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo get_base_url(); ?>css/jquery.multiselect.css">
  <link href="<?php echo get_base_url(); ?>stylesheet/public.css" media="all" rel="stylesheet" type="text/css" />
  <link rel="icon" href="<?php echo get_base_url(); ?>favicon.ico" type="image/x-icon"/>
  <link rel="stylesheet" type="text/css" href="<?php echo get_base_url(); ?>vendor/rs-plugin/css/settings.css" media="screen"/>
  <link rel="stylesheet" href="<?php echo get_base_url(); ?>vendor/rs-plugin/css/extralayer.css">
  <link rel="stylesheet" href="<?php echo get_base_url(); ?>vendor/owl/css/owl.carousel.css">
  <link rel="stylesheet" href="<?php echo get_base_url(); ?>vendor/owl/css/owl.theme.default.css">
  <link rel="stylesheet" href="<?php echo get_base_url(); ?>vendor/owl/css/owl.theme.css">
  <link rel="stylesheet" href="<?php echo get_base_url(); ?>css/animate.css">
  <link rel="stylesheet" href="<?php echo get_base_url(); ?>css/style.css">
  <link href="<?php echo get_base_url(); ?>css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
  <link href="<?php echo get_base_url(); ?>css/funkycheckbox.css" media="all" rel="stylesheet" type="text/css" />
  <link href="<?php echo get_base_url(); ?>css/bootstrap-datetimepicker.css" media="all" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo get_base_url(); ?>css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo get_base_url(); ?>css/intlTelInput.css">

    <!--Show tool list for a selected exam -->

    <script>

	function showTool(str) {

	  if (str=="") {

		document.getElementById("txtHint").innerHTML="";

		return;

	  } 

	  if (window.XMLHttpRequest) {

		// code for IE7+, Firefox, Chrome, Opera, Safari

		xmlhttp=new XMLHttpRequest();

	  } else { // code for IE6, IE5

		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");

	  }

	  xmlhttp.onreadystatechange=function() {

		if (xmlhttp.readyState==4 && xmlhttp.status==200) {

		  document.getElementById("txtHint").innerHTML=xmlhttp.responseText;

		}

	  }

	  xmlhttp.open("GET","includes/get_tool.php?q="+str,true);

	  xmlhttp.send();

	}

	</script> 

      

    <!--Show class list for a selected user -->

    <script>

	function showClass(str) {

	  if (str=="") {

		document.getElementById("txtHint").innerHTML="";

		return;

	  } 

	  if (window.XMLHttpRequest) {

		// code for IE7+, Firefox, Chrome, Opera, Safari

		xmlhttp=new XMLHttpRequest();

	  } else { // code for IE6, IE5

		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");

	  }

	  xmlhttp.onreadystatechange=function() {

		if (xmlhttp.readyState==4 && xmlhttp.status==200) {

		  document.getElementById("txtHint").innerHTML=xmlhttp.responseText;

		}

	  }

	  xmlhttp.open("GET","includes/get_class.php?q="+str,true);

	  xmlhttp.send();

	}

	</script>

      

    <!--Show user list from a selected class -->

    <script>

	function showUserClass(str) {

	  if (str=="") {

		document.getElementById("txtHint").innerHTML="";

		return;

	  } 

	  if (window.XMLHttpRequest) {

		// code for IE7+, Firefox, Chrome, Opera, Safari

		xmlhttp=new XMLHttpRequest();

	  } else { // code for IE6, IE5

		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");

	  }

	  xmlhttp.onreadystatechange=function() {

		if (xmlhttp.readyState==4 && xmlhttp.status==200) {

		  document.getElementById("txtHint").innerHTML=xmlhttp.responseText;

		}

	  }

	  xmlhttp.open("GET","includes/get_user_class.php?q="+str,true);

	  xmlhttp.send();

	}

	</script>

      

    <!--Show user list from a selected class -->

    <script>

	function showUserClassAll(str) {

	  if (str=="") {

		document.getElementById("txtHint").innerHTML="";

		return;

	  } 

	  if (window.XMLHttpRequest) {

		// code for IE7+, Firefox, Chrome, Opera, Safari

		xmlhttp=new XMLHttpRequest();

	  } else { // code for IE6, IE5

		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");

	  }

	  xmlhttp.onreadystatechange=function() {

		if (xmlhttp.readyState==4 && xmlhttp.status==200) {

		  document.getElementById("txtHint").innerHTML=xmlhttp.responseText;

		}

	  }

	  xmlhttp.open("GET","includes/get_user_class_all.php?q="+str,true);

	  xmlhttp.send();

	}

	</script>

</head>

<body>
<input type="hidden" id="base_url" value="<?php echo get_base_url(); ?>" />
<div id="body-container">
<div class="container mainnav">
<nav class="navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="?id=0"><img src="<?php echo get_base_url(); ?>/images/blackboardbi_logo_200.png" class="img-responsive" /></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav main-navigation" style="float:right;">
            <li><a href="<?php echo get_base_url(); ?>index.php?id=0">Home</a></li>
            <li><a href="?id=5">About Us</a></li>
            <li><a href="?id=1">Exams</a></li>
            <li><a href="?id=6">FAQ</a></li>
            <!--<li><a href="?id=7">Demo</a></li>-->
            <li><a href="?id=3">Contact Us</a></li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
</div>
<div class="container-fluid" style="background: #58595b;">
<div class="container subnav">
        <aside id="sidebar" class="col-sm-12 hidden-xs">

            <ul class="nav navbar-nav submain">
                <?php
                if (logged_in()) {
                    echo '<li><a href="?id=8&login=1&testout=1">Dashboard</a></li>';
                }

                if (logged_in() && $aca_accesslevel == 1){
                    echo '<li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=10&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Take Test</a></li>
                    <li><a href="javascript:voi(0)" onclick="MM_goToURL(\'parent\',\'?id=11&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Practice a Question</a></li>';
                    if(isset($_SESSION['aca_studenttype']) && $_SESSION['aca_studenttype'] == 1) {
                        echo '<li><a href="javascrip:void(0)" onclick="MM_goToURL(\'parent\',\'?id=12&login=1&opt=1&key=0&set=0\');return document.MM_returnValue">Instructor Quiz</a></li>';
                    }
                    echo '<li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=21&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Subscribe to Exam</a></li>';
                    if(isset($_SESSION['aca_studenttype']) && $_SESSION['aca_studenttype'] == 1) {
                        echo '<li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=22&login=1&opt=1&key=0&set=1\');return document.MM_returnValue">Join a Class</a></li>';
                    }
					if ($trial_expire >= date("Y-m-d")){
						echo '<li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=118&login=1&opt=1&set=4\');return document.MM_returnValue">Take a Trial</a></li>';
					}
                }

                if (logged_in() && $aca_accesslevel == 5){
                    echo '<li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=10&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Take Test</a></li>';
                    echo '<li><a href="javascript:voi(0)" onclick="MM_goToURL(\'parent\',\'?id=11&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Practice a Question</a></li>';
                    echo '<li><a href="javascrip:void(0)" onclick="MM_goToURL(\'parent\',\'?id=12&login=1&opt=51&key=0&set=0\');return document.MM_returnValue">Instructor Quiz</a></li>';
                    echo '<li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=22&login=1&opt=51&key=0&set=1\');return document.MM_returnValue">Join a Class</a></li>';
                    echo '<li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=28&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Student Assessment</a></li>';
                    echo '<li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=26&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Evaluation</a></li>';
                    //echo '<li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=50&set=0&opt=1\');return document.MM_returnValue">Search</a></li>';

                }

                if (logged_in() && $aca_accesslevel == 9){
                    echo '<li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Admin</a>
                        <ul class="dropdown-menu">
                    <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=109&login=1&cat2=1&opt=1&key=0&set=13\');return document.MM_returnValue">Site Configuration</a></li>
                    <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=112&login=1&set=0\');return document.MM_returnValue">Invoice Configuration</a></li>
                  </ul>
                  </li>
                    <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">People</a>
                        <ul class="dropdown-menu">
                    <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=20&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Users</a></li>
                    <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=33&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Instructors</a></li>
                  </ul>
                  </li>
                  <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Exams</a>
                        <ul class="dropdown-menu">
                    <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=30&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Exam Maintenance</a></li>
                    <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=31&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Subject Maintenance</a></li>
                    <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=32&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Question Maintenance</a></li>
                    <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=34&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Class Maintenance</a></li>
                    <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=39&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Error Maintenance</a></li>
                    <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=27&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Assesment Questions</a></li>

                  </ul></li>
                  <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Accounting</a>
                        <ul class="dropdown-menu">
                    <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=108&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Voucher Maintenance</a></li>
                    <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=104&login=1&opt=0&key=0&set=0\');return document.MM_returnValue">Transaction Maintenance</a></li>

                  </ul></li>
                  <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=110&set=0\');return document.MM_returnValue">Video</a></li>
                  <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=111&set=0\');return document.MM_returnValue">FAQ</a></li>
                  <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=50&set=0&opt=1\');return document.MM_returnValue">Search</a></li>';

                }

                ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php



                if (logged_in()) {
                    echo '<li><a href="javascript:void(0)" onclick="signOut()">Sign Out <span class="glyphicon glyphicon-off"></span></a></li>';
                    echo '<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$_SESSION["aca_name"].' <span class="glyphicon glyphicon-user"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=20&login=1&cat2=1&opt=1&key=0&set=13\');return document.MM_returnValue">Update Details</a></li>
                    <li><a href="javascript:void(0)" onclick="MM_goToURL(\'parent\',\'?id=113\');return document.MM_returnValue">Purchase History</a></li>
                  </ul></li>';
                } else {
                    echo '<li><a href="javascript:void(0)" onclick="signUp()">Register <span class="glyphicon glyphicon-pencil"></span></a></li>
                    <li><a href="javascript:void(0)" onclick="signIn()">Sign In <span class="glyphicon glyphicon-off"></span></a></li>';
                }
                ?>

            </ul>

        </aside>
</div>
</div>

<?php

            if((!isset($id) || $id == 0) || (strpos($_SERVER['REQUEST_URI'], "home") !== false)){
                include 'includes/slideshow.php';
            }

?>

<!-- Header information --------------------------------  -->

<div class="container main-container">
  <div class="row">

  

<?php


if(isset($_GET["forgot"]) && $_GET["forgot"] == 1) {

    $query = "SELECT * FROM aca_user WHERE status = '1' and email = '".$_POST["femail"]."' LIMIT 1 ";
    $result_set = mysqli_query($connection,$query);
    confirm_query($result_set);
    if (mysqli_num_rows($result_set) == 1) {
        $found_user = mysqli_fetch_array($result_set);
        $username = $found_user['name'];
        $email = $found_user['email'];

        $newpassword = "";
        // create a password

        $newpassword = "T";
        $newpassword .= rand(1,9);
        $newpassword .= rand(10,16);
        $newpassword .= "x";
        $newpassword .= rand(11,77);
        $newpassword .= "V";
        $newpassword .= rand(20,29);

        $encryption = new Encryption;

        $password = $encryption->encrypt_request($newpassword);

        $query = "update aca_user set password = '".$password."', origpass = '".$newpassword."' where email = '".$email."'";

        $result = mysqli_query( $connection, $query);


        include 'lib/swift_required.php';

        $msgcontent = '';
        $output = '';

        $message = Swift_Message::newInstance();
        $message->setSubject("BlackboardBI Training Forgot Password");
        $message->setFrom(array($admin_email => "BlackboardBI Training"));
        $message->setTo(array($email => $username));
        $message->setBcc(array("jaco@itechnologiessolutions.co.za" => "Jaco Ferreira"));
        $message->setBody('This email requires HTML to view.');

        $msgcontent.= '<p>Dear '.$username.', </p><br />
                                We received a request to reset the password on your BlackboardBI Training account.<br />
                                If you did not initiate this password reset request, you can report it to us by contacting our team at <a href="'.$support_email.'">'.$support_email.'</a><br />
                                If you suspect someone may have unauthorised access to your account or would like to change your password to one you can easily remember, visit <a href="https://www.blackboardbi.com/training/?id=20&opt=5&key=10&set=313">https://www.blackboardbi.com/training/?id=20&opt=5&key=10&set=313</a> to change your password as a precaution.<br />
                                <br />
                                <strong>Here are your login details:</strong><br />
                                <br />
                                <strong>Username: '.$email.'</strong><br />
                                <strong>Password: '.$newpassword.'</strong>
                                &nbsp;<br />
                                &nbsp;<br />
                                Regards<br />
                                The Webmaster<br />';

        $message->addPart($msgcontent, 'text/html');

        $transport = Swift_MailTransport::newInstance();
        $mailer = Swift_Mailer::newInstance($transport);
        $result = $mailer->send($message);

        if($result) {
            echo "<div class=\"alert alert-success\" role=\"alert\">";
            echo "<a href=\"#\" class=\"alert-link\">An email with reset instructions has been sent.</a>";
            echo "</div>";
        } else {
            echo "<div class=\"alert alert-success\" role=\"alert\">";
            echo "<a href=\"#\" class=\"alert-link\">An error occured while trying to reset your password. Please try again later.</a>";
            echo "</div>";
        }
    } else {
        echo "<div class=\"alert alert-success\" role=\"alert\">";
        echo "<a href=\"#\" class=\"alert-link\">We were unable to locate a BlackboardBI training account with the entered email address.</a>";
        echo "</div>";
    }
}


if (isset($_GET['testout']) && $_GET['testout'] == 1) {		
		$message = "Your test has been cancelled!";
		$usernamelog = $username;
		$username = "";
		$password = "";
		$userx = 0;
		$login = 0;
		$q_count = 0;
		$q_tot = 0;
		$aca_qno = 0;
		$_SESSION['aca_userthis'] = NULL;
		$_SESSION['aca_userexam'] = NULL;
		$_SESSION['aca_q_count'] = NULL;
		$_SESSION['aca_q'] = NULL;
		$_SESSION['aca_q_tot'] = NULL;
		$_SESSION['aca_subject'] = NULL;
		unset($_SESSION['aca_userthis']);
		unset($_SESSION['aca_userexam']);
		unset($_SESSION['aca_q_count']);
		unset($_SESSION['aca_q']);
		unset($_SESSION['aca_subject']);

		/*echo "<div class=\"alert alert-success\" role=\"alert\">";
  			echo "<a href=\"#\" class=\"alert-link\">".$message."</a>";
		echo "</div>";*/
}

//session expire message
		if(isset($_GET["sessid"]) && $_GET["sessid"] == 1){
			echo "<div class=\"alert alert-danger\" role=\"alert\">";

			echo "<a href=\"#\" class=\"alert-link\">Your session has expired. Please log in again</a>";

			echo "</div>";
		}

if (logged_in() && $_GET["id"] == '8') {
    if (logged_in()){
      if($aca_accesslevel == 1){
          include("includes/scores.php");
      }
        if($aca_accesslevel == 9){
			
			$ttest = 0;
			if ($ttest == 1) {
		    $cat3 = $_SESSION['aca_userx']; // SW added 2017/01/15
			echo "<div class=\"alert alert-success\" role=\"alert\">";
  			echo "TEST Select Results</br>";
  			echo "TEST CAT3".$cat3." | Access: ".$aca_accesslevel."</br>";
		    echo "</div>";	
			}
			
            include("includes/results.php");
        }

        if($aca_accesslevel == 5){
            include("includes/results.php");
        }
    }
}

if (!logged_in()) {	// Sign In


	/*echo "<div class=\"container-fluid bg-3 text-center\"> ";
    echo "<h3></h3>";
	echo "</div>";
	echo "<p>&nbsp;</p>";*/

	// login 0 for initial login form, login = 8 when password or username incorrect

	$forgetpassword =  "<a href=\"?id=0&opt=".$opt."&login=4&set=0\">Forget Password</a>";

	if (isset($_POST['submit']) && $login == 3) { // Die vorm is getuur

		$errors = array();
		$username = trim(mysql_prep($_POST['aemail']));
		$password = trim(mysql_prep($_POST['apwd']));
		$gip = get_ip(0);

				if ($test_aca == 1) {
				  echo "Test:: Login:".$login."  after login post</br>";
				  echo "Test:: Username:".$username."</br>";
				  echo "Test:: Password:".$password."</br>";
				  echo "Test:: X_PIN   :".$x_pin."</br></br>";

				  // test connection
				  	$query = "SELECT user_id, username, email, `name`, password FROM aca_user WHERE status = '1' LIMIT 1 ";
					$result_set = mysqli_query($connection,$query);
					confirm_query($result_set);
					$found_user = mysqli_fetch_array($result_set);
				    echo "Test:: Connection :::::</br>";
					echo $found_user['user_id'] . "</br>";
					echo $found_user['username'] . "</br>";
					echo $found_user['name'] . "</br>";
					echo $found_user['password'] . "</br>";
				}

		if ( empty($errors)) {

            if ($test_aca == 1) {
                echo "Test:: Do if no errors " . $password . "</br>";
            }

            $encryptpass = new Encryption();

            $epassword2 = $encryptpass->encrypt_request($password);

            // Check database to see if username and the hashed password exists there.
            $query = "SELECT user_id, username, email, name, access_level, student_type, instructor_id, password,trial ";
            $query .= " FROM aca_user";
            $query .= " WHERE email = '".$username."' ";
            $query .= " AND password = '".$epassword2."' ";
            $query .= " AND status = '1' ";
            $query .= " LIMIT 1";

            $result_setZ = mysqli_query($connection, $query);

            confirm_query($result_setZ);

                //if ($_POST["captcha"] == $_SESSION["captcha"]) {

                    if (mysqli_num_rows($result_setZ) == 1) {

                        $found_user = mysqli_fetch_array($result_setZ);

                        $_SESSION['aca_userx'] = $found_user['user_id'];

                        $_SESSION['aca_username'] = $found_user['username'];

                        $_SESSION['aca_name'] = $found_user['name'];

                        $_SESSION['aca_accesslevel'] = $found_user['access_level'];

                        $_SESSION['aca_studenttype'] = $found_user['student_type'];
						
						$_SESSION['aca_trial'] = $found_user['trial'];

                        $split = explode(" ",$found_user['name']);
                        $_SESSION["user_detail"]["name"] = $split[0];
                        $_SESSION["user_detail"]["surname"] = $split[1];
                        $_SESSION["user_detail"]["email"] = $found_user['email'];
                        $_SESSION["user_detail"]["mobile"] = $found_user['mobile'];

                        if ($found_user['access_level'] == 9 || $found_user['access_level'] == 999) {  // instructor = 9 and administrator = 999

                            $_SESSION['aca_instructor'] = $found_user['instructor_id'];

                        } else {

                            $_SESSION['aca_instructor'] = 0;

                        }

                        $userx = $_SESSION['aca_userx'];

                        $username = $_SESSION['aca_username'];

                        $aca_accesslevel = $_SESSION['aca_accesslevel'];

                        $aca_studenttype = $_SESSION['aca_studenttype'];

                        $aca_name = $_SESSION['aca_name'];

                        $aca_instructor = $_SESSION['aca_instructor'];


                        if ($test_aca == 1) {

                            echo "Test:: Login:" . $login . "  after login check pass, assigned sessions</br>";

                            echo "Test:: Username:" . $username . "</br>";

                            echo "Test:: AccessLevel:" . $aca_accesslevel . "</br>";

                        }


                        $login = 1;

                        @header("Location:?id=8&opt=0&set=0&login=3&testout=1");
						exit;
                        /*echo "<div class=\"alert alert-success\" role=\"alert\">";

                        echo "<a href=\"#\" class=\"alert-link\">Welcome back " . $username . ". You are signed in.</a>";

                        echo "</div>";*/

                        // display options +++++++++++++++++++++++++++++++++++

                        //include("includes/menu.php");


                    } else {


                        // username / password combo was not found in the database

                        echo "<div class=\"alert alert-danger\" role=\"alert\">";

                        echo "<a href=\"#\" class=\"alert-link\">There was an error with the credentials provided. Please try again.</a>";

                        echo "</div>";


                        $login = 8;


                        if ($test_aca == 9) {

                            echo 'Test:: Login:' . $login . '  after login check fail</br>
                              Test:: Username:' . $username . '</br>';

                        }
                    }
                /*} else {
                    echo "<div class=\"alert alert-danger\" role=\"alert\">
                                <a href=\"#\" class=\"alert-link\">Captcha incorrect.</a>
                          </div>";

                    $login = 8;

                    if ($test_aca == 9) {
                        echo 'Test:: Login:' . $login . '  after login check fail</br>
                              Test:: Username:' . $username . '</br>';
                    }
                }*/
            }
		}
		
		
		



	if (($login == 0 || $login == 8) && ($id != "29" && $id < "100") ) {	// Display login form

		$username = "";

		$message = "";

		$qno = "";	

		


	} // end of login = 0

	
	

	if ($login == 4) {	// Display forgot password message						

		echo "<p>Contact the administrator to reset the password.</p>";

		//echo "<p>Go to the login screen.".$cancel."</p>";

	} // end of login = 4

}







// Include content from include pages										

if (logged_in())  {		



		// set == 1 new

		// set == 2 insert

		// set == 3 edit

		// set == 4 update

		// set == 5 edit status

		// set == 9 delete 



		if ($id == 10) { include ("includes/test.php"); }

		if ($id == 11) { include ("includes/practice.php"); }

		if ($id == 12) { include ("includes/quiz.php"); }

		if ($id == 13) { include ("includes/detail.php"); }

		if ($id == 20) { include ("includes/user.php"); }

		if ($id == 21) { include ("includes/user_exam.php"); }

		if ($id == 22) { include ("includes/user_class.php"); }

		if ($id == 41) { include ("includes/scores.php"); } // student test scores

		if ($id == 26) { include ("includes/evaluation.php"); }
		
		if ($id == 118) { include ("includes/trial.php"); }


	if ($aca_accesslevel >= 5) {

		if ($id == 23) { include ("includes/class_exam.php"); }

		if ($id == 27) { include ("includes/assessment_questions.php"); }

		if ($id == 28) { include ("includes/assessment.php"); }

		if ($id == 30) { include ("includes/exam.php"); }

		if ($id == 31) { include ("includes/tool.php"); }

		if ($id == 32) { include ("includes/question.php"); }

		if ($id == 33) { include ("includes/instructor.php"); }

		if ($id == 34) { include ("includes/class.php"); }

		if ($id == 39) { include ("includes/error.php"); }

		if ($id == 40) { include ("includes/results.php"); }

		if ($id == 42) { include ("includes/trend.php"); }

		if ($id == 50) { include ("includes/search.php"); }

		if ($id == 99) { include ("includes/help.php"); }

		if ($id == 104) { include ("includes/transactions.php"); }

		if ($id == 105) { include ("includes/transaction.details.php"); }
		if ($id == 108) { include ("includes/voucher.php"); }



	}

		//if ($id <> 0 && $set == 11) { include ("includes/upload.php"); }

}

		// Register

        if($id == 1) { include ("includes/exam_list.php"); }
        if($id == 2) { include ("includes/exam_info.php"); }
        if($id == 3) { include ("includes/contact_us.php"); }
        if($id == 4) { include ("includes/terms.php"); }
        if($id == 5) { include ("includes/about_us.php"); }
        if($id == 6) { include ("includes/faq.php"); }
        if($id == 7) { include ("includes/demo.php"); }
        if($id == 101) { include ("includes/confirm-subscription.php"); }
        if($id == 102) { include ("includes/paygate.result.php"); }
        if($id == 103) { include ("includes/eft.result.php"); }
        if ($id == 106) { include ("includes/newusersub.php"); }
        if ($id == 107) { include ("includes/generateinvoice.php"); }
        if ($id == 109) { include ("includes/site_config.php"); }
        if ($id == 110) { include ("includes/video_admin.php"); }
        if ($id == 111) { include ("includes/faq_admin.php"); }
        if ($id == 112) { include ("includes/invoice-config.php"); }
        if ($id == 113) { include ("includes/purchase-history.php"); }
        if ($id == 114) { include ("includes/purchase-detail.php"); }
        if ($id == 115) { include ("includes/paypal_success.php"); }
        if ($id == 116) { include ("includes/paypal_ipn.php"); }
        if ($id == 117) { include ("includes/paypal_cancel.php"); }
        if ($id == 919) { include ("includes/exampdf.php"); }
		//if ($id == 29) { include ("includes/register.php"); }


// ------------- Home page Footer		 -------------------------------

		

//if (!logged_in() && $id == 0) {	// Display Adds
if ($id == 0) {	// Display Adds

		// other resources

		// http://sap-certification.info/bi/

		// http://www.itexamguide.com/C-TBI30-74_braindumps.html

		// http://www.dumpcollection.com/C_TBI30_74_braindumps.html


    echo '<div class="col-sm-12 main-content" style="padding-top:20px;">';
                            // sign user out

                            if (isset($_GET["logout"]) && $_GET["logout"] == 1) {

                                    //echo "<div><p>You are signed out. <a href=\"?id=0&login=0\">[Sign In]</a></p></div>";
                            //echo $username;
                                    echo "<div class=\"alert alert-success\" role=\"alert\">";

                                        echo "<a href=\"#\" class=\"alert-link\">You are signed out.</a>";

                                    echo "</div>";

                            }
                            echo '<div class="col-sm-12"><h2>Welcome</h2><div class="colored-line-left"></div></div>
                              <div class="clearfix"></div>';

                            echo '<div class="col-sm-12 content-pad"><p>Welcome to the Blackboard BI online SAP exam simulation platform.</p>
<p>We are here to share our wealth of experience in consulting and SAP training with all SAP aspirants and help our users clear the SAP Certification exam. We know it needs dedication, time and money to become a SAP Certified consultant. We value your time and money that you invest for the preparation for the SAP Certification exam and we are here to ensure that you achieve your goal without failure. Our aim is to familiarize you with real SAP Certification exam simulation and to empower you to clear the SAP Certification exam with a good score.</p>
<p>What we offer?</p>
<p>We provide a platform that enables our users to practice for the SAP certification exam in a similar way that the real exam is presented. It simulates the actual SAP certification exam environment with randomized questions and multiple-choice answers from a premium question bank. Our questions bank has covered all possible questions that can be asked from each topic covered in the syllabus.</p>
<p>All our materials and practice exams are designed by experienced industry experts and certified SAP consultants with a large project experience. All materials on this site are exceptional and unique, as are made by us. Thanks to the quality and accountability of our resources we have established numerous success rates and have the testimonies and feedback from some of our candidates.</p>

<p>All the material to prepare for SAP certification, you can buy online in 5 minutes and start practicing exams immediately. For any support issues please mail our support team at <a href="mailto:'.$support_email.'">'.$support_email.'</a> and we will respond within 6-8 hours. Our secure internet payment is supported by PayPal secure portal.</p>
</p>
</div>
                            <div class="col-sm-12" style="padding-top: 30px !important;">
                                ';

                                                $result1 = get_aca_exam_list(0,12,1);
                                                $num = mysqli_num_rows($result1);

                                                $i=0;

                                                while ($i < $num)
                                                {
                                                    $exam_image = mysqli_result   ($result1,$i,"exam_image");
                                                    $code = mysqli_result   ($result1,$i,"code");
                                                    $description= mysqli_result   ($result1,$i,"description");
                                                    $cost = mysqli_result   ($result1,$i,"cost");
                                                    $exam_id = mysqli_result   ($result1,$i,"exam_id");
                                                    
                                                        echo '<div class="form-group col-sm-4">
                                                            <div class="panel panel-default exams-panel">
                                                            <div class="panel-body"><div class="col-sm-12 exams-container">
                                                            <div class="col-sm-12 text-center" style="min-height:200px;">
                                                                <p><img src="' . $exam_image . '" height="50" /></p>
                                                                <p><strong>' . $code . '</strong> - ' . $description . '</p>
                                                                <p>'.$_SESSION["currencySymbol"].' '.formatMoney(($_SESSION["currencyconverter"] * $cost),2).'</p>
                                                            </div>
                                                            <div class="col-sm-12 text-center exams-buttons">
                                                                <p><a href="'.get_base_url().'?id=2&key='.$exam_id.'" class="btn btn-default">Read more</a>&nbsp;<a href="'.get_base_url().'?id=106&key=' . $exam_id . '&step=1" class="btn">Subscribe</a></p>
                                                            </div>
                                                            </div>
                                                            </div>
                                                            </div>
                                                            </div>';
                                                    
                                                    $i++;
                                                }


                                        echo '
                                </div>
                             </div>';

		echo "<p>&nbsp;</p>";	

}

include "modals/login.php";
include "modals/register.php";
include "modals/newvoucher.php";
include "modals/demovideo.php";
include "modals/forgotpassword.php";
ob_end_flush();

?>



  </div> <!-- end of row -->
</div>
<!-- end of containers --><div class="clearfix"></div>
<div class="container-fluid footer">
<div class="container">

    <div class="col-sm-12 text-center copyright-social">

        <p>Copyright &copy; <?php echo $company_name.' '.date("Y"); ?><br />
		<a href="https://www.facebook.com/blackboardbi/" target="blank"><i class="fa fa-facebook"></i></a>
		<a href="https://www.linkedin.com/company/11222872/" target="blank"><i class="fa fa-linkedin"></i></a>
		<a href="" target="blank"><i class="fa fa-google-plus"></i></a>
		</p>

    </div>
    <div class="col-sm-12 text-center">

        <a href="?id=4">Terms &amp; Conditions</a>

    </div>

</div>

</div>

<script src="<?php echo get_base_url(); ?>js/intlTelInput.js"></script>

<script type="text/javascript">

	$("#sub_contactnumber").intlTelInput();
	$("#r_mobilenumber").intlTelInput();
	$("#r_telnumber").intlTelInput();

    $('#bs-example-navbar-collapse-1').on('show.bs.collapse', function () {
        $('#bs-example-navbar-collapse-1').append($('#sidebar').html());
        $('#bs-example-navbar-collapse-1 ul').last().removeClass('nav-pills nav-stacked').addClass('navbar navbar-nav');
    });
    $('#bs-example-navbar-collapse-1').on('hidden.bs.collapse', function () {
        $('#bs-example-navbar-collapse-1 ul:last-child').remove();
    });
    $(window).on('resize', function () {
        if (window.innerWidth > 768) {$('#bs-example-navbar-collapse-1').collapse('hide');}
    });


    var LoginreCaptcha;
    var RegisterreCaptcha;
    var ForgotPassreCaptcha;
    var onloadCallback = function() {
        // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
        // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
        //Render the recaptcha1 on the element with ID "recaptcha1"
        LoginreCaptcha = grecaptcha.render('LoginreCaptcha', {
            'sitekey': '6LeiwywUAAAAAMrQO1MF7DJpDWdIlXqwMUvTKPRE', //Replace this with your Site key
            'theme': 'light'
        });
        RegisterreCaptcha = grecaptcha.render('RegisterreCaptcha', {
            'sitekey': '6LeiwywUAAAAAMrQO1MF7DJpDWdIlXqwMUvTKPRE', //Replace this with your Site key
            'theme': 'light'
        });
        ForgotPassreCaptcha = grecaptcha.render('ForgotPassreCaptcha', {
            'sitekey': '6LeiwywUAAAAAMrQO1MF7DJpDWdIlXqwMUvTKPRE', //Replace this with your Site key
            'theme': 'light'
        });
    };
	
	var t;
    window.onload = resetTimer();
    // DOM Events
    document.onmousemove = resetTimer();
    document.onkeypress = resetTimer();
    console.log('loaded');

	function logout() {
        //alert("You are now logged out.")
        location.href = 'index.php?id=0&sessid=1'
    }

    function resetTimer() {
		
        clearTimeout(t);
		//10 minutes
        t = setTimeout(logout, 1800000)
        
    }
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>

<!--<pre>
	<?php echo var_dump($_SERVER); ?>
</pre>-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110599041-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-110599041-1');
</script>
</body>

</html>

